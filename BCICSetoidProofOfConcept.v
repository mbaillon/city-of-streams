Set Definitional UIP.
Set Primitive Projections.
Section BranchingTranslation.
Require Import Base.
Require Import Dialogue.  
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Definition I : Set := nat.
Definition O : Set := nat.
Set Printing Universes.


Inductive True : SProp := T.
Inductive False : SProp :=.
Inductive and (A : SProp) (B : SProp) : SProp := conj : A -> B -> and A B.

Inductive eqn (A : Type) (x : A) : A -> SProp := refl : @eqn A x x.

Definition Rw (A B : Type) (e : eqn A B) (x : A) : B :=
  match e with | refl _ => x end.

Definition SRw (A B : SProp) (e : eqn A B) (x : A) : B :=
  match e with | refl _ => x end.

Definition BackRw (A B : Type) (e : eqn A B) : B -> A :=
  match e with | refl _ => fun x => x end.

Definition BackSRw (A B : SProp) (e : eqn A B) : B -> A :=
  match e with | refl _ => fun x => x end.

Definition Sf_equal (A : Type) (B : A -> Type) (f : forall a, B a)
           (x y : A) (e : eqn x y) : eqn (match e in eqn _ K return B K with
                                          | refl _ => f x
                                          end) (f y) :=
  match e with
  | refl _ => refl _
  end.
  
  
Axiom F : False.
Ltac admit := case F.

Record Ssig (A : SProp) (B : A -> SProp) : SProp := Sexist { Sone : A;  Stwo : B Sone }.
Record Ssigt (A : Type) (B : A -> SProp) : Type := SexisT { Sfst : A;  Ssnd : B Sfst }.

Lemma Ssigt_equal (A1 A2 : Type) (B1 : A1 -> SProp) (B2 : A2 -> SProp)
      (eq1 : eqn A1 A2) (eq2 : eqn (fun x => B1 (BackRw eq1 x)) B2) :
  eqn (Ssigt B1) (Ssigt B2).
Proof.
  simpl.
  destruct eq2.
  destruct eq1.
  reflexivity.
Defined.  

Record Setoid : Type := {
  set : Type;
  equ : set -> set -> SProp;
  equ_cst : forall (x : set), equ x x
  }.
Arguments equ _ : clear implicits.
Arguments equ_cst _ : clear implicits.
Definition Ctx := Setoid.

Definition SetRw (A B : Setoid) (e : eqn A B) (x : A.(set)) : B.(set) :=
  match e with | refl _ => x end.

Definition BackSetRw (A B : Setoid) (e : eqn A B) : B.(set) -> A.(set) :=
  match e with | refl _ => fun x => x end.

Inductive BranchingBool :=
| Btrue : BranchingBool
| Bfalse : BranchingBool
| Bêta : I -> (O -> BranchingBool) -> BranchingBool.

Inductive BeBool (alpha : I -> O) : BranchingBool -> BranchingBool -> SProp :=
| Betrue : BeBool alpha Btrue Btrue
| Befalse : BeBool alpha Bfalse Bfalse
| Bêtae1 : forall (n : BranchingBool) (i : I) (f : O -> BranchingBool)
                 (H : BeBool _ n (f (alpha i))),
    BeBool _ n (Bêta i f)
| Bêtae2 : forall (n : BranchingBool) (i : I) (f : O -> BranchingBool)
                  (H : BeBool _ (f (alpha i)) n),
    BeBool _ (Bêta i f) n.

Lemma BoolRefl (alpha : I -> O) : forall (b : BranchingBool),
    BeBool alpha b b.
Proof.
  refine (fix f b := match b with
                     | Btrue => Betrue alpha
                     | Bfalse => Befalse alpha
                     | Bêta i k => Bêtae1 (Bêtae2 (f (k (alpha i))))
                     end).
Defined.  

Definition Bool_K (alpha : I -> O) (i : I) (k : O -> BranchingBool) :
    BeBool alpha (k (alpha i)) (Bêta i k) :=
  Bêtae1 (BoolRefl alpha (k (alpha i))).



Lemma BoolRec (alpha : I -> O)
      (P : BranchingBool -> Setoid)
      (Pe : forall (b1 b2 : BranchingBool) (be : BeBool alpha b1 b2),
          eqn (P b1) (P b2))
      (ptrue : (P Btrue).(set))
      (pfalse : (P Bfalse).(set)) :
  forall b, (P b).(set).
Proof.
  refine (fix f b := match b with
                     | Btrue => ptrue
                     | Bfalse => pfalse
                     | Bêta i k => SetRw (Pe _ _ (Bool_K alpha i k))
                                         (f (k (alpha i)))
                     end).
Defined.

Lemma BoolRece (alpha : I -> O)
      (P : BranchingBool -> Setoid)
      (Pe : forall (b1 b2 : BranchingBool) (be : BeBool alpha b1 b2),
          eqn (P b1) (P b2))
      (ptrue : (P Btrue).(set))
      (pfalse : (P Bfalse).(set)) :
  forall (b1 b2 : BranchingBool) (be : BeBool alpha b1 b2),
    (P (b2)).(equ) (@SetRw (P b1) (P b2) (Pe b1 b2 be) (BoolRec Pe ptrue pfalse b1))
                   (BoolRec Pe ptrue pfalse b2).
Proof.
  intros.
  induction be.
  - simpl.
    exact ((P Btrue).(equ_cst) _).
  - exact ((P Bfalse).(equ_cst) _).
  - simpl in *.
    revert IHbe.
    generalize (Pe (f (alpha i)) (Bêta i f) (Bool_K alpha i f)).
    generalize (Pe n (f (alpha i)) be).
    generalize (Pe n (Bêta i f) (Bêtae1 be)).
    generalize (BoolRec Pe ptrue pfalse n).
    generalize ((BoolRec Pe ptrue pfalse (f (alpha i)))).
    generalize (P (Bêta i f)).
    generalize (P (f (alpha i))).
    generalize (P n).
    intros S0 S1 S2 s1 s0 e02 e01 e12.
    induction e01 ; simpl.
    induction e02 ; simpl.
    exact (fun x => x).
  - simpl in * ; revert IHbe.
    generalize (Pe (f (alpha i)) (Bêta i f) (Bool_K alpha i f)).
    generalize (Pe (f (alpha i)) n be).
    generalize (Pe (Bêta i f) n (Bêtae2 be)).
    generalize (BoolRec Pe ptrue pfalse n).
    generalize ((BoolRec Pe ptrue pfalse (f (alpha i)))).
    generalize (P (Bêta i f)).
    generalize (P (f (alpha i))).
    generalize (P n).
    intros S0 S1 S2 s1 s0 e02 e01 e12.
    induction e01 ; simpl.
    induction e02 ; simpl.
    exact (fun x => x).
Defined.



Inductive BranchingNat :=
| B0 : BranchingNat
| BS : BranchingNat -> BranchingNat
| Bêtanat : I -> (O -> BranchingNat) -> BranchingNat.

Inductive Benat (alpha : I -> O) : BranchingNat -> BranchingNat -> SProp :=
| Be0 : Benat alpha B0 B0
| BeS : forall (n m : BranchingNat) (e : Benat alpha n m),
    Benat alpha (BS n) (BS m)
| Bêtaenat1 : forall (n : BranchingNat) (i : I) (f : O -> BranchingNat)
                 (H : Benat alpha n (f (alpha i))),
    Benat alpha n (Bêtanat i f)
| Bêtaenat2 : forall (n : BranchingNat) (i : I) (f : O -> BranchingNat)
                 (H : Benat alpha (f (alpha i)) n),
    Benat alpha (Bêtanat i f) n.


Lemma Nat_Refl (alpha : I -> O) : forall (n : BranchingNat),
    Benat alpha n n.
Proof.
  refine (fix f n := match n with
                     | B0 => Be0 alpha
                     | BS k => @BeS alpha k k (f k)
                     | Bêtanat i k => _
                     end).
  refine (Bêtaenat1 _).
  refine (Bêtaenat2 _).
  exact (f (k (alpha i))).
Defined.

Definition Nat_K (alpha : I -> O) (i : I) (k : O -> BranchingNat) :
    Benat alpha (k (alpha i)) (Bêtanat i k) :=
  Bêtaenat1 (Nat_Refl alpha (k (alpha i))).



Lemma NatRec (alpha : I -> O)
      (P : BranchingNat -> Setoid)
      (Pe : forall (b1 b2 : BranchingNat) (be : Benat alpha b1 b2),
          eqn (P b1) (P b2))
      (p0 : (P B0).(set))
      (pS : forall n : BranchingNat, (P n).(set) -> (P (BS n)).(set)) :
  forall b, (P b).(set).
Proof.
  refine (fix f b := match b with
                     | B0 => p0
                     | BS k => pS k (f k)
                     | Bêtanat i k => SetRw (Pe _ _ (Nat_K alpha i k))
                                         (f (k (alpha i)))
                     end).
Defined.

Definition Pitrans (alpha : I -> O)
           (P : BranchingNat -> Setoid)
           (Pe : forall (b1 b2 : BranchingNat) (be : Benat alpha b1 b2),
               eqn (P b1) (P b2))
           (n1 n2 : BranchingNat) (ne : Benat alpha n1 n2)
           (f : (P n1).(set) -> (P (BS n1)).(set)) :
  (P n2).(set) -> (P (BS n2)).(set).
Proof.
  intros p.
  refine (SetRw (Pe (BS n1) (BS n2) (BeS ne)) (f _)).
  refine (BackSetRw (Pe n1 n2 ne) p).
Defined.  

Print Pitrans.

Lemma NatRece (alpha : I -> O)
      (P : BranchingNat -> Setoid)
      (Pe : forall (b1 b2 : BranchingNat) (be : Benat alpha b1 b2),
          eqn (P b1) (P b2))
      (p0 : (P B0).(set))
      (pS : forall n : BranchingNat, (P n).(set) -> (P (BS n)).(set))

      (pSe : forall (n1 n2 : BranchingNat) (ne : Benat alpha n1 n2)
                     (p1 p2 : (P n2).(set))
                     (pe : (P n2).(equ) p1 p2),
          (P (BS n2)).(equ) (SetRw (Pe _ _ (BeS ne)) (pS n1 (BackSetRw (Pe _ _ ne) p1)))
                                   (pS n2 p2)):
  forall (b1 b2 : BranchingNat) (be : Benat alpha b1 b2),
    (P (b2)).(equ) (@SetRw (P b1) (P b2) (Pe b1 b2 be) (NatRec Pe p0 pS b1))
                   (NatRec Pe p0 pS b2).
Proof.
  intros.
  induction be.
  - simpl.
    exact ((P B0).(equ_cst) _).
  - simpl.
    generalize (pSe n m be _ _ IHbe).
    change (equ (P (BS m))
                (SetRw (Pe (BS n) (BS m) (BeS be))
                       ((fun k => pS n k)
                           (BackSetRw (Pe n m be)
                                      (SetRw (Pe n m be) (NatRec Pe p0 pS n)))))
                ((fun k => pS m k) (NatRec Pe p0 pS m)) ->
            equ (P (BS m))
                (SetRw (Pe (BS n) (BS m) (BeS be))
                       ((fun k => pS n k) (NatRec Pe p0 pS n))) ((fun k => pS m k) (NatRec Pe p0 pS m))).
    generalize (fun k => pS m k) (fun k => pS n k).
    generalize (NatRec Pe p0 pS n) (NatRec Pe p0 pS m).
    generalize (Pe (BS n) (BS m) (BeS be)).
    generalize (Pe n m be).
    generalize (P m) (P n) (P (BS m)) (P (BS n)).
    intros A B C D eAB eCD b a fac fbd.
    destruct eCD.
    simpl.
    now destruct eAB.
  - simpl in *.
    revert IHbe.
    generalize (Pe (f (alpha i)) (Bêtanat i f) (Nat_K alpha i f)).
    generalize (Pe n (f (alpha i)) be).
    generalize (Pe n (Bêtanat i f) (Bêtaenat1 be)).
    generalize (NatRec Pe p0 pS n).
    generalize ((NatRec Pe p0 pS (f (alpha i)))).
    generalize (P (Bêtanat i f)).
    generalize (P (f (alpha i))).
    generalize (P n).
    intros S0 S1 S2 s1 s0 e02 e01 e12.
    induction e01 ; simpl.
    induction e02 ; simpl.
    exact (fun x => x).
  - simpl in * ; revert IHbe.
    generalize (Pe (f (alpha i)) (Bêtanat i f) (Nat_K alpha i f)).
    generalize (Pe (f (alpha i)) n be).
    generalize (Pe (Bêtanat i f) n (Bêtaenat2 be)).
    generalize (NatRec Pe p0 pS n).
    generalize ((NatRec Pe p0 pS (f (alpha i)))).
    generalize (P (Bêtanat i f)).
    generalize (P (f (alpha i))).
    generalize (P n).
    intros S0 S1 S2 s1 s0 e02 e01 e12.
    induction e01 ; simpl.
    induction e02 ; simpl.
    exact (fun x => x).
Defined.


Fixpoint BNEta (n : nat) : BranchingNat :=
  match n with
  | 0 => B0
  | S k => BS (BNEta k)
  end.

Fixpoint Generic_aux (n : nat) (b : BranchingNat) {struct b} :
  BranchingNat :=
  match b with
  | B0 => Bêtanat n BNEta
  | BS k => Generic_aux (S n) k
  | Bêtanat i f => Bêtanat i (fun a : nat => Generic_aux n (f a))
  end.

Definition Generic : BranchingNat -> BranchingNat := Generic_aux 0.


Definition Generice_aux (alpha : I -> O) :
  forall (n1 n2 : BranchingNat) (ne : Benat alpha n1 n2) (k : nat),
    Benat alpha (Generic_aux k n1) (Generic_aux k n2).
Proof.
  intros n1 n2 ne.
  induction ne.
  - intro k ; apply Nat_Refl.
  - simpl.
    exact (fun k => IHne (S k)).
  - simpl.
    intro k.
    unshelve refine (Bêtaenat1 _).
    exact (IHne k).
  - simpl.
    intro k.
    unshelve refine (Bêtaenat2 _).
    exact (IHne k).
Defined.

Definition Generice  (alpha : I -> O) :
  forall (n1 n2 : BranchingNat) (ne : Benat alpha n1 n2),
    Benat alpha (Generic n1) (Generic n2).
Proof.
  intros.
  unfold Generic.
  exact (@Generice_aux alpha n1 n2 ne 0).
Defined.
