\documentclass[a4paper]{easychair}


\bibliographystyle{plainurl}% the mandatory bibstyle

%-- Title, etc.
\title{The syntactic tale of the oracle and the trees \\
  A model of continuity in a dependent setting}


\author{Martin Baillon \inst{1}
  \and
  Pierre-Marie P\'edrot \inst{1}} 


% Institutes for affiliations are also joined by \and,
\institute{INRIA, France}

%  \authorrunning{} has to be set for the shorter version of the authors' names;
% otherwise a warning will be rendered in the running heads. When processed by
% EasyChair, this command is mandatory: a document without \authorrunning
% will be rejected by EasyChair

 \authorrunning{M. Baillon, P.-M. P\'edrot} 

 % \titlerunning{} has to be set to either the main title or its shorter
% version for the running heads. When processed by
% EasyChair, this command is mandatory: a document without \titlerunning
% will be rejected by EasyChair
\titlerunning{A model of continuity in a dependent setting}



%-- Packages
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
% \usepackage[english]{babel}
\usepackage{bussproofs}
\usepackage{tikz-cd}
\usepackage{tipa}
\usepackage{coqdoc}
\usepackage{url}
\usepackage{ stmaryrd }
\usepackage{multicol}
\usepackage{amsthm}

%-- Configuration
% \newgeometry{vmargin={20mm}, hmargin={15 mm, 20 mm}}

%-- Custom environments
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{exemple}{Example}
\newtheorem{traduction}{Traduction}
% \theoremstyle{remark}
% \newtheorem*{remark}{Remark}

\newtheorem*{notation}{Notation}

%-- Macros

\newcommand{\MLTT}{\mathsf{MLTT}}
\newcommand{\BTT}{\mathsf{BTT}}
\newcommand{\CIC}{\mathsf{CIC}}
\newcommand{\CCO}{\mathsf{CC}_\omega}
\newcommand{\SysT}{\mathsf{T}}

\newcommand{\nat}{\mathbb{N}}
\newcommand{\bool}{\mathbb{B}}
\newcommand{\Type}{\square}
\newcommand{\TYPE}{\square\kern-0.65em\square}
\newcommand{\BTYPE}{\TYPE^b}

\newcommand{\cstr}[1]{\mathtt{#1}}

\newcommand{\Dial}{\mathfrak{D}}
\newcommand{\deval}{\partial}
\newcommand{\bind}{\mathtt{bind}}

\newcommand{\Pind}[1]{#1\mathbf{\_ind}}
\newcommand{\Prec}[1]{#1\mathbf{\_rect}}
\newcommand{\Pstr}[1]{\theta_{#1}}
\newcommand{\Pcase}[1]{#1\mathbf{\_case}}

\newcommand{\continuous}[1]{\mathtt{continuous}\ #1}

% Oracle translation: 1st arg = oracle, 2nd arg = source term/typecontext
\newcommand*\otransterm[2]{[#2]^{#1}}
\newcommand*\otranstype[2]{\llbracket {#2} \rrbracket^{#1}}

% Dialogue translation: arg = source term/typecontext
\newcommand*\dtransterm[1]{[#1]_{\Dial}}
\newcommand*\dtranstype[1]{\llbracket {#1} \rrbracket_{\Dial}}

% Axiom translation: 1st arg = oracle, 2nd arg = source term/typecontext
\newcommand*\atransterm[2]{[#2]^{#1}_a}
\newcommand*\atranstype[2]{\llbracket {#2} \rrbracket^{#1}_a}

% Branching translation: arg = source term/typecontext
\newcommand*\btransterm[1]{[#1]_b}
\newcommand*\btranstype[1]{\llbracket {#1} \rrbracket_b}

% Parametricity translation: 1st arg = oracle, 2nd arg = source term/typecontext
\newcommand*\etransterm[2]{[#2]^{#1}_{\epsilon}}
\newcommand*\etranstype[2]{\llbracket {#2} \rrbracket^{#1}_{\epsilon}}

% Global translation: 1st arg = oracle, 2nd arg = source term/typecontext
\newcommand*\gtransterm[2]{[#2]^{#1}_{g}}
\newcommand*\gtranstype[2]{\llbracket {#2} \rrbracket^{#1}_{g}}

\begin{document}
\maketitle

A folklore result from computability theory is that any computable
function must be continuous. There are many ways to prove, or even
merely state, this theorem, since it depends in particular on how
computable functions are represented. Assuming we pick the
$\lambda$-calculus as our favorite computational system, one of the
most straightforward paths boils down to building a semantic model for
it,  typically some flavor of \emph{Complete Partial Orders}
(CPOs). By construction, CPOs being a specific kind of topological
spaces, all functions are then interpreted as continuous functions in
the model. For some types simple enough such as $\mathbb{R}
\rightarrow \mathbb{R}$, CPO-continuity implies continuity in the
traditional sense, thus proving the claim.
% Give some refs

Instead of going down the semantic route, Escardó developed an
alternative syntactic technique called \emph{effectful
  forcing}~\cite{escardo1} to prove the continuity of all functions of
type $(\nat \rightarrow \nat) \rightarrow \nat$ that are definable in
System $\SysT$. While semantic models such as CPOs are crafted inside
a non-computational metatheory, Escardó's technique amounts to
building a model of System $\SysT$ inside intensional Martin-Löf type
theory ($\MLTT$), that is, in a proper programming language. The
\emph{effectful} epithet is justified by the fact it intuitively
consists in embedding System $\SysT$ inside two impure extensions,
each featuring a different kind of side-effects, and by constraining
them via a logical relation. This argument being purely
syntactic, it can be leveraged to interpret much richer languages than
System~$\SysT$. We describe here how to generalize the latter technique
so as to recover his continuity result for a dependent type theory
similar to $\MLTT$.

Unfortunately, since Escardó's model introduces observable
side-effects, the type theory resulting from our generalization needs
to be slightly weakened down, or would otherwise be
inconsistent~\cite{PedrotT20}. We thus provide a model of Baclofen
type theory ($\BTT$)~\cite{weaning} rather than $\MLTT$. $\BTT$ is a
type theory with dependent products and a predicative hierarchy of
universes.  The main difference with $\MLTT$ actually lies in the
typing rule for dependent elimination, where the predicate needs to be
restricted so as to be linear. For instance, when $\MLTT$ has a
single dependent eliminator for the type of booleans, $\BTT$ features
two different eliminators, a non-dependent one, $\Pcase{\bool}$, and a
strict dependent one, $\Prec{\bool}$. The latter comes with the
following typing rule:
\begin{center}
\AxiomC{\strut$\vdash P: \bool \rightarrow \Type $}
\AxiomC{\strut$\vdash u_t: P\ \cstr{true} $}
\AxiomC{\strut$\vdash u_f: P\ \cstr{false} $}
\TrinaryInfC{\strut$\vdash \Prec{\bool}\ P\ u_t\ u_f: \Pi (b: \bool).\, \theta_{\bool}\ P\ b$}
\DisplayProof
\end{center}
\noindent where $\theta_{\bool}$ constant is
the \emph{storage operator}, getting rid of side effects. We have the
following reduction rules:
\begin{enumerate}
\item $\Pstr{\bool} \ P  \ \cstr{true} \equiv P \ \cstr{true} $
\item $\Pstr{\bool} \ P  \ \cstr{false} \equiv P \ \cstr{false}$
\item $\Pstr{\bool} \ P \ \beta \equiv \mathtt{unit} $ for any $\beta$ non
  standard inhabitant of $\bool$
\end{enumerate}
% More blah blah

\begin{theorem}
  Any function $\vdash_{\BTT} f: (\nat \rightarrow \nat) \rightarrow \nat$ is continuous.
\end{theorem}

Following Escardó, the proof goes by considering the operator
$\Dial : \Type \rightarrow \Type$, which given a type $A: \Type$, associates
the type of well-founded, $\nat$-branching trees, with inner nodes
labeled in $\nat$ and leaves labeled in $A$. In Coq, this amounts to
the following inductive definition:
\begin{center}
$\coqdockw{Inductive}\ \Dial\ (A: \Type): \Type \quad \mathrel{\mathtt{:=}}\quad%
\eta : A \rightarrow \Dial\ A%
\quad\mid\quad \beta : (\nat \rightarrow \Dial\ A) \rightarrow \nat \rightarrow \Dial\ A.$%
\end{center}
\noindent These \emph{dialogue trees} can be seen as functions of
type $(\nat \rightarrow \nat) \rightarrow A$. Intuitively, every inner node
is a call to an oracle $\alpha: \nat \rightarrow \nat$, and the answer is
the label of the leaf. This interpretation is implemented by a recursively defined
dialogue function:
\begin{center}
  \begin{tabular}{lll}
    $\deval$ & $:$ & $\Pi \{A: \Type\}\, (\alpha: \nat \rightarrow \nat)\, (d: \Dial\ A).\, A$ \\
    $\deval\ \alpha\ (\eta\ x)$ & $ :=$ & $x$ \\
    $\deval\ \alpha\ (\beta\ k\ i)$ & $ :=$ & $\deval\ \alpha\ (k\ (\alpha\ i))$ \\
  \end{tabular}
\end{center}

Thanks to the well-foundedness of these dialogue trees, it is
relatively straightforward to show that functions defined this way are
continuous on $\nat \rightarrow \nat$, equipped with the usual Baire
topology. The next step is to prove that every $\BTT$-definable
function is extensionally equal to such a dialogue.


The effectful forcing technique consists in embedding System $\SysT$
inside two impure extensions, and to translate the latter into
$\MLTT$, thus building a weak form of syntactic model~\cite{Hofmann97,
  syntactic}. These extensions are:
\begin{enumerate}
  \item System $\SysT \Omega$, which is mainly System
    $\SysT$ with a formal oracle $\Omega: \iota \rightarrow
    \iota$. Its translation $\otransterm{\omega}{.}$ into $\MLTT$ is
    simply  the standard interpretation of
    System $\SysT$, parameterized by an oracle $\omega: \nat
    \rightarrow \nat$. We have: $\otranstype{\omega}{\iota} = \nat$,
    $\otransterm{\omega}{0} = 0$, $\otransterm{\omega}{\cstr{Succ}} = \cstr{Succ}$, $\otransterm{\omega}{\Omega} =
    \omega$, etc.
  \item the \emph{dialogue
      interpretation}, which is again $\SysT \Omega$, but this time
    translated into $\MLTT$ using a non-standard translation
    $\dtransterm{.}$. Here, $\dtranstype{\iota} = \Dial\ \nat$, $\dtransterm{0} = \eta \ 0$ and every function is
    interpreted via the bind function of the
    $\Dial$ monad. For instance, $\dtransterm{\cstr{Succ}}$ is the function that
    applies $\cstr{Succ}$ to every leaf of a tree. Finally, $\dtransterm{\Omega}
    = \gamma$ where $\gamma: \Dial\ \nat \rightarrow \Dial\ \nat$ is a
    well chosen term from $\MLTT$.
\end{enumerate}
\noindent A logical relation constrains these two extensions so as to
ensure a fundamental property, namely that for every term $t: \iota$
of System $\SysT$, and for every $\omega: \nat \rightarrow \nat$, we have:
\[ \otransterm{\omega}{t} = \deval \ \omega \ \dtransterm{t}. \]
Crucially, $\otransterm{\omega}{t}$ depends
on $\omega$ whereas $\dtransterm{t}$ does not. From this property and
a bit of work, it follows that every function $f: (\nat
\rightarrow \nat) \rightarrow \nat $ definable in System $\SysT$ is
continuous.\bigskip

In our model, we follow a similar route, albeit for some
slight adjustments: we first define the \emph{axiom translation}
$\atransterm{\alpha}{.}$ from $\BTT$ to $\MLTT$, parameterized by an
oracle $\alpha : \nat \rightarrow \nat$, that adds $\alpha$ to every
context, thus mimicking the first translation, to System $\SysT \Omega$.

We then define the \emph{branching translation} $\btransterm{.}$ which
resembles a lot the Dialogue interpretation but for a crucial
change: a type $A$ is not interpreted as $\Dial\ A$ but as what would amount to an algebra of the
free $\Dial$ monad if we assumed \texttt{funext}. This
change ensures that the \emph{branching translation} provides a model
of $\BTT$.

Finally, the logical relation becomes an additional
layer of parametricity. More precisely, it is a case of \emph{cheap binary
parametricity}, following the taxonomy from Boulier~\cite{Boulier}. However, in order to interpret universes we must also require our parametricity to be
algebraic with respect to the $\Dial$ operator, making it a
layer of \emph{algebraic binary parametricity}.

 We have formalized in Coq our proof that Escardó's result extends to $\BTT$
 \cite{github_code}. We also discuss whether this extension is
 the best we can hope for, as a naive phrasing of this result would
 make $\MLTT$ inconsistent~\cite{escardo2}.

\newpage
\bibliography{bibliographie}

\end{document}
