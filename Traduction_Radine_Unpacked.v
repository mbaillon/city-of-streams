Require Import ssreflect.
Require Import Dialogue.
Require Import Base.
Set Implicit Arguments.
Set Primitive Projections.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.

Variables (X Y : Type).

Section AxiomMonad.


Implicit Type (A B : Type).


Definition ACtx := (X -> Y) -> Type.

Definition ACtx_El (Gamma : ACtx) := forall alpha, Gamma alpha.

Definition ATy (Gamma : ACtx) := forall alpha : X -> Y, (Gamma alpha) -> Type.

Definition AType (Gamma : ACtx) : ATy Gamma :=
  fun alpha gamma => Type.

Definition AEl (Gamma : ACtx) (A : ATy Gamma) :=
  forall (alpha : X -> Y) (gamma : Gamma alpha), A alpha gamma. 


Definition ACtx_nil : ACtx := fun alpha => unit.


Definition ACtx_cons (Gamma : ACtx) (A : ATy Gamma) :=
  fun (alpha : X -> Y) => tsig (fun (gamma : Gamma alpha) => A alpha gamma).

Definition Actx_cons (Gamma : ACtx) (A : ATy Gamma) (alpha : X -> Y)
           (gamma : Gamma alpha) (a : A alpha gamma) : (ACtx_cons A) alpha.
Proof.
  exists gamma ; exact a.
Defined.

Definition Aweak (Gamma : ACtx) (A : ATy Gamma) (B : ATy Gamma) : ATy (ACtx_cons A) :=
  fun alpha gamma => B alpha gamma.(fst).

Definition AweakEl (Gamma : ACtx) (A : ATy Gamma) : AEl (@Aweak Gamma A A) :=
  fun (alpha : X -> Y) (gamma : ACtx_cons A alpha) => gamma.(snd).

Definition Aweakterm (Gamma : ACtx) (A B : ATy Gamma) (b : AEl B) : AEl (@Aweak _ A B) :=
  fun (alpha : X -> Y) (gamma : ACtx_cons A alpha) => b alpha gamma.(fst). 


Definition ASub (Gamma : ACtx) (A : ATy Gamma) (B : ATy (ACtx_cons A)) (a : AEl A) : ATy Gamma :=
  fun alpha gamma => B alpha (exist (a alpha gamma)).


Definition AArr (Gamma : ACtx) (A B : ATy Gamma) : ATy Gamma :=
  fun alpha gamma => 
    A alpha gamma -> B alpha gamma.
  
Definition APi (Gamma : ACtx) (A : ATy Gamma) (B : ATy (ACtx_cons A)) : ATy Gamma :=
  fun alpha gamma =>
    forall a : A alpha gamma, B alpha (exist a).


Definition ALam (Gamma : ACtx) (A : ATy Gamma) (B : ATy (ACtx_cons A)) (f : AEl B)
  : AEl (APi B) := 
  fun (alpha : X -> Y) (gamma : Gamma alpha) (a : A alpha gamma) => f alpha (exist a).


Definition AApp (Gamma : ACtx) (A : ATy Gamma)
           (B : ATy (ACtx_cons A)) (f : AEl (APi  B)) : AEl B :=
  fun (alpha : X -> Y) (gamma : ACtx_cons A alpha) => f alpha gamma.(fst) gamma.(snd).

Definition AApp2 (Gamma : ACtx) (A : ATy Gamma) (B : ATy (ACtx_cons A))
           (f : AEl (APi B)) (a : AEl A) : AEl (ASub B a) :=
  fun (alpha : X -> Y) (gamma : Gamma alpha) => f alpha gamma (a alpha gamma). 

Definition ARevApp (Gamma : ACtx) (A : ATy Gamma) (B : ATy (ACtx_cons A))
           (a : AEl A) :
  AEl (@Aweak _ (APi B) (@ASub _ A B a)) :=
  fun (alpha : X -> Y) (gamma : ACtx_cons (APi B) alpha) =>
    gamma.(snd) (a alpha gamma.(fst)).

Lemma AAppLam (Gamma : ACtx) (A : ATy Gamma) (B : ATy (ACtx_cons A)) (f : AEl B)
      (alpha : X -> Y) :
  (fun gamma => AApp (ALam f) gamma) = (fun gamma => f alpha gamma). 
Proof. reflexivity. Qed.


Definition Abool (Gamma : ACtx) : ATy Gamma := fun alpha gamma => bool. 


Definition Atrue (Gamma : ACtx) : AEl (@Abool Gamma) :=
  fun alpha gamma => true.

Definition Afalse (Gamma : ACtx) : AEl (@Abool Gamma) :=
  fun alpha gamma => false.


Definition Abool_rec (Gamma : ACtx) :
  AEl (APi (APi (@APi (ACtx_cons (@AweakEl _ (@AType Gamma)))
                        (Aweak (@AweakEl _ (@AType Gamma)))
                        (APi (@Aweak _ (@Abool (ACtx_cons (Aweak (@AweakEl _ (@AType Gamma)))))
                                     (Aweak (Aweak (@AweakEl _ (@AType Gamma))))))))).
Proof.
  unfold AEl ; unfold APi ; unfold AType ; unfold Aweak ; unfold Abool ; simpl.
  now refine (fun alpha ga P ptrue pfalse b => match b with
          | true => ptrue
          | false => pfalse
         end).
Defined.


Definition Abool_stor (Gamma : ACtx) :
  AEl (APi (APi (@AType (ACtx_cons (@Abool (ACtx_cons (APi (@AType (ACtx_cons (@Abool Gamma)))))))))).
Proof.
  unfold AEl ; unfold APi ; unfold AType ; unfold Abool ; simpl.
  now refine (fun alpha ga P b =>
                @Abool_rec Gamma alpha ga ((bool -> Type) -> Type) (fun k => k true) (fun k => k false) b P).
Defined.


Definition test (Gamma : ACtx) :
  forall (alpha : X -> Y)
         (gamma : ACtx_cons
                    (@Abool (fun alpha0 : X -> Y =>
                               tsig (fun (gamma : ACtx_cons
                                                    (ARevApp
                                                       (@Atrue Gamma)) alpha0) =>
                                       (Aweakterm (@ARevApp _ _ (@AType _) (@Afalse _)) gamma)))) alpha),
    AType
    {|
    fst := {|
           fst := gamma;
           snd := snd (fst (fst (fst gamma))) |};
    snd := snd gamma |} :=
      fun alpha gamma => @Abool_stor _ alpha gamma gamma.(fst).(fst).(fst).(snd) gamma.(snd).

(*Definition test2 (Gamma : ACtx) :
  AEl (@Aweak _ (@ARevApp _ _ (@AType (ACtx_cons (@Abool Gamma))) (@Atrue _))
              (@AType _)) :=
  Aweakterm (@ARevApp _ _ (@AType _) (@Afalse _)).


Definition Abool_rect (Gamma : ACtx) :
  AEl (@APi Gamma (@APi _ (@Abool Gamma) (@AType (ACtx_cons (@Abool Gamma))))
            (@APi _  (@ARevApp _ _ (@AType (ACtx_cons (@Abool Gamma)))  (@Atrue _))
                  (@APi _ (Aweakterm (@ARevApp _ _ (@AType _) (@Afalse _)))
                        (@APi _ (@Abool _)  (fun alpha gamma => @Abool_stor _ alpha gamma gamma.(fst).(fst).(fst).(snd) gamma.(snd)))))).*)

Definition Abool_rect (Gamma : ACtx) :
  AEl (@APi Gamma (@APi _ (@Abool Gamma) (@AType (ACtx_cons (@Abool Gamma))))
            (@APi _  (fun alpha (gamma : ACtx_cons (APi (AType (Gamma:=ACtx_cons (Abool (Gamma:=Gamma))))) alpha) =>
                        gamma.(snd) true)
                  (@APi _ (fun (alpha : X -> Y)
                               (gamma : ACtx_cons (fun (beta : X -> Y)
                                                       (delta : ACtx_cons (APi (@AType (ACtx_cons (@Abool Gamma)))) beta) =>
                                                     delta.(snd) true) alpha) => gamma.(fst).(snd) false)
                        (@APi _ (@Abool _)  (fun alpha gamma => @Abool_stor _ alpha gamma gamma.(fst).(fst).(fst).(snd) gamma.(snd)))))) :=
  fun alpha gamma P ptrue pfalse b =>
    match b with
    | true => ptrue
    | false => pfalse
    end.


Definition Anat (Gamma :ACtx) : ATy Gamma := fun alpha gamma => nat.

Definition A0 (Gamma : ACtx) : AEl (@Anat Gamma) := fun alpha gamma => 0.

Definition ASucc (Gamma : ACtx) :
  AEl (@APi Gamma (@Anat Gamma) (@Anat (ACtx_cons (@Anat Gamma)))) :=
    fun _ _ n => S n.


Definition Alist (Gamma : ACtx) : AEl (APi (@AType (ACtx_cons (@AType Gamma)))) :=
  fun alpha gamma a => list a.


Definition Anil (Gamma : ACtx) : AEl (AApp (@Alist Gamma)) := fun alpha ga => nil.


Definition Acons (Gamma : ACtx) :
  AEl (@APi Gamma (@AType Gamma)
            (@APi (ACtx_cons (@AType Gamma)) (fun alpha gamma => gamma.(snd))
                  (@APi (ACtx_cons (fun (alpha : X -> Y)
                                        (gamma : ACtx_cons (AType (Gamma:=Gamma)) alpha)
                                    => snd gamma))
                  (@Aweak (ACtx_cons (@AType Gamma)) (fun alpha gamma => gamma.(snd))
                          (AApp (@Alist Gamma)))
                  (@Aweak (ACtx_cons (fun (alpha : X -> Y)
                                          (gamma : ACtx_cons (AType (Gamma:=Gamma)) alpha)
                                      => snd gamma))
                          (@Aweak (ACtx_cons (@AType Gamma)) (fun alpha gamma => gamma.(snd))
                                  (AApp (@Alist Gamma)))
                          (@Aweak (ACtx_cons (@AType Gamma)) (fun alpha gamma => gamma.(snd))
                                  (AApp (@Alist Gamma))))))).
Proof.
  unfold AEl ; unfold APi ; unfold AApp ; unfold Aweak ; simpl.
  exact (fun alpha gamma A a la => cons a la).
Defined.
  
End AxiomMonad.


Section DialogueMonadwFamilies.


Definition BCtx := Type.

Definition BTy (Gamma : BCtx) := Gamma -> BranchingTYPE X Y.

Definition BType (Gamma : BCtx) : BTy Gamma :=
  fun _ => @exist Type (fun A =>  (Y -> A) -> X -> A)
                  (BranchingTYPE X Y) (fun _ _ => Bunit X Y).

Definition BEl' (Gamma : BCtx) (A : BTy Gamma) :=
  forall (gamma : Gamma), BEl (A gamma). 

Definition BCtx_nil : BCtx := unit.

Definition BCtx_cons (Gamma : BCtx) (A : BTy Gamma) :=
  tsig (fun (gamma : Gamma) => BEl (A gamma)).

Definition Bctx_cons (Gamma : BCtx) (B : BTy Gamma)
           (gamma : Gamma) (b : BEl (B gamma)) : BCtx_cons B.
Proof.
  exists gamma ; exact b.
Defined.


Definition Bweak (Gamma : BCtx) (A : BTy Gamma) (B : BTy Gamma) : BTy (BCtx_cons A) :=
  fun gamma => B gamma.(fst).

Definition BweakEl (Gamma : BCtx) (A : BTy Gamma) : BEl' (@Bweak Gamma A A) :=
  fun (gamma : BCtx_cons A) => gamma.(snd).

Definition Bweakterm (Gamma : BCtx) (A B : BTy Gamma) (b : BEl' B) : BEl' (@Bweak _ A B) :=
  fun (gamma : BCtx_cons A) => b gamma.(fst). 

Definition BSub (Gamma : BCtx) (A : BTy Gamma) (B : BTy (BCtx_cons A)) (a : BEl' A) : BTy Gamma :=
  fun gamma => B ({|fst := gamma ; snd :=(a gamma) |}).

Definition BPi (Gamma : BCtx) (A : BTy Gamma) (B : BTy (BCtx_cons A)) : BTy Gamma.
Proof.
  intro gamma.
  unfold BTy in B ; simpl in B.
  exists (forall a : BEl (A gamma),
             BEl (B {| fst := gamma; snd := a|})).
  intros f x a.
  apply (B {| fst := gamma; snd := a |}).(snd) ; trivial.
  exact (fun y => f y a).
Defined.


Definition BLam (Gamma : BCtx) (A : BTy Gamma) (B : BTy (BCtx_cons A)) (f : BEl' B)
  : BEl' (BPi B) := 
  fun (gamma : Gamma) (a : BEl (A gamma)) => f {| fst := gamma; snd := a |}.


Definition BApp' (Gamma : BCtx) (A : BTy Gamma)
           (B : BTy (BCtx_cons A)) (f : BEl' (BPi  B)) : BEl' B :=
  fun (gamma : BCtx_cons A) => f gamma.(fst) gamma.(snd).

Definition BApp2 (Gamma : BCtx) (A : BTy Gamma)
           (B : BTy (BCtx_cons A)) (f : BEl' (BPi B)) (a : BEl' A) : BEl' (BSub B a) :=
  fun gamma => f gamma (a gamma).

Definition BRevApp (Gamma : BCtx) (A : BTy Gamma)
           (B : BTy (BCtx_cons A)) (b : BEl' A) :
  BEl' (@Bweak _ (BPi B) (@BSub _ A B b)) :=
  fun  (gamma : BCtx_cons (BPi B)) =>
    gamma.(snd) (b  gamma.(fst)).

Lemma BAppLam (Gamma : BCtx) (A : BTy Gamma) (B : BTy (BCtx_cons A)) (f : BEl' B) :
  (fun gamma => BApp' (BLam f) gamma) = (fun gamma => f gamma). 
Proof. reflexivity. Qed.

Definition Bbool' (Gamma : BCtx) : BTy Gamma := fun gamma => Bbool X Y.
Definition Btrue' (Gamma : BCtx) : BEl' (@Bbool' Gamma) := (fun _ => @Btrue X Y).
Definition Bfalse' (Gamma : BCtx) : BEl' (@Bbool' Gamma) := (fun _ => @Bfalse X Y).



Definition Bbool_rec (Gamma : BCtx) :
  BEl' (@BPi Gamma (@BType Gamma)
            (@BPi (BCtx_cons (@BType Gamma)) (@BweakEl _ (@BType Gamma))
                  (@BPi (BCtx_cons (@BweakEl _ (@BType Gamma)))
                        (@Bweak (BCtx_cons (@BType Gamma))
                                (@BweakEl _ (@BType Gamma))
                                (@BweakEl _ (@BType Gamma)))
                        (@BPi (BCtx_cons (Bweak (@BweakEl _ (@BType Gamma))))
                              (@Bbool' (BCtx_cons (Bweak (@BweakEl _ (@BType Gamma)))))
                              (@Bweak (BCtx_cons (Bweak (@BweakEl _ (@BType Gamma))))
                                      (@Bbool' (BCtx_cons (Bweak (@BweakEl _ (@BType Gamma)))))
                                      (Bweak (Bweak (@BweakEl _ (@BType Gamma))))))))).
Proof.
  unfold BEl' ; unfold BPi ; unfold BType ; unfold Bweak ; unfold Bbool' ; simpl.
  unshelve refine (fun ga P ptrue pfalse => _).
  now refine (fix f b := match b with
                         | Btrue _ _  => ptrue
                         | Bfalse _ _ => pfalse
                         | Bêtabool g x => P.(snd) (fun y => f (g y)) x
                         end).
Defined.

Definition Bbool_stor (Gamma : BCtx) :
  BEl' (@BPi Gamma (@BPi Gamma (@Bbool' Gamma) (@BType (BCtx_cons (@Bbool' Gamma))))
            (@BPi (BCtx_cons (@BPi Gamma (@Bbool' Gamma) (@BType (BCtx_cons (@Bbool' Gamma)))))
                  (@Bbool' (BCtx_cons (@BPi Gamma (@Bbool' Gamma) (@BType (BCtx_cons (@Bbool' Gamma))))))
                  (@BType (BCtx_cons
                             (@Bbool' (BCtx_cons (@BPi Gamma (@Bbool' Gamma) (@BType (BCtx_cons (@Bbool' Gamma)))))))))).
Proof.
  unfold BEl' ; unfold BPi ; unfold BType ; unfold Bbool' ; simpl.
  exact (fun ga P b => @Bbool_rec Gamma ga
                                   (@BPi Gamma (@BPi Gamma (@Bbool' Gamma) (@BType _)) (@BType _) ga)
                                  (fun k => k (@Btrue X Y))
                                  (fun k => k (@Bfalse X Y)) b P).
Defined.


Definition Bbool_rect (Gamma : BCtx) :
    BEl' (@BPi Gamma (@BPi _ (@Bbool' Gamma) (@BType (BCtx_cons (@Bbool' Gamma))))
            (@BPi _ (fun (gamma : BCtx_cons (@BPi Gamma (@Bbool' Gamma) (@BType (BCtx_cons (@Bbool' Gamma))))) =>
                       gamma.(snd) (@Btrue X Y))
                  (@BPi _ (fun (gamma : BCtx_cons (fun (delta : BCtx_cons (BPi (@BType (BCtx_cons (@Bbool' Gamma))))) =>
                                                     delta.(snd) (@Btrue X Y))) => gamma.(fst).(snd) (@Bfalse X Y))
                        (@BPi _ (@Bbool' _)
                              (fun gamma => @Bbool_stor _ gamma gamma.(fst).(fst).(fst).(snd) gamma.(snd)))))) :=
  fun ga P ptrue pfalse b =>
    match b with
    | Btrue _ _ => ptrue
    | Bfalse _ _ => pfalse
    | Bêtabool g x => tt
    end.



Definition Bnat' (Gamma : BCtx) : BTy Gamma := fun gamma => Bnat X Y.

Definition B0' (Gamma : BCtx) : BEl' (@Bnat' Gamma) := fun gamma => B0 X Y. 

Definition BSucc (Gamma : BCtx) :
  BEl' (@BPi Gamma (@Bnat' Gamma) (@Bnat' (BCtx_cons (@Bnat' Gamma)))) :=
  fun gamma => (@BS X Y).


Definition Blist' (Gamma : BCtx) :
  BEl' (BPi (@BType (BCtx_cons (@BType Gamma)))) := fun gamma A => @Blist X Y A.


Definition Bnil' (Gamma : BCtx) : BEl' (BApp' (@Blist' Gamma)) :=
  fun ga => @Bnil X Y ga.(snd).


Definition Bcons' (Gamma : BCtx) :
  BEl' (@BPi Gamma (@BType Gamma)
            (@BPi (BCtx_cons (@BType Gamma)) (fun gamma => gamma.(snd))
                  (@BPi (BCtx_cons (fun (gamma : BCtx_cons (BType (Gamma:= Gamma)) )
                                    => snd gamma))
                  (@Bweak (BCtx_cons (@BType Gamma)) (fun  gamma => gamma.(snd))
                          (BApp' (@Blist' Gamma)))
                  (@Bweak (BCtx_cons (fun (gamma : BCtx_cons (BType (Gamma:=Gamma)) )
                                      => snd gamma))
                          (@Bweak (BCtx_cons (@BType Gamma)) (fun  gamma => gamma.(snd))
                                  (BApp' (@Blist' Gamma)))
                          (@Bweak (BCtx_cons (@BType Gamma)) (fun  gamma => gamma.(snd))
                                  (BApp' (@Blist' Gamma))))))) :=
  fun gamma A x l => Bcons x l.



  
End DialogueMonadwFamilies.

Section Parametricity.

Definition ECtx (Ga : ACtx) (Gb : BCtx) := forall alpha : X -> Y, (Ga alpha) -> Gb -> Type.


Definition ETy (Gammaa : ACtx) (Gammab : BCtx) (Gammae : ECtx Gammaa Gammab)
           (Xa : ATy Gammaa) (Xb : BTy Gammab) :=
  forall (alpha : X -> Y) (gammaa : Gammaa alpha) (gammab : Gammab) (gammae : Gammae alpha gammaa gammab), Xa alpha gammaa -> BEl (Xb gammab) -> Type.


Definition EEl (Gammaa : ACtx) (Gammab : BCtx) (Gammae : ECtx Gammaa Gammab)
           (Xa : ATy Gammaa) (Xb : BTy Gammab) (Xe : ETy Gammae Xa Xb)
           (xa : AEl Xa) (xb : BEl' Xb) :=
  forall (alpha : X -> Y) (gammaa : Gammaa alpha) (gammab : Gammab) (gammae : Gammae alpha gammaa gammab),  
    Xe alpha gammaa gammab gammae (xa alpha gammaa) (xb gammab).


Definition ECtx_nil (Ga : ACtx) (Gb : BCtx) : ECtx Ga Gb := fun _ _ _ => unit.


Definition ECtx_cons (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb)
           (Xa : ATy Ga) (Xb : BTy Gb) (Xe : ETy Ge Xa Xb) : ECtx (ACtx_cons Xa) (BCtx_cons Xb).
Proof.
  intros alpha ga gb.
  destruct ga as [ga xa] ; destruct gb as [gb xb].
  unfold ETy in Xe.
  unfold ECtx in Ge.
  apply (@tsig (Ge alpha ga gb)).
  intro ge.
  apply (Xe alpha ga gb ge xa xb). 
Defined.


Definition Eweak (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb) (Xa : ATy Ga) (Xb : BTy Gb) (Xe : ETy Ge Xa Xb)
           (Ya : ATy Ga) (Yb : BTy Gb) (Ye : ETy Ge Ya Yb) :
  ETy (@ECtx_cons Ga Gb Ge Xa Xb Xe) (@Aweak Ga Xa Ya) (@Bweak Gb Xb Yb) :=
  fun alpha ga gb ge ya yb =>
    Ye alpha ga.(fst) gb.(fst) ge.(fst) ya yb.


Definition EweakEl (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb) (Xa : ATy Ga) (Xb : BTy Gb) (Xe : ETy Ge Xa Xb)
  : EEl (@Eweak Ga Gb Ge Xa Xb Xe Xa Xb Xe) (@AweakEl Ga Xa) (@BweakEl Gb Xb) :=
  fun alpha ga gb ge => ge.(snd).


Definition Ectx_cons (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb)
           (Xa : ATy Ga) (Xb : BTy Gb) (Xe : ETy Ge Xa Xb) (alpha : X -> Y)
           (ga : Ga alpha) (gb : Gb) (ge : Ge alpha ga gb)
                    (xa : Xa alpha ga) (xb : BEl (Xb gb)) (xe : Xe alpha ga gb ge xa xb) :
              @ECtx_cons Ga Gb Ge Xa Xb Xe alpha (Actx_cons xa) (Bctx_cons xb). 
Proof.
  exists ge ; exact xe.
Defined.


Definition EType (Gammaa : ACtx) (Gammab : BCtx) (Gammae : ECtx Gammaa Gammab) :
  ETy Gammae (@AType Gammaa) (@BType Gammab). 
Proof.
  unfold ETy ; intros alpha ga gb ge Xa Xb.
  exact (Xa -> BEl Xb -> Type).
Defined.

Definition EPi (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb)
           (Xa : ATy Ga) (Xb : BTy Gb) (Xe : ETy Ge Xa Xb)
           (Ya : ATy (ACtx_cons Xa)) (Yb : BTy (BCtx_cons Xb)) (Ye : ETy (ECtx_cons Xe) Ya Yb) :
  ETy Ge (APi Ya) (BPi Yb).
Proof.
  unfold ETy ; intros alpha ga gb ge ya yb.
  unfold ETy in Ye.
  unfold APi in ya ; simpl in ya.
  unfold ETy in Xe.
  exact (forall (xa : Xa alpha ga) (xb : BEl (Xb gb))
                (xe : Xe alpha ga gb ge xa xb),
            Ye alpha (Actx_cons xa) (Bctx_cons xb)
               (@Ectx_cons Ga Gb Ge Xa Xb Xe alpha ga gb ge xa xb xe) (ya xa) (yb xb)).
Defined.

Definition ELam (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb)
           (Xa : ATy Ga) (Xb : BTy Gb) (Xe : ETy Ge Xa Xb)
           (Ya : ATy (ACtx_cons Xa)) (Yb : BTy (BCtx_cons Xb)) (Ye : ETy (ECtx_cons Xe) Ya Yb)
           (fa : AEl Ya) (fb : BEl' Yb) (fe : EEl Ye fa fb) :  EEl (EPi Ye) (ALam fa) (BLam fb).
Proof.
  intros alpha ga gb ge xa xb xe.
  exact: fe.
Defined.


Definition EApp (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb)
           (Xa : ATy Ga) (Xb : BTy Gb) (Xe : ETy Ge Xa Xb)
           (Ya : ATy (ACtx_cons Xa)) (Yb : BTy (BCtx_cons Xb)) (Ye : ETy (ECtx_cons Xe) Ya Yb)
           (fa : AEl (APi Ya)) (fb : BEl' (BPi Yb)) (fe : EEl (EPi Ye) fa fb) (alpha : X -> Y)
           (ga : ACtx_cons Xa alpha) (gb : BCtx_cons Xb) (ge : ECtx_cons Xe ga gb) :
  Ye alpha ga gb ge (AApp fa ga) (BApp' fb gb). 
Proof.
  exact: fe.
Defined.


Lemma EAppLam (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb)
      (Xa : ATy Ga) (Xb : BTy Gb) (Xe : ETy Ge Xa Xb)
      (Ya : ATy (ACtx_cons Xa)) (Yb : BTy (BCtx_cons Xb)) (Ye : ETy (ECtx_cons Xe) Ya Yb)
      (fa : AEl Ya) (fb : BEl' Yb) (fe : EEl Ye fa fb)
      (alpha : X -> Y) :
  (fun  (ga : ACtx_cons Xa alpha) (gb : BCtx_cons Xb) (ge : ECtx_cons Xe ga gb) =>
     EApp (ELam fe)  ge) =
  (fun (ga : ACtx_cons Xa alpha) (gb : BCtx_cons Xb) (ge : ECtx_cons Xe ga gb) =>
     fe alpha ga gb ge).
Proof. reflexivity. Qed.


Inductive EParabool (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb) :
  forall (alpha : X -> Y) (ga : Ga alpha) (gb : Gb) (ge : Ge alpha ga gb),
    (@Abool Ga alpha ga) -> (BEl (@Bbool' Gb gb)) -> Type :=
| EParatrue : forall (alpha : X -> Y) (ga : Ga alpha) (gb : Gb) (ge : Ge alpha ga gb),
    EParabool ge true (@Btrue X Y)
| EParafalse : forall (alpha : X -> Y) (ga : Ga alpha) (gb : Gb) (ge : Ge alpha ga gb),
    EParabool ge false (@Bfalse X Y).

Definition Ebool (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb) : ETy Ge (@Abool Ga) (@Bbool' Gb).
Proof. unfold ETy ; simpl ; now apply EParabool. Defined.

Check @EParatrue ACtx_nil BCtx_nil (@ECtx_nil ACtx_nil BCtx_nil) :
   @EEl ACtx_nil BCtx_nil (@ECtx_nil ACtx_nil BCtx_nil) (@Abool ACtx_nil) (@Bbool' BCtx_nil) (@Ebool ACtx_nil BCtx_nil (@ECtx_nil ACtx_nil BCtx_nil)) (@Atrue ACtx_nil) (@Btrue' BCtx_nil).

Definition Etrue (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb) :
  @EEl Ga Gb Ge (@Abool Ga) (@Bbool' Gb) (@Ebool Ga Gb Ge) (@Atrue Ga) (@Btrue' Gb) :=
  @EParatrue Ga Gb Ge.


Definition Ebool_rec (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb) :
  EEl (@EPi _ _ Ge _ _ (@EType _ _ Ge) _ _
            (@EPi _ _ (ECtx_cons (@EType _ _ Ge)) _ _ (@EweakEl _ _ _ _ _ (@EType _ _ Ge)) _ _
                  (@EPi _ _ (ECtx_cons (@EweakEl _ _ _ _ _ (@EType _ _ Ge))) _ _
                        (@Eweak _ _ (ECtx_cons (@EType _ _ Ge)) _ _
                                (@EweakEl _ _ _ _ _ (@EType _ _ Ge)) _ _
                                (@EweakEl _ _ _ _ _ (@EType _ _ Ge))) _ _
                        (@EPi _ _ (ECtx_cons (Eweak (@EweakEl _ _ _ _ _ (@EType _ _ Ge)))) _ _
                              (@Ebool _ _ (ECtx_cons (Eweak (@EweakEl _ _ _ _ _ (@EType _ _ Ge))))) _ _
                              (@Eweak _ _ (ECtx_cons (Eweak (@EweakEl _ _ _ _ _ (@EType _ _ Ge)))) _ _
                                      (@Ebool _ _ (ECtx_cons (Eweak (@EweakEl _ _ _ _ _ (@EType _ _ Ge))))) _ _
                                      (Eweak (Eweak (@EweakEl _ _ _ _ _ (@EType _ _ Ge)))))))))
      (@Abool_rec Ga) (@Bbool_rec Gb).
Proof.
  unfold EEl ; unfold EPi ; unfold EType ; simpl.
  unfold Eweak ; unfold EweakEl ; unfold Aweak ; unfold AweakEl ; unfold Bweak ; unfold BweakEl  ; simpl.
  unfold Abool ; unfold Bbool' ; unfold Ebool ; simpl.
  intros alpha ga gb ge Pa Pb Pe patrue pbtrue petrue pafalse pbfalse pefalse ba bb be.
  induction be ; simpl ; assumption.
Defined.


Definition Ebool_stor (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb) :
  EEl (@EPi Ga Gb Ge _ _ (@EPi Ga Gb Ge (@Abool Ga) (@Bbool' Gb) (@Ebool Ga Gb Ge)
                           (@AType (ACtx_cons (@Abool Ga))) (@BType (BCtx_cons (@Bbool' Gb)))
                           (@EType _ _ (ECtx_cons (@Ebool Ga Gb Ge)))) _ _
            (@EPi _ _ (ECtx_cons (EPi (@EType _ _ (ECtx_cons (@Ebool Ga Gb Ge))))) _ _
                  (@Ebool _ _ (ECtx_cons (EPi (@EType _ _ (ECtx_cons (@Ebool Ga Gb Ge)))))) _ _
                  (@EType _ _ (ECtx_cons (@Ebool _ _ (ECtx_cons (EPi (@EType _ _ (ECtx_cons (@Ebool Ga Gb Ge)))))))))) (@Abool_stor Ga) (@Bbool_stor Gb).
Proof.
  intros alpha ga gb ge Pa Pb Pe ba bb be stora storb.
  induction be.
  - exact (Pe true (Btrue X Y) (EParatrue ge) stora storb).
  - exact ((Pe false (Bfalse X Y) (EParafalse ge) stora storb)).
Defined.



Definition Ebool_rect (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb) :
  EEl (@EPi _ _ Ge _ _ (@EPi _ _ _ _ _ (@Ebool _ _ Ge) _ _ (@EType _ _ (ECtx_cons (@Ebool _ _ Ge)))) _ _
            (@EPi _ _ _ _ _
                  (fun _ _ _ (ge : (ECtx_cons (EPi (@EType _ _ (ECtx_cons (@Ebool _ _ Ge))))) _ _ _) xa xb =>
                     ge.(snd) true (Btrue X Y) (EParatrue ge.(fst)) xa xb) _ _
                  (@EPi _ _ _ _ _
                        (fun _ _ _ (ge : (ECtx_cons
                                            (fun alpha ga gb (ge : (ECtx_cons (EPi (@EType _ _ (ECtx_cons (@Ebool _ _ Ge))))) alpha ga gb) xa xb =>
                                               ge.(snd) true (Btrue X Y) (EParatrue ge.(fst)) xa xb)) _ _ _) xa xb =>
                           ge.(fst).(snd) false (Bfalse X Y) (EParafalse ge.(fst).(fst)) xa xb) _ _
                        (@EPi _ _ _ _ _
                              (@Ebool _ _ _) _ _
                              (fun alpha ga gb (ge : ECtx_cons (@Ebool _ _ (ECtx_cons (fun alpha ga gb
                           (ge : ECtx_cons (fun alpha ga gb (ge : (ECtx_cons (EPi (@EType _ _ (ECtx_cons (@Ebool _ _ Ge))))) alpha ga gb) xa xb =>
                                              ge.(snd) true (Btrue X Y) (EParatrue ge.(fst)) xa xb) ga gb) xa xb =>
                         ge.(fst).(snd) false (Bfalse X Y) (EParafalse ge.(fst).(fst)) xa xb))) _ _) stora storb =>
                                 match ge.(snd) with
                                 | EParatrue _ => ge.(fst).(fst).(fst).(snd) true (Btrue X Y) (EParatrue _) ga.(fst).(fst).(snd) gb.(fst).(fst).(snd)
                                 | EParafalse _ => ge.(fst).(fst).(fst).(snd) false (Bfalse X Y) (EParafalse _) ga.(fst).(snd) gb.(fst).(snd)
                                 end)))))
      (@Abool_rect Ga) (@Bbool_rect Gb) :=
  fun alpha ga gb ge Pa Pb Pe patrue pbtrue petrue pafalse pbfalse pefalse ba bb be =>
    match be as be0 return match be0 return Type with
                           | EParatrue _ => Pe true (Btrue X Y) (EParatrue ge) patrue pbtrue
                           | EParafalse _ => Pe false (Bfalse X Y) (EParafalse ge) pafalse pbfalse
                           end
    with
    | EParatrue _ => petrue
    | EParafalse _ => pefalse
    end.


 

    
Inductive EParanat (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb) :
  forall (alpha : X -> Y) (ga : Ga alpha) (gb : Gb) (ge : Ge alpha ga gb),
    (@Anat Ga alpha ga) -> (BEl (@Bnat' Gb gb)) -> Type :=
| EPara0 : forall (alpha : X -> Y) (ga : Ga alpha) (gb : Gb) (ge : Ge alpha ga gb),
    EParanat ge 0 (B0 X Y)
| EParaS : forall (alpha : X -> Y) (ga : Ga alpha) (gb : Gb) (ge : Ge alpha ga gb)
                 (no : nat) (nt : BEl (Bnat X Y)) (ne : EParanat ge no nt),
    EParanat ge (S no) (BS nt).

Definition Enat' (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb) : ETy Ge (@Anat Ga) (@Bnat' Gb).
Proof. unfold ETy ; simpl ; now apply EParanat. Defined.


Inductive EParaPnat (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb) :
  forall (alpha : X -> Y) (ga : Ga alpha) (gb : Gb) (ge : Ge alpha ga gb),
    (@Anat Ga alpha ga) -> (BEl (@Bnat' Gb gb)) -> Type :=
| EParaP0 : forall (alpha : X -> Y) (ga : Ga alpha) (gb : Gb) (ge : Ge alpha ga gb),
    EParaPnat ge 0 (B0 X Y)
| EParaPS : forall (alpha : X -> Y) (ga : Ga alpha) (gb : Gb) (ge : Ge alpha ga gb)
                 (no : nat) (nt : BEl (Bnat X Y)) (ne : EParaPnat ge no nt),
    EParaPnat ge (S no) (BS nt)
| EParaPbêta : forall (alpha : X -> Y) (ga : Ga alpha) (gb : Gb) (ge : Ge alpha ga gb)
                       (no : nat) (f : (Y -> BEl (Bnat X Y))) (x : X)
                       (rel : EParaPnat ge no (Devale (f (alpha x)) alpha)),
    EParaPnat ge no (Bêtanat f x).

Definition EPnat (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb) :
  ETy Ge (@Anat Ga) (@Bnat' Gb).
Proof. unfold ETy ; simpl ; now apply EParaPnat. Defined.


Lemma EPnat_equal (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb)
      (no : AEl (@Anat Ga)) (nt : BEl' (@Bnat' Gb)) (ne : EEl (@EPnat Ga Gb Ge) no nt) :
  forall (alpha : X -> Y) (ga : Ga alpha) (gb : Gb) (ge : Ge alpha ga gb),
    BNdialogue (nt gb) alpha = (no alpha ga).
Proof.
  intros.
  unfold EEl in ne ; simpl in ne.
  specialize ne with alpha ga gb ge.
  induction ne as [ | alpha ga gb ge mo mb ne ihne | ] ; trivial ; simpl.
  - now rewrite ihne.
  - now rewrite Devale_BNdialogue.
Qed.    



Inductive EParalist (Ga : ACtx) (Gb : BCtx) (Ge : ECtx Ga Gb) :
  forall (alpha : X -> Y) (ga : Ga alpha) (gb : Gb) (ge : Ge alpha ga gb)
         (Xa : ATy Ga) (Xb : BTy Gb) (Xe : ETy Ge Xa Xb),
    (@Alist Ga alpha ga (Xa alpha ga)) -> (BEl (@Blist' Gb gb (Xb gb))) -> Type :=
| EParanil : forall (alpha : X -> Y) (ga : Ga alpha) (gb : Gb) (ge : Ge alpha ga gb)
                    (Xa : ATy Ga) (Xb : BTy Gb) (Xe : ETy Ge Xa Xb),
    EParalist ge Xe nil (@Bnil X Y (Xb gb))
| EParacons : forall (alpha : X -> Y) (ga : Ga alpha) (gb : Gb) (ge : Ge alpha ga gb)
                     (Xa : ATy Ga) (Xb : BTy Gb) (Xe : ETy Ge Xa Xb)
                     (xa : Xa alpha ga) (xb : BEl (Xb gb)) (xe : Xe alpha ga gb ge xa xb)
                     (la : @Alist Ga alpha ga (Xa alpha ga)) (lb : BEl (@Blist' Gb gb (Xb gb))),
    EParalist ge Xe (@Acons Ga alpha ga (Xa alpha ga) xa la)
              (@Bcons' Gb gb (Xb gb) xb lb).

End Parametricity.
