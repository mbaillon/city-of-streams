Require Import List.
Require Import Streams.
Require Import ssreflect.
Require Import Dialogue.
Require Import Base.
Set Implicit Arguments.
Set Primitive Projections.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Universe Polymorphism.
Set Primitive Projections.
Set Polymorphic Inductive Cumulativity.


Section RadinEscardoXu.

Variables X Y : Type.

Definition RadinTYPE :=
  triple (fun (A : (BranchingTYPE X Y)) (B : OracleTYPE X Y) =>
            (forall alpha, BEl A -> (B alpha) -> Type)).


Definition RadinEL (R : RadinTYPE) :=
  triple (fun (a : BEl R.(q1)) (b : forall alpha, R.(q2) alpha) =>
            (forall alpha, R.(q3) alpha a (b alpha))).

Notation "[| A |]b" := (BEl A).
Notation "[| A |]o" := (OEl A).
Notation "[| A |]r" := (RadinEL A).


(*Definition BTypeᶠ : BranchingTYPE :=
  @exist Type (fun A =>  (nat -> A) -> nat -> A) BranchingTYPE (fun _ _ => Bunit).


Definition OTypeᶠ : OracleTYPE := fun _ => Type.*)


Definition RadinTypeᶠ : RadinTYPE.
Proof.
  exists (BTypeᶠ X Y) (fun _ => Type).
  intros alpha A B.
  exact ([| A |]b -> B -> Type).
Defined.

Check RadinTypeᶠ : RadinEL RadinTypeᶠ.


Definition RadinBArr (A B : BranchingTYPE X Y) : BranchingTYPE X Y.
Proof.
  exists ([| A |]b -> [| B |]b).
  now apply (fun f x a => B.(snd) (fun y => f y a) x).
Defined.


Definition RadinBPI (A : BranchingTYPE X Y) (B : [| (RadinBArr A (BTypeᶠ X Y)) |]b) :
  BranchingTYPE X Y.
Proof.
  exists (forall x : [| A |]b, [| B x |]b).
  now apply (fun (f : (Y -> forall x : [| A |]b, [| B x |]b)) (x : X) (a : [| A |]b) =>
           (B a).(snd) (fun y => f y a) x).
Defined.

Definition RadinOArr (A B : OracleTYPE X Y) : OracleTYPE X Y.
Proof.
  intro p.
  exact (A p -> B p).
Defined.


Definition RadinOPI (A : OracleTYPE X Y) (B : [|RadinOArr A (@OTypeᶠ X Y) |]o) :
  OracleTYPE X Y.
Proof.
  intro p.
  exact (forall a : A p, B p a).
Defined.


Definition RadinArr (A B : RadinTYPE) : RadinTYPE.
Proof.
  exists (RadinBArr A.(q1) B.(q1)) (RadinOArr A.(q2) B.(q2)).
  intros alpha f g.
  exact (forall (a : [| A |]r), B.(q3) alpha (f a.(q1)) (g (a.(q2) alpha))). 
Defined.


(*Definition RadinLam {A B : RadinTYPE} (f : [| A |] -> [| B |]) :
  [| Arr A B|].
Proof.
  exists (fun a => (f a).(q1)) (fun p a => (f a).(q2) p).
  exact (fun a => (f a).(q3)).
Defined.*)


Definition RadinPI (A : RadinTYPE) (B : [| RadinArr A RadinTypeᶠ |]r) : RadinTYPE.
Proof.
  exists (RadinBPI B.(q1)) (@RadinOPI A.(q2) B.(q2)).
  unfold RadinEL in B ; simpl in B.
  unfold RadinEL ; simpl.
  pose proof B.(q3) as Bq3.
  simpl in Bq3.
  intros alpha f g.
  exact (forall (a : [|A|]r),
             B.(q3) alpha a (f a.(q1)) (g (a.(q2) alpha))).
Defined.


(*Notation "'RPi'  x .. y , P" := (@RadinPI _ (RadinLam (fun x => .. (@RadinPI _ (RadinLam (fun y => P))) ..)))
                                 (at level 200, x binder, y binder, right associativity).*)

Definition RadinBApp {A B : BranchingTYPE X Y}
           (f : [| RadinBArr A B |]b) (a : [| A |]b) : [|B|]b :=
  f a.

Definition RadinOApp {A B : OracleTYPE X Y}
           (f : [|RadinOArr A B|]o) (a : [| A |]o) : [| B |]o :=
  fun alpha => f alpha (a alpha).

Definition RadinOApp' {A : OracleTYPE X Y} {B : [| RadinOArr A (@OTypeᶠ X Y) |]o}
           (f : [|RadinOPI B|]o) (a : [|A|]o) : [|RadinOApp B a|]o :=
  fun alpha => f alpha (a alpha).



Definition RadinApp {A B : RadinTYPE} (f : [| RadinArr A B |]r) (a : [| A |]r) :
  [| B |]r.
Proof.
  exists (RadinBApp f.(q1) a.(q1))  (RadinOApp f.(q2) a.(q2)).
  unfold RadinBApp ; simpl.
  exact (fun alpha => f.(q3) alpha a).
Defined.


Inductive Paradinbool: (X -> Y) -> [| Bbool X Y |]b -> bool -> Type :=
| Paradintrue : forall (alpha : X -> Y), Paradinbool alpha (@Btrue X Y) true
| Paradinfalse : forall (alpha : X -> Y), Paradinbool alpha (@Bfalse X Y) false.


Definition RadinBool : RadinTYPE.
Proof.
  exists (Bbool X Y) (fun _ => bool).
  exact Paradinbool.
Defined.


Inductive Paradinnat : (X -> Y) -> [| Bnat X Y |]b -> nat -> Type :=
| Paradin0 : forall (alpha : X -> Y), Paradinnat alpha (B0 X Y) 0
| ParadinS : forall (alpha : X -> Y) (nt : BEl (Bnat X Y)) (no : nat) (ne : Paradinnat alpha nt no),
    Paradinnat alpha (BS nt) (S no).


Definition RadinNat : RadinTYPE.
Proof.
  exists (Bnat X Y) (fun _ => nat).
  exact Paradinnat.
Defined.
  
Definition RadinZero : [| RadinNat |]r :=
  {| q1 := B0 X Y; q2 := fun _ => 0; q3 := Paradin0 |}.


Definition RadinSucc_aux : [|RadinNat|]r -> [|RadinNat|]r.
Proof.
  intro n.
  exists (BS n.(q1)) (fun alpha => S (n.(q2) alpha)).
  exact (fun alpha => ParadinS (n.(q3) alpha)).  
Defined.


Definition RadinSucc : [| RadinArr RadinNat RadinNat |]r.
Proof.
  unfold RadinArr ; simpl.
  exists (@BS X Y) (fun alpha => S).
  exact (fun alpha n => ParadinS (n.(q3) alpha)).
Defined.


Inductive ParadinPnat : (X -> Y) -> [| Bnat X Y |]b -> nat -> Type :=
| ParadinP0 : forall (alpha : X -> Y), ParadinPnat alpha (B0 X Y) 0
| ParadinPS : forall (alpha : X -> Y) (nt : [|Bnat X Y|]b)
                     (no : nat) (ne : ParadinPnat alpha nt no),
    ParadinPnat alpha (BS nt) (S no)
| ParadinPbêta : forall (alpha : X -> Y) (f : (Y -> [|Bnat X Y|]b))
                        (x : X) (no : nat)
                        (rel : ParadinPnat alpha (Devale (f (alpha x)) alpha) no),
ParadinPnat alpha (Bêtanat f x) no.


Definition RadinPnat : RadinTYPE.
Proof.
  exists (Bnat X Y) (fun alpha => nat).
  now apply ParadinPnat.
Defined.


Definition RadinP0 : [| RadinPnat |]r.
Proof.
  exists (B0 X Y) (fun _ => 0).
  exact ParadinP0.
Defined.


Definition RadinPS_aux : [|RadinPnat|]r -> [|RadinPnat|]r.
Proof.
  intro n.
  destruct n as [nt no ne] ; simpl in ne.
  exists (BS nt) (fun alpha => S (no alpha)).
  exact (fun alpha => ParadinPS (ne alpha)).
Defined.


Definition RadinPS : [| RadinArr RadinPnat RadinPnat |]r.
Proof.
  exists (@BS X Y) (fun alpha no => S no ).
  intros alpha n ; simpl.
  exact (ParadinPS (n.(q3) alpha)).
Defined.


Lemma RadinPnat_equal (n : [|RadinPnat|]r) (alpha : X -> Y) :
    BNdialogue (n.(q1)) alpha = n.(q2) alpha.
Proof.
  destruct n as [nt no ne] ; simpl.
  elim: (ne alpha) => [ | bêta mt mo me ihme | bêta f x mo rel ihme] ; trivial ; simpl.
  - exact: eq_S.
  - rewrite Devale_BNdialogue.
    exact: ihme.
Qed.



(*Definition RadinOlist : [|RadinOArr (@OTypeᶠ X Y) (@OTypeᶠ X Y) |]o :=
  fun p A => list A.

Definition RadinOnil : [|@RadinOPI (@OTypeᶠ X Y) RadinOlist|]o := fun alpha A => nil.


Definition RadinOcons {A : OracleTYPE X Y} :
  [|RadinOArr A (RadinOArr (RadinOApp RadinOlist A) (RadinOApp RadinOlist A)) |]o :=
  fun alpha a l => cons a l.




Inductive Paradinlist {A : RadinTYPE} : (X -> Y) -> [| @Blist X Y (A.(q1)) |]b -> [| RadinOApp RadinOlist (A.(q2)) |]o -> Type :=
| Paradinnil : forall (alpha : X -> Y),
    Paradinlist alpha (@Bnil X Y A.(q1)) (RadinOApp' RadinOnil A.(q2))
| Paradincons : forall (alpha : X -> Y) (a : [| A |]r)
                       (lt : [| @Blist X Y (A.(q1)) |]b)
                       (lo : [| RadinOApp RadinOlist (A.(q2))|]o)
                       (le : Paradinlist alpha lt lo),
    Paradinlist alpha (Bcons a.(q1) lt)
                (RadinOApp (RadinOApp RadinOcons A.(q2)) lo (a.(q2)) lo).

Definition List (A : TYPE) : TYPE.
Proof.
  exists (@Blist A.(q1)) (@Olist A.(q2)).
  exact (@Paralist A).
Defined.

Definition Nil {A : TYPE} : [| List A |].
Proof.
  exists Bnil (Onil A.(q2)).
  by apply Paranil.
Defined.

Definition Cons (A : TYPE) : [| A |] -> [| List A |] -> [| List A |].
Proof.
  intros a l.
  exists (Bcons a.(q1) l.(q1)) (Ocons a.(q2) l.(q2)).
  exact (Paracons a l.(q3)).
Defined.


Inductive RadinEqfin {A B : RadinTYPE} :
  ([|A|]r -> [|B|]r) -> [|RadinList A|]r -> ([|A|]r -> [|B|]r) -> Type :=
| Radineqnil : forall (alpha bêta : ([|A|] -> [|B|])),
    Eqfin alpha Nil bêta
| Radineqcons : forall alpha l bêta n,
    (alpha n = bêta n) -> (Eqfin alpha l bêta) -> (Eqfin alpha (Cons n l) bêta).


Lemma inversion_eqfinconstl {A B : TYPE} (alpha bêta :  [|A|] -> [|B|])
      (l : [|List A|]) (n : [|A|]) :
    (Eqfin alpha (Cons n l) bêta ) -> (Eqfin alpha l bêta).
Proof.
  intro H.
  simple inversion H as [ gamma delta H2 HeqNil |
                          alpha' l' bêta' n' HeqApp Finite_Eq Eqalpha HeqCons Eqbêta].
  - exfalso.
    discriminate HeqNil.
  - destruct l ; destruct l'.
    pose proof f_equal (@Tail A) HeqCons as aux.
    simpl in aux ; rewrite - aux.
    now rewrite - Eqalpha ; rewrite - Eqbêta.
Defined.


Definition Continuous {A B C : TYPE} (f : ([|A|] -> [|B|]) -> [|C|]) : Type :=
  forall (alpha : [|A|] -> [|B|]),
    {l & forall (bêta : [|A|] -> [|B|]),
          Eqfin alpha l bêta -> f alpha = f bêta}.*)

End RadinEscardoXu.

Section RadinEscardoXunat.

Implicit Type (A B : Type).

Notation "[| A |]b" := (BEl A).
Notation "[| A |]r" := (RadinEL A).

Definition Bnatnat := @Bnat nat nat.
Definition RadinNatnat := @RadinNat nat nat.
Definition RadinPnatnat := @RadinPnat nat nat.



Lemma Generic_aux_Devale (k n : nat)  (d : [| Bnatnat |]b) (alpha : nat -> nat) :
  ParadinPnat alpha (Generic_aux k (Devale d alpha)) n ->
  ParadinPnat alpha (Devale (Generic_aux k d) alpha) n.
Proof.
  elim: d k.
  - intros k Hyp ; simpl ; simpl in Hyp.
    now inversion_clear Hyp.
  - intros d ihd k Hyp.
    simpl ; simpl in Hyp.
    inversion Hyp ; simpl.
    + exact: ParadinP0.
    + exact: ParadinPS.
    + exact: rel.
  - intros f ihd x k Hyp. 
    simpl ; simpl in Hyp.
    now apply ihd.
Qed.

Definition RadinPGeneric : [| RadinArr RadinPnatnat RadinPnatnat |]r.
Proof.
  unfold RadinArr ; unfold RadinEL ; simpl.
  exists (Generic_aux 0) (fun alpha => alpha).
  intros alpha n.
  destruct n as [nt no ne] ; simpl.
  have aux :  forall (k : nat), ParadinPnat alpha (Generic_aux k nt) (alpha (k + (no alpha))).
  - intro k.
    elim: (ne alpha) k.
    + simpl ; intros bêta k.
      rewrite - plus_n_O.
      apply (ParadinPbêta).
      induction (bêta k) ;  simpl. 
      * exact (ParadinP0 bêta).
      * rewrite Devale_BNEta in IHn.
        exact: (ParadinPS).
    + simpl.
      intros bêta mt mo me1 me2 k.
      rewrite - plus_n_Sm ; rewrite - plus_Sn_m.
      exact (me2 (S k)).
    + intros bêta f x mo me1 me2 k.
      simpl.
      apply ParadinPbêta.
      now apply Generic_aux_Devale.
  - exact: (aux 0).
Defined.      


Lemma NaturRadinPGeneric (alpha : nat -> nat) (n : [|RadinPnatnat|]r) :
  BNdialogue (RadinApp RadinPGeneric n).(q1) alpha = alpha (n.(q2) alpha).
Proof.
  destruct n as [nt no ne] ; simpl ; unfold RadinBApp ; simpl.
  have aux : forall (k : nat),
      BNdialogue (Generic_aux k nt) alpha = alpha (k + (no alpha)).
  - elim: (ne alpha) ; simpl.
    + intros bêta k ; now rewrite NaturBNEtaDialogue ; rewrite - plus_n_O.
    + intros bêta mt mo me ihme k.
      rewrite - plus_n_Sm ; rewrite - plus_Sn_m.
      exact: ihme.
    + intros bêta f x mo me ihme k.
      now specialize ihme with k ; rewrite Generic_Devale_Dialogue in ihme.
  - exact: aux.
Qed.

Theorem continuity (f : [|RadinArr (RadinArr RadinPnatnat RadinPnatnat) RadinPnatnat|]r) :
  forall alpha, continuous (fun p => f.(q2) p p) alpha.
Proof.
  intro alpha ; simpl.
  pose proof (RadinPnat_equal (RadinApp f RadinPGeneric)) as Eq.
  destruct f as [ft fo fe] ; simpl ; simpl in ft ; simpl in fo ; simpl in fe.
  simpl in Eq ; unfold RadinBApp in Eq ; unfold RadinOApp in Eq ; simpl in Eq.
  pose proof (fun alpha => fe alpha RadinPGeneric) as Hyp ; simpl in Hyp.
  destruct (BNDialogue_Continuity (ft (Generic_aux 0)) alpha) as [l ihl].
  exists l ; intros bêta Finite_Eq.
  rewrite - Eq ; rewrite - (Eq bêta).
  exact: ihl.
Qed.  



End RadinEscardoXunat.
