Set Definitional UIP.
Set Primitive Projections.
Section BranchingTranslation.
Require Import Base.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Definition I : Set := nat.
Definition O : Set := nat.


Inductive True : SProp := T.
Inductive False : SProp :=.
Inductive and (A : SProp) (B : SProp) : SProp := conj : A -> B -> and A B.

Inductive eqn (A : Type) (x : A) : A -> SProp := refl : @eqn A x x.

Definition Rw (A B : Type) (e : eqn A B) (x : A) : B :=
  match e with | refl _ => x end.

Definition SRw (A B : SProp) (e : eqn A B) (x : A) : B :=
  match e with | refl _ => x end.

Definition BackRw (A B : Type) (e : eqn A B) : B -> A :=
  match e with | refl _ => fun x => x end.

Definition BackSRw (A B : SProp) (e : eqn A B) : B -> A :=
  match e with | refl _ => fun x => x end.

Definition Sf_equal (A : Type) (B : A -> Type) (f : forall a, B a)
           (x y : A) (e : eqn x y) : eqn (match e in eqn _ K return B K with
                                          | refl _ => f x
                                          end) (f y) :=
  match e with
  | refl _ => refl _
  end.
  
  
Axiom F : False.
Ltac admit := case F.

Record Ssig (A : SProp) (B : A -> SProp) : SProp := Sexist { Sone : A;  Stwo : B Sone }.
Record Ssigt (A : Type) (B : A -> SProp) : Type := SexisT { Sfst : A;  Ssnd : B Sfst }.
Record Ssig2 (A : SProp) (B : SProp) (C : A -> B -> SProp) : SProp :=
  Sexist3 { Sone3 : A;  Stwo3 : B ; Sthree3 : C Sone3 Stwo3 }.


Lemma Ssigt_equal (A1 A2 : Type) (B1 : A1 -> SProp) (B2 : A2 -> SProp)
      (eq1 : eqn A1 A2) (eq2 : eqn (fun x => B1 (BackRw eq1 x)) B2) :
  eqn (Ssigt B1) (Ssigt B2).
Proof.
  simpl.
  destruct eq2.
  destruct eq1.
  reflexivity.
Defined.

Record BranchingTYPE := {
  K : Type;
  dig : I -> (O -> K) -> K;
                       }.

Coercion K : BranchingTYPE >-> Sortclass.
Arguments dig _ : clear implicits.
Print tsig.

Inductive BranchingBool :=
| Btrue : BranchingBool
| Bfalse : BranchingBool
| Bêta : I -> (O -> BranchingBool) -> BranchingBool.

Inductive Boole : BranchingBool -> BranchingBool -> Type :=
| Betrue : Boole Btrue Btrue
| Befalse : Boole Bfalse Bfalse
| Bêtae1 : forall (n : BranchingBool) (i : I) (f : O -> BranchingBool)
                  (o : O)
                 (H : Boole n (f o)),
    Boole n (Bêta i f)
| Bêtae2 : forall (n : BranchingBool) (i : I) (f : O -> BranchingBool)
                  (o : O)
                  (H : Boole (f o) n),
    Boole (Bêta i f) n
| Digamma : forall (i : I) (f g : O -> BranchingBool)
                   (H : forall o, Boole (f o) (g o)),
    Boole (Bêta i f) (Bêta i g).

Inductive SBoole : forall (a b c d : BranchingBool)
                          (eab : Boole a b)
                          (ecd : Boole c d), SProp :=
| SBetrue : SBoole Betrue Betrue
| SBefalse : SBoole Befalse Befalse
| SBêtae1 : forall (n : BranchingBool) (i : I) (f : O -> BranchingBool)
                   (o : O)
                   (H : Boole n (f o))
                   (SH : SBoole H H),
    SBoole (Bêtae1 i H) (Bêtae1 i H)
| SBêtae11 : forall (n : BranchingBool) (i : I) (f : O -> BranchingBool)
                   (o : O)
                   (H : Boole n (f o))
                   (SH : SBoole H H),
    SBoole H (Bêtae1 i H)
| SBêtae12 : forall (n : BranchingBool) (i : I) (f : O -> BranchingBool)
                   (o : O)
                   (H : Boole n (f o))
                   (SH : SBoole H H),
    SBoole (Bêtae1 i H) H
| SBêtae2 : forall (n : BranchingBool) (i : I) (f : O -> BranchingBool)
                   (o : O)
                   (H : Boole (f o) n)
                   (SH : SBoole H H),
    SBoole (Bêtae2 i H) (Bêtae2 i H)
| SBêtae21 : forall (n : BranchingBool) (i : I) (f : O -> BranchingBool)
                   (o : O)
                   (H : Boole (f o) n)
                   (SH : SBoole H H),
    SBoole H (Bêtae2 i H)
| SBêtae22 : forall (n : BranchingBool) (i : I) (f : O -> BranchingBool)
                   (o : O)
                   (H : Boole (f o) n)
                   (SH : SBoole H H),
    SBoole (Bêtae2 i H) H
| SDigamma : forall (i : I) (f g : O -> BranchingBool)
                    (H : forall o, Boole (f o) (g o))
                    (SH : forall o, SBoole (H o) (H o)),
    SBoole (Digamma i H) (Digamma i H)
| SDigamma1 : forall (i : I) (f g : O -> BranchingBool)
                     (o : O)
                     (H : forall o, Boole (f o) (g o))
                     (SH : forall o, SBoole (H o) (H o)),
    SBoole (H o) (Digamma i H)
| SDigamma2 : forall (i : I) (f g : O -> BranchingBool)
                     (o : O)
                    (H : forall o, Boole (f o) (g o))
                    (SH : forall o, SBoole (H o) (H o)),
    SBoole (Digamma i H) (H o).



Lemma BoolRefl : forall (b : BranchingBool),
    Boole b b.
Proof.
  refine (fix f b := match b with
                     | Btrue => Betrue
                     | Bfalse => Befalse
                     | Bêta i k => _
                     end).
  refine (Digamma _ _).
  exact (fun o => f (k o)).
Defined.

Lemma SBoolRefl : forall (b1 b2 : BranchingBool)
                        (be : Boole b1 b2),
    SBoole be be.
Proof.
  refine (fix f b1 b2 be {struct be} := match be with
                     | Betrue => SBetrue
                     | Befalse => SBefalse
                     | Bêtae1 i H => _
                     | Bêtae2 i H => _
                     | Digamma i H => _
                            end).
  - refine (SBêtae1 _ _).
    exact (f _ _ H).
  - refine (SBêtae2 _ _).
    exact (f _ _ H).
  - refine (SDigamma _ _).
    exact (fun o => f _ _ (H o)).
Defined.  

Print eqn.

Definition BiAp : forall (A B C : Type) (F : A -> B -> C) (x1 x2 : A) (y1 y2 : B)
                             (ex : eqn x1 x2) (ey : eqn y1 y2),
    eqn (F x1 y1) (F x2 y2).
Proof.
  intros.
  destruct ex.
  now destruct ey.
Defined.

Definition Bif_Sequal : forall (A1 A2 B1 B2 C : Type)
                               (F : A1 -> B1 -> C)
                               (G : A2 -> B2 -> C)
                               (eA : eqn A1 A2)
                               (eB : eqn B1 B2)
                               (e : eqn F (fun a b => G (Rw eA a) (Rw eB b)))
                               (a : A1) (b : B1),
    eqn (F a b) (G  (Rw eA a) (Rw eB b)).
Proof.
  intros; destruct eA ; destruct eB.
  simpl in *.
  change (eqn F0 G) in e.
  now destruct e.
Defined.


Definition TriSf_Sequal (A1 A2 B1 B2 : Type)
                       (F : A1 -> B1 -> Type)
                       (G : A2 -> B2 -> Type)
                       (SF: forall a1 a2 b1 b2, F a1 b1 -> F a2 b2 -> SProp)
                       (SG : forall a1 a2 b1 b2, G a1 b1 -> G a2 b2 -> SProp)
                       (eqA : eqn A1 A2)
                       (eqB : eqn B1 B2)
                       (e : eqn F (fun a b => G (Rw eqA a) (Rw eqB b)))
                       (se : eqn SF (fun (a1 a2 : A1) (b1 b2 : B1) (f1 : F a1 b1) (f2 : F a2 b2)
                                     => SG (Rw eqA a1) (Rw eqA a2) (Rw eqB b1) (Rw eqB b2)
                                           (Rw (Bif_Sequal e a1 b1) f1)
                                           (Rw (Bif_Sequal e a2 b2) f2)))
                       (a : A1) (b : B1) (f : F a b) : 
  eqn (SF a a b b f f)
      (SG (Rw eqA a) (Rw eqA a) (Rw eqB b) (Rw eqB b)
          (Rw (Bif_Sequal e a b) f)
          (Rw (Bif_Sequal e a b) f)).
Proof.
  change (eqn (SF a a b b f f)
              ((fun (a1 a2 : A1) (b1 b2 : B1) (f1 : F a1 b1) (f2 : F a2 b2) =>
          SG (Rw eqA a1) (Rw eqA a2)
            (Rw eqB b1) (Rw eqB b2)
            (Rw
               (Bif_Sequal e a1 b1) f1)
            (Rw
               (Bif_Sequal e a2 b2) f2)) a a b b f f)).
  destruct se.
  reflexivity.
Defined.
  
Definition Threeqn (A B C D : Type)
           (P1 :  A -> C -> Type)
           (P2 :  B -> D -> Type)
           (SP1 : forall (a1 a2 : A) (c1 c2 : C) (p1 : P1 a1 c1) (p2 : P1 a2 c2),
                           SProp)
           (SP2 : forall (b1 b2 : B) (d1 d2 : D) (p1 : P2 b1 d1) (p2 : P2 b2 d2),
                           SProp) : SProp.
Proof.
  unshelve refine (Ssig2 _).
  - exact (eqn A B).
  - exact (eqn C D).
  - intros e1 e2.
    unshelve refine (Ssig _).
    + refine (eqn P1 _).
      intros a b.
      exact (P2 (Rw e1 a) (Rw e2 b)).
    + simpl.
      intros.
      refine (eqn SP1 _).
      intros.
      refine (SP2 _ _ _ _ _ _).
      * refine (Rw _ p1).
        exact (@Bif_Sequal _ _ _ _ Type P1 P2 _ _ H a1 c1).
      * refine (Rw _ p2).
        exact (@Bif_Sequal _ _ _ _ Type P1 P2 _ _ H a2 c2).
Defined.

Record Prod (A B : Type) (C : A -> B -> Type)
       (D : forall (a1 a2 : A) (b1 b2 : B) (c1 : C a1 b1) (c2 : C a2 b2), SProp) :=
  {Pi1 : A ; Pi2 : B ; Pi3 : C Pi1 Pi2 ; Pi4 : D Pi1 Pi1 Pi2 Pi2 Pi3 Pi3}.

Lemma BoolRec (PL PR : forall (b1 b2 : BranchingBool)
                              (be : Boole b1 b2)
                              (sbe : SBoole be be),
                  BranchingTYPE)
      (PP : forall (b1 b2 : BranchingBool)
                   (be : Boole b1 b2)
                   (sbe : SBoole be be),
          PL b1 b2 be sbe -> PR b1 b2 be sbe -> BranchingTYPE)
      (PPdig : forall (b1 b2 : BranchingBool)
                      (be : Boole b1 b2)
                      (sbe : SBoole be be)
                      (i : I)
                      (fl : O -> PL b1 b2 be sbe)
                      (fr : O -> PR b1 b2 be sbe)
                      (H : forall o, PP _ _ _ _ (fl o) (fr o)),
          PP _ _ _ _ ((PL _ _ _ _).(dig) i fl) ((PR _ _ _ _).(dig) i fr))
      (SPP : forall (b1 b2 : BranchingBool)
                    (be : Boole b1 b2)
                    (sbe : SBoole be be)
                    (pl1 pl2 : PL b1 b2 be sbe)
                    (pr1 pr2 : PR b1 b2 be sbe)
                    (pe1 : PP _ _ _ _ pl1 pr1)
                    (pe2 : PP _ _ _ _ pl2 pr2),
          SProp)
      (SPPdig : forall (b1 b2 : BranchingBool)
                      (be : Boole b1 b2)
                      (sbe : SBoole be be)
                      (i : I)
                      (fl1 fl2 : O -> PL b1 b2 be sbe)
                      (fr1 fr2 : O -> PR b1 b2 be sbe)
                      (H1 : forall o, PP _ _ _ _ (fl1 o) (fr1 o))
                      (H2 : forall o, PP _ _ _ _ (fl2 o) (fr2 o))
                      (SH : forall o, SPP _ _ _ _ (fl1 o) (fl2 o) (fr1 o) (fr2 o) (H1 o) (H2 o)),
          SPP _ _ _ _ ((PL _ _ _ _).(dig) i fl1)
              ((PL _ _ _ _).(dig) i fl2)
              ((PR _ _ _ _).(dig) i fr1)
              ((PR _ _ _ _).(dig) i fr2)
              (PPdig _ _ _ _ _ fl1 fr1 H1)
              (PPdig _ _ _ _ _ fl2 fr2 H2))
      (FL : forall (b1 b2 : BranchingBool)
                   (be : Boole b1 b2)
                   (sbe : SBoole be be),
          PL b1 b2 be sbe -> PR b1 b2 be sbe)
      (FR : forall (b1 b2 : BranchingBool)
                   (be : Boole b1 b2)
                   (sbe : SBoole be be),
          PR b1 b2 be sbe -> PL b1 b2 be sbe)
      (PepsSP : forall (b1 b2 c1 c2 : BranchingBool)
                       (be : Boole b1 b2)
                       (ce : Boole c1 c2)
                       (sbe : SBoole be be)
                       (sce : SBoole ce ce)
                       (se : SBoole be ce),
          @Threeqn (PL b1 b2 be sbe) (PL c1 c2 ce sce)
                   (PR b1 b2 be sbe) (PR c1 c2 ce sce)
                   (PP b1 b2 be sbe) (PP c1 c2 ce sce)
                   (SPP b1 b2 be sbe) (SPP c1 c2 ce sce))
      (ptrueL : PL Btrue Btrue Betrue SBetrue)
      (ptrueR : PR Btrue Btrue Betrue SBetrue)
      (ptrueP : PP Btrue Btrue Betrue SBetrue ptrueL ptrueR)
      (ptrueSP : SPP Btrue Btrue Betrue SBetrue ptrueL ptrueL ptrueR ptrueR ptrueP ptrueP)
      (pfalseL : PL Bfalse Bfalse Befalse SBefalse)
      (pfalseR : PR Bfalse Bfalse Befalse SBefalse)
      (pfalseP : PP Bfalse Bfalse Befalse SBefalse pfalseL pfalseR)
      (pfalseSP : SPP Bfalse Bfalse Befalse SBefalse pfalseL pfalseL pfalseR pfalseR pfalseP pfalseP)
      (b : BranchingBool) (be : Boole b b) (sbe : SBoole be be) :
  @Prod (PL b b be sbe) (PR b b be sbe) (PP b b be sbe) (SPP b b be sbe).
Proof.
  simpl.
  induction be.
  - unshelve econstructor ; assumption.
  - unshelve econstructor ; assumption.
  - unshelve econstructor.
    + unshelve refine (Rw _ (IHbe _).(Pi1)).
      * exact (SBoolRefl _).
      * refine (PepsSP _ _ _ _ _ _ _ _ _).(Sone3).
        refine (SBêtae11 _ _).
        exact (SBoolRefl _).
    + unshelve refine (Rw _ (IHbe _).(Pi2)).
      * exact (SBoolRefl _).
      * refine (PepsSP _ _ _ _ _ _ _ _ _).(Stwo3).
        refine (SBêtae11 _ _).
        exact (SBoolRefl _).
    + unshelve refine (Rw _ (IHbe _).(Pi3)).
      * exact (SBoolRefl _).
      * unshelve refine (@Bif_Sequal _ _ _ _ Type
                            (PP n (f o) be (SBoolRefl be))
                            (PP n (Bêta i f) (Bêtae1 i be) sbe) _ _ _
                            (Pi1 (IHbe (SBoolRefl be)))
                            (Pi2 (IHbe (SBoolRefl be)))).
        refine (PepsSP _ _ _ _ _ _ _ _ _).(Sthree3).(Sone).
    + unshelve refine (SRw _ (IHbe _).(Pi4)).
      * exact (SBoolRefl _).
      * refine (TriSf_Sequal _ _).
        exact (PepsSP _ _ _ _ _ _ _ _ _).(Sthree3).(Stwo).
  - unshelve econstructor.
    + unshelve refine (Rw _ (IHbe _).(Pi1)).
      * exact (SBoolRefl _).
      * refine (PepsSP _ _ _ _ _ _ _ _ _).(Sone3).
        refine (SBêtae21 _ _).
        exact (SBoolRefl _).
    + unshelve refine (Rw _ (IHbe _).(Pi2)).
      * exact (SBoolRefl _).
      * refine (PepsSP _ _ _ _ _ _ _ _ _).(Stwo3).
        refine (SBêtae21 _ _).
        exact (SBoolRefl _).
    + unshelve refine (Rw _ (IHbe _).(Pi3)).
      * exact (SBoolRefl _).
      * unshelve refine (@Bif_Sequal _ _ _ _ Type
                            (PP (f o) n be (SBoolRefl be))
                            (PP (Bêta i f) n (Bêtae2 i be) sbe) _ _ _
                            (Pi1 (IHbe (SBoolRefl be)))
                            (Pi2 (IHbe (SBoolRefl be)))).
        refine (PepsSP _ _ _ _ _ _ _ _ _).(Sthree3).(Sone).
    + unshelve refine (SRw _ (IHbe _).(Pi4)).
      * exact (SBoolRefl _).
      * refine (TriSf_Sequal _ _).
        exact (PepsSP _ _ _ _ _ _ _ _ _).(Sthree3).(Stwo).
  - unshelve econstructor.
    + unshelve refine ((PL _ _ _ _).(dig) i _).
      intro o.
      unshelve refine (Rw _ (X o _).(Pi1)).
      * exact (SBoolRefl _).
      * refine (PepsSP _ _ _ _ _ _ _ _ _).(Sone3).
        refine (SDigamma1 _ _ _).
        exact (fun _ => SBoolRefl _).
    + unshelve refine ((PR _ _ _ _).(dig) i _).
      intro o.
      unshelve refine (Rw _ (X o _).(Pi2)).
      * exact (SBoolRefl _).
      * refine (PepsSP _ _ _ _ _ _ _ _ _).(Stwo3).
        refine (SDigamma1 _ _ _).
        exact (fun _ => SBoolRefl _).
    + refine (PPdig _ _ _ _ _ _ _ _).
      intro o.
      generalize (X o (SBoolRefl _)) ; intro HH.
      destruct HH as [HHL HHR HHP].
      simpl in *.
      refine (Rw _ HHP).
      unshelve refine (@Bif_Sequal _ _ _ _ Type
                            (PP (f o) (g o) (H o) (SBoolRefl (H o)))
                            (PP (Bêta i f) (Bêta i g) (Digamma i H) sbe) _ _ _
                            _ _).
      simpl.
      refine (PepsSP _ _ _ _ _ _ _ _ _).(Sthree3).(Sone).
    + simpl.
      refine (SPPdig _ _ _ _ _ _ _ _ _ _ _ _).
      intro o.
      generalize (X o (SBoolRefl _)) ; intro HH.
      destruct HH as [HHL HHR HHP HHSP].
      simpl in *.
      unshelve refine (SRw _ HHSP).
      refine (TriSf_Sequal _ _).
      exact (PepsSP _ _ _ _ _ _ _ _ _).(Sthree3).(Stwo).
Defined.    

      


Inductive BranchingNat :=
| B0 : BranchingNat
| BS : BranchingNat -> BranchingNat
| Bêtanat : I -> (O -> BranchingNat) -> BranchingNat.


Inductive BNate : BranchingNat -> BranchingNat -> Type :=
| BNe0 : BNate B0 B0
| BNS : forall (n m : BranchingNat) (nme : BNate n m),
    BNate (BS n) (BS m)
| BêtaNe1 : forall (n : BranchingNat) (i : I) (f : O -> BranchingNat)
                  (o : O)
                  (H : BNate n (f o)),
    BNate n (Bêtanat i f)
| BêtaNe2 : forall (n : BranchingNat) (i : I) (f : O -> BranchingNat)
                  (o : O)
                  (H : BNate (f o) n),
    BNate (Bêtanat i f) n
| BNDigamma : forall (i : I) (f g : O -> BranchingNat)
                   (H : forall o, BNate (f o) (g o)),
    BNate (Bêtanat i f) (Bêtanat i g).

Inductive SBNe : forall (a b c d : BranchingNat)
                          (eab : BNate a b)
                          (ecd : BNate c d), SProp :=
| SBNe0 : SBNe BNe0 BNe0
| SBNS : forall (a b c d : BranchingNat)
                (eab : BNate a b)
                (ecd : BNate c d)
                (es : SBNe eab ecd),
    SBNe (BNS eab) (BNS ecd)
| SBêtaNe1 : forall (n : BranchingNat) (i : I) (f : O -> BranchingNat)
                   (o : O)
                   (H : BNate n (f o))
                   (SH : SBNe H H),
    SBNe (BêtaNe1 i H) (BêtaNe1 i H)
| SBêtaNe11 : forall (n : BranchingNat) (i : I) (f : O -> BranchingNat)
                   (o : O)
                   (H : BNate n (f o))
                   (SH : SBNe H H),
    SBNe H (BêtaNe1 i H)
| SBêtaNe12 : forall (n : BranchingNat) (i : I) (f : O -> BranchingNat)
                   (o : O)
                   (H : BNate n (f o))
                   (SH : SBNe H H),
    SBNe (BêtaNe1 i H) H
| SBêtaNe2 : forall (n : BranchingNat) (i : I) (f : O -> BranchingNat)
                   (o : O)
                   (H : BNate (f o) n)
                   (SH : SBNe H H),
    SBNe (BêtaNe2 i H) (BêtaNe2 i H)
| SBêtaNe21 : forall (n : BranchingNat) (i : I) (f : O -> BranchingNat)
                   (o : O)
                   (H : BNate (f o) n)
                   (SH : SBNe H H),
    SBNe H (BêtaNe2 i H)
| SBêtaNe22 : forall (n : BranchingNat) (i : I) (f : O -> BranchingNat)
                   (o : O)
                   (H : BNate (f o) n)
                   (SH : SBNe H H),
    SBNe (BêtaNe2 i H) H
| SBNDigamma : forall (i : I) (f g : O -> BranchingNat)
                    (H : forall o, BNate (f o) (g o))
                    (SH : forall o, SBNe (H o) (H o)),
    SBNe (BNDigamma i H) (BNDigamma i H)
| SBNDigamma1 : forall (i : I) (f g : O -> BranchingNat)
                     (o : O)
                     (H : forall o, BNate (f o) (g o))
                     (SH : forall o, SBNe (H o) (H o)),
    SBNe (H o) (BNDigamma i H)
| SBNDigamma2 : forall (i : I) (f g : O -> BranchingNat)
                     (o : O)
                     (H : forall o, BNate (f o) (g o))
                     (SH : forall o, SBNe (H o) (H o)),
    SBNe (BNDigamma i H) (H o).



Lemma NatRefl : forall (b : BranchingNat),
    BNate b b.
Proof.
  refine (fix f b := match b with
                     | B0 => BNe0
                     | BS k => BNS (f k)
                     | Bêtanat i k => _
                     end).
  refine (BNDigamma _ _).
  exact (fun o => f (k o)).
Defined.

Lemma SNatRefl : forall (b1 b2 : BranchingNat)
                        (be : BNate b1 b2),
    SBNe be be.
Proof.
  refine (fix f b1 b2 be {struct be} :=
            match be with
            | BNe0 => SBNe0
            | BNS ke => SBNS (f _ _ ke)
            | BêtaNe1 i H => _
            | BêtaNe2 i H => _
            | BNDigamma i H => _
            end).
  - refine (SBêtaNe1 _ _).
    exact (f _ _ H).
  - refine (SBêtaNe2 _ _).
    exact (f _ _ H).
  - refine (SBNDigamma _ _).
    exact (fun o => f _ _ (H o)).
Defined.  

Lemma NatRec (PL PR : forall (b1 b2 : BranchingNat)
                              (be : BNate b1 b2)
                              (sbe : SBNe be be),
                 BranchingTYPE)
      (PP : forall (b1 b2 : BranchingNat)
                   (be : BNate b1 b2)
                   (sbe : SBNe be be),
          PL b1 b2 be sbe -> PR b1 b2 be sbe -> BranchingTYPE)
      (PPdig : forall (b1 b2 : BranchingNat)
                      (be : BNate b1 b2)
                      (sbe : SBNe be be)
                      (i : I)
                      (fl : O -> PL b1 b2 be sbe)
                      (fr : O -> PR b1 b2 be sbe)
                      (H : forall o, PP _ _ _ _ (fl o) (fr o)),
          PP _ _ _ _ ((PL _ _ _ _).(dig) i fl) ((PR _ _ _ _).(dig) i fr))
      (SPP : forall (b1 b2 : BranchingNat)
                    (be : BNate b1 b2)
                    (sbe : SBNe be be)
                    (pl1 pl2 : PL b1 b2 be sbe)
                    (pr1 pr2 : PR b1 b2 be sbe)
                    (pe1 : PP _ _ _ _ pl1 pr1)
                    (pe2 : PP _ _ _ _ pl2 pr2),
          SProp)
      (SPPdig : forall (b1 b2 : BranchingNat)
                       (be : BNate b1 b2)
                       (sbe : SBNe be be)
                      (i : I)
                      (fl1 fl2 : O -> PL b1 b2 be sbe)
                      (fr1 fr2 : O -> PR b1 b2 be sbe)
                      (H1 : forall o, PP _ _ _ _ (fl1 o) (fr1 o))
                      (H2 : forall o, PP _ _ _ _ (fl2 o) (fr2 o))
                      (SH : forall o, SPP _ _ _ _ (fl1 o) (fl2 o) (fr1 o) (fr2 o) (H1 o) (H2 o)),
          SPP _ _ _ _ ((PL _ _ _ _).(dig) i fl1)
              ((PL _ _ _ _).(dig) i fl2)
              ((PR _ _ _ _).(dig) i fr1)
              ((PR _ _ _ _).(dig) i fr2)
              (PPdig _ _ _ _ _ fl1 fr1 H1)
              (PPdig _ _ _ _ _ fl2 fr2 H2))
      (FL : forall (b1 b2 : BranchingNat)
                   (be : BNate b1 b2)
                   (sbe : SBNe be be),
          PL b1 b2 be sbe -> PR b1 b2 be sbe)
      (FR : forall (b1 b2 : BranchingNat)
                   (be : BNate b1 b2)
                   (sbe : SBNe be be),
          PR b1 b2 be sbe -> PL b1 b2 be sbe)
      (PepsSP : forall (b1 b2 c1 c2 : BranchingNat)
                       (be : BNate b1 b2)
                       (ce : BNate c1 c2)
                       (sbe : SBNe be be)
                       (sce : SBNe ce ce)
                       (se : SBNe be ce),
          @Threeqn (PL b1 b2 be sbe) (PL c1 c2 ce sce)
                   (PR b1 b2 be sbe) (PR c1 c2 ce sce)
                   (PP b1 b2 be sbe) (PP c1 c2 ce sce)
                   (SPP b1 b2 be sbe) (SPP c1 c2 ce sce))
      (p0L : PL B0 B0 BNe0 SBNe0)
      (p0R : PR B0 B0 BNe0 SBNe0)
      (p0P : PP B0 B0 BNe0 SBNe0 p0L p0R)
      (ptrueSP : SPP B0 B0 BNe0 SBNe0 p0L p0L p0R p0R p0P p0P)
      (pSL : forall (n m : BranchingNat)
                    (ne : BNate n m)
                    (sne : SBNe ne ne),
          PL n m ne sne -> PL (BS n) (BS m) (BNS ne) (SBNS sne))
      (pSR : forall (n m : BranchingNat)
                    (ne : BNate n m)
                    (sne : SBNe ne ne),
          PR n m ne sne -> PR (BS n) (BS m) (BNS ne) (SBNS sne))
      (pSP : forall (n m : BranchingNat)
                    (ne : BNate n m)
                    (sne : SBNe ne ne)
                    (pL : PL n m ne sne)
                    (pR : PR n m ne sne),
          PP n m ne sne pL pR ->
          PP (BS n) (BS m) (BNS ne) (SBNS sne) (pSL _ _ _ _ pL) (pSR _ _ _ _ pR))
      (pSSP : forall (n1 m1 : BranchingNat)
                     (ne1 : BNate n1 m1)
                     (sne1 : SBNe ne1 ne1)
                     (pL1 pL2 : PL n1 m1 ne1 sne1)
                     (pR1 pR2 : PR n1 m1 ne1 sne1)
                     (pP1 : PP n1 m1 ne1 sne1 pL1 pR1)
                     (pP2 : PP n1 m1 ne1 sne1 pL2 pR2)
                     (pSP1 : SPP n1 m1 ne1 sne1 pL1 pL1 pR1 pR1 pP1 pP1)
                     (pSP2 : SPP n1 m1 ne1 sne1 pL2 pL2 pR2 pR2 pP2 pP2)
                     (pSP_ : SPP n1 m1 ne1 sne1 pL1 pL2 pR1 pR2 pP1 pP2),
          SPP (BS n1) (BS m1) (BNS ne1) (SBNS sne1)
              (pSL _ _ _ _ pL1) (pSL _ _ _ _ pL2)
              (pSR _ _ _ _ pR1) (pSR _ _ _ _ pR2)
              (pSP _ _ _ _ _ _ pP1) (pSP _ _ _ _ _ _ pP2))
      (b : BranchingNat) (be : BNate b b) (sbe : SBNe be be) :
  @Prod (PL b b be sbe) (PR b b be sbe) (PP b b be sbe) (SPP b b be sbe).
Proof.
  simpl.
  induction be.
  - unshelve econstructor ; assumption.
  - unshelve econstructor.
    + exact (pSL _ _ _ _ (IHbe (SNatRefl _)).(Pi1)).
    + exact (pSR _ _ _ _ (IHbe (SNatRefl _)).(Pi2)).
    + exact (pSP _ _ _ _ _ _ (IHbe (SNatRefl _)).(Pi3)).
    + exact (pSSP _ _ _ _ _ _ _ _ _ _ (Pi4 (IHbe (SNatRefl _)))
                  (Pi4 (IHbe (SNatRefl _)))
                  (IHbe (SNatRefl _)).(Pi4)).
  - unshelve econstructor.
    + unshelve refine (Rw _ (IHbe _).(Pi1)).
      * exact (SNatRefl _).
      * refine (PepsSP _ _ _ _ _ _ _ _ _).(Sone3).
        refine (SBêtaNe11 i _).
        exact (SNatRefl _).
    + unshelve refine (Rw _ (IHbe (SNatRefl _)).(Pi2)).
      refine (PepsSP _ _ _ _ _ _ _ _ _).(Stwo3).
      refine (SBêtaNe11 _ _).
      exact (SNatRefl _).
    + unshelve refine (Rw _ (IHbe (SNatRefl _)).(Pi3)).
      unshelve refine (@Bif_Sequal _ _ _ _ Type
                                   (PP n (f o) be (SNatRefl be))
                                   (PP n (Bêtanat i f) (BêtaNe1 i be) sbe) _ _ _
                                   (Pi1 (IHbe ((SNatRefl _))))
                                   (Pi2 (IHbe (SNatRefl _)))).
      refine (PepsSP _ _ _ _ _ _ _ _ _).(Sthree3).(Sone).
    + unshelve refine (SRw _ (IHbe (SNatRefl _)).(Pi4)).
      refine (TriSf_Sequal _ _).
      exact (PepsSP _ _ _ _ _ _ _ _ _).(Sthree3).(Stwo).
  - unshelve econstructor.
    + unshelve refine (Rw _ (IHbe (SNatRefl _)).(Pi1)).
      * refine (PepsSP _ _ _ _ _ _ _ _ _).(Sone3).
        refine (SBêtaNe21 i _).
        exact (SNatRefl _).
    + unshelve refine (Rw _ (IHbe (SNatRefl _)).(Pi2)).
      refine (PepsSP _ _ _ _ _ _ _ _ _).(Stwo3).
      refine (SBêtaNe21 _ _).
      exact (SNatRefl _).
    + unshelve refine (Rw _ (IHbe (SNatRefl _)).(Pi3)).
      unshelve refine (@Bif_Sequal _ _ _ _ Type
                                   (PP (f o) n be (SNatRefl be))
                                   (PP (Bêtanat i f) n (BêtaNe2 i be) sbe) _ _ _
                                   (Pi1 (IHbe ((SNatRefl _))))
                                   (Pi2 (IHbe (SNatRefl _)))).
      refine (PepsSP _ _ _ _ _ _ _ _ _).(Sthree3).(Sone).
    + unshelve refine (SRw _ (IHbe (SNatRefl _)).(Pi4)).
      refine (TriSf_Sequal _ _).
      exact (PepsSP _ _ _ _ _ _ _ _ _).(Sthree3).(Stwo).
  - unshelve econstructor.
    + unshelve refine ((PL _ _ _ _).(dig) i _).
      intro o.
      unshelve refine (Rw _ (X o (SNatRefl _)).(Pi1)).
      refine (PepsSP _ _ _ _ _ _ _ _ _).(Sone3).
      refine (SBNDigamma1 _ _ _).
      exact (fun _ => SNatRefl _).
    + unshelve refine ((PR _ _ _ _).(dig) i _).
      intro o.
      unshelve refine (Rw _ (X o (SNatRefl _)).(Pi2)).
      refine (PepsSP _ _ _ _ _ _ _ _ _).(Stwo3).
      refine (SBNDigamma1 _ _ _).
      exact (fun _ => SNatRefl _).
    + refine (PPdig _ _ _ _ _ _ _ _).
      intro o.
      generalize (X o (SNatRefl _)) ; intro HH.
      destruct HH as [HHL HHR HHP].
      simpl in *.
      refine (Rw _ HHP).
      unshelve refine (@Bif_Sequal _ _ _ _ Type
                            (PP (f o) (g o) (H o) (SNatRefl _))
                            (PP (Bêtanat i f) (Bêtanat i g) (BNDigamma i H) sbe) _ _ _
                            _ _).
      refine (PepsSP _ _ _ _ _ _ _ _ _).(Sthree3).(Sone).
    + simpl.
      refine (SPPdig _ _ _ _ _ _ _ _ _ _ _ _).
      intro o.
      generalize (X o (SNatRefl _)) ; intro HH.
      destruct HH as [HHL HHR HHP HHSP].
      simpl in *.
      unshelve refine (SRw _ HHSP).
      refine (TriSf_Sequal _ _).
      exact (PepsSP _ _ _ _ _ _ _ _ _).(Sthree3).(Stwo).
Defined.


Fixpoint BNEta (n : nat) : BranchingNat :=
  match n with
  | 0 => B0
  | S k => BS (BNEta k)
  end.

Fixpoint Generic_aux (n : nat) (b : BranchingNat) {struct b} :
  BranchingNat :=
  match b with
  | B0 => Bêtanat n BNEta
  | BS k => Generic_aux (S n) k
  | Bêtanat i f => Bêtanat i (fun a : nat => Generic_aux n (f a))
  end.

Definition Generic : BranchingNat -> BranchingNat := Generic_aux 0.


Definition Generice_aux :
  forall (n1 n2 : BranchingNat) (ne : BNate n1 n2) (k : nat),
    BNate (Generic_aux k n1) (Generic_aux k n2).
Proof.
  intros n1 n2 ne.
  induction ne.
  - intro k ; apply NatRefl.
  - simpl.
    exact (fun k => IHne (S k)).
  - simpl.
    intro k.
    unshelve refine (BêtaNe1 _ _).
    exact o.
    exact (IHne k).
  - simpl.
    intro k.
    unshelve refine (BêtaNe2 _ _).
    exact o.
    exact (IHne k).
  - simpl.
    intro k.
    unshelve refine (BNDigamma _ _).
    exact (fun o => H0 o k).
Defined.

Definition Generice :
  forall (n1 n2 : BranchingNat) (ne : BNate  n1 n2),
    BNate (Generic n1) (Generic n2).
Proof.
  intros.
  unfold Generic.
  exact (@Generice_aux n1 n2 ne 0).
Defined.

Definition SGenerice_aux :
  forall (n1 n2 m1 m2 : BranchingNat) (ne1 : BNate n1 m1) (ne2 : BNate n2 m2)
         (sne1 : SBNe ne1 ne1) (sne2 : SBNe ne2 ne2)
         (sne : SBNe ne1 ne2) (k : nat),
    SBNe (Generice_aux ne1 k) (Generice_aux ne2 k).
Proof.
  intros ; revert k.
  induction sne.
  - simpl.
    intro k.
    exact (SNatRefl _).
  - simpl.
    intro k.
    exact (IHsne (SNatRefl _) (SNatRefl _) (S k)).
  - simpl.
    intro k.
    refine (SBêtaNe1 _ _).
    exact (IHsne (SNatRefl _) (SNatRefl _) k).
  - simpl ; intros.
    refine (SBêtaNe11 _ _).
    exact (IHsne (SNatRefl _) (SNatRefl _) k).
  - simpl ; intros.
    refine (SBêtaNe12 _ _).
    exact (IHsne (SNatRefl _) (SNatRefl _) k).
  - simpl.
    intro k.
    refine (SBêtaNe2 _ _).
    exact (IHsne (SNatRefl _) (SNatRefl _) k).
  - simpl ; intros.
    refine (SBêtaNe21 _ _).
    exact (IHsne (SNatRefl _) (SNatRefl _) k).
  - simpl ; intros.
    refine (SBêtaNe22 _ _).
    exact (IHsne (SNatRefl _) (SNatRefl _) k).
  - simpl.
    intro k.
    refine (SBNDigamma _ _).
    exact (fun o => H0 o (SNatRefl _) (SNatRefl _) k).
  - simpl.
    intro k.
    refine (SBNDigamma1 _ _ _).
    exact (fun o => H0 o (SNatRefl _) (SNatRefl _) k).
  - simpl.
    intro k.
    refine (SBNDigamma2 _ _ _).
    exact (fun o => H0 o (SNatRefl _) (SNatRefl _) k).
Defined.


Definition SGenerice :
  forall (n1 n2 m1 m2 : BranchingNat) (ne1 : BNate n1 m1) (ne2 : BNate n2 m2)
         (sne1 : SBNe ne1 ne1) (sne2 : SBNe ne2 ne2)
         (sne : SBNe ne1 ne2) (k : nat),
    SBNe (Generice ne1) (Generice ne2).
Proof.
  intros.
  exact (SGenerice_aux sne1 sne2 sne 0).
Defined.
