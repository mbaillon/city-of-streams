\documentclass{easychair}

\usepackage[T1]{fontenc}
\usepackage{inputenc}[utf8x]
\usepackage{bussproofs}
\usepackage{amssymb}

\bibliographystyle{plainurl}

\newcommand{\mltt}{\mathsf{MLTT}}
\newcommand{\Dmltt}{\mathsf{MLTT}^{\digamma}}
\newcommand{\oracle}{\alpha}
\newcommand{\local}[2]{{\alpha({#1}) \mapsto #2}}

\title{In Cantor Space, No One Can Hear You Stream}

\author{Martin Baillon \inst{1}
\and
Assia Mahboubi \inst{1}
\and
Pierre-Marie Pédrot \inst{1}}

\institute{INRIA, France}

\authorrunning{M. Baillon, A. Mahboubi, P.-M. Pédrot}
\titlerunning{In Cantor Space, No One Can Hear You Stream}

\begin{document}

\maketitle

It is well-known that all computable functions are continuous. This result has been enshrined in various kinds of Brouwerian intuitionistic mathematics through flavours of bar recursion~\cite{Troelstra88} with varying degrees of compatibility with classical mathematics. In type theory, continuity is usually phrased as a property of functions $\varphi : (\mathbb{N} \rightarrow A) \rightarrow B$ for suitable types $A$ and $B$, and boils down to the fact that such a function can only depend on a finite prefix of any argument $\alpha : \mathbb{N} \rightarrow A$. In this article, we will care about the case $A := \mathbb{B}$, for which $\mathbb{N} \rightarrow \mathbb{B}$ is known as the \emph{Cantor space}, and $B$ is some hereditarily positive type, typically $B := \mathbb{N}$.


Streams of booleans are incomprehensible beings. Infinite processions crawling around the stars like never-ending snakes, they stay in the shadows, lurking in the outskirts of computation. For cardinality reasons, most of them cannot even be named. Ungraspable, continuity of all functionals over the Cantor space means that they will forever stay alien to our human understanding. No function can actually witness their horror unheeded. In Cantor space, no one can hear you stream.


For cardinality reasons, most streams of booleans cannot even be named. Continuity of all functionals over the Cantor space means that, as alien to our human understanding this space can be, no function can actually witness this horror unheeded. Thus, in Cantor space, no one can hear you stream.

As anecdotal as it may seem, continuity and its alter-egos have extremely strong consequences, at least when given internally. For instance, bar recursion is enough to realize the principle known as double-negation shift, and thus to reach the logical strength of second-order arithmetic in a system that is otherwise simply-typed~\cite{Blot22}. This is one of the reasons of the importance of such principles in the eyes of the Brouwerian crowd, who used them to develop constructive analysis. Continuity is furthermore tightly related to choice principles~\cite{Brede21}.

There has been a revival around this topic in recent years. In addition to the already mentioned papers, Escardó gave a proof that System $\mathsf{T}$ enjoys continuity in an external way~\cite{Escardo13} using a kind of side-effect reminiscent of interaction-trees~\cite{XiaZHHMPZ20}. Baillon et al. gave a generalization of this proof to Baclofen Type Theory, a variant of $\mltt$ with a restricted form of large elimination \cite{BaillonMP22}. Escardó and Xu also studied more semantical approaches to the same kinds of questions~\cite{XuE13,EscardoX16}. From the community of PER semantics, there was an important series of papers by several authors including Rahli in the intersection of all of them~\cite{RahliBC17,RahliB18,RahliBCC19,CohenRahli23,Cohen23,Cohen24} considering various forms of continuity in realizability models of $\mltt$.

The work we describe here can be seen as a variant on the models considered in the latter trend. It lies in the continuity of the first author's PhD~\cite{Baillon23} and can be seen as a mechanized refinement of a previous work by Coquand and Jaber~\cite{Coquand10}. We prove external continuity of functionals over the Cantor space in $\mltt$, i.e. terms of type $\vdash M : (\mathbb{N} \rightarrow \mathbb{B}) \rightarrow \mathbb{N}$ by embedding them into a larger type theory $\Dmltt$ which is basically $\mltt$ where terms are extended with a single abstract oracle $\oracle : \mathbb{N}\rightarrow\mathbb{B}$ and contexts with local partial knowledge about the oracle
\[\Gamma := \ldots \mid \Gamma, \local{n}{b} \qquad \text{$n\in \mathbf{N}, b \in\mathbf{B}$}\]
where $\mathbf{N} := \{0, 1, \ldots\}$ and $\mathbf{B} := \{\mathtt{tt}, \mathtt{ff}\}$ stand respectively for integer and boolean literals. Such a hypothesis intuitively means that the value of $\oracle$ at $n$ is known to be $b$. This partial knowledge can be reflected equationally through the conversion rule below.

\[
\AxiomC{$\local{n}{b} \in \Gamma$}
\UnaryInfC{\strut$\Gamma \vdash \alpha\ \overline{n} \equiv \overline{b} : \mathbb{B}$}
\DisplayProof
\]

Moreover, all kinds of judgements of $\Dmltt$ are extended with a built-in \emph{splitting} principle that allows accumulating knowledge about the oracle. For instance, for term typing we have
\[
\AxiomC{\strut$\Gamma, \local{n}{\mathtt{tt}} \vdash M : A$}
\AxiomC{\strut$\Gamma, \local{n}{\mathtt{ff}} \vdash M : A$}
\AxiomC{\strut$\local{n}{-}\not\in\Gamma$}
\TrinaryInfC{\strut$\Gamma \vdash M : A$}
\DisplayProof
\]

It is somewhat immediate to the astute reader that the intended model for this theory is some kind of sheaf model~\cite{maclane1992}, i.e. a proof-relevant version of Beth semantics. Basing ourselves upon the \texttt{logrel-coq} development~\cite{Adjedj24}, we prove in Coq that $\Dmltt$ enjoys a form of canonicity and strong normalization using a logical relation model similar to the now famous NbE model of Abel et al.~\cite{Abel17}. While our syntax is essentially the same as the one from the Coquand-Jaber note~\cite{Coquand10}, we prove a somewhat more general result. Our model is indeed a kind of realizability, i.e. based on reduction instead of evaluation in the metatheory. As a result, rather than just equational canonicity, we also prove that well-typed terms actually \emph{reduce} to a normal form. As is usual in sheaf models, canonicity is weakened to some kind of decision tree, e.g. for $\mathbb{N}$ it implies that for every closed $\vdash M : \mathbb{N}$ there is a finite splitting tree such that $M$ reduces to an integer on each branch. This is a generalization of the trivial case of canonicity under consistent, purely negative axioms~\cite{Coquand13}.

Furthermore, our model features a first-class handling of variables, so it scales to open terms. This is achieved by the standard trick ensuring that all relations are presheaves over contexts and that \emph{neutrals} are always realizers. While this technique is a well-known , the resulting property offers a stark contrast with the models of Rahli et al., such as $\text{TT}^{\Box}_{\mathcal C}$~\cite{Cohen24}, where only \emph{closed} terms are manipulated in the semantics. The latter hardwires some logical consequences in the internal language, such as equality reflection and, in particular, function extensionality. Unfortunately, apart from not being provable in $\mltt$ and making type-checking undecidable, extensionality is often at odds with strong continuity principles. This forces some of the strongest continuity principles to be made innocuous by squashing them, thus rendering them computationally irrelevant.

Just like a proper handling of variables makes the notion of neutrals paramount, our work highlights the importance of their sheaf equivalent, that we call \emph{$\oracle$-neutrals}. They are basically the equivalent of neutrals, where the head is not a variable but $\oracle \ n$ with $\local{n}{-}\not\in\Gamma$. Like mere neutrals, they block computation but contrarily to them, an $\oracle$-neutral can be unlocked by a weakening adding $\local{n}{b}$. Indeed, extending contexts can increase computational knowledge about the oracle. The resulting logical relation also smells very strongly of call-by-push-value ($\mathsf{CBPV}$)~\cite{Levy04} and more generally of semantics of effectful languages. As a matter of fact, the relation is split between a strong and a weak variant. The former corresponds to pure head values that are not allowed to split, when the latter corresponds to effectful terms that require splitting to be well-typed.

We will give insights about this model and describe some of the ongoing work about the formalization and the expected results to come.

\newpage
\bibliography{biblio}

\end{document}
