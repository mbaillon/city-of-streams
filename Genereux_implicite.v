Set Primitive Projections.
Set Universe Polymorphism.
Require Import Dialogue.
Require Import Base.
Require Import ssreflect.



Section AvareXY.

Unset Implicit Arguments.
Variables X Y : Type.
Definition ℙ := X -> Y.

Definition ACtx := Type.

Definition BCtx := Type.

Definition Ctx :=
  ℙ -> Type.
(*
Definition Ctx_El (Γ : Ctx) (α : ℙ) :=
    triple (fun (a : (Γ α).(q1)) (b : (Γ α).(q2)) =>
              (Γ α).(q3) a b).

Notation "Γ ‡ α" := (Ctx_El Γ α) (at level 11).
*)
Definition ATy (Γ : Ctx) : Type :=
  forall α : X -> Y, Γ α -> Type.

Definition BTy (Γ : Ctx) : Type :=
  forall α : X -> Y, Γ α -> BranchingTYPE X Y.

Definition Ty (Γ : Ctx) : Type :=
  forall α : X -> Y, Γ α -> triple (fun (A : Type) (B : BranchingTYPE X Y) =>
                                      tsig (fun (Te : A -> BEl B -> Type) =>
                                              forall (a : A) (f : Y -> BEl B) (x : X)
                                                     (fe : Te a (f (α x))),
                                                Te a ((B).(snd) f x))).
(*    triple (fun (A :  ATy Γ) (B : BTy Γ) =>
              forall (α : ℙ) (γ : Γ α),
                tsig (fun (Te : A α γ -> BEl (B α γ) -> Type) =>
                        forall (a : A α γ) (f : Y -> BEl (B α γ)) (x : X) (fe : Te a (f (α x))), Te a (((B α γ)).(snd) f x))).

Definition AEl (Γ : Ctx) (A : ATy Γ) :
  Type :=
  forall (α : X -> Y) (gamma : Γ α), A α gamma.

Definition BEl' (Γ : Ctx) (B : BTy Γ) :=
  forall(α : X -> Y)  (gamma : Γ α), BEl (B α gamma). 
*)
Definition RElt (Γ : Ctx) (R : Ty Γ) : Type :=
  forall (α : X -> Y) (gamma : Γ α),
    triple (fun (a : (R α gamma).(q1)) (b : BEl (R α gamma).(q2)) =>
                (R α gamma).(q3).(fst) a b).


Definition Ty_inst {Γ : Ctx} {α : ℙ} (R : Ty Γ) (γ : Γ α) : Type
  :=
    triple (fun (a : (R α γ).(q1)) (b : BEl (R α γ).(q2)) =>
            ((R α γ).(q3)).(fst) a b).


Definition Inst {Γ : Ctx} {R : Ty Γ} {α : ℙ} (r : RElt _ R)
            (γ : Γ α) : Ty_inst R γ.
Proof.
  exists (r α γ).(q1) (r α γ).(q2).
  exact (r α γ).(q3).
Defined.  


Notation "r ® γ" := (Inst r γ) (at level 10, left associativity).

Definition ACtx_nil : ACtx := unit.
Definition BCtx_nil : BCtx := unit.

Definition Ctx_nil : Ctx :=  (fun α => unit).


Definition ctx_nil {α : ℙ} : Ctx_nil α := tt.

Notation "()" := ctx_nil.
(*
Definition ACtx_cons (Gamma : Ctx) (A : ATy Gamma) (α : ℙ) :
  ACtx := tsig (fun (gamma : Gamma ‡ α) => A α gamma).

Definition BCtx_cons (Gamma : Ctx) (A : BTy Gamma) (α : ℙ) :=
  tsig (fun (gamma : Gamma ‡ α) => BEl (A α gamma)).
*)

Definition Ctx_cons (Γ : Ctx) (R : Ty Γ) : Ctx.
Proof.
  unshelve refine (fun α => @tsig (Γ α) _).
  unshelve refine (fun gamma => triple _).
  - exact (R α gamma).(q1).
  - exact (BEl (R α gamma).(q2)).
  - exact ((R α gamma).(q3).(fst)).
Defined.    
(*
  - exact (ACtx_cons Γ R.(q1) α).
  - exact (BCtx_cons Γ R.(q2) α).
  - intros Acons Bcons.
    cbv in Acons.
    Print tsig.
    cbv in Γ.
    cbv in Bcons.
    apply (@tsig ((Γ α).(q3) Acons.(fst).(q1) Bcons.(fst).(q2))).
    intro γe.
    exact ((R.(q3) α (exist3 γe)).(fst) Acons.(snd) Bcons.(snd)).
Defined.
*)

Notation "Γ · A" := (Ctx_cons Γ A) (at level 50, left associativity).



Definition ctx_cons {Γ : Ctx} {R : Ty Γ} {α : ℙ}
           (γ : Γ α) (x : Ty_inst R γ) :
  (Γ · R) α.
Proof.
  destruct x as [xa xb xe].
  exists γ.
  refine (exist3 xe).
Defined.

Notation "γ ·· x" := (ctx_cons γ x) (at level 50, left associativity).


Definition Ctx_tl {Γ : Ctx} {R : Ty Γ} {α : ℙ}
           (γ : (Γ · R) α) : Γ α.
Proof.
  exact (γ.(fst)).
Defined.


Notation "◊ g" := (Ctx_tl g) (at level 10).


Definition Ctx_hd {Γ : Ctx} {R : Ty Γ} {α : ℙ}
           (γ : (Γ · R) α) : Ty_inst R (◊ γ).
Proof.
  exists γ.(snd).(q1) γ.(snd).(q2).
  exact γ.(snd).(q3).
Defined.

Notation "¿ g" := (Ctx_hd g) (at level 10).


Definition Lift {Γ : Ctx} {R1 : Ty Γ}  (R2 : Ty Γ) : Ty (Γ · R1).
Proof.
  unshelve refine (fun α γ => exist3 _).
  + refine ((R2 α γ.(fst)).(q1)).
  + refine ((R2 α γ.(fst)).(q2)).
  + exact (R2 α (◊ γ)).(q3).    
Defined.

Notation "⇑ A" := (Lift A) (at level 10).


Definition Var {Γ : Ctx} (R : Ty Γ) : RElt (Γ · R) (⇑ R).
Proof.
  unshelve refine  (fun α γ => exist3 _).
  + refine (γ.(snd).(q1)).
  + refine (γ.(snd).(q2)).
  + exact (γ.(snd).(q3)). 
Defined.

Notation "#" := (Var _) (at level 0).


Definition Weaken {Γ : Ctx} {R S : Ty Γ} (r : RElt Γ R) :
  RElt (Γ · S) (⇑ R).
Proof.
  unshelve refine (fun α γ => exist3 _).
  + refine ((r α (◊ γ)).(q1)).
  + refine ((r α (◊ γ)).(q2)).
  + exact ((r α (◊ γ)).(q3)).
Defined.

Notation "↑ M" := (Weaken M) (at level 10).

Definition Sub {Γ : Ctx} {R : Ty Γ} (S : Ty (Γ · R)) (r : RElt Γ R) :
  Ty Γ := fun α γ => (S α (γ ·· (r® γ))).


Notation "S {{ r }} " := (Sub S r) (at level 20, left associativity).

         
Definition RPi {Γ : Ctx} (A : Ty Γ) (B : Ty (Γ ·  A)) : Ty Γ.
Proof.
  unshelve refine (fun α γ => exist3 _).
  - refine (forall (x : Ty_inst A γ), (B α (γ ·· x)).(q1)).
  - unshelve refine (exist _).
    refine (forall (x : Ty_inst A γ), BEl (B α (γ ·· x)).(q2)).
    refine (fun f x a => (B α (γ ·· a)).(q2).(snd) (fun y => f y a) x).
  - unshelve refine (exist _).
    + exact (fun fr fd => forall a : Ty_inst A γ,
            (B α (γ ·· a)).(q3).(fst) (fr a) (fd a)).
    + unshelve refine (fun _ _ _ fe a => (B α (γ ·· a)).(q3).(snd) _ _ _ _).
      exact (fe a). 
Defined.


Notation "∏" := (RPi).

Notation "A → B" := (RPi A (⇑ B)) (at level 99, right associativity, B at level 200).


Definition RLam {Γ : Ctx} {A : Ty Γ} (B : Ty (Γ · A)) (f : RElt (Γ · A) B)
  : RElt Γ (RPi A B).
Proof.
  unshelve refine (fun α γ => exist3 _).
  + refine (fun x => (f α (γ ·· x)).(q1)).
  + refine (fun x => (f α (γ ·· x)).(q2)).
  + exact (fun  x => (f α (γ ·· x)).(q3)).
Defined.


Notation "'λ'" := RLam.


Definition RApp {Γ : Ctx} {A : Ty Γ} {B : Ty (Γ · A)}
           (f : RElt Γ (RPi A B)) (x : RElt Γ A) : RElt Γ (B {{ x }}).
Proof.
  unshelve refine (fun α γ => exist3 _).
  + refine ((f α γ).(q1) (x α γ)).
  + refine ((f α γ).(q2) (x α γ)).
  + exact ((f α γ).(q3) (x α γ)).
Defined.

Notation "t • u" := (RApp t u) (at level 12, left associativity).


Definition U {Γ : Ctx} : Ty Γ.
Proof.
  unshelve refine (fun α γ => exist3 _).
  - refine Type.
  - unshelve refine (exist _).
    + refine (BranchingTYPE X Y).
    + refine (fun _ _ => Bunit X Y).
  - unshelve refine (exist _).
    + unshelve refine (fun A B => @tsig (A -> BEl B -> Type) _).
      * exact (fun Te => forall (a : A) (f : Y -> BEl B) (x : X) (fe : Te a (f (α x))),
                Te a ((B.(snd) f x))).
    + intros A f x fe.
      unshelve refine (exist _).
      * exact (fun _ _ => unit).
      * simpl.
        exact (fun _ _ _ _ => tt).
Defined.

Definition RElt_inst {Γ : Ctx} {α : ℙ} {γ : Γ α}
           (R : Ty_inst U γ) : Type :=
    triple (fun (a : R.(q1)) (b : BEl R.(q2)) =>
                R.(q3).(fst) a b).

Definition RPi_inst {Γ : Ctx} {α : ℙ} {γ : Γ α} (A : Ty_inst U γ)
           (B : Ty_inst U (γ ·· A)) : Ty_inst U γ.
Proof.
  unshelve refine (exist3 _).
  - refine (forall (x : RElt_inst A), B.(q1)).
  - unshelve refine (exist _).
    refine (forall (x : RElt_inst A), BEl B.(q2)).
    refine (fun f x a => B.(q2).(snd) (fun y => f y a) x).
  - unshelve refine (exist _).
    + exact (fun fr fd => forall a : RElt_inst A,
            B.(q3).(fst) (fr a) (fd a)).
    + unshelve refine (fun _ _ _ fe a => B.(q3).(snd) _ _ _ _).
      exact (fe a). 
Defined.
  
Definition RApp_inst {Γ : Ctx} {α : ℙ} {γ : Γ α} {A : Ty Γ}
           {B : Ty (Γ · A)} (f : Ty_inst (RPi A B) γ) (x : Ty_inst A γ) :
  Ty_inst B (γ ·· x).
Proof.
  unshelve refine (exist3 _).
  - exact (f.(q1) x).
  - exact (f.(q2) x).
  - exact (f.(q3) x).
Defined.

Definition Lam {Γ : Ctx} {A : Ty Γ} {B : Ty (Γ · A)} (α : ℙ) (γ : Γ α) 
           (f : forall (a : Ty_inst A γ), Ty_inst B (γ ·· a)) : Ty_inst (∏ A B) γ.
Proof.
  unshelve refine (exist3 _).
  - exact (fun x => (f x).(q1)).
  - exact (fun x => (f x).(q2)).
  - exact (fun x => (f x).(q3)).
Defined.


Definition App {Γ : Ctx} {A : Ty Γ} {B : Ty (Γ · A)} (α : ℙ) (γ : Γ α) 
           (f :  Ty_inst (∏ A B) γ) :
  forall (a : Ty_inst A γ), Ty_inst B (γ ·· a).
Proof.  
  unshelve refine (fun a => exist3 _).
  - exact (f.(q1) a).
  - exact (f.(q2) a).
  - exact (f.(q3) a).
Defined.
  
Definition RProp {Γ : Ctx} : Ty Γ.
Proof.
  unshelve refine (fun α γ => exist3 _).
  - refine Prop.
  - unshelve refine (exist _).
    + refine ({P : Prop & (Y -> P) -> X -> P}).
    + refine (fun _ _ => _).
      refine (@exist _ _ True _).
      refine (fun _ _ => I).
  - unshelve refine (exist _).
    + exact (fun A B => tsig (fun (Te : A -> B.(fst) -> Type) =>
              forall (a : A) (f : Y -> B.(fst) ) (x : X) (fe : Te a (f (α x))),
                Te a ((B.(snd) f x)))).
    + simpl ; intros.
      unshelve refine (exist _).
      * exact (fun _ _ => True).
      * simpl.
        exact (fun _ _ _ _ => I).
Defined.

Inductive BranchingFalse : Prop :=
| BêtaFalse : (Y -> BranchingFalse) -> X -> BranchingFalse.

Definition RFalse {Γ : Ctx} : RElt Γ RProp.
Proof.
  unshelve refine (fun α γ => exist3 _).
  - refine False.
  - unshelve refine (exist _). 
    + exact BranchingFalse.
    + exact BêtaFalse.
  - simpl.
    unshelve refine (exist _).
    + contradiction.
    + contradiction.
Defined.


Definition REL {Γ : Ctx} (A : RElt Γ U) : Ty Γ.
Proof.
  unshelve refine (fun α γ => exist3 _).
  - refine (A α γ).(q1).
  - refine (A α γ).(q2).
  - exact (A α γ).(q3).
Defined.

Notation "[ A ]" := (REL A).

Definition REL_Prop {Γ : Ctx} (A : RElt Γ RProp) : Ty Γ.
Proof.
    unshelve refine (fun α γ => exist3 _).
  - refine (A α γ).(q1).
  - unshelve refine (exist _).
    + refine (A α γ).(q2).(fst).
    + exact (A α γ).(q2).(snd).
  - exact (A α γ).(q3).
Defined.


Inductive RParabool (α : X -> Y) :
  bool -> (BranchingBool X Y) -> Type :=
| RParatrue : RParabool α true (@Btrue X Y)
| RParafalse : RParabool α false (@Bfalse X Y)
| RParabêtabool : forall  (bo : bool) (f : (Y ->  BranchingBool X Y)) (x : X)
                        (rel : RParabool α bo (f (α x)) ),
    RParabool α bo (Bêtabool f x).


Definition Rbool {Γ : Ctx} : Ty Γ.
Proof.
  unshelve refine (fun α γ => exist3 _).
  - exact bool.
  - exact (Bbool X Y).
  - unshelve refine (exist _).
    + exact (RParabool α).
    + exact (RParabêtabool α).
Defined.

Definition Rtrue {Γ : Ctx} : RElt Γ Rbool.
Proof.
  unshelve refine (fun α γ => exist3 _).
  - exact true.
  - exact (Btrue X Y).
  - exact (RParatrue α).
Defined.


Definition Rfalse {Γ : Ctx} : RElt Γ Rbool.
Proof.
  unshelve refine (fun α γ => exist3 _).
  - exact false.
  - exact (Bfalse X Y).
  - exact (RParafalse α).
Defined.


Definition Rbool_rec (Γ : Ctx) :
   RElt Γ (∏ U ([Var U] → [Var U] → Rbool → [Var U])).
Proof.
  unshelve refine (fun α γ => exist3 _).
  - refine (fun P ptrue pfalse b => match b.(q1) with
                                  | true => ptrue.(q1)
                                  | false => pfalse.(q1)
                                    end).
  - refine (fun P ptrue pfalse b => _).
    now refine (let fix f b := match b with
                         | Btrue _ _  => ptrue.(q2)
                         | Bfalse _ _ => pfalse.(q2)
                         | Bêtabool g x => P.(q2).(snd) (fun y => f (g y)) x
                         end in f b.(q2)).  
  - cbv.
    refine (fun  P ptrue pfalse b => _).
    destruct b as [bo bb be].
    revert be ; revert  bb ; revert bo ; simpl.
    unshelve refine (fix f _ _ b := match b with
                              | RParatrue _ => _
                              | RParafalse _ => _
                              | RParabêtabool _ _ _ _ rel =>
                                P.(q3).(snd) _ _  _ (f _ _ rel)
                              end).
    + exact ptrue.(q3).
    + exact pfalse.(q3).
Defined.

Definition RAppN {Γ : Ctx} {A : Ty Γ} {B : Ty Γ} (f : RElt Γ (A → B))(x : RElt Γ A) : RElt Γ B :=
  @RApp Γ A (⇑ B) f x.

Definition RLamN {Γ : Ctx} {A : Ty Γ} {B : Ty Γ} (f : RElt (Γ · A) (⇑ B)) : RElt Γ (A → B) :=
  @RLam Γ A (⇑ B) f.

Definition Rbool_stor {Γ : Ctx} :
  RElt Γ (Rbool → (Rbool → U) → U).
Proof.
  pose (r := RApp (Rbool_rec Γ) ((Rbool → U) → U)).
  let T := constr:(RElt Γ (((Rbool → U) → U) → ((Rbool → U) → U) → Rbool → ((Rbool → U) → U))) in
  unshelve refine (let r : T := r in _).
  unshelve refine (RAppN (RAppN r _) _).
  + refine (RLamN (@RAppN _ Rbool _ _ _)).
    - refine #.
    - refine Rtrue.
  + refine (RLamN (@RAppN _ Rbool _ _ _)).
    - refine #.
    - refine Rfalse.
Defined.

Definition Rbool_rect {Γ : Ctx} :
  RElt Γ (∏ (Rbool → U) (
    [@RApp (Γ · (Rbool → U)) Rbool U (Var (Rbool → U)) Rtrue] →
    [@RApp (Γ · (Rbool → U)) Rbool U # Rfalse] →
    ∏ Rbool [@RApp _  _ U
                   (@RApp (Γ · (Rbool → U) · Rbool) _ _ Rbool_stor #) (↑#)])).
Proof.
  unshelve refine (fun α γ => exist3 _).
  - refine (fun P ptrue pfalse b =>
                     match b.(q1) as b0 return
                           (if b0
                            then
                              fun Q : Ty_inst (Rbool → U) (γ ·· P ·· b) =>
                                q1 Q (Rtrue _ _)
                            else
                              fun Q : Ty_inst (Rbool → U) (γ ·· P ·· b) =>
                                q1 Q (Rfalse _ _))
                             ((↑ #) α (γ ·· P ·· b)) with
                     | true => ptrue.(q1)
                     | false => pfalse.(q1)
                     end).
  - simpl.
    unshelve refine (fun P ptrue pfalse b =>
                       match b.(q2) with
                       | Btrue _ _ => _
                       | Bfalse _ _ => _
                       | Bêtabool _ _ => _
                       end).
    + exact ptrue.(q2).
    + exact pfalse.(q2).
    + exact tt.
  - intros P ptrue pfalse b.
    simpl.
    unshelve refine (match b.(q3) with
                     | RParatrue _ => _
                     | RParafalse _ => _
                     | RParabêtabool _ _ _ _ _ => _
                     end).
    + now apply ptrue.(q3).
    + now apply pfalse.(q3).
    + now apply tt.
Defined.


Inductive RParanat (α : X -> Y) : nat -> BranchingNat X Y -> Type :=
| RP0 : RParanat α 0 (B0 X Y)
| RPS : forall (no : nat) (nt :  BranchingNat X Y)
                     (ne : RParanat α no nt),
    RParanat α (S no) (BS nt)
| RPbêta : forall  (no : nat) (f : (Y ->  BranchingNat X Y)) (x : X)
                        (rel : RParanat α no (f (α x)) ),
    RParanat α no (Bêtanat f x).


Definition Rnat {Γ : Ctx} : Ty Γ.
Proof.
  unshelve refine (fun α γ => @exist3 _ _ _ nat (Bnat X Y) _).
  exact (exist (RPbêta α)).
Defined.

Definition R0 {Γ : Ctx} : RElt Γ Rnat.
Proof.
  unshelve refine (fun α γ => exist3 _).
  - exact 0.
  - exact (B0 X Y).
  - now refine (RP0 α).
Defined.

Definition RS {Γ : Ctx} : RElt Γ (Rnat → Rnat).
Proof.
  unshelve refine (fun α γ => exist3 _).
  - exact (fun n => S n.(q1)). 
  - exact (fun n => BS n.(q2)).
  - now refine (fun n => RPS α n.(q1) n.(q2) n.(q3)).
Defined.


Lemma RParanat_equal {Γ : Ctx} (n : RElt Γ Rnat)
      (α : X -> Y) (γ : Γ α)  :
    BNdialogue (n ® γ).(q2) α = (n ® γ).(q1).
Proof.
  destruct (n ® γ) as [nt no ne] ; simpl.
  elim: ne =>
  [ | β mt mo me | β f x mo rel] ; trivial ; simpl.
  exact: eq_S.
Qed.


Definition Rnat_rec_ext2 {Γ : Ctx} (α : X -> Y) (γ : Γ α) :
  forall (P : Ty_inst U γ)
         (p0 : RElt_inst P)
         (pS : Ty_inst Rnat γ -> RElt_inst P -> RElt_inst P)
         (a3 : Ty_inst Rnat γ),
      triple (fun (a : P.(q1)) (b : BEl P.(q2)) =>
                (P.(q3)).(fst) a b).
Proof.
  simpl ; unshelve refine (fun P p0 pS n => _).
  unshelve refine (let fix g no nt ne :=
                       match ne as n in RParanat _ mo mt return
                             triple (fun (a : q1 P) (b : BEl (X:=X) (Y:=Y) (q2 P)) =>
                                       fst (q3 P) a b)                                   
                       with
                       | RP0 _ => p0
                       | RPS _ ko kt ke => (pS (exist3 ke) (g ko kt ke))
                       | RPbêta _ ko f x rel => exist3 (P.(q3).(snd) (g ko (f (α x)) rel).(q1)  _ x _)
                       end
                         in g n.(q1) n.(q2) n.(q3)).
  - refine (fun y => (g ko (f (α x)) rel).(q2)).
  - exact (g ko (f (α x)) rel).(q3).
Defined.

Definition Rnat_rec_ext {Γ : Ctx} (α : X -> Y) (γ : Γ α) :
  forall (P : Ty_inst U γ) (a1 : Ty_inst [Var U] (γ ·· P))
                    (a2 : Ty_inst (Rnat → [Var U] → [Var U]) (γ ·· P))
                    (a3 : Ty_inst Rnat (γ ·· P)),
      triple (fun (a : P.(q1)) (b : BEl P.(q2)) =>
                (P.(q3)).(fst) a b). 
Proof.
  simpl ; unshelve refine (fun P p0 pS n => _).
  unshelve refine (let fix g no nt ne :=
                       match ne as n in RParanat _ mo mt return
                             triple (fun (a : q1 P) (b : BEl (X:=X) (Y:=Y) (q2 P)) =>
                                       fst (q3 P) a b)                                   
                       with
                       | RP0 _ => p0
                       | RPS _ ko kt ke => exist3 (pS.(q3) (exist3 ke) (g ko kt ke))
                       | RPbêta _ ko f x rel => exist3 (P.(q3).(snd) (g ko (f (α x)) rel).(q1)  _ x _)
                       end
                         in g n.(q1) n.(q2) n.(q3)).
  - refine (fun y => (g ko (f (α x)) rel).(q2)).
  - exact (g ko (f (α x)) rel).(q3).
Defined. 



Definition Rnat_rec (Γ : Ctx) :
   RElt Γ (∏ U ([Var U] → (Rnat → [Var U] → [Var U]) → Rnat → [Var U])).
Proof.
  unshelve refine (fun α γ => exist3 _).
  + exact (fun P p0 pS n => (Rnat_rec_ext α γ P p0 pS n).(q1)).
  + exact (fun P p0 pS n => (Rnat_rec_ext α γ P p0 pS n).(q2)).
  + exact (fun P p0 pS n => (Rnat_rec_ext α γ P p0 pS n).(q3)).
Defined.    


Definition Rnat_stor {Γ : Ctx} :
  RElt Γ (Rnat → (Rnat → U) → U).
Proof.
  pose (r := RApp (Rnat_rec Γ) ((Rnat → U) → U)).
  let T := constr:(RElt Γ (((Rnat → U) → U) → (Rnat → ((Rnat → U) → U) → ((Rnat → U) → U)) → Rnat → ((Rnat → U) → U))) in
  unshelve refine (let r : T := r in _).
  unshelve refine (RAppN (RAppN r _) _).
  - refine (RLamN (@RAppN _ Rnat _ _ _)).
    + now refine #.
    + now refine R0.
  - refine (@RLamN _ _ _ _).
    refine (@RLamN (@Ctx_cons Γ Rnat) (⇑ ((Rnat → U) → U)) (⇑ ((Rnat → U) → U)) _).
    refine (@RLamN (Γ · Rnat · (⇑ ((Rnat → U) → U)))
                  (⇑(⇑(Rnat → U))) (⇑ (⇑ U)) _).
    refine (@RAppN _ (Rnat → U) _ _ _).
    + now refine (↑#).
    + apply RLamN.
      refine (@RAppN _ Rnat _ _ _).
      * now refine (↑#).
      * now refine (@RAppN _ Rnat _ RS (↑↑↑(Var Rnat))).
Defined.



  (*      refine (@RLamN _ _ _ _).
    refine (↑(@RLamN _ _ _ _)).
    refine (@RLamN (Γ · ((Rnat → U) → U)) (⇑(Rnat → U)) (⇑ U) _).
    refine (@RAppN _ (Rnat → U) _ _ _).
    + now refine (↑#).
    + apply RLamN.
      refine (@RAppN _ Rnat _ _ _).
      * now refine (↑#).
      * now refine (@RAppN _ Rnat _ RS (Var Rnat)).
Defined.      *)
Print Lam.

Definition Rnat_rect_ext {Γ : Ctx} (α : X -> Y) (γ : Γ α) :
  forall (P : forall (n : Ty_inst Rnat γ), Ty_inst U (γ ·· n))
         (p0 : RElt_inst (P (R0 α γ)))
         (pS : forall (n : Ty_inst Rnat (γ ·· (Lam α γ P))),
             Ty_inst [@RApp (Γ · (Rnat → U) · Rnat) _ U
                            (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor #) (↑#)] (γ ·· (Lam α γ P) ·· n)
             -> Ty_inst [@RApp (Γ · (Rnat → U) · Rnat) _ U
                               (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor
                                      (@RApp (Γ · (Rnat → U) · Rnat) _ _ RS #)) (↑#)] (γ ·· (Lam α γ P) ·· n))
         (n : Ty_inst Rnat (γ ·· (Lam α γ P))),
     Ty_inst ((@RApp (Γ · (Rnat → U) · Rnat) _ U
                    (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor #) (↑#))) (γ ·· (Lam α γ P) ·· n).
Proof.
  cbv.
  intros P p0 pS n.
  destruct n as [na nb ne].
  elim: ne.
  - exact p0.
  - exact (fun no nt ne p => pS (@exist3 _ _ _ no nt ne) p).
  - exact (fun ko f x rel p => @exist3 _ _ _ p.(q1) tt tt).
Defined.     
(*  unshelve refine (let fix g no nt ne :=
                       match ne as me in RParanat _ mo mt return
                             Ty_inst ((@RApp (Γ · (Rnat → U) · Rnat) _ U
                                             (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor #) (↑#)))
                                     (@ctx_cons _ Rnat α (γ ·· (Lam α γ P))
                                                (@exist3 _ _ (fun (a : nat) (b : BranchingNat X Y) => RParanat α a b) mo mt me))                                   
                       with
                       | RP0 _ => p0
                       | RPS _ ko kt ke => (pS (exist3 ke) (g ko kt ke))
                       | RPbêta _ ko f x rel => @exist3 _ _ _ (g ko (f (α x)) rel).(q1) tt tt
                       end
                         in g n.(q1) n.(q2) n.(q3)).
Defined.*)

  (*
Definition Rnat_rect_ext2 {Γ : Ctx} (α : X -> Y) (γ : Γ α) :
  forall (P : Ty_inst (Rnat → U) γ)
         (p0 : Ty_inst [@RApp (Γ · (Rnat → U)) Rnat U # R0] (γ ·· P))
         (pS : Ty_inst (∏ Rnat ([@RApp (Γ · (Rnat → U) · Rnat) _ U
                                       (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor #) (↑#)] →
                                [@RApp (Γ · (Rnat → U) · Rnat) _ U
                                       (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor
                                              (@RApp (Γ · (Rnat → U) · Rnat) _ _ RS #)) (↑#)])) (γ ·· P))
         (n : Ty_inst Rnat (γ ·· P)),
    Ty_inst ((@RApp (Γ · (Rnat → U) · Rnat) _ U
                    (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor #) (↑#))) (γ ·· P ·· n).
Proof.
  intros P p0 pS n.
  cbv in pS.
  unshelve refine (let fix g no nt ne :=
                       match ne as me in RParanat _ mo mt return
                             Ty_inst ((@RApp (Γ · (Rnat → U) · Rnat) _ U
                                             (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor #) (↑#)))
                                     (@ctx_cons _ Rnat α (γ ·· P) (@exist3 _ _ (fun (a : nat) (b : BranchingNat X Y) => RParanat α a b) mo mt me))                                   
                       with
                       | RP0 _ => p0
                       | RPS _ ko kt ke => exist3 (pS.(q3) (exist3 ke) (g ko kt ke))
                       | RPbêta _ ko f x rel => @exist3 _ _ _ (g ko (f (α x)) rel).(q1) tt tt
                       end
                         in g n.(q1) n.(q2) n.(q3)).
Defined.*)

Print App.

Definition Rnat_rect {Γ : Ctx} :
  RElt Γ (∏ (Rnat → U)
            ([@RApp (Γ · (Rnat → U)) Rnat U # R0] →
             [∏ Rnat ([@RApp (Γ · (Rnat → U) · Rnat) _ U
                             (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor #) (↑#)] →
                      [@RApp (Γ · (Rnat → U) · Rnat) _ U
                             (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor
                                    (@RApp (Γ · (Rnat → U) · Rnat) _ _ RS #)) (↑#)])] →
             ∏ Rnat [@RApp (Γ · (Rnat → U) · Rnat) _ U
                           (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor #) (↑#)])).
Proof.
  unshelve refine (fun α γ => exist3 _).
  + exact (fun P p0 pS n => (Rnat_rect_ext α γ (App α γ P) p0 (fun n => App α (γ ·· P ·· n) (App α (γ ·· P) pS n)) n).(q1)).
  + exact (fun P p0 pS n => (Rnat_rect_ext α γ (App α γ P) p0 (fun n => App α (γ ·· P ·· n) (App α (γ ·· P) pS n)) n).(q2)).
  + exact (fun P p0 pS n => (Rnat_rect_ext α γ (App α γ P) p0 (fun n => App α (γ ·· P ·· n) (App α (γ ·· P) pS n)) n).(q3)).
Defined.


(*Goal forall (Γ : Ctx)
  (P : RElt Γ (Rnat → U))
  (T0 := [P • R0])
  (p0 : RElt Γ T0)

  (TS := (∏ Rnat ([@RApp (Γ · Rnat) _ U
                              (@RApp (Γ · Rnat) _ _ Rnat_stor #) (↑P)] →
                       [@RApp (Γ · Rnat) _ U
                              (@RApp (Γ · Rnat) _ _ Rnat_stor
                                     (@RApp (Γ · Rnat) _ _ RS #)) (↑P)])))

  
  (pS : RElt Γ TS)
  (TR := @RPi Γ Rnat [Rnat_stor • Var Rnat • ↑ P])
  ,
  @RApp Γ Rnat (@RApp (Γ · Rnat) _ U
                      (@RApp (Γ · Rnat) _ _ Rnat_stor #) (↑P))
        (@RAppN Γ _ _ (@RAppN Γ T0 (TS → TR) (@RApp Γ (Rnat → U) _ (@Rnat_rect Γ) P) p0) pS) R0.
  =
  @RApp Γ _ _ (@RAppN Γ _ _ (@RAppN Γ T0 (TS → TR) ((@Rnat_rect Γ) • P) p0) pS) R0.
Proof.
reflexivity.
Qed.

 Goal forall (Γ : Ctx)
  (P : RElt Γ (Rnat → U))
  (T0 := [P • R0])
  (p0 : RElt Γ T0)
  (TS := (∏ Rnat ([@RApp (Γ · Rnat) _ U
                         (@RApp (Γ · Rnat) _ _ Rnat_stor #) (↑P)] →
                  [@RApp (Γ · Rnat) _ U
                         (@RApp (Γ · Rnat) _ _ Rnat_stor
                                (@RApp (Γ · Rnat) _ _ RS #)) (↑P)])))
  (pS : RElt Γ TS)
  (TR := @RPi Γ Rnat [Rnat_stor • Var Rnat • ↑ P])
  (n : RElt Γ Rnat)
  ,
    
    q1 (@RApp Γ _ _ (@RAppN Γ _ _ (@RAppN Γ T0 (TS → TR) ((@Rnat_rect Γ) • P) p0) pS) (RApp RS n))
    =
    q1 (@RAppN Γ (@RApp Γ (Rnat → U) _ (@RApp Γ _ _ Rnat_stor n) P) (Rnat_stor • (RS • n) • P) (RApp pS n) (@RApp Γ _ _ (@RAppN Γ _ _ (@RAppN Γ T0 (TS → TR) ((@Rnat_rect Γ) • P) p0) pS) n)).
Proof. reflexivity. Qed.
 *)

Print RElt_inst.

Inductive Bsig_aux {Γ : Ctx} {α : X -> Y} {γ : Γ α} (A : Ty_inst U γ)
          (B :Ty_inst ((Var U) → U) (γ ··  A)) : Type :=
| Bex : forall (a : RElt_inst A) (b :  (RApp_inst B a).(q2).(fst)), Bsig_aux A B
| bêtasig : (Y -> Bsig_aux A B) -> X -> Bsig_aux A B.

Inductive RParasig {Γ : Ctx} {α : X -> Y} {γ : Γ α} (A : Ty_inst U γ)
          (B :Ty_inst ((Var U) → U) (γ ··  A)) : tsig B.(q1) -> Bsig_aux A B -> Type :=
| Paraex : forall (a : RElt_inst A) (b : RElt_inst (RApp_inst B a)),
    RParasig A B (@exist _ _ a b.(q1)) (Bex A B a b.(q2))
| Parabêtasig : forall (ba : tsig B.(q1)) (f : Y -> Bsig_aux A B) (x : X) (rel : RParasig A B ba (f (α x))),
    RParasig A B ba (bêtasig A B f x).

Definition Rsig {Γ : Ctx} : RElt Γ (∏ U (((Var U) → U) → U)).
Proof.
  unshelve refine (fun alpha gamma => exist3 _). 
  - cbv.
    intros A B.
    exact (tsig B.(q1)). 
  - unshelve refine (fun A B => exist _).
    + refine (Bsig_aux A B).
    + exact (bêtasig A B).
  - unshelve refine (fun A B => exist _). 
    + exact (RParasig A B).
    + exact (Parabêtasig A B).
Defined.      


Inductive RParalist {Γ : Ctx} {α : X -> Y} {γ : Γ α} (A : Ty_inst U γ)
  : list A.(q1) -> BEl (@Blist X Y A.(q2)) -> Type :=
| RParanil : RParalist A nil (@Bnil X Y _)
| RParacons : forall (la : list A.(q1)) (lb :  @BranchingList X Y A.(q2))
                     (le : RParalist A la lb)
                     (x : RElt_inst A),
    RParalist A (x.(q1) :: la) (Bcons x.(q2) lb)  
| RParabêtalist : forall (la : list A.(q1)) (f : Y -> BEl (@Blist X Y A.(q2))) (x : X)
                         (rel : RParalist A la (f (α x))),
    RParalist A la (Bêtalist f x).


Definition Rlist {Γ : Ctx} : RElt Γ (U → U).
Proof.
  unshelve refine (fun alpha gamma => exist3 _).
  - exact (fun A => list A.(q1)).
  - exact (fun A => @Blist X Y A.(q2)).
  - unshelve refine (fun A => exist _).    
    + exact (RParalist A).
    + exact (RParabêtalist A).
Defined.

Definition Rnil {Γ : Ctx} {A : Ty Γ} : RElt Γ [RApp Rlist A]. 
Proof.
  cbv.
  unshelve refine (fun alpha gamma => exist3 _).
  - exact nil.
  - exact (@Bnil X Y _).
  - exact (RParanil (A alpha gamma)).
Defined.

Definition Rcons_ext {Γ : Ctx} {A : Ty Γ} {α : X -> Y} {γ : Γ α} :
  (Ty_inst A γ) -> Ty_inst [RApp Rlist A] γ -> Ty_inst [RApp Rlist A] γ.
Proof.
  intros x l.
  unshelve refine (exist3 _).
  - exact (@cons (A α γ).(q1) x.(q1) l.(q1)).
  - exact (@Bcons X Y (A α γ).(q2) x.(q2) l.(q2)).
  - cbv.
    refine (RParacons (A α γ) l.(q1) l.(q2) l.(q3) x).
Defined.


Inductive Beq (A : BranchingTYPE X Y) (a : BEl A) : BEl A -> Type :=
| Beq_refl : Beq A a a
| Bêtaeq : forall (b : BEl A), (Y -> Beq A a b) -> X -> Beq A a b.

Inductive RParaeq {Γ : Ctx} {α : X -> Y} {γ : Γ α} (A : Ty_inst U γ) (a : RElt_inst A) :
  forall (b : RElt_inst A),
  (a.(q1) = b.(q1)) -> (Beq A.(q2) a.(q2) b.(q2)) -> Type :=
| RParaeq_refl : RParaeq A a a (eq_refl a.(q1)) (Beq_refl A.(q2) a.(q2))
| RParabêtaeq : forall (b : RElt_inst A) (e : a.(q1) = b.(q1))
                       (f : Y -> Beq A.(q2) a.(q2) b.(q2)) (x : X)
                       (rel : RParaeq A a b e (f (α x))),
    RParaeq A a b e (Bêtaeq A.(q2) a.(q2) b.(q2) f x).


Definition Req {Γ : Ctx} : RElt Γ (∏ U ((Var U) → (Var U) → U)).
Proof.
  unshelve refine (fun alpha gamma => exist3 _).
  - exact (fun A a b => @eq A.(q1) a.(q1) b.(q1)).
  - unshelve refine (fun A a b => exist _).
    + exact (Beq A.(q2) a.(q2) b.(q2)).
    + exact (Bêtaeq A.(q2) a.(q2) b.(q2)).
  - unshelve refine (fun A a b => exist _).
    + exact (RParaeq A a b).
    + exact (RParabêtaeq A a b).
Defined.



Inductive AEqFin {Γ : Ctx} {α : X -> Y} {γ : Γ α} :
  (Ty_inst Rnat γ -> nat) -> Ty_inst [RApp Rlist Rnat] γ -> (Ty_inst Rnat γ -> nat) -> Type :=
| AEqnil : forall (f g : (Ty_inst Rnat γ -> nat)), AEqFin f (Rnil α γ) g
| AEqcons : forall (f g : (Ty_inst Rnat γ -> nat))
                       (l : Ty_inst [RApp Rlist Rnat] γ)
                       (n : Ty_inst Rnat γ),
    (f n = g n) -> (AEqFin f l g) -> (AEqFin f (Rcons_ext n l) g).


Inductive BEqFin {Γ : Ctx} {α : X -> Y} {γ : Γ α} :
  (Ty_inst Rnat γ -> BranchingNat X Y) -> Ty_inst [RApp Rlist Rnat] γ ->
  (Ty_inst Rnat γ -> BranchingNat X Y) -> Type :=
| BEqnil : forall (f g : Ty_inst Rnat γ -> BranchingNat X Y),
    BEqFin f (Rnil α γ) g
| BEqcons : forall (f g : (Ty_inst Rnat γ -> BranchingNat X Y))
                   (l : Ty_inst [RApp Rlist Rnat] γ)
                   (n : Ty_inst Rnat γ),
    (Beq (Bnat X Y) (f n) (g n)) -> (BEqFin f l g) -> (BEqFin f (Rcons_ext n l) g)
| BêtaEqFin : forall (f g : Ty_inst Rnat γ -> BranchingNat X Y)
                     (l : Ty_inst [RApp Rlist Rnat] γ)
                     (h : Y -> BEqFin f l g) (x : X),
    BEqFin f l g.



Inductive RParaEqFin {Γ : Ctx} {α : X -> Y} {γ : Γ α} :
  forall (f : Ty_inst [Rnat → Rnat] γ) (l : Ty_inst [RApp Rlist Rnat] γ)
         (g : Ty_inst [Rnat → Rnat] γ),
    AEqFin f.(q1) l g.(q1) -> BEqFin f.(q2) l g.(q2) -> Type :=
| RParaEqnil : forall (f g : Ty_inst [Rnat → Rnat] γ),
    RParaEqFin f (Rnil ® γ) g (AEqnil f.(q1) g.(q1)) (BEqnil f.(q2) g.(q2))
| RParaEqcons : forall (f g : Ty_inst [Rnat → Rnat] γ)
                       (l : Ty_inst [RApp Rlist Rnat] γ)
                       (n : Ty_inst Rnat γ)
                       (i : AEqFin f.(q1) l g.(q1))
                       (j : BEqFin f.(q2) l g.(q2))
                       (k : RElt_inst (@RApp_inst Γ α γ Rnat _
                                                  (@RApp_inst Γ α γ Rnat (((Var U) → U) {{Rnat}})
                                                              ((@RApp Γ U ((Var U) → (Var U) → U) Req Rnat) ® γ) (RApp_inst f n))
                                                  (RApp_inst g n))),
    RParaEqFin f l g i j ->
    RParaEqFin f (Rcons_ext n l) g
               (AEqcons f.(q1) g.(q1) l n k.(q1) i)
               (BEqcons f.(q2) g.(q2) l n k.(q2) j)
| RParabêtaEqFin : forall (f g : Ty_inst [Rnat → Rnat] γ)
                       (l : Ty_inst [RApp Rlist Rnat] γ)
                       (i : AEqFin f.(q1) l g.(q1))
                       (h : Y -> BEqFin f.(q2) l g.(q2)) (x : X)
                       (rel : RParaEqFin f l g i (h (α x))),
    RParaEqFin f l g i (BêtaEqFin f.(q2) g.(q2) l h x). 

Definition REqFin  {Γ : Ctx} :
  RElt Γ ((Rnat → Rnat) →
            [RApp Rlist Rnat] →
               (Rnat → Rnat) → U).
Proof.
  unshelve refine (fun alpha gamma => exist3 _).
  - exact (fun f l g => AEqFin f.(q1) l g.(q1)).
  - unshelve refine (fun f l g => exist _).
    + exact (BEqFin f.(q2) l g.(q2)).
    + exact (BêtaEqFin f.(q2) g.(q2) l).
  - unshelve refine (fun f l g => exist _).
    + exact (RParaEqFin f l g).
    + exact (RParabêtaEqFin f g l).
Defined.

Definition REL_inv {Γ : Ctx} (A : Ty Γ)  : RElt Γ U.
Proof.
  unshelve refine (fun α γ => exist3 _).
  - refine (A α γ).(q1).
  - refine (A α γ).(q2).
  - exact (A α γ).(q3).
Defined.

  

Definition Rcontinuous {Γ : Ctx} :
  RElt Γ (((Rnat → Rnat) → Rnat) →
          (Rnat → Rnat) → U).
Proof.
  refine (RLamN _).
  refine (@RLamN (@Ctx_cons Γ ((Rnat → Rnat) → Rnat)) (⇑ ((Rnat → Rnat))) (⇑ U) _).
  refine (@RAppN _ ((@RApp (Γ · ((Rnat → Rnat) → Rnat) · ⇑ (Rnat → Rnat)) _ _ Rlist Rnat) → U) U _ _).
  refine (@RApp _  _ _ Rsig (RApp Rlist Rnat)).
  refine (@RLamN _ _ _ _).
  refine (REL_inv _).
  unshelve refine (RPi (Rnat → Rnat) _).
  unshelve refine (RPi _ _).
  refine (REL _).
  refine (@RAppN _ (Rnat → Rnat) U _ (↑↑(Var (Rnat → Rnat)))).
  refine (@RAppN _ (⇑ ⇑(@RApp (Γ · ((Rnat → Rnat) → Rnat) · ⇑ (Rnat → Rnat)) _ _ Rlist Rnat)) ((Rnat → Rnat) → U) _ (↑ (Var (@RApp (Γ · ((Rnat → Rnat) → Rnat) · ⇑ (Rnat → Rnat)) _ _ Rlist Rnat)))).
  refine (@RAppN _ (Rnat → Rnat) (⇑ (⇑ (Rlist • Rnat)) → (Rnat → Rnat) → U) _ ((Var (Rnat → Rnat)))).
  refine (↑ ↑ REqFin).
  refine (REL _).
  refine (@RAppN _ Rnat U _ _).
  refine (@RAppN _ Rnat (Rnat → U) _ _).
  refine (RApp Req Rnat).
  unshelve refine (@RAppN _ ((Rnat → Rnat)) Rnat _ _).
  - refine (↑ ↑ ↑ ↑ (Var  ((Rnat → Rnat) → Rnat))). 
  - refine (↑ ↑ ↑ (Var (Rnat → Rnat))).
  - unshelve refine (@RAppN _ ((Rnat → Rnat)) Rnat _ _).
    + refine (↑ ↑ ↑ ↑ (Var  ((Rnat → Rnat) → Rnat))).
    + refine (↑ (Var (Rnat → Rnat))).
Defined.



End AvareXY.

Section Avarenat.

Notation "r ® γ" := (Inst (BranchingNat nat nat) nat r γ) (at level 10, left associativity).
Notation "()" := (ctx_nil (BranchingNat nat nat) nat).
Notation "Γ · A" := (Ctx_cons (BranchingNat nat nat) nat Γ A) (at level 50, left associativity).
Notation "γ ·· x" := (ctx_cons (BranchingNat nat nat) nat γ x) (at level 50, left associativity).
Notation "◊ g" := (Ctx_tl (BranchingNat nat nat) nat g) (at level 10).
Notation "¿ g" := (Ctx_hd (BranchingNat nat nat) nat g) (at level 10).
Notation "⇑ A" := (Lift (BranchingNat nat nat) nat A) (at level 10).
Notation "#" := (Var (BranchingNat nat nat) nat _) (at level 0).
Notation "↑ M" := (Weaken (BranchingNat nat nat) nat M) (at level 10).
Notation "S {{ r }} " := (Sub (BranchingNat nat nat) nat S r) (at level 20, left associativity).
Notation "∏" := (RPi (BranchingNat nat nat) nat).
Notation "A → B" := (RPi (BranchingNat nat nat) nat A (⇑ B)) (at level 99, right associativity, B at level 200).
Notation "'λ'" := (RLam (BranchingNat nat nat) nat).
Notation "t • u" := (RApp (BranchingNat nat nat) nat t u) (at level 12, left associativity).
Notation "'Ctx'" := (Ctx (BranchingNat nat nat) nat).
Notation "'RElt'" := (RElt (BranchingNat nat nat) nat).
Notation "'Rnat'" := (@Rnat (BranchingNat nat nat) nat).

Fixpoint Bplus (n m : BranchingNat nat nat) : BranchingNat nat nat :=
  match m with
  | B0 _ _ => n
  | BS k => Bplus (BS n) k
  | Bêtanat g x => Bêtanat (fun y => Bplus n (g y)) x
  end.
Print BNEta.
Fixpoint Bplus_nat (n : BranchingNat nat nat) (m : nat) : BranchingNat nat nat :=
  match n with
  | B0 _ _ => BNEta nat nat m
  | BS k => BS (Bplus_nat k m)
  | Bêtanat g x => Bêtanat (fun y => Bplus_nat (g y) m) x
  end.

Print Generic_aux.

Definition RGeneric_aux {Γ : Ctx} {α : (BranchingNat nat nat) -> nat} {γ : Γ α} (n : Ty_inst _ _ Rnat γ) (k : BranchingNat nat nat) :
  RParanat _ _ α (α (Bplus_nat k n.(q1))) (Generic_aux k n.(q2)).
Proof.
  unshelve refine (let fix f (na : nat) (nb : BranchingNat nat nat) (ne : RParanat nat nat α na nb) (k : nat) {struct ne} :=
                       match ne as me in RParanat _ _ _ ma mb
                             return RParanat _ _ α (α (k + ma)) (Generic_aux k mb)
                       with
                       | RP0 _ _ _ => RPbêta nat nat _ _ _ _ _
                       | RPS _ _ _ _ _ _ => _
                       | RPbêta _ _ _ _ _ _ _ => _
                       end
                         in f n.(q1) n.(q2) _ k).
  - rewrite - plus_n_O.
    refine (let fix g n := match n as n0
                                 return (RParanat nat nat α n0 (BNEta nat nat (n0)))
                           with
                           | 0 =>  (RP0 _ _ α)
                           | S k => RPS nat nat α k (BNEta nat nat k) (g k)
                           end
            in g (α k)).
  - rewrite - plus_n_Sm ; rewrite - plus_Sn_m.
    exact (f n0 b r (S k)).
  - simpl. apply RPbêta. now apply (f n0 (b (α n1)) r k) .
  - exact: n.(q3).
Defined.
    
  
Definition RGeneric {Γ : Ctx} : RElt Γ (Rnat → Rnat).
Proof.
  unshelve refine (fun α γ => exist3  _).
  - exact (fun n => α n.(q1)).
  - exact (fun n => Generic_aux 0 n.(q2)).
  - now refine (fun n => RGeneric_aux n 0).
Defined.      


Lemma NaturRadinPGeneric {Γ : Ctx} {α : nat -> nat} (n : RElt Γ Rnat)
  (γ : Γ α):
  BNdialogue ((RGeneric • n) ® γ).(q2) α = α (n ® γ).(q1). 
Proof.
  cbv.
  destruct (n α γ) as [no nt ne] ; simpl.
  have aux : forall (k : nat),
      BNdialogue (Generic_aux k nt) α = α (k + no).
  - elim: ne ; simpl.
    + intros k ; now rewrite NaturBNEtaDialogue ; rewrite - plus_n_O.
    + intros mt mo me ihme k.
      rewrite - plus_n_Sm ; rewrite - plus_Sn_m.
      exact: ihme.
    + intros f x mo me ihme k.
      now specialize ihme with k.
  - exact: aux.
Qed.

(*
Theorem continuity (f : RElt (Ctx_nil nat nat) ((Rnat → Rnat) → Rnat)) (α : nat -> nat) :
  continuous (fun p => (f p ()).(q1) p) α.
Proof.
  pose proof (@RParanat_equal nat nat (Ctx_nil nat nat) (f • RGeneric)) as Eq.
  destruct (BNDialogue_Continuity (f.(q2) (@ctx_nil nat nat α).(q1) (Generic_aux 0)) α)
    as [l ihl].
  exists l ; intros β Finite_Eq.
  simpl in Eq ; rewrite - (Eq α () ) ; rewrite - (Eq β () ).
  exact: ihl.
Qed.  
 *)

Lemma RParanat_equal2 {Γ : Ctx} (alpha : nat -> nat) (gamma : Γ alpha)
      (n : Ty_inst nat nat Rnat gamma) :
  BNdialogue n.(q2) alpha = n.(q1).
  + destruct n as [nt no ne] ; simpl.
    elim: ne ; trivial ; simpl.
    intros mo mt me ; apply eq_S.
Qed.


Definition EtaNat {Γ : Ctx} (n : nat) : RElt Γ Rnat.
Proof.
  induction n.
  - exact (R0 nat nat).
  - refine (RApp nat nat (RS nat nat) IHn).
Defined.


Definition EtaList {Γ : Ctx} (l : list nat) : RElt Γ ((Rlist nat nat) • Rnat).
Proof.
  induction l.
  - exact (Rnil nat nat).
  - unshelve refine (fun alpha gamma => _).
    refine (Rcons_ext nat nat (EtaNat a alpha gamma) (IHl alpha gamma)).
Defined.    


Definition Rtheorem {Γ : Ctx} :
  RElt Γ (∏ ((Rnat → Rnat) → Rnat)
            (∏ (Rnat → Rnat)
               (RApp nat nat (RApp nat nat (Rcontinuous nat nat) (↑ (Var nat nat ((Rnat → Rnat) → Rnat))))
               (Var nat nat (Rnat → Rnat))))).
Proof.
  unshelve refine (fun alpha gamma => exist3 _).
  - unshelve refine (fun f om => _).
    cbv.
    destruct (BNDialogue_Continuity (f.(q2) (RGeneric alpha gamma)) alpha)
      as [l ihl].
    unshelve refine (exist _).
    + exact (EtaList l alpha _).
    + intro om'.
      cbn.
Qed.


    pose proof (@RParanat_equal nat nat Γ (RApp_inst nat nat f (RGeneric alpha gamma))) as Eq.
    unshelve refine (exist _).
    About BNDialogue_Continuity.

    
End Avarenat.
