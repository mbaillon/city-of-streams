Require Import List.
Require Import Streams.
Require Import ssreflect.
Set Implicit Arguments.
Set Primitive Projections.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import Dialogue.

Section SystemT.
  
(*Definition des fonctions de base nécessaires à l'interprétation de System T + Streams*)

Fixpoint Rec {X : Type} (f : X -> X) (x : X) (n : nat) :=
  match n with
  | 0 => x
  | S k => f (Rec f x k)
  end.


Definition Kcomb {X Y : Type} (x : X) (y : Y) := x.


Definition Scomb {X Y Z : Type} (f : X -> Y -> Z) (g : X -> Y) (x : X) := f x (g x).


CoFixpoint Make {A B : Type} (f : A -> (A * B)) (a : A) :=
Cons (snd (f a)) (Make f (fst (f a))).


Lemma Make_Str_nth (n k : nat) (X : Type) (h : nat -> X) :
  Str_nth n (Make (fun n0 => (S n0, h n0)) k) = h (n + k).
Proof.
  unfold Str_nth ; elim: n k => [ | n ihn k] ; trivial.
  now rewrite (ihn (S k)) ; rewrite - plus_n_Sm ; rewrite - plus_Sn_m.
Qed.


(*Construction de System T*)


Inductive ty :=
| btnat : ty
| tnat : ty
| tarr : ty -> ty -> ty
| product : ty -> ty -> ty
| stream : ty -> ty.

Notation " A '→' B " := (tarr A B) (at level 30, right associativity) : t_scope.
Notation " A '#' B " := (product A B) (at level 30, right associativity) : t_scope.
Open Scope t_scope.


Inductive te : ty -> Type :=
| bz : te btnat
| bs : te (btnat → btnat)
| z    : te tnat
| s    : te (tnat → tnat)
| brec : forall (sigma : ty), te ((sigma → sigma) → sigma → btnat → sigma)
| rec  : forall (sigma : ty), te ((sigma → sigma) → sigma → tnat → sigma)
| tapp : forall (sigma tau : ty), te (sigma → tau) -> te sigma -> te tau
| Kte : forall (sigma tau : ty), te (sigma → tau → sigma)
| Ste : forall (ro sigma tau : ty), te ((ro → sigma → tau) → (ro → sigma) → ro → tau)
| make : forall (ro tau : ty), te ((ro →  (ro # tau)) → ro → (stream tau))
| hdte : forall (ro : ty), te ((stream ro) → ro)
| tlte : forall (ro : ty), te ((stream ro) → (stream ro)).



(*Traduction classique de System T + Streams*)


Fixpoint Transty (sigma : ty) : Type :=
  match sigma with
  | btnat => nat
  | tnat => nat
  | ro → tau => ((Transty ro) -> (Transty tau))
  | product ro tau => ((Transty ro) * (Transty tau))
  | stream ro => Stream (Transty ro)
  end.


Fixpoint Transte (sigma : ty) (t : te sigma) : Transty sigma :=
  match t as t0 in te sig0 return (Transty sig0) with
  | bz => 0
  | bs => S
  | z => 0
  | s => S
  | brec tau => @Rec (Transty tau)
  | rec tau => @Rec (Transty tau)
  | tapp f g => (Transte f ) (Transte g)
  | Kte ro tau => @Kcomb (Transty ro) (Transty tau)
  | Ste ro sigma tau => @Scomb (Transty ro) (Transty sigma) (Transty tau)
  | make ro tau => @Make (Transty ro) (Transty tau)
  | hdte ro => @hd (Transty ro)
  | tlte ro => @tl (Transty ro)
  end.


Definition T_definable {sigma : ty} (f : Transty sigma) : Type :=
  {x : te sigma & f = Transte x}.

End SystemT.


Section OracleSystemT.
  
Notation " A '→' B " := (tarr A B) (at level 30, right associativity) : t_scope.
Notation " A '#' B " := (product A B) (at level 30, right associativity) : t_scope.
Open Scope t_scope.


(*Définition et traduction classique de System T + Streams avec oracle *)

Inductive teom : ty -> Type :=
| bom : teom (stream btnat)
| om : teom (stream tnat)
| bzom : teom btnat
| bsom : teom (btnat → btnat)
| zom    : teom tnat
| som    : teom (tnat → tnat)
| brecom  : forall (sigma : ty), teom ((sigma → sigma) → sigma → btnat → sigma)
| recom  : forall (sigma : ty), teom ((sigma → sigma) → sigma → tnat → sigma)
| tappom : forall (sigma tau : ty), teom (sigma → tau) -> teom sigma -> teom tau
| Kteom : forall (sigma tau : ty), teom (sigma → tau → sigma)
| Steom : forall (ro sigma tau : ty), teom ((ro → sigma → tau) → (ro → sigma) → ro → tau)
| makeom : forall (ro tau : ty), teom ((ro →  (ro # tau)) → ro → (stream tau))
| hdteom : forall (ro : ty), teom ((stream ro) → ro)
| tlteom : forall (ro : ty), teom ((stream ro) → (stream ro)).


Fixpoint Transteom (sigma : ty) (t : teom sigma) (alpha : Stream nat) : Transty sigma :=
  match t with
  | bom => alpha
  | om => alpha
  | bzom => 0
  | bsom => S
  | zom => 0
  | som => S
  | brecom tau => @Rec (Transty tau)
  | recom tau => @Rec (Transty tau)
  | tappom f g => (Transteom f alpha) (Transteom g alpha)
  | Kteom ro tau => @Kcomb (Transty ro) (Transty tau)
  | Steom ro sigma tau => @Scomb (Transty ro) (Transty sigma) (Transty tau)
  | makeom ro tau => @Make (Transty ro) (Transty tau)
  | hdteom ro => @hd (Transty ro)
  | tlteom ro => @tl (Transty ro)
  end.


(*Inclusion de System T + Streams dans System T + Streams avec oracle*)

Fixpoint embed {sigma : ty} (t : te sigma) : teom sigma :=
  match t with
  | bz => bzom
  | bs => bsom
  | z => zom
  | s => som
  | brec tau => brecom tau
  | rec tau => recom tau
  | tapp f g => tappom (embed f) (embed g)
  | Kte ro tau => Kteom ro tau
  | Ste ro sigma tau => Steom ro sigma tau
  | make ro tau => makeom ro tau
  | hdte ro => hdteom ro
  | tlte ro => tlteom ro
  end.


Lemma preservation {sigma : ty} (t : te sigma) (alpha : Stream nat) :
  Transte t = Transteom (embed t) alpha.
Proof.
  induction t as [ | | | | | | a b c iht1 d iht2 | | | | | ] ; trivial ; simpl.
  now rewrite iht1 ; rewrite - iht2.
Qed.
  
  
End OracleSystemT.




Section DialogueOracleSystemT.

 
Notation " A '→' B " := (tarr A B) (at level 30, right associativity) : t_scope.
Notation " A '#' B " := (product A B) (at level 30, right associativity) : t_scope.
Open Scope t_scope.


(*Traduction dialoguée des types de System T + Streams*)


Fixpoint DialogueTransty (sigma : ty) : Type :=
  match sigma with
  | btnat => StreamBranchingNat nat
  | tnat => (NatStreamDialogue nat)
  | ro → tau => (DialogueTransty ro) -> (DialogueTransty tau)
  | ro # tau => ((DialogueTransty ro) * (DialogueTransty tau))
  | stream ro => Stream (DialogueTransty ro)
  end.


(*Généralisation de l'extension de Kleisli, pour pouvoir interpréter les types de System T + Streams dans la traduction dialoguée*)


Fixpoint GenBind {sigma : ty} :=
  match sigma as sig0 return
        (nat -> (DialogueTransty sig0)) -> (NatStreamDialogue nat) -> (DialogueTransty sig0)
  with
  | tnat => @Bind nat nat nat _
  | btnat => fun f d => BNBind f (DialoguetoBN d)
  | ro → tau => fun f d x => GenBind (fun y => f y x) d
  | ro # tau => fun f d => (GenBind (fun y => fst (f y)) d, GenBind (fun y => snd (f y) ) d)
  | stream ro => fun (f : nat -> Stream (DialogueTransty ro)) d =>
                   Make (fun n => (S n, GenBind (fun x => Str_nth n (f x)) d)) 0
  end.

Fixpoint GenBNBind {sigma : ty} :=
  match sigma as sig0 return
        (nat -> (DialogueTransty sig0)) -> (StreamBranchingNat nat) -> (DialogueTransty sig0)
  with
  | tnat => fun f b => (Bind f (BNtoDialogue b))
  | btnat => @BNBind nat nat
  | ro → tau => fun f d x => GenBNBind (fun y => f y x) d
  | ro # tau => fun f d => (GenBNBind (fun y => fst (f y)) d, GenBNBind (fun y => snd (f y) ) d)
  | stream ro => fun (f : nat -> Stream (DialogueTransty ro)) d =>
                   Make (fun n => (S n, GenBNBind (fun x => Str_nth n (f x)) d)) 0
  end.


Definition Rec' {sigma : ty} (f : (DialogueTransty sigma) -> (DialogueTransty sigma))
           (x : DialogueTransty sigma) :=
  GenBind (Rec f x).


Definition Brec {sigma : ty} (f : (DialogueTransty sigma) -> (DialogueTransty sigma))
           (x : DialogueTransty sigma) : (StreamBranchingNat nat -> (DialogueTransty sigma)) :=
  GenBNBind (Rec f x).


(*Création de Generic, crucial pour la preuve de continuité*) 


Definition Generic_tree (n : nat) : NatStreamDialogue nat :=
  bêta (fun k => êta _ _ k) n.


Definition BNGeneric_tree (n : nat) : StreamBranchingNat nat :=
  Bêtanat (BNEta _ _) n.


Definition Generic_stream : Stream (NatStreamDialogue nat) :=
  Make (fun n => (S n, Generic_tree n)) 0.


Definition BNGeneric_stream : Stream (StreamBranchingNat nat) :=
  Make (fun n => (S n, BNGeneric_tree n)) 0.


Lemma Generic_stream_nth (n : nat) :
    Str_nth n (Generic_stream) = Generic_tree n.
Proof.
  have Hyp (k m : nat) :
    Str_nth k (Make (fun n0 : nat => (S n0, Generic_tree n0)) m) =
    Generic_tree (k + m).
  - unfold Str_nth ; elim: k m => [ | k ihk m] ; trivial ; simpl.
    now rewrite (ihk (S m)) ; rewrite plus_n_Sm.
  - unfold Generic_stream ; simpl.    
    now rewrite (Hyp n 0) ; rewrite - plus_n_O.
Qed.


Lemma BNGeneric_stream_nth (n : nat) :
    Str_nth n (BNGeneric_stream) = BNGeneric_tree n.
Proof.
  have Hyp (k m : nat) :
    Str_nth k (Make (fun n0 : nat => (S n0, BNGeneric_tree n0)) m) =
    BNGeneric_tree (k + m).
    - unfold Str_nth ; elim: k m => [ | k ihk m] ; trivial ; simpl.
      now rewrite (ihk (S m)) ; rewrite plus_n_Sm.
    - unfold BNGeneric_stream ; simpl.    
      now rewrite (Hyp n 0) ; rewrite - plus_n_O.
Qed.


(*Commutation du diagramme attendu*)

Lemma NaturGeneric_stream (alpha : Stream nat) (n : nat) :
  Str_nth n alpha = stream_dialogue (Str_nth n Generic_stream) alpha.
Proof.
  now rewrite (Generic_stream_nth n).
Qed.


Lemma NaturBNGeneric_stream (alpha : Stream nat) (n : nat) :
  Str_nth n alpha = stream_BNdialogue (Str_nth n BNGeneric_stream) alpha.
Proof.
  rewrite (BNGeneric_stream_nth n).
  unfold stream_BNdialogue ; unfold BNGeneric_tree ; simpl.
  now rewrite (NaturBNEtaDialogue (Str_nth n alpha)).
Qed.

  
(*Traduction dialoguée des termes de System T + Streams avec oracle*)


Fixpoint DialogueTransteom {sigma : ty} (t : teom sigma) : (DialogueTransty sigma) :=
  match t in teom tau return (DialogueTransty tau)
  with
  | bom => BNGeneric_stream
  | om => Generic_stream
  | bzom => B0 _ _
  | zom => zero' _
  | bsom => @BS _ _
  | som => @S' _
  | brecom tau => @Brec tau
  | recom tau => @Rec' tau
  | tappom f g => (DialogueTransteom f) (DialogueTransteom g)
  | Kteom ro tau => @Kcomb (DialogueTransty ro) (DialogueTransty tau)
  | Steom ro sigma tau => @Scomb (DialogueTransty ro) (DialogueTransty sigma) (DialogueTransty tau)
  | makeom ro tau => @Make (DialogueTransty ro) (DialogueTransty tau)
  | hdteom ro => @hd (DialogueTransty ro)
  | tlteom ro => @tl (DialogueTransty ro)
  end.


Definition Dialogue_term (t : te ((stream tnat) → tnat)) : NatStreamDialogue nat :=
  DialogueTransteom (tappom (embed t) om).


Definition BNDialogue_term (t : te ((stream btnat) → btnat)) : StreamBranchingNat nat :=
  DialogueTransteom (tappom (embed t) bom).


End DialogueOracleSystemT.






Section ContinuitySystemT.


Notation " A '→' B " := (tarr A B) (at level 30, right associativity) : t_scope.
Notation " A '#' B " := (product A B) (at level 30, right associativity) : t_scope.
Open Scope t_scope.

(*Définition de la relation reliant les deux traductions*)


Fixpoint Rel (sigma : ty) : ((Stream nat) -> Transty sigma) -> (DialogueTransty sigma) -> Type :=
  match sigma as sig0 return ((Stream nat) -> Transty sig0) -> (DialogueTransty sig0) -> Type
  with
  | btnat => fun f x => forall (alpha : (Stream nat)), f alpha = stream_BNdialogue x alpha
  | tnat => fun f x => forall (alpha : (Stream nat)), f alpha = stream_dialogue x alpha
  | ro → tau => fun f x => (forall (g : (Stream nat) -> Transty ro) (g' : DialogueTransty ro),
                               Rel g g' ->
                               Rel (fun alpha => f alpha (g alpha)) (x g'))
  | ro # tau => fun f x => (prod (Rel (fun alpha => fst (f alpha)) (fst x))
                                 (Rel (fun alpha => snd (f alpha)) (snd x)))
  | stream ro => fun f x => ((forall (n : nat),
                                 Rel (fun alpha => Str_nth n (f alpha)) (Str_nth n x)))
  end.


(*Lemme technique de propagation de cette relation*)


Lemma Rel_Kleisli (sigma : ty)
         (g : nat -> (Stream nat) -> Transty sigma)
         (g' : nat -> DialogueTransty sigma) :
    (forall (k : nat), Rel (g k) (g' k)) ->
    forall (i : (Stream nat) -> nat) (j : NatStreamDialogue nat),
      @Rel tnat i j ->
      Rel (fun alpha => g (i alpha) alpha) (GenBind g' j).
Proof.
  intros Relk i j Relij.
  induction sigma.
    (*Cas btnat*)
  - intro alpha.
    rewrite (Relk (i alpha) alpha) ; rewrite Relij.
    unfold stream_BNdialogue ; unfold stream_dialogue.
    rewrite (NaturBNBindDialogue g' (fun n => Str_nth n alpha) (DialoguetoBN j)).
    rewrite (EqDialogueBNdialogue (fun n => Str_nth n alpha)j).
    reflexivity.
    (*Cas tnat*)
  - intro alpha.
    rewrite Relij ; rewrite Relk.
    unfold stream_dialogue ; rewrite NaturBindDialogue.
    reflexivity.
  (*Cas ro → tau*)
  - intros f f' Relf.
    apply (IHsigma2 (fun n alpha => g n alpha (f alpha)) (fun n => g' n f')).
    exact (fun k => Relk k f f' Relf).
  (*Cas ro # tau*)
  - split.
    + apply (IHsigma1  (fun n alpha => fst (g n alpha)) (fun n => fst (g' n))).
      now apply Relk.
    + apply (IHsigma2  (fun n alpha => snd (g n alpha)) (fun n => snd (g' n))).
      now apply Relk.
  (*Cas stream tau*)
  - intro n ; simpl ; simpl in Relk.
    rewrite Make_Str_nth ; rewrite - plus_n_O.
    apply (IHsigma (fun m s => Str_nth n (g m s)) (fun d => Str_nth n (g' d))).
    exact (fun k => Relk k n).
Qed.


Lemma BNRel_Kleisli (sigma : ty)
      (g : nat -> (Stream nat) -> Transty sigma)
      (g' : nat -> DialogueTransty sigma) :
    (forall (k : nat), Rel (g k) (g' k)) ->
    forall (i : (Stream nat) -> nat) (j : StreamBranchingNat nat),
      @Rel btnat i j ->
      Rel (fun alpha => g (i alpha) alpha) (GenBNBind g' j).
Proof.
  intros Relk i j Relij.
  induction sigma.
    (*Cas btnat*)
  - intro alpha.
    rewrite (Relk (i alpha) alpha) ; rewrite Relij.
    unfold stream_BNdialogue ; unfold stream_dialogue.
    now rewrite - NaturBNBindDialogue.
    (*Cas tnat*)
  - intro alpha.
    rewrite Relij ; rewrite Relk.
    unfold stream_BNdialogue ; unfold stream_dialogue.
    now rewrite NaturBindDialogue ; rewrite EqBNdialogueDialogue.
  (*Cas ro → tau*)
  - intros f f' Relf.
    apply (IHsigma2 (fun n alpha => g n alpha (f alpha)) (fun n => g' n f')).
    exact (fun k => Relk k f f' Relf).
  (*Cas ro # tau*)
  - split.
    + apply (IHsigma1  (fun n alpha => fst (g n alpha)) (fun n => fst (g' n))).
      now apply Relk.
    + apply (IHsigma2  (fun n alpha => snd (g n alpha)) (fun n => snd (g' n))).
      now apply Relk.
  (*Cas stream tau*)
  - intro n ; simpl ; simpl in Relk.
    rewrite Make_Str_nth ; rewrite - plus_n_O.
    apply (IHsigma (fun m s => Str_nth n (g m s)) (fun d => Str_nth n (g' d))).
    exact (fun k => Relk k n).
Qed.


(*Lemme principal*)

Lemma main {sigma : ty} (t : teom sigma) :
    Rel (Transteom t) (DialogueTransteom t).
Proof.
  induction t ; simpl ; trivial.
  (*Cas Bomega*)
  - exact (fun n alpha => NaturBNGeneric_stream alpha n).
  (*Cas Omega*)
  - exact ( fun n alpha => NaturGeneric_stream alpha n).
  (*Cas Sun*)
  - simpl ; unfold stream_BNdialogue ; simpl.
    intros g g' eqalpha alpha.
    now apply eq_S.
  (*Cas S*)
  - unfold stream_dialogue ; unfold S'; simpl.
    intros g g' Hyp alpha.
    rewrite - NaturMapDialogue.
    now rewrite Hyp.
    (*Cas Brec*)
  - intros g g' Hyp f f' Relf i j Relij.
    apply (@BNRel_Kleisli sigma (fun n alpha => Rec (g alpha) (f alpha) n)
                          (Rec g' f')) ; trivial.
    induction k as [ | k ihk]; trivial ; simpl.
    exact: Hyp.
      (*Cas Rec*)
  - intros g g' Hyp f f' Relf i j Relij.
    unfold Rec'.
    apply (@Rel_Kleisli sigma (fun n alpha => Rec (g alpha) (f alpha) n)
                        (Rec g' f')) ; trivial.
    induction k as [ | k ihk] ; trivial ; simpl.
    exact: Hyp.
  (*Cas App*)
  - now apply IHt1.
    (*Cas Scomb*)
  - intros g g' Hyp f f' HypKleisli h h' Relh.
    apply (Hyp h h' Relh).
    exact: HypKleisli.
    (*Cas Make*)
  - intros g g' Hyp f f' Relf n.
    elim: n g g' Hyp f f' Relf => [ | n ihn] ; intros.
    + now apply Hyp.
    + apply (ihn g g' Hyp (fun x => fst (g x (f x))) (fst (g' f'))).
      exact (fst (Hyp f f' Relf)).
      (*Cas hd*)
  - intros g g' Hypn ; exact (Hypn 0).
    (*Cas tl*)
  - intros g g' Hypn n ; exact (Hypn (S n)).
Qed.

(*Preuve de correspondance des deux traductions*)


Lemma dialogue_tree_correct :
  forall (t : te ((stream tnat) → tnat)) (alpha : Stream nat),
    (Transte t) alpha = stream_dialogue (Dialogue_term t) alpha.
Proof.
  intros t alpha.
  rewrite (preservation t alpha).
  apply (main (embed t) (fun alpha => alpha) (Generic_stream)).
  exact (fun n bêta => NaturGeneric_stream bêta n).
Qed.


Lemma BNdialogue_tree_correct :
  forall (t : te ((stream btnat) → btnat)) (alpha : Stream nat),
    (Transte t) alpha = stream_BNdialogue (BNDialogue_term t) alpha.
Proof.
  intros t alpha.
  rewrite (preservation t alpha).
  apply (main (embed t) (fun alpha => alpha) (BNGeneric_stream)).
  exact (fun n bêta => NaturBNGeneric_stream bêta n).
Qed.


(*Énoncé et démonstration du théorème attendu*)


Theorem Definable_continuous (f : (Stream nat) -> nat) :
  @T_definable ((stream tnat) → tnat) f ->
  forall (alpha :Stream nat), stream_continuous f alpha.
Proof.
  intros Hyp alpha ; destruct Hyp as [t eqt].
  apply stream_eloquent_continuous.
  exists (Dialogue_term t) ; intro bêta.
  now rewrite eqt ; rewrite (dialogue_tree_correct).
Qed.


Theorem BDefinable_continuous (f : (Stream nat) -> nat)  :
  @T_definable ((stream btnat) → btnat) f ->
  forall (alpha :Stream nat), stream_continuous f alpha.
Proof.
  intros Hyp alpha ; destruct Hyp as [t eqt].
  apply (stream_BNeloquent_continuous nat).
  exists (BNDialogue_term t) ; intro bêta.
  now rewrite eqt ; rewrite (BNdialogue_tree_correct).
Qed.


End ContinuitySystemT.
