Set Definitional UIP.
Set Primitive Projections.
Require Import Base.
Require Import Dialogue.
Section BranchingTranslation.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Printing Width 90.
Variables (I : Set).
Variable (O : Set).
Variable (alpha : I -> O).

Section BlablaSProp.

Inductive True : SProp := T.
Inductive False : SProp :=.
Inductive and (A : SProp) (B : SProp) : SProp := conj : A -> B -> and A B.

Inductive eqn (A : Type) (x : A) : A -> SProp := refl : @eqn A x x.

Definition Sym (A : Type) (x y : A) (e : eqn x y) : eqn y x :=
  match e with | refl _ => refl _ end.

Definition Trans (A : Type) (x y z : A) (e : eqn x y) (e' : eqn y z) : eqn x z :=
  match e, e' with |refl _, refl _ => refl _ end.


Definition Rw (A B : Type) (e : eqn A B) (x : A) : B :=
  match e with | refl _ => x end.


Definition SRw (A B : SProp) (e : eqn A B) (x : A) : B :=
  match e with | refl _ => x end.

Definition Ap (A B : Type) (f : A -> B) (x y : A) (e : eqn x y) :
  eqn (f x) (f y) :=
  match e with
  | refl _ => refl (f x)
  end.

Definition ApD (A : Type) (B : A -> Type) (f : forall x, B x) (x y : A) (e : eqn x y) :
  eqn (Rw (Ap B e) (f x)) (f y) :=
  match e with
  | refl _ => refl _
  end.

Definition SAp (A : Type) (f : A -> SProp) (x y : A) (e : eqn x y) :
  eqn (f x) (f y) :=
  match e with
  | refl _ => refl (f x)
  end.


Definition Rw_sym1 (A B : Type) (e : eqn A B) (x: A):  (eqn (Rw (Sym e) (Rw e x)) x) :=
  match e with | refl _ => refl _ end.


Definition Rw_sym2 (A B : Type) (e : eqn A B) (x: B):  (eqn (Rw e (Rw (Sym e) x)) x).
Proof.
  now destruct e.
Defined.

Definition Rw_e_syme (A B : Type) (eq1 : eqn A B) (a : A) (b : B) (eq2 : eqn (Rw eq1 a) b) :
  eqn a (Rw (Sym eq1) b).
Proof.
  now destruct eq1.
Defined.

Definition Rw_syme_e (A B : Type) (eq1 : eqn A B) (a : A) (b : B) (eq2 : eqn a (Rw (Sym eq1) b)) :
  eqn (Rw eq1 a) b .
Proof.
  now destruct eq1.
Defined.

Definition Rw_trans (A B C : Type) (eq1 : eqn A B) (eq2 : eqn B C) (x : A) :
  eqn (Rw eq2 (Rw eq1 x)) (Rw (Trans eq1 eq2) x).
Proof.
  destruct eq1.
  now destruct eq2.
Defined.
  

Lemma Rw_app_sym1 (A B C : Type) (f : A -> C) (g : B -> C)
      (eq : eqn A B)
      (e : forall x, eqn (f x) (g (Rw eq x))) :
  forall x , eqn (f (Rw (Sym eq) x)) (g x).
Proof.
  intro x.
  destruct eq.
  exact (e x).
Defined.

Lemma Rw_app_sym2 (A B C : Type) (f : A -> C) (g : B -> C)
      (eq : eqn A B)
      (e : forall x , eqn (f (Rw (Sym eq) x)) (g x)) :
  forall x, eqn (f x) (g (Rw eq x)).
Proof.
  intro x.
  destruct eq.
  exact (e x).
Defined.


Definition f_Sequal (A B : Type) (f g : A -> B) (e : eqn f g)
           (x : A) : eqn (f x) (g x) :=
  match e in (eqn _ h) return (eqn (f x) (h x)) with
  | refl _ => refl (f x)
  end.

Definition Df_Sequal (A : Type) (B : A -> Type)
           (f g : forall x : A, B x) (e : eqn f g)
           (x : A) : eqn (f x) (g x) :=
  match e in (eqn _ h) return (eqn (f x) (h x)) with
  | refl _ => refl (f x)
  end.

Definition SDf_Sequal (A : SProp) (B : A -> Type)
           (f g : forall x : A, B x) (e : eqn f g)
           (x : A) : eqn (f x) (g x) :=
  match e in (eqn _ h) return (eqn (f x) (h x)) with
  | refl _ => refl (f x)
  end.

Lemma BiRw_app_sym1 (A B C : Type)
      (Ae : A -> SProp)
      (Be : B -> SProp)
      (f : forall (x : A), Ae x -> C)
      (g : forall (x : B), Be x -> C)
      (eq : eqn A B)
      (eq2 : eqn Ae (fun x => Be (Rw eq x)))
      (e : forall x xe, eqn (f x xe) (g (Rw eq x) (SRw (f_Sequal eq2 x) xe))) :
  forall (x : B) (xe : Be x),
    eqn (f (Rw (Sym eq) x) (SRw (Sym (Rw_app_sym1 (g:= Be) (eq:= eq) (f_Sequal eq2) x)) xe))
        (g x xe).
Proof.
  intros.
  destruct eq.
  simpl in *.
  change (eqn Ae Be) in eq2.
  destruct eq2.
  exact (e x xe).
Defined.

Lemma BiRw_app_sym2 (A B C : Type)
      (Ae : A -> SProp)
      (Be : B -> SProp)
      (f : forall (x : A), Ae x -> C)
      (g : forall (x : B), Be x -> C)
      (eq : eqn A B)
      (eq2 : eqn Ae (fun x => Be (Rw eq x)))
      (e:  forall (x : B) (xe : Be x),
    eqn (f (Rw (Sym eq) x) (SRw (Sym (Rw_app_sym1 (g:= Be) (eq:= eq) (f_Sequal eq2) x)) xe))
        (g x xe)) :
  forall x xe, eqn (f x xe) (g (Rw eq x) (SRw (f_Sequal eq2 x) xe)).
Proof.
  intros.
  destruct eq.
  simpl in *.
  change (eqn Ae Be) in eq2.
  destruct eq2.
  exact (e x xe).
Defined.

  
Axiom F : False.
Ltac admit := case F.


Axiom Funext :
  forall (A B : Type) (f g : A -> B) (e : forall x, eqn (f x) (g x)), eqn f g.

Axiom SFunext :
  forall (A : SProp) (B : Type) (f g : A -> B) (e : forall x, eqn (f x) (g x)), eqn f g.

Axiom DFunext :
  forall (A : Type) (B : A -> Type)
         (f g : forall x : A, B x)
         (e : forall x, eqn (f x) (g x)), eqn f g.

Axiom SDFunext :
  forall (A : SProp) (B : A -> Type)
         (f g : forall x : A, B x)
         (e : forall x, eqn (f x) (g x)), eqn f g.

Lemma GFunext1 : forall (A: Type) (B1 B2 : A->Type)
                        (eq2 : eqn B1 B2)
                        (f : forall x : A, B1 x) (g : forall x : A, B2 x)
                        (eq3: forall x : A, eqn (Rw (f_Sequal eq2 x) (f x)) (g x)),
    eqn (fun x => (Rw (f_Sequal eq2 x) (f x))) g.
Proof.
  intros.
  destruct eq2.
  simpl in *.
  exact (DFunext eq3).
Qed.

Lemma GFunext2 : forall (A1 A2 : Type) (B1 : A1->Type) (B2 : A2 -> Type)
          (eq1 : eqn A1 A2) (eq2 : eqn (fun x => B1 (Rw (Sym eq1) x)) B2)
          (f : forall x : A1, B1 x) (g : forall x : A2, B2 x)
          (eq3: forall x : A2, eqn (Rw (f_Sequal eq2 x) (f (Rw (Sym eq1) x))) (g x)),
    eqn (fun x => (Rw (f_Sequal eq2 x) (f (Rw (Sym eq1) x)))) g.
Proof.
  intros.
  destruct eq2.
  destruct eq1.
  simpl in *.
  exact (DFunext eq3).
Qed.



Lemma Piext :
  forall (A1 A2 : Type) (B1 : A1 -> Type) (B2 : A2 -> Type)
         (eq1 : eqn A1 A2) (eq2 : eqn B1 (fun x => B2 (Rw eq1 x))),
           eqn (forall x : A1, B1 x) (forall x : A2, B2 x).
Proof.
  intros.
  destruct (Sym eq2).
  now destruct eq1.
Defined.

Lemma PiSext :
  forall (A1 A2 : Type) (B1 : A1 -> SProp) (B2 : A2 -> SProp)
         (eq1 : eqn A1 A2) (eq2 : eqn B1 (fun x => B2 (Rw eq1 x))),
           eqn (forall x : A1, B1 x) (forall x : A2, B2 x).
Proof.
  intros.
  destruct (Sym eq2).
  now destruct eq1.
Defined.

Lemma SPiext :
  forall (A1 A2 : SProp) (B1 : A1 -> Type) (B2 : A2 -> Type)
         (eq1 : eqn A1 A2) (eq2 : eqn B1 (fun x => B2 (SRw eq1 x))),
           eqn (forall x : A1, B1 x) (forall x : A2, B2 x).
Proof.
  intros.
  destruct (Sym eq2).
  now destruct eq1.
Defined.

Lemma SPiSext :
  forall (A1 A2 : SProp) (B1 : A1 -> SProp) (B2 : A2 -> SProp)
         (eq1 : eqn A1 A2) (eq2 : eqn B1 (fun x => B2 (SRw eq1 x))),
           eqn (forall x : A1, B1 x) (forall x : A2, B2 x).
Proof.
  intros.
  destruct (Sym eq2).
  now destruct eq1.
Defined.

Lemma BiPiext (A : Type) (Ae : A -> A -> SProp) (B : forall (x : A) (xe : Ae x x), Type)
      (f : forall (x : A) (xe : Ae x x), B x xe)
      (C : Type)
      (Ce : C -> C -> SProp)
      (D : forall (x : C) (xe : Ce x x), Type)
      (eq1 : eqn A C)
      (eq2 : forall x : A, eqn (Ae x x) (Ce (Rw eq1 x) (Rw eq1 x)))
      (eq3 : forall (x : A) (xe : Ae x x), eqn (B x xe) (D (Rw eq1 x) (SRw (eq2 x) xe))) :
  eqn (forall (x : A) (xe : Ae x x), B x xe) (forall (x : C) (xe : Ce x x), D x xe).
Proof.
  unshelve refine (Piext _).
  - exact eq1.
  - refine (Funext (fun x => _)).
    unshelve refine (SPiext _).
    + exact (eq2 x).
    + refine (SFunext (fun xe => _)).
      exact (eq3 x xe).
Defined.  
  
  

Lemma Rw_fun (A : Type) (B : A -> Type)
      (f : forall x : A, B x)
      (C : Type)
      (D : C -> Type)
      (eq1 : eqn C A)
      (eq2 : forall x : C, eqn (B (Rw eq1 x)) (D x))
      (eq : eqn (forall x : A, B x) (forall x : C, D x))
      (c : C) :
  eqn (Rw eq f c) (Rw (eq2 c) (f (Rw eq1 c))).
Proof.
  change (eqn (Rw (@Piext _ _ _ _ (Sym eq1) (Funext (Rw_app_sym2 eq2))) f c)
              (Rw (f_Sequal (Funext eq2) c) (f (Rw eq1 c)))).
  clear eq.
  destruct eq1 ; simpl in *.
  now destruct (Funext eq2).
Defined.
Print Funext.




Lemma Rw_Bifun (A : Type) (Ae : A -> A -> SProp) (B : forall (x : A) (xe : Ae x x), Type)
      (f : forall (x : A) (xe : Ae x x), B x xe)
      (C : Type)
      (Ce : C -> C -> SProp)
      (D : forall (x : C) (xe : Ce x x), Type)
      (eq1 : eqn C A)
      (eq2 : forall x : C, eqn (Ce x x) (Ae (Rw eq1 x) (Rw eq1 x)))
      (eq3 : forall (x : C) (xe : Ce x x), eqn (B (Rw eq1 x) (SRw (eq2 x) xe)) (D x xe))
      (eq : eqn (forall (x : A) (xe : Ae x x), B x xe) (forall (x : C) (xe : Ce x x), D x xe))
      (c : C) (ce : Ce c c):
  eqn (Rw eq f c ce) (Rw (eq3 c ce) (f (Rw eq1 c) (SRw (eq2 c) ce))).
Proof.
  change (forall (x : C) (xe : Ce x x), eqn (B (Rw eq1 x)
                                               (SRw ((f_Sequal (Funext eq2)) x) xe))
                                            (D x xe)) in eq3.
  change (eqn (Rw (@Piext _ _ (fun x : A => forall xe : Ae x x, B x xe)
                          (fun x : C => forall xe : Ce x x, D x xe) (Sym eq1)
                          ((Funext (@Rw_app_sym2 _ _ _ (fun x : A => forall xe : Ae x x, B x xe)
                          (fun x : C => forall xe : Ce x x, D x xe) (Sym eq1)
                          (fun c => Sym (SPiext (Df_Sequal (DFunext (fun x : C => (Sym (SFunext (fun xe : Ce x x => eq3 x xe))))) c)))))))
                  f c ce)
              (Rw (Sym (SDf_Sequal (Df_Sequal (DFunext
                                                 (fun (x : C) =>
                                                    (Sym (SFunext (fun xe => eq3 x xe))))) c) ce))
                  (f (Rw eq1 c) (SRw ((f_Sequal (Funext eq2)) c) ce)))).
  revert eq3.
  generalize (Funext eq2) ; clear eq2 ; intros eq2 eq3.
  generalize (DFunext (fun (x : C) => (Sym (SFunext (fun xe => eq3 x xe))))).
  clear eq3 ; intro eq3.
  destruct eq1 ; simpl in *.
  clear eq.
  revert f.
  change (eqn D
              (fun (x : C) (xe : Ce x x) => B x (@SRw ((fun x : C => Ce x x) x)
                                                     ((fun x : C => Ae x x) x)
                                                     (f_Sequal eq2 x) xe)))
         in eq3.
  change (forall f : forall (x : C) (xe : (fun x : C => Ae x x) x), B x xe,
             eqn
               (Rw
                  (@Piext C C (fun x : C => forall xe : (fun x : C => Ae x x) x,
                                  B x xe)
                         (fun x : C => forall xe : (fun x : C => Ce x x) x, D x xe)
                         (refl C)
                         (Sym
                            (@Funext C Type
                                    (fun c0 : C => forall x : (fun x : C => Ce x x) c0, D c0 x)
                                    (fun c0 : C => forall x : (fun x : C => Ae x x) c0, B c0 x)
                                    (fun c0 : C =>
                                       @SPiext ((fun x : C => Ce x x) c0)
                                              ((fun x : C => Ae x x) c0)
                                              (D c0) (B c0)
                                              (f_Sequal eq2 c0)
                                              (@Df_Sequal _
                                                         (fun x : C => ((fun x : C => Ce x x) x)
                                                                       -> Type) _
                                                         (fun (x : C)
                                                              (xe : (fun x : C => Ce x x) x) =>
                                                            B x (@SRw ((fun x : C => Ce x x) x)
                                                                     ((fun x : C => Ae x x) x)
                                                                     (f_Sequal eq2 x) xe))
                                                         eq3 c0)))))
                  f c ce)
               (Rw
                  (Sym
                     (@SDf_Sequal ((fun x : C => Ce x x) c)
                                 (fun _ : (fun x : C => Ce x x) c => Type) (D c)
                                 (fun xe : (fun x : C => Ce x x) c =>
                                    B c (@SRw ((fun x : C => Ce x x) c)
                                             ((fun x : C => Ae x x) c)
                                             (f_Sequal eq2 c) xe))
                                 (@Df_Sequal C
                                            (fun x : C => (fun x : C => Ce x x) x -> Type) D
                                            (fun (x : C) (xe : (fun x : C => Ce x x) x) =>
                                               B x (@SRw ((fun x : C => Ce x x) x)
                                                        ((fun x : C => Ae x x) x)
                                                        (f_Sequal eq2 x) xe)) eq3 c) ce))
                  (f c (@SRw ((fun x : C => Ce x x) c)
                            ((fun x : C => Ae x x) c)
                            (f_Sequal eq2 c) ce)))).
  change (forall x : C, (fun x => Ae x x) x -> Type) in B.
  change (eqn D
          (fun (x : C) (xe : (fun x : C => Ce x x) x) =>
             B x
               (@SRw ((fun x0 : C => Ce x0 x0) x)
                    ((fun x0 : C => Ae x0 x0) x)
                    (f_Sequal eq2 x) xe))) in eq3.
  destruct eq2.
  change (eqn D B) in eq3.
  now destruct eq3.
Defined.


Lemma Rw_Bifun2 (A : Type) (Ae : A -> A -> SProp) (B : forall (x : A) (xe : Ae x x), Type)
      (f : forall (x : A) (xe : Ae x x), B x xe)
      (C : Type)
      (Ce : C -> C -> SProp)
      (D : forall (x : C) (xe : Ce x x), Type)
      (eq1 : eqn A C)
      (eq2 : forall x : A, eqn (Ae x x) (Ce (Rw eq1 x) (Rw eq1 x)))
      (eq3 : forall (x : A) (xe : Ae x x), eqn (B x xe) (D (Rw eq1 x) (SRw (eq2 x) xe)))
      (eq : eqn (forall (x : A) (xe : Ae x x), B x xe) (forall (x : C) (xe : Ce x x), D x xe))
      (a : A) (ae : Ae a a):
  eqn (Rw eq f (Rw eq1 a) (SRw (eq2 a) ae))
      (Rw (eq3 a ae) (f a ae)).
Proof.
  simpl.
  refine (Trans (@Rw_Bifun A Ae B f C Ce D (Sym eq1) (Rw_app_sym2 (fun x => Sym (eq2 x)))
                               (BiRw_app_sym1 (g:= D) (eq:= eq1) (eq2:= Funext eq2) eq3) eq
                               (Rw eq1 a) (@SRw (Ae a a) (Ce (Rw eq1 a) (Rw eq1 a)) (eq2 a) ae)) _).
  simpl.
  clear eq.
  generalize (((BiRw_app_sym1 (g:= D) (eq:= eq1) (eq2:= Funext eq2) eq3) _ (SRw (eq2 a) ae))).
  intro e.
  destruct eq1.
  simpl in *.
  change (eqn (B a (SRw (Sym (eq2 a)) (SRw (eq2 a) ae)))
              (D a (SRw (eq2 a) ae))) in e.
  revert e.
  generalize (eq3 a ae).
  clear eq3.
  generalize (eq2 a).
  clear eq2.
  intros eq1.
  generalize (D a (SRw eq1 ae)).
  clear D.
  intros D eq2 eq3.
  destruct eq1.
  simpl.
  reflexivity.
Defined.

Arguments SRw [_ _] _.
Arguments SDf_Sequal [_ _ _ _ ] _.
Arguments Df_Sequal [_ _ _ _ ] _.
Arguments Piext [_ _ _ _] _.
Arguments SPiext [_ _ _ _] _.
Arguments PiSext [_ _ _ _] _.
Arguments SPiSext [_ _ _ _] _.
Arguments Funext [_ _ _ _] _.

Record BranchingTYPE@{v} := {
  car : Type@{v} ;
  ask : forall (i : I) (f : O -> car), car ;
  rel : car -> car -> SProp;
  ask_rel : forall (x : car) (i : I) (f : O -> car)
                   (fe : rel x (f (alpha i))),
      rel x (ask i f);
                       }.
Arguments ask _ : clear implicits.
Arguments ask_rel _ : clear implicits.
Arguments rel _ : clear implicits.

Definition Eq_car (A B : BranchingTYPE) (e : eqn A B) : eqn A.(car) B.(car) :=
  match e with | refl _ => refl _ end.


Definition Eq_rel (A B : BranchingTYPE) (e : eqn A B) (x y : B.(car)) :
  eqn (A.(rel) (Rw (Sym (Eq_car e)) x) (Rw (Sym (Eq_car e)) y))
      (B.(rel) x y).
Proof.
  now destruct e.
Defined.

Definition Eq_rel2 (A B : BranchingTYPE) (e : eqn A B) (x y : A.(car)) :
  eqn (A.(rel) x y)
      (B.(rel) (Rw (Eq_car e) x) (Rw (Eq_car e) y)).
Proof.
  now destruct e.
Defined.
Lemma Eq_rel3 (A B : BranchingTYPE) (e : eqn A B)
      (a1 a2 : A.(car)) (b1 b2 : B.(car))
      (eq1 : eqn (Rw (Eq_car e) a1) b1)
      (eq2 : eqn (Rw (Eq_car e) a2) b2) :
  eqn (A.(rel) a1 a2)
      (B.(rel) b1 b2).
Proof.
  intros.
  destruct e.
  destruct eq1; destruct eq2.
  reflexivity.
Defined.

Definition Eq_ask (A B : BranchingTYPE) (i : I) (f : O -> A.(car)) (g : O -> B.(car))
                        (eq1 : eqn A B)
                        (eq2 : forall (o: O), eqn (Rw (Eq_car eq1) (f o)) (g o)) :
                    eqn (Rw (Eq_car eq1) (ask A i f)) (ask B i g).
Proof.
  intros.
  destruct eq1.
  now destruct (Funext eq2).
Defined.


Lemma BRw_fun (A : Type) (B : A -> BranchingTYPE)
      (f : forall x : A, (B x).(car))
      (C : Type)
      (D : C -> BranchingTYPE)
      (eq1 : eqn C A)
      (eq2 : forall x : C, eqn (B (Rw eq1 x)) (D x))
      (eq : eqn (forall x : A, (B x).(car)) (forall x : C, (D x).(car)))
      (c : C) :
  eqn (Rw eq f c) (Rw (Eq_car (eq2 c)) (f (Rw eq1 c))).
Proof.
  change (eqn (Rw (@Piext _ _ _ _ (Sym eq1) (Funext (Rw_app_sym2 (fun x => Eq_car (eq2 x))))) f c) (Rw (f_Sequal (Funext (fun x => Eq_car (eq2 x))) c) (f (Rw eq1 c)))).
  clear eq.
  destruct eq1 ; simpl in *.
  now destruct (Funext eq2).
Defined.

Lemma BRw_Bifun (A : Type) (Ae : A -> A -> SProp)
      (B : forall (x : A) (xe : Ae x x), BranchingTYPE)
      (f : forall (x : A) (xe : Ae x x), (B x xe).(car))
      (C : Type)
      (Ce : C -> C -> SProp)
      (D : forall (x : C) (xe : Ce x x), BranchingTYPE)
      (eq1 : eqn C A)
      (eq2 : forall x : C, eqn (Ce x x) (Ae (Rw eq1 x) (Rw eq1 x)))
      (eq3 : forall (x : C) (xe : Ce x x), eqn (B (Rw eq1 x) (SRw (eq2 x) xe)) (D x xe))
      (eq : eqn (forall (x : A) (xe : Ae x x), (B x xe).(car))
                (forall (x : C) (xe : Ce x x), (D x xe).(car)))
      (c : C) (ce : Ce c c):
  eqn (Rw eq f c ce) (Rw (Eq_car (eq3 c ce)) (f (Rw eq1 c) (SRw (eq2 c) ce))).
Proof.
  exact (@Rw_Bifun A Ae (fun x xe => (B x xe).(car)) f C Ce
       (fun x xe => (D x xe).(car)) eq1 eq2 (fun x xe => Eq_car (eq3 x xe)) eq c ce).
Defined.


Lemma Rw_Biask : (forall (A : Type) (Ae : A -> A -> SProp)
                      (B : forall (a : A) (ae : Ae a a), BranchingTYPE)
                      (C : Type) (Ce : C -> C -> SProp)
                      (D : forall (c : C) (ce : Ce c c), BranchingTYPE)
                      (i : I)
                      (f : O -> forall (a : A) (ae : Ae a a), car (B a ae))
                      (e : eqn (forall (c : C) (ce : Ce c c), car (D c ce))
                               (forall (a : A) (ae : Ae a a), car (B a ae)))
                      (eqCA : eqn C A)
                      (eqAeCe : forall (c1 c2 : C), eqn (Ce c1 c2) (Ae (Rw eqCA c1) (Rw eqCA c2)))
                      (he : forall (c : C) (ce : Ce c c),
                          eqn (B (Rw eqCA c) (SRw ((eqAeCe c c)) ce))
                              (D c ce)),
                  eqn (Rw e (fun (c : C) (ce : Ce c c) =>
                               ask (D c ce) i (fun o => (Rw (Sym e) (f o)) c ce)))
                      (fun (a : A) (ae : Ae a a) => ask (B a ae) i (fun o => f o a ae))).
Proof.
  intros.
  refine (DFunext (fun a => _)).
  refine (SDFunext (fun ae => _)).
  generalize ((Sym (@BiRw_app_sym2 _ _ _
                                   (fun x => Ae x x) (fun y => Ce y y) B D
                                   (Sym eqCA)
                                   (Funext (@Rw_app_sym2 _ _ _ (fun a => Ae a a) _ (Sym eqCA) (fun x => Sym (eqAeCe x x)))) he a ae))) ; intro eq.
  unshelve refine (@Trans _ _ _ _ (@BRw_Bifun C Ce D
                                              (fun (c : C) (ce : Ce c c) =>
                                                 ask (D c ce) i
                                                     (fun o : O => Rw (Sym e) (f o) c ce))
                                              A Ae B (Sym eqCA)
                                              (@Rw_app_sym2 _ _ _ (fun a => Ae a a) _
                                                            (Sym eqCA)
                                                            (fun x => Sym (eqAeCe x x)))
                                              (fun c ce => Sym
                                                             (@BiRw_app_sym2 _ _ _
                                                                             (fun x => Ae x x) (fun y => Ce y y) B D
                                                                             (Sym eqCA)
                                                                             (Funext (@Rw_app_sym2 _ _ _ (fun a => Ae a a) _ (Sym eqCA) (fun x => Sym (eqAeCe x x)))) he c ce)) e a ae) _).

  refine (@Eq_ask (D (Rw (Sym eqCA) a)
                     (SRw
                            (@Rw_app_sym2 _ _ _ _ _ (Sym eqCA)
                                         (fun x : C => Sym (eqAeCe x x)) a) ae))
                         (B a ae) i _ _ eq _).
  destruct eqCA ; simpl in *.
  generalize  (Sym (eqAeCe a a)).
  intro e0.
  change (eqn ((D a (SRw e0 ae))) ((B a ae))) in eq.
  intro o.
  refine (@Rw_syme_e _ _ (Eq_car eq) _ _ _).
  refine (Trans (@BRw_Bifun C Ae B (f o) C Ce D (refl C) (fun x => eqAeCe x x) he
                            (Sym e) a (SRw e0 ae)) _).
  clear e.
  now destruct (he a (SRw e0 ae)).
Defined.

Lemma PiDomCodomExt :
  forall (A1 A2 : BranchingTYPE)
         (B1 : forall (x : A1.(car)) (xe : A1.(rel) x x), BranchingTYPE)
         (B2 : forall (x : A2.(car)) (xe : A2.(rel) x x), BranchingTYPE)
         (eq : eqn (forall (x0 : car A1) (xe0 : rel A1 x0 x0), car (B1 x0 xe0))
                   (forall (x0 : car A2) (xe0 : rel A2 x0 x0), car (B2 x0 xe0)))
         (eq1 : eqn A1 A2)
         (eq2 : eqn (fun x xe => B1 (Rw (Sym (Eq_car eq1)) x)
                                    (SRw (Sym (Eq_rel eq1 x x)) xe)) B2)
         (f : forall (x : A1.(car)) (xe : A1.(rel) x x), (B1 x xe).(car)),
    eqn (Rw eq f) (fun (x : A2.(car)) (xe : A2.(rel) x x) =>
                     (Rw (Eq_car (SDf_Sequal (Df_Sequal eq2 x) xe)) (f (Rw (Sym (Eq_car eq1)) x) (SRw (Sym (Eq_rel eq1 x x)) xe)))).
Proof.
  intros.
  destruct eq2.
  destruct eq1.
  simpl.
  revert f.
  unfold Rw.
  destruct eq.
  exact (fun f => refl _ ).
Defined.



Definition BT_eq (A B : BranchingTYPE)
           (eq_car : eqn A.(car) B.(car))
           (eq_ask : eqn (fun i f => Rw eq_car (A.(ask) i (fun o => Rw (Sym eq_car) (f o))))
                         B.(ask))
           (eq_rel : eqn A.(rel)
                         (fun x y => B.(rel) (Rw eq_car x) (Rw eq_car y))) :
  eqn A B.
Proof.
  simpl.
  destruct A ; destruct B ; simpl in *.
  destruct eq_ask.
  destruct eq_car.
  destruct (Sym eq_rel).
  simpl.
  reflexivity.
Defined.

Record BCtx@{u v} : Type@{v} := {
  K : Type@{u};
  R : forall (t1 t2 : K), SProp;
                               }.
Coercion K : BCtx >-> Sortclass.
Arguments R _ : clear implicits.

Record Sub (Gamma Delta : BCtx) := {
  sub_fun : forall (gamma : Gamma) (gammae : Gamma.(R) gamma gamma), Delta;
  sub_rel : forall (g1 g2 : Gamma) (g1e : Gamma.(R) g1 g1) (g2e : Gamma.(R) g2 g2)
                   (e : Gamma.(R) g1 g2),
      Delta.(R) (sub_fun g1e) (sub_fun g2e);
  }.


Coercion sub_fun : Sub >-> Funclass.

Arguments sub_fun {_ _}.
Arguments sub_rel {_ _}.

Definition Idn {Γ : BCtx} : Sub Γ Γ.
Proof.
  unshelve econstructor.
  + unshelve refine (fun γ γε => γ).
  + unshelve refine (fun _ _ _ _ γε => γε).
Defined.

Definition Cmp {Γ Δ Ξ : BCtx} (σ : Sub Γ Δ) (ρ : Sub Δ Ξ) : Sub Γ Ξ.
Proof.
  unshelve econstructor.
  + unshelve refine (fun γ γε => ρ _ _).
    unshelve refine (σ _ γε).
    unshelve refine (σ.(sub_rel) _ _ _ _ γε).
  + unshelve refine (fun g1 g2 g1e g2e e => ρ.(sub_rel) _ _ _ _ _).
    exact (σ.(sub_rel) _ _ _ _ e).
Defined.

Lemma Idn_Cmp : forall {Γ Δ} (σ : Sub Γ Δ), Cmp Idn σ = σ.
Proof.
reflexivity.
Qed.

Lemma Cmp_Idn : forall {Γ Δ} (σ : Sub Γ Δ), Cmp σ Idn = σ.
Proof.
reflexivity.
Qed.

Lemma Cmp_Cmp : forall {Γ Δ Ξ Ω} (σ : Sub Γ Δ) (ρ : Sub Δ Ξ) (θ : Sub Ξ Ω),
  Cmp (Cmp σ ρ) θ = Cmp σ (Cmp ρ θ).
Proof.
reflexivity.
Qed.



Record BTy (Gamma : BCtx) : Type :=  {
  typ : forall (gamma : Gamma) (gammae : Gamma.(R) gamma gamma), BranchingTYPE;
  eps : forall (g1 g2 : Gamma) (g1e : Gamma.(R) g1 g1) (g2e : Gamma.(R) g2 g2)
               (e : Gamma.(R) g1 g2),
      eqn (typ g1e) (typ g2e);
                                   }.

Arguments eps [_] _.



Definition BU_aux@{u v | u < v} : BranchingTYPE@{v}.
Proof.
  unshelve refine (Build_BranchingTYPE@{v} _).
  - exact BranchingTYPE@{u}.
  - exact (fun i F => F (alpha i)).
  - exact (@eqn _).
  - simpl.
    exact (fun _ _ _ x => x).
Defined.

Definition BU (Gamma : BCtx) : BTy Gamma.
Proof.
  unshelve econstructor.
  - exact (fun _ _ => BU_aux). 
  - simpl.
    exact (fun _ _ _ _ _  => refl _ ).
Defined.


Definition Typ_subs {Γ Δ : BCtx} (σ : Sub Δ Γ) (A : BTy Γ) : BTy Δ.
Proof.
  unshelve econstructor.
  + unshelve refine (fun δ δε => A.(typ) (gamma:= (σ δ δε)) _).
    exact (σ.(sub_rel) δ δ δε δε δε).
  + unshelve refine (fun d1 d2 d1e d2e e => A.(eps) _ _ _ _ _).
    exact (σ.(sub_rel) _ _ _ _ e).
Defined.


Record Trm (Gamma : BCtx) (A : BTy Gamma) :=
  {
  elt : forall (g : Gamma) (ge : Gamma.(R) g g),
      (A.(typ) ge).(car);
  par : forall (g1 g2 : Gamma)
               (g1e : Gamma.(R) g1 g1)
               (g2e : Gamma.(R) g2 g2)
               (e : Gamma.(R) g1 g2),
      (A.(typ) g2e).(rel) (Rw (Eq_car (A.(eps) g1 g2 g1e g2e e)) (elt g1e))
                          (elt g2e);
  }.

Arguments Trm _ : clear implicits.
Arguments elt [_] [_] _  .
Arguments par [_] [_] _ .



Definition Trm_subs {Γ Δ : BCtx} (σ : Sub Δ Γ) {A : BTy Γ}
  (t : @Trm Γ A) : @Trm Δ (Typ_subs σ A).
Proof.
  unshelve econstructor.
  + simpl; refine (fun γ γε => _).
    exact (t.(elt) _ _ ).
  + simpl; refine (fun g1 g2 g1e g2e e => _).
    exact (t.(par) _ _ _ _ _).
Defined.


Definition EL {Gamma : BCtx} (A : Trm Gamma (BU Gamma)) : BTy Gamma.
Proof.
  unshelve econstructor.
  - exact A.(elt).
  - exact A.(par).
Defined.

Notation " [ A ] " := (EL A).


Definition Rfl {Γ : BCtx} (A : BTy Γ) : Trm Γ (BU _).
Proof.
  unshelve econstructor; simpl.
  - refine A.(typ). 
  - refine A.(eps).
Defined.

Lemma Rfl_EL : forall (Γ : BCtx) (A : BTy Γ), EL (Rfl A) = A.
Proof.
reflexivity.
Qed.


Definition Nil : BCtx.
Proof.
  unshelve econstructor.
  + refine unit.
  + refine (fun _ _ => True).
Defined.


Record Ext_car (A : Type) (B : A -> SProp) (C : forall (a : A) (b : B a), Type) : Type :=
  exist3 { one : A;  two : B one ; three : C one two }. 

Print tsig.
Record Ssig (A : SProp) (B : A -> SProp) : SProp := Sexist { Sone : A;  Stwo : B Sone }.


Definition Ext (Gamma : BCtx)
           (A : BTy Gamma) : BCtx.
Proof.
  unshelve econstructor.
  - unshelve refine (Ext_car _).
    + exact Gamma.
    + exact (fun x => Gamma.(R) x x).
    + simpl.
      exact (fun g ge => (A.(typ) ge).(car)).
  - intros g1 g2.
    unshelve refine (Ssig _).
    + exact (Gamma.(R) g1.(one) g2.(one)).
    + refine (fun e => (A.(typ) g2.(two)).(rel) _ g2.(three)).
      unshelve refine (Rw (Eq_car _) _).
      * exact (A.(typ) g1.(two)).
      * exact (A.(eps) _ _ _ _ e).
      * exact (g1.(three)).
Defined.      


Notation "Γ · A" := (@Ext Γ A) (at level 50, left associativity).



Definition Cns (Γ : BCtx) (A : BTy Γ) (t : Trm Γ A) : Sub Γ (@Ext Γ A).
Proof.
  unshelve econstructor; simpl.
  - refine (fun γ γε => _).
    unshelve econstructor.
    + exact γ.
    + exact γε.
    + exact (t.(elt) γ γε).
  - simpl; refine (fun g1 g2 g1e g2e e => _).
    unshelve econstructor; simpl; [assumption|].
    exact (t.(par) _ _ _ _ _).
Defined.


Definition Wkn (Γ : BCtx) (A : BTy Γ) : Sub (@Ext Γ A) Γ.
Proof.
  unshelve econstructor; simpl.
  - refine (fun γ γε => γ.(one)).
  - exact (fun g1 g2 g1e g2e e => e.(Sone)).
Defined.

Definition Lft {Γ Δ : BCtx} (σ : Sub Γ Δ) (A : BTy Δ) :
  Sub (@Ext Γ (Typ_subs σ A)) (@Ext Δ A).
Proof.
  unshelve econstructor.
  - intros gamma gammae ; unshelve econstructor.
    + exact (σ gamma.(one) gamma.(two)).
    + exact (σ.(sub_rel) _ _ _ _ gamma.(two)).
    + exact (gamma.(three)).
  - cbv beta iota.
    intros g1 g2 g1e g2e e.
    unshelve econstructor.
    + refine (σ.(sub_rel) _ _ _ _ _).
      exact e.(Sone).
    + simpl.
      exact e.(Stwo).
Defined.


Definition Var {Γ : BCtx} {A : BTy Γ} : Trm (@Ext Γ A) (Typ_subs (@Wkn Γ A) A).
Proof.
  unshelve econstructor.
  - intros g ge.
    exact g.(three).
  - cbv beta iota ; intros g1 g2 g1e g2e e.
    exact e.(Stwo).
Defined.


(*Definition BSub (Gamma : BCtx)
           (A : BTy Gamma) (B : BTy (Gamma · A)) (a : Trm A) : BTy Gamma.
Proof.
  unshelve econstructor.
  - unshelve refine (fun gamma gammae => B.(typ) (Sexist _)).
    + unshelve refine (exist3 _).
      * exact (gamma).
      * exact gammae.
      * exact (a.(elt) gamma gammae).
    + exact gammae.
    + exact (a.(par) _ _ _ _ _).
  - simpl.
    unshelve refine (fun g1 g2 g1e g2e e => _).
    unshelve refine (B.(eps) _ _ _ _ _).
    unshelve refine (Sexist _).
    + exact e.
    + exact (a.(par) _ _ _ _ _).
Defined.*)

Definition Prd {Gamma : BCtx} (A : BTy Gamma) (B : BTy (@Ext Gamma A)) : BTy Gamma.
Proof.
  unshelve econstructor.
  - unshelve refine (fun gamma gammae => _).
    unshelve econstructor.
    + refine (forall (a : (A.(typ) gammae).(car)) (ae : (A.(typ) gammae).(rel) a a), _).
        unshelve refine ((B.(typ) _).(car)).
        * exists gamma gammae.
          exact a.
        * exists gammae.
          exact ae.
    + unshelve refine (fun i f a ae => _).
      refine ((B.(typ) _).(ask) i _).
      exact (fun o => f o a ae).
    + intros f g.
      refine (forall (a1 a2 : car (typ A gammae))
                     (a1e : rel (typ A gammae) a1 a1)
                     (a2e : rel (typ A gammae) a2 a2)
                     (ae : rel (typ A gammae) a1 a2),
                 _: SProp).
      unshelve refine ((B.(typ) _ ).(rel) _ _).
      * exists gamma gammae.
        exact a2.
      * exists gammae.
        exact a2e.
      * unshelve refine (Rw (Eq_car _) _).
        -- unshelve refine (typ B _).
           exists gamma gammae ; exact a1.
           exists gammae ; exact a1e.
        -- refine (B.(eps) _ _ _ _ _).
           exists gammae.
           exact ae.
        -- exact (f a1 a1e).
      * exact (g a2 a2e).
    + simpl.
      intros.
      refine ((typ B _).(ask_rel) _ _ _ _).
      refine (fe a1 a2 a1e a2e ae).
  - simpl.
    intros.
    unshelve refine (BT_eq _ _) ; simpl.
    + simpl.
      unshelve refine (Piext _ _).
      * exact (Eq_car (A.(eps) g1 g2 g1e g2e e)).
      * apply Funext.
        intro x.
        unshelve refine (SPiext _ _).
        -- exact (Eq_rel2 (A.(eps) g1 g2 g1e g2e e) _ _).
        -- simpl.
           apply SFunext.
           intro xe.
           refine (Eq_car _).
           refine (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e x)
                  (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e
                           (Rw (Eq_car (A.(eps) g1 g2 g1e g2e e)) x))
                            {| Sone := g1e; Stwo := xe |}
                            _ _).
           unshelve refine (Sexist _).
           ++ exact e.
           ++ simpl.
              refine (SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) _ _) _).
              exact xe.
    + simpl.
      refine (Funext _) ; intro i.
      refine (Funext _) ; intro f.
      unshelve refine (Rw_Biask i f _ _).
      * exact (Eq_car (eps A g1 g2 g1e g2e e)).
      * refine (fun x y => Eq_rel2 ((eps A g1 g2 g1e g2e e)) x y).
      * intros a ae.
        refine (Sym (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e a)
                          (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e
                                   (Rw (Eq_car (eps A g1 g2 g1e g2e e)) a))
                          {| Sone := g1e; Stwo := ae |}
                          {| Sone := g2e;
                             Stwo := SRw ((Eq_rel2 ((eps A g1 g2 g1e g2e e)) a a)) ae |}
                          _)).
        exists e.
        exact (SRw ((Eq_rel2 ((eps A g1 g2 g1e g2e e)) a a)) ae).
    + refine (Funext _) ; intro f.
      refine (Funext _) ; intro g.
      refine (PiSext (Eq_car (eps A g1 g2 g1e g2e e)) _).
      refine (Funext _) ; intro a1.
      refine (PiSext (Eq_car (eps A g1 g2 g1e g2e e)) _).
      refine (Funext _) ; intro a2.
      refine (SPiSext (Eq_rel2 (eps A g1 g2 g1e g2e e) a1 a1) _).
      refine (SFunext _) ; intro a1e.
      refine (SPiSext (Eq_rel2 (eps A g1 g2 g1e g2e e) a2 a2) _).
      refine (SFunext _) ; intro a2e.
      refine (SPiSext (Eq_rel2 (eps A g1 g2 g1e g2e e) a1 a2) _).
      refine (SFunext _) ; intro ae.
      unshelve refine (@Eq_rel3 _ _ _ _ _ _ _ _ _).
      * refine (B.(eps) _ _ _ _ _).
         exists e.
         exact (SRw (Eq_rel2 _ _ _) a2e).
      * simpl.
        unshelve refine (Trans _ _).
        -- unshelve refine (Rw (Eq_car (eps B _ _ _ _ _)) _).
           ++ exists g1 g1e.
              exact a1.
           ++ exists g1e.
              exact a1e.
           ++ exists e.
              exact (SRw (Eq_rel2 _ _ _) ae).
           ++ exact (f a1 a1e).
        -- exact (Rw_trans _ _ _).
        -- simpl.
           unshelve refine (Trans _ _).
           ++ unshelve refine (Rw (Eq_car (eps B _ _ _ _ _)) _).
              ** exists g2 g2e ; exact (Rw (Eq_car (eps A g1 g2 g1e g2e e)) a1).
              ** exact {| Sone := g2e;
                          Stwo := SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) a1 a1) a1e |}.
              ** exact {| Sone := g2e;
                          Stwo := SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) a1 a2) ae |}.
              ** unshelve refine (Rw (Eq_car (eps B _ _ _ _ _)) _).
                 --- exists g1 g1e.
                     exact a1.
                 --- exists g1e ; exact a1e.
                 --- exists e.
                     exact (SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) a1 a1) a1e).
                 --- exact (f a1 a1e).
           ++ simpl.
              exact (Sym (Rw_trans _ _ _)).
           ++ refine (Ap _ _).
              refine (Sym
                        (Rw_Bifun2 f
                                   (D:= fun (a : (A.(typ) g2e).(car))
                                            (ae : (A.(typ) g2e).(rel) a a) =>
                                          car (typ B (gamma:=
                                                        (@exist3 _ (fun x => Gamma.(R) x x) _
                                                                 g2 g2e a))
                                                   {| Sone := g2e; Stwo := ae |}))
                                   (eq2:= fun x => Eq_rel2
                                                     ((A.(eps) g1 g2 g1e g2e e))
                                                     x x)
                                   (fun x xe =>
                                      Eq_car (B.(eps)
                                                  (@exist3 _
                                                           (fun x => Gamma.(R) x x)
                                                           _ g1 g1e x)
                                                  (@exist3 _
                                                           (fun x => Gamma.(R) x x)
                                                           _ g2 g2e
                                                           (Rw (Eq_car (eps A g1 g2 g1e g2e e)) x))
                                                  {| Sone := g1e; Stwo := xe |}
                                                  {| Sone := g2e;
                                                     Stwo := SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) x x) xe |}
                                                  {| Sone := e;
                                                     Stwo := SRw (Eq_rel2 _ _ _) xe |}))
                                   _ (a:= a1) a1e)).
      * simpl.
        exact (Sym (Rw_Bifun2 g (D:= fun (a : (A.(typ) g2e).(car))
                                      (ae : (A.(typ) g2e).(rel) a a) =>
                                    car (typ B (gamma:=
                                                  (@exist3 _ (fun x => Gamma.(R) x x) _
                                                           g2 g2e a))
                                             {| Sone := g2e; Stwo := ae |}))
                               (eq2:= fun x => Eq_rel2
                                               ((A.(eps) g1 g2 g1e g2e e))
                                               x x)
                               (fun x xe =>
                                  Eq_car (B.(eps)
                                              (@exist3 _
                                                       (fun x => Gamma.(R) x x)
                                                       _ g1 g1e x)
                                              (@exist3 _
                                                       (fun x => Gamma.(R) x x)
                                                       _ g2 g2e
                                                       (Rw (Eq_car (eps A g1 g2 g1e g2e e)) x))
                                              {| Sone := g1e; Stwo := xe |}
                                              {| Sone := g2e;
                                                 Stwo := SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) x x) xe |}
                                              {| Sone := e;
                                                 Stwo := SRw (Eq_rel2 _ _ _) xe |}))
               _ _ )).
Defined.        

Arguments Prd [_] _.

Definition App {Gamma : BCtx} {A : BTy Gamma} {B : BTy (@Ext Gamma A)}
  (f : Trm Gamma (Prd A B)) (a : Trm Gamma A) : Trm Gamma (Typ_subs (Cns a) B).
Proof.
  unshelve econstructor.
  - refine (fun gamma gammae => (f.(elt) gamma gammae) (a.(elt) gamma gammae) _).
  - simpl.
    intros.
    unshelve refine (SRw (@Ap _ SProp (fun x => rel (typ B _) x
                                           (elt f g2 g2e (elt a g2 g2e) (par a g2 g2 g2e g2e g2e)))
                              _ _ _) _).
    + unshelve refine (Rw (Eq_car (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e
                                 (Rw (Eq_car (eps A g1 g2 g1e g2e e)) (elt a g1 g1e)))
                                  (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e (elt a g2 g2e))
                                  _
                                  _
                                  _)) _).
      * exists g2e.
        refine (SRw (Eq_rel2 (A.(eps) g1 g2 g1e g2e e) (elt a g1 g1e) (elt a g1 g1e)) _).
        exact (a.(par) g1 g1 g1e g1e g1e).
      * exists g2e.
        exact (a.(par) g1 g2 g1e g2e e).
      * unshelve refine (Rw (Eq_car (B.(eps)
                                         (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e (elt a g1 g1e))
                                         (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e
                                                  (Rw (Eq_car (eps A g1 g2 g1e g2e e)) (elt a g1 g1e)))
                                         {| Sone := g1e; Stwo := par a g1 g1 g1e g1e g1e |}
                                         {| Sone := g2e; Stwo := _ |}
                                         {| Sone := e; Stwo := _ |})) _).
        -- refine (SRw (Eq_rel2 (A.(eps) g1 g2 g1e g2e e) (elt a g1 g1e) (elt a g1 g1e)) _).
           exact (a.(par) g1 g1 g1e g1e g1e).
        -- exact (elt f g1 g1e (elt a g1 g1e) (par a g1 g1 g1e g1e g1e)).
    + exact (Rw_trans _ _ _).
    + generalize ((Eq_car (eps (Prd A B) g1 g2 g1e g2e e))) ;intro eq.
      unshelve refine (SRw (@Eq_rel3 _ _ (refl _) _ _ _ _ _ _) _).
      * unshelve refine (Rw (Eq_car (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e
                                         (Rw (Eq_car (eps A g1 g2 g1e g2e e)) (elt a g1 g1e)))
                                (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e (elt a g2 g2e))
                                {| Sone := g2e; Stwo := _ |}
                                {| Sone := g2e; Stwo := par a g2 g2 g2e g2e g2e |}
                                {| Sone := g2e; Stwo := par a g1 g2 g1e g2e e |})) _).
        -- refine (SRw (Eq_rel2 (A.(eps) g1 g2 g1e g2e e) (elt a g1 g1e) (elt a g1 g1e)) _).
           exact (a.(par) g1 g1 g1e g1e g1e).
        -- exact (Rw eq (elt f g1 g1e)
                     (Rw (Eq_car (eps A g1 g2 g1e g2e e)) (elt a g1 g1e))
                     (SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) (elt a g1 g1e) (elt a g1 g1e))
                          (a.(par) g1 g1 g1e g1e g1e))).
      * refine (elt f g2 g2e (elt a g2 g2e) (par a g2 g2 g2e g2e g2e)).
      * simpl.
        refine (Ap _ _).
        exact (Rw_Bifun2 (elt f g1 g1e) (C:= (A.(typ) g2e).(car))
                            (D:= fun a (ae : (A.(typ) g2e).(rel) a a) =>
                                   car (B.(typ) (gamma := @exist3 _ (fun x => Gamma.(R) x x)
                                                                  _ g2 g2e a)
                                                {| Sone := g2e; Stwo := ae |}))
                            (eq1:= Eq_car (eps A g1 g2 g1e g2e e))
                            (eq2:= fun x : car (typ A g1e) => Eq_rel2 (A.(eps) g1 g2 g1e g2e e) x x)
                            (fun (x : car (typ A g1e)) (xe : rel (typ A g1e) x x) =>
                              Eq_car (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e x)
                                       (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e
                                                (Rw (Eq_car (eps A g1 g2 g1e g2e e)) x))
                                       {| Sone := g1e; Stwo := xe |}
                                       {| Sone := g2e;
                                          Stwo := SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) x x) xe |}
                                       {| Sone := e;
                                          Stwo := SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) x x) xe |}))
                            (Eq_car (eps (Prd A B) g1 g2 g1e g2e e))
                            ((par a g1 g1 g1e g1e g1e))).
      * reflexivity.
      * simpl.
        exact (f.(par) g1 g2 g1e g2e e (Rw (Eq_car (eps A g1 g2 g1e g2e e)) (elt a g1 g1e))
                          (elt a g2 g2e)
                          (SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) (elt a g1 g1e) (elt a g1 g1e))
                               (par a g1 g1 g1e g1e g1e))
                          (par a g2 g2 g2e g2e g2e) _).
Defined.

Print Prd.


Definition Lam {Gamma : BCtx} (A : BTy Gamma) {B : BTy (Gamma · A)}
  (f : Trm (Gamma · A) B) : Trm Gamma (Prd A B).
Proof.
  unshelve econstructor.
  - simpl ; intros g ge a ae.
    refine (f.(elt) _ _).
  - cbv beta iota.
    intros.
    intros a1 a2 a1e a2e ae.
    generalize (Eq_car (eps (Prd A B) g1 g2 g1e g2e e)) ; intro eq ; simpl.
    unshelve refine (SRw (@Eq_rel3 _ _ (refl _) _ _ _ _ _ _) _).
    + unshelve refine (Rw (Eq_car (B.(eps) _ _ _ _ _)) _).
      * exists g2 g2e.
        exact a1.
      * exists g2e.
        exact a1e.
      * exists g2e.
        exact ae.
      * unshelve refine (Rw (Eq_car (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e
                                  (Rw (Sym (Eq_car (A.(eps) g1 g2 g1e g2e e))) a1))
                         (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e a1)
                         {| Sone := g1e; Stwo := SRw (Eq_rel2 (Sym (A.(eps) g1 g2 g1e g2e e))
                                                              a1 a1) a1e |}
                         {| Sone := g2e; Stwo := a1e |}
                         {| Sone := e;
                            Stwo := SRw (Ap (fun z => rel (typ A g2e) z a1)
                                            (Sym (Rw_sym2 (Eq_car (eps A g1 g2 g1e g2e e)) a1)))
                                        a1e |})) _).
        refine (f.(elt) _ _).
    + exact (elt f (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e a2)
                 {| Sone := g2e; Stwo := a2e |}).
    + simpl.
      refine (Ap _ _).
      refine (Sym _).
      exact (Rw_Bifun _ (fun (x : (A.(typ) g2e).(car)) (xe : (A.(typ) g2e).(rel) x x) =>
                           Eq_car (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e
                                                    (Rw (Sym (Eq_car (A.(eps) g1 g2 g1e g2e e))) x))
                                           (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e x)
                                           {| Sone := g1e;
                                              Stwo := SRw (Eq_rel2 (Sym (A.(eps) g1 g2 g1e g2e e))
                                                                   x x) xe |}
                                           {| Sone := g2e; Stwo := xe |}
                                           {| Sone := e;
                                              Stwo := SRw (Ap (fun z => rel (typ A g2e) z x)
                                                              (Sym (Rw_sym2 (Eq_car (eps A g1 g2 g1e g2e e)) x))) xe |}))
                      eq a1e).
    + reflexivity.
    + unshelve refine (SRw (@Eq_rel3 _ _ (refl _) _ _ _ _ _ _)
                           (f.(par) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e
                                             (Rw (Sym (Eq_car (eps A g1 g2 g1e g2e e))) a1))
                                    (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e a2)
                                    {| Sone := g1e; Stwo := SRw (Eq_rel2 (Sym (A.(eps) g1 g2 g1e g2e e)) a1 a1) a1e |}
                                    {| Sone := g2e; Stwo := a2e |}
                                    {| Sone := e;
                                       Stwo := SRw (Ap (fun z => rel (typ A g2e) z a2)
                                                       (Sym (Rw_sym2 (Eq_car (eps A g1 g2 g1e g2e e))
                                                                     a1))) ae |})).
      * simpl.
        exact (Sym (Rw_trans _ _ _)).
      * reflexivity.
Defined.

Lemma Lam_App_eqn :
  forall (Γ : BCtx) (A : BTy Γ) (B : BTy (@Ext Γ A)) (t : Trm (@Ext Γ A) B) (u : Trm Γ A),
  App (Lam t) u = Trm_subs (Cns u) t.
Proof.
  reflexivity.
Qed.


Lemma App_Lam_eqn :
  forall (Γ : BCtx) (A : BTy Γ) (B : BTy (@Ext Γ A))
  (t : Trm Γ (Prd A B)),
    Lam (@App (@Ext Γ A) (Typ_subs (@Wkn Γ A) A) (Typ_subs (Lft (@Wkn Γ A) A) B)
              (Trm_subs (@Wkn Γ A) t) Var) = t.
Proof.
reflexivity.
Qed.

Inductive BranchingBool :=
| Btrue : BranchingBool
| Bfalse : BranchingBool
| BêtaBool : I -> (O -> BranchingBool) -> BranchingBool.

Inductive EBool : BranchingBool -> BranchingBool -> SProp :=
| EBtrue : EBool Btrue Btrue
| EBfalse : EBool Bfalse Bfalse
| EBêtaBool1 : forall (b : BranchingBool) (i : I) (f : O -> BranchingBool)
                      (H : EBool b (f (alpha i))),
    EBool b (BêtaBool i f)
| EBêtaBool2 : forall (b : BranchingBool) (i : I) (f : O -> BranchingBool)
                  (H : EBool (f (alpha i)) b),
    EBool (BêtaBool i f) b.

Lemma BoolRefl: forall (b : BranchingBool),
    EBool b b.
Proof.
  refine (fix f b := match b with
                     | Btrue => EBtrue
                     | Bfalse => EBfalse
                     | BêtaBool i k => EBêtaBool1 (EBêtaBool2 (f (k (alpha i))))
                     end).
Defined.  

Definition EBool_Branch (i : I) (k : O -> BranchingBool) :
    EBool (k (alpha i)) (BêtaBool i k) :=
  EBêtaBool1 (BoolRefl (k (alpha i))).


Definition BBool : BranchingTYPE.
Proof.
  unshelve econstructor.
  - exact BranchingBool.
  - exact BêtaBool.
  - exact EBool.
  - exact EBêtaBool1.
Defined.

Definition Bool {Gamma : BCtx} : BTy Gamma.
Proof.
  unshelve econstructor.
  - exact (fun _ _ => BBool).
  - exact (fun _ _ _ _ _ => refl _).
Defined.

Definition BTrue {Gamma : BCtx} : Trm Gamma Bool.
Proof.
  unshelve econstructor.
  - exact (fun _ _ => Btrue).
  - exact (fun _ _ _ _ _ => EBtrue).
Defined.

Definition BFalse {Gamma : BCtx} : Trm Gamma Bool.
Proof.
  unshelve econstructor.
  - exact (fun _ _ => Bfalse).
  - exact (fun _ _ _ _ _ => EBfalse).
Defined.

Print Typ_subs.

Notation "⇑ B" := (@Typ_subs _ _ (Wkn _) B) (at level 10).

Notation "A → B" := (Prd A (⇑ B))
                        (at level 99, right associativity, B at level 200).
Print Rw.

Definition Prd_subst : forall Γ Δ A B σ,
  @Typ_subs Γ Δ σ (@Prd Γ A B) =
  @Prd Δ (@Typ_subs Γ Δ σ A) (@Typ_subs (@Ext Γ A) (@Ext Δ (@Typ_subs Γ Δ σ A)) (@Lft Δ Γ σ A) B).
Proof.
reflexivity.
Defined.

(*
>>>>>>> 557e6a7f69285419fd60762c65e0daa4caa22d07
Definition Bool_rect_aux {Gamma : BCtx} :
  forall (gamma : Gamma) (gammae : Gamma.(R) gamma gamma),
    ((Prd (Bool → (BU _))
         ([@App _ Bool (BU _) (@Var Gamma (Prd Bool (BU _))) BTrue]
          → [@App _ Bool (BU _) (@Var Gamma (Prd Bool (BU _))) BFalse]
          → Prd Bool [@App _ Bool (BU _)
                           (Trm_subs (Wkn Bool) (@Var Gamma (Bool → (BU _))))
                           (@Var _ Bool)])).(typ) gammae).(car) :=
  fun (g : Gamma) (ge : Gamma.(R) g g)
      (P :  forall a : BranchingBool, EBool a a -> BranchingTYPE)
      (Pe : forall (a1 a2 : BranchingBool)
                   (a1e : EBool a1 a1) (a2e : EBool a2 a2),
          EBool a1 a2 -> eqn (P a1 a1e) (P a2 a2e))
      (ptrue : car (P Btrue EBtrue))
      (ptruee : rel (P Btrue EBtrue) ptrue ptrue)
      (pfalse : car (P Bfalse EBfalse))
      (pfalsee : rel (P Bfalse EBfalse) pfalse pfalse) =>
    fix f (b : BranchingBool) :=
    match b return (forall (be : EBool b b), car (P b be))
    with
    | Btrue => fun _ => ptrue
    | Bfalse => fun _ => pfalse
    | BêtaBool i h =>
      fun be =>
        @Rw (P (h (alpha i)) (BoolRefl (h (alpha i)))).(car) _
                                                             (Eq_car (Pe _ _ _ _ (EBool_Branch i h)))
                                                             (f (h (alpha i))
                                                                (BoolRefl (h (alpha i))))
    end.
  change (forall (a : BranchingBool) (ae : EBool a a), car (P a ae)).
  unshelve refine (fix f b := match b with
                              | Btrue => fun _ => ptrue
                              | Bfalse => fun _ => pfalse
                              | BêtaBool i h => fun be => _
                              end).
  unshelve refine (@Rw (P (h (alpha i)) (BoolRefl (h (alpha i)))).(car) _ _ _).
  + refine (Eq_car (Pe _ _ _ _ _)).
    exact (EBool_Branch i h).
  + exact (f (h (alpha i)) (BoolRefl (h (alpha i)))).
Defined.
*)

Ltac prd_subst :=
match goal with [ |- context [@Typ_subs _ ?Δ ?σ (@Prd ?Γ ?A ?B)] ] =>
  change (@Typ_subs Γ Δ σ (@Prd Γ A B)) with
  (@Prd Δ (@Typ_subs Γ Δ σ A) (@Typ_subs (@Ext Γ A) (@Ext Δ (@Typ_subs Γ Δ σ A)) (@Lft Δ Γ σ A) B))
end.

Definition Bool_rect {Gamma : BCtx} :
  Trm Gamma (Prd (Bool → (BU _))
                  ([@App _ Bool (BU _) (@Var Gamma (Prd Bool (BU _))) BTrue]
                   → [@App _ Bool (BU _) (@Var Gamma (Prd Bool (BU _))) BFalse]
                   → Prd Bool [@App _ Bool (BU _)
                                    (Trm_subs (Wkn Bool) (@Var Gamma (Bool → (BU _))))
                                    (@Var _ Bool)])).
Proof.
refine (Lam _).
refine (Lam _).
match goal with [ |- context [@Typ_subs _ ?Δ ?σ (@Prd ?Γ ?A ?B)] ] =>
  rewrite (@Prd_subst Γ Δ A B σ)
end.
refine (Lam _).
match goal with [ |- context [@Typ_subs _ ?Δ ?σ (@Prd ?Γ ?A ?B)] ] =>
  rewrite (@Prd_subst Γ Δ A B σ)
end.
match goal with [ |- context [@Typ_subs _ ?Δ ?σ (@Prd ?Γ ?A ?B)] ] =>
  rewrite (@Prd_subst Γ Δ A B σ)
end.

rewrite (Prd_subst _ _).

  unshelve econstructor.
  - cbv beta iota.
    intros g ge P Pe ptrue ptruee pfalse pfalsee.
    cbn in *.
    unshelve refine (fix f b := match b with
                                   | Btrue => fun _ => ptrue
                                   | Bfalse => fun _ => pfalse
                                   | BêtaBool i h => fun be => _
                                   end).
    unshelve refine (@Rw (P (h (alpha i)) (BoolRefl (h (alpha i)))).(car) _ _ _).
    + refine (Eq_car (Pe _ _ _ _ _)).
      exact (EBool_Branch i h).
    + exact (f (h (alpha i)) (BoolRefl (h (alpha i)))).
  - unshelve refine (fun (g1 : Gamma) (g2 : Gamma)
                         (g1e : Gamma.(R) g1 g1)
                         (g2e : Gamma.(R) g2 g2)
                         (e : Gamma.(R) g1 g2)
                         (P1 : forall a : BranchingBool, EBool a a -> BranchingTYPE)
                         (P2 : forall a : BranchingBool, EBool a a -> BranchingTYPE)
                         (P1e : forall (a1 a2 : BranchingBool)
                                       (a1e : EBool a1 a1) (a2e : EBool a2 a2),
                             EBool a1 a2 -> eqn (P1 a1 a1e) (P1 a2 a2e))
                         (P2e : forall (a1 a2 : BranchingBool)
                                       (a1e : EBool a1 a1) (a2e : EBool a2 a2),
                             EBool a1 a2 -> eqn (P2 a1 a1e) (P2 a2 a2e))
                         (Pe : forall (a1 a2 : BranchingBool)
                                      (a1e : EBool a1 a1) (a2e : EBool a2 a2),
                             EBool a1 a2 -> eqn (P1 a1 a1e) (P2 a2 a2e))
                         (ptt1 : car (P2 Btrue EBtrue))
                         (ptt2 : car (P2 Btrue EBtrue))
                         (ptt1e : rel (P2 Btrue EBtrue) ptt1 ptt1)
                         (ptt2e : rel (P2 Btrue EBtrue) ptt2 ptt2)
                         (ptte : rel (P2 Btrue EBtrue) ptt1 ptt2)
                         (pff1 : car (P2 Bfalse EBfalse))
                         (pff2 : car (P2 Bfalse EBfalse))
                         (pff1e : rel (P2 Bfalse EBfalse) pff1 pff1)
                         (pff2e : rel (P2 Bfalse EBfalse) pff2 pff2)
                         (pffe : rel (P2 Bfalse EBfalse) pff1 pff2)
                         (b1 : BranchingBool)
                         (b2 : BranchingBool)
                         (b1e : EBool b1 b1) 
                         (b2e : EBool b2 b2) 
                         (be : EBool b1 b2) => _).
   intros g1 g2 g1e g2e e P1 P2 P1e P2e Pe ptt1 ptt2 ptt1e ptt2e ptte
           pff1 pff2 pff1e pff2e pffe.
    simpl in pff1 ; simpl in pff2 ; simpl in pff1e ; simpl in pff2e ; simpl in pffe.
    simpl in ptt1 ; simpl in ptt2 ; simpl in ptt1e ; simpl in ptt2e ; simpl in ptte.
    simpl in P1 ; simpl in P2 ; simpl in P1e ; simpl in P2e ; simpl in Pe.
    simpl in Pe.
    unshelve refine (fun (g1 : Gamma) (g2 : Gamma)
                         (g1e : Gamma.(R) g1 g1)
                         (g2e : Gamma.(R) g2 g2)
                         (e : Gamma.(R) g1 g2)
                         (P1 : forall a : BranchingBool, EBool a a -> BranchingTYPE)
                         (P2 : forall a : BranchingBool, EBool a a -> BranchingTYPE)
                         (P1e : forall (a1 a2 : BranchingBool)
                                       (a1e : EBool a1 a1) (a2e : EBool a2 a2),
                             EBool a1 a2 -> eqn (P1 a1 a1e) (P1 a2 a2e))
                         (P2e : forall (a1 a2 : BranchingBool)
                                       (a1e : EBool a1 a1) (a2e : EBool a2 a2),
                             EBool a1 a2 -> eqn (P2 a1 a1e) (P2 a2 a2e))
                         (Pe : forall (a1 a2 : BranchingBool)
                                      (a1e : EBool a1 a1) (a2e : EBool a2 a2),
                             EBool a1 a2 -> eqn (P1 a1 a1e) (P2 a2 a2e))
                         (ptt1 : car (P2 Btrue EBtrue))
                         (ptt2 : car (P2 Btrue EBtrue))
                         (ptt1e : rel (P2 Btrue EBtrue) ptt1 ptt1)
                         (ptt2e : rel (P2 Btrue EBtrue) ptt2 ptt2)
                         (ptte : rel (P2 Btrue EBtrue) ptt1 ptt2)
                         (pff1 : car (P2 Bfalse EBfalse))
                         (pff2 : car (P2 Bfalse EBfalse))
                         (pff1e : rel (P2 Bfalse EBfalse) pff1 pff1)
                         (pff2e : rel (P2 Bfalse EBfalse) pff2 pff2)
                         (pffe : rel (P2 Bfalse EBfalse) pff1 pff2) => _).
                         
    intros b1 b2 b1e b2e be.
    simpl in b1 ; simpl in b2 ; simpl in b1e ; simpl in b2e ; simpl in be.





    simpl.
    cbv beta iota.
    simpl.
    simpl in pff1 ; simpl in pff2.
      exact (BoolRefl _).
    +


      induction (h (alpha i)).
      * exact EBtrue.
      * exact EBfalse.
      * refine (EBêtaBool _).
        exact (H (alpha i)).
    
    cbn beta iota in *.
    simpl in *.

    simpl.
  refine (Lam _).
  refine (Lam _).
  refine (Trm_subs (Wkn _) (Lam _)).
  unshelve econstructor.
  cbv beta iota.
  refine (fun P => EL (App P _)).
