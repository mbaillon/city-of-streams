Set Definitional UIP.
Set Primitive Projections.
Require Import Base.
Require Import Dialogue.
Section BranchingTranslation.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Printing Width 90.
Variables (I : Set).
Variable (O : Set).
Variable (alpha : I -> O).

Section BlablaSProp.

Inductive True : SProp := T.
Inductive False : SProp :=.
Inductive and (A : SProp) (B : SProp) : SProp := conj : A -> B -> and A B.

Inductive eqn (A : Type) (x : A) : A -> SProp := refl : @eqn A x x.

Definition Sym (A : Type) (x y : A) (e : eqn x y) : eqn y x :=
  match e with | refl _ => refl _ end.

Definition Trans (A : Type) (x y z : A) (e : eqn x y) (e' : eqn y z) : eqn x z :=
  match e, e' with |refl _, refl _ => refl _ end.


Definition Rw (A B : Type) (e : eqn A B) (x : A) : B :=
  match e with | refl _ => x end.


Definition SRw (A B : SProp) (e : eqn A B) (x : A) : B :=
  match e with | refl _ => x end.

Definition Ap (A B : Type) (f : A -> B) (x y : A) (e : eqn x y) :
  eqn (f x) (f y) :=
  match e with
  | refl _ => refl (f x)
  end.

Definition ApD (A : Type) (B : A -> Type) (f : forall x, B x) (x y : A) (e : eqn x y) :
  eqn (Rw (Ap B e) (f x)) (f y) :=
  match e with
  | refl _ => refl _
  end.

Definition SAp (A : Type) (f : A -> SProp) (x y : A) (e : eqn x y) :
  eqn (f x) (f y) :=
  match e with
  | refl _ => refl (f x)
  end.


Definition Rw_sym1 (A B : Type) (e : eqn A B) (x: A):  (eqn (Rw (Sym e) (Rw e x)) x) :=
  match e with | refl _ => refl _ end.


Definition Rw_sym2 (A B : Type) (e : eqn A B) (x: B):  (eqn (Rw e (Rw (Sym e) x)) x).
Proof.
  now destruct e.
Defined.

Definition Rw_e_syme (A B : Type) (eq1 : eqn A B) (a : A) (b : B) (eq2 : eqn (Rw eq1 a) b) :
  eqn a (Rw (Sym eq1) b).
Proof.
  now destruct eq1.
Defined.

Definition Rw_syme_e (A B : Type) (eq1 : eqn A B) (a : A) (b : B) (eq2 : eqn a (Rw (Sym eq1) b)) :
  eqn (Rw eq1 a) b .
Proof.
  now destruct eq1.
Defined.

Definition Rw_trans (A B C : Type) (eq1 : eqn A B) (eq2 : eqn B C) (x : A) :
  eqn (Rw eq2 (Rw eq1 x)) (Rw (Trans eq1 eq2) x).
Proof.
  destruct eq1.
  now destruct eq2.
Defined.
  

Lemma Rw_app_sym1 (A B C : Type) (f : A -> C) (g : B -> C)
      (eq : eqn A B)
      (e : forall x, eqn (f x) (g (Rw eq x))) :
  forall x , eqn (f (Rw (Sym eq) x)) (g x).
Proof.
  intro x.
  destruct eq.
  exact (e x).
Defined.

Lemma Rw_app_sym2 (A B C : Type) (f : A -> C) (g : B -> C)
      (eq : eqn A B)
      (e : forall x , eqn (f (Rw (Sym eq) x)) (g x)) :
  forall x, eqn (f x) (g (Rw eq x)).
Proof.
  intro x.
  destruct eq.
  exact (e x).
Defined.


Definition f_Sequal (A B : Type) (f g : A -> B) (e : eqn f g)
           (x : A) : eqn (f x) (g x) :=
  match e in (eqn _ h) return (eqn (f x) (h x)) with
  | refl _ => refl (f x)
  end.

Definition Df_Sequal (A : Type) (B : A -> Type)
           (f g : forall x : A, B x) (e : eqn f g)
           (x : A) : eqn (f x) (g x) :=
  match e in (eqn _ h) return (eqn (f x) (h x)) with
  | refl _ => refl (f x)
  end.

Definition SDf_Sequal (A : SProp) (B : A -> Type)
           (f g : forall x : A, B x) (e : eqn f g)
           (x : A) : eqn (f x) (g x) :=
  match e in (eqn _ h) return (eqn (f x) (h x)) with
  | refl _ => refl (f x)
  end.

Lemma BiRw_app_sym1 (A B C : Type)
      (Ae : A -> SProp)
      (Be : B -> SProp)
      (f : forall (x : A), Ae x -> C)
      (g : forall (x : B), Be x -> C)
      (eq : eqn A B)
      (eq2 : eqn Ae (fun x => Be (Rw eq x)))
      (e : forall x xe, eqn (f x xe) (g (Rw eq x) (SRw (f_Sequal eq2 x) xe))) :
  forall (x : B) (xe : Be x),
    eqn (f (Rw (Sym eq) x) (SRw (Sym (Rw_app_sym1 (g:= Be) (eq:= eq) (f_Sequal eq2) x)) xe))
        (g x xe).
Proof.
  intros.
  destruct eq.
  simpl in *.
  change (eqn Ae Be) in eq2.
  destruct eq2.
  exact (e x xe).
Defined.

Lemma BiRw_app_sym2 (A B C : Type)
      (Ae : A -> SProp)
      (Be : B -> SProp)
      (f : forall (x : A), Ae x -> C)
      (g : forall (x : B), Be x -> C)
      (eq : eqn A B)
      (eq2 : eqn Ae (fun x => Be (Rw eq x)))
      (e:  forall (x : B) (xe : Be x),
    eqn (f (Rw (Sym eq) x) (SRw (Sym (Rw_app_sym1 (g:= Be) (eq:= eq) (f_Sequal eq2) x)) xe))
        (g x xe)) :
  forall x xe, eqn (f x xe) (g (Rw eq x) (SRw (f_Sequal eq2 x) xe)).
Proof.
  intros.
  destruct eq.
  simpl in *.
  change (eqn Ae Be) in eq2.
  destruct eq2.
  exact (e x xe).
Defined.

  
Axiom F : False.
Ltac admit := case F.


Axiom Funext :
  forall (A B : Type) (f g : A -> B) (e : forall x, eqn (f x) (g x)), eqn f g.

Axiom SFunext :
  forall (A : SProp) (B : Type) (f g : A -> B) (e : forall x, eqn (f x) (g x)), eqn f g.

Axiom DFunext :
  forall (A : Type) (B : A -> Type)
         (f g : forall x : A, B x)
         (e : forall x, eqn (f x) (g x)), eqn f g.

Axiom SDFunext :
  forall (A : SProp) (B : A -> Type)
         (f g : forall x : A, B x)
         (e : forall x, eqn (f x) (g x)), eqn f g.

Lemma GFunext1 : forall (A: Type) (B1 B2 : A->Type)
                        (eq2 : eqn B1 B2)
                        (f : forall x : A, B1 x) (g : forall x : A, B2 x)
                        (eq3: forall x : A, eqn (Rw (f_Sequal eq2 x) (f x)) (g x)),
    eqn (fun x => (Rw (f_Sequal eq2 x) (f x))) g.
Proof.
  intros.
  destruct eq2.
  simpl in *.
  exact (DFunext eq3).
Qed.

Lemma GFunext2 : forall (A1 A2 : Type) (B1 : A1->Type) (B2 : A2 -> Type)
          (eq1 : eqn A1 A2) (eq2 : eqn (fun x => B1 (Rw (Sym eq1) x)) B2)
          (f : forall x : A1, B1 x) (g : forall x : A2, B2 x)
          (eq3: forall x : A2, eqn (Rw (f_Sequal eq2 x) (f (Rw (Sym eq1) x))) (g x)),
    eqn (fun x => (Rw (f_Sequal eq2 x) (f (Rw (Sym eq1) x)))) g.
Proof.
  intros.
  destruct eq2.
  destruct eq1.
  simpl in *.
  exact (DFunext eq3).
Qed.



Lemma Piext :
  forall (A1 A2 : Type) (B1 : A1 -> Type) (B2 : A2 -> Type)
         (eq1 : eqn A1 A2) (eq2 : eqn B1 (fun x => B2 (Rw eq1 x))),
           eqn (forall x : A1, B1 x) (forall x : A2, B2 x).
Proof.
  intros.
  destruct (Sym eq2).
  now destruct eq1.
Defined.

Lemma PiSext :
  forall (A1 A2 : Type) (B1 : A1 -> SProp) (B2 : A2 -> SProp)
         (eq1 : eqn A1 A2) (eq2 : eqn B1 (fun x => B2 (Rw eq1 x))),
           eqn (forall x : A1, B1 x) (forall x : A2, B2 x).
Proof.
  intros.
  destruct (Sym eq2).
  now destruct eq1.
Defined.

Lemma SPiext :
  forall (A1 A2 : SProp) (B1 : A1 -> Type) (B2 : A2 -> Type)
         (eq1 : eqn A1 A2) (eq2 : eqn B1 (fun x => B2 (SRw eq1 x))),
           eqn (forall x : A1, B1 x) (forall x : A2, B2 x).
Proof.
  intros.
  destruct (Sym eq2).
  now destruct eq1.
Defined.

Lemma SPiSext :
  forall (A1 A2 : SProp) (B1 : A1 -> SProp) (B2 : A2 -> SProp)
         (eq1 : eqn A1 A2) (eq2 : eqn B1 (fun x => B2 (SRw eq1 x))),
           eqn (forall x : A1, B1 x) (forall x : A2, B2 x).
Proof.
  intros.
  destruct (Sym eq2).
  now destruct eq1.
Defined.

Lemma BiPiext (A : Type) (Ae : A -> A -> SProp) (B : forall (x : A) (xe : Ae x x), Type)
      (f : forall (x : A) (xe : Ae x x), B x xe)
      (C : Type)
      (Ce : C -> C -> SProp)
      (D : forall (x : C) (xe : Ce x x), Type)
      (eq1 : eqn A C)
      (eq2 : forall x : A, eqn (Ae x x) (Ce (Rw eq1 x) (Rw eq1 x)))
      (eq3 : forall (x : A) (xe : Ae x x), eqn (B x xe) (D (Rw eq1 x) (SRw (eq2 x) xe))) :
  eqn (forall (x : A) (xe : Ae x x), B x xe) (forall (x : C) (xe : Ce x x), D x xe).
Proof.
  unshelve refine (Piext _).
  - exact eq1.
  - refine (Funext (fun x => _)).
    unshelve refine (SPiext _).
    + exact (eq2 x).
    + refine (SFunext (fun xe => _)).
      exact (eq3 x xe).
Defined.  
  
  

Lemma Rw_fun (A : Type) (B : A -> Type)
      (f : forall x : A, B x)
      (C : Type)
      (D : C -> Type)
      (eq1 : eqn C A)
      (eq2 : forall x : C, eqn (B (Rw eq1 x)) (D x))
      (eq : eqn (forall x : A, B x) (forall x : C, D x))
      (c : C) :
  eqn (Rw eq f c) (Rw (eq2 c) (f (Rw eq1 c))).
Proof.
  change (eqn (Rw (@Piext _ _ _ _ (Sym eq1) (Funext (Rw_app_sym2 eq2))) f c)
              (Rw (f_Sequal (Funext eq2) c) (f (Rw eq1 c)))).
  clear eq.
  destruct eq1 ; simpl in *.
  now destruct (Funext eq2).
Defined.
Print Funext.

Arguments SRw _ : clear implicits.
Arguments SDf_Sequal _ : clear implicits.
Arguments Df_Sequal _ : clear implicits.
Arguments Piext _ : clear implicits.
Arguments SPiext _ : clear implicits.
Arguments PiSext _ : clear implicits.
Arguments Funext _ : clear implicits.


Lemma Rw_Bifun (A : Type) (Ae : A -> A -> SProp) (B : forall (x : A) (xe : Ae x x), Type)
      (f : forall (x : A) (xe : Ae x x), B x xe)
      (C : Type)
      (Ce : C -> C -> SProp)
      (D : forall (x : C) (xe : Ce x x), Type)
      (eq1 : eqn C A)
      (eq2 : forall x : C, eqn (Ce x x) (Ae (Rw eq1 x) (Rw eq1 x)))
      (eq3 : forall (x : C) (xe : Ce x x), eqn (B (Rw eq1 x) (SRw _ _ (eq2 x) xe)) (D x xe))
      (eq : eqn (forall (x : A) (xe : Ae x x), B x xe) (forall (x : C) (xe : Ce x x), D x xe))
      (c : C) (ce : Ce c c):
  eqn (Rw eq f c ce) (Rw (eq3 c ce) (f (Rw eq1 c) (SRw _ _ (eq2 c) ce))).
Proof.
  change (forall (x : C) (xe : Ce x x), eqn (B (Rw eq1 x) (SRw _ _ ((f_Sequal (Funext _ _ _ _ eq2)) x) xe)) (D x xe)) in eq3.
  change (eqn (Rw (@Piext _ _ (fun x : A => forall xe : Ae x x, B x xe)
                          (fun x : C => forall xe : Ce x x, D x xe) (Sym eq1)
                          ((Funext _ _ _ _ (@Rw_app_sym2 _ _ _ (fun x : A => forall xe : Ae x x, B x xe)
                          (fun x : C => forall xe : Ce x x, D x xe) (Sym eq1)
                          (fun c => Sym (SPiext _ _ _ _ _ (Df_Sequal _ _ _ _ (DFunext (fun x : C => (Sym (SFunext (fun xe : Ce x x => eq3 x xe))))) c)))))))
                  f c ce)
              (Rw (Sym (SDf_Sequal _ _ _ _ (Df_Sequal _ _ _ _ (DFunext (fun (x : C) => (Sym (SFunext (fun xe => eq3 x xe))))) c) ce)) (f (Rw eq1 c) (SRw _ _ ((f_Sequal (Funext _ _ _ _ eq2)) c) ce)))).
  revert eq3.
  generalize (Funext _ _ _ _ eq2) ; clear eq2 ; intros eq2 eq3.
  generalize (DFunext (fun (x : C) => (Sym (SFunext (fun xe => eq3 x xe))))).
  clear eq3 ; intro eq3.
  destruct eq1 ; simpl in *.
  clear eq.
  revert f.
  change (eqn D
              (fun (x : C) (xe : Ce x x) => B x (SRw ((fun x : C => Ce x x) x)
                                                     ((fun x : C => Ae x x) x)
                                                     (f_Sequal eq2 x) xe)))
         in eq3.
  change (forall f : forall (x : C) (xe : (fun x : C => Ae x x) x), B x xe,
             eqn
               (Rw
                  (Piext C C (fun x : C => forall xe : (fun x : C => Ae x x) x,
                                  B x xe)
                         (fun x : C => forall xe : (fun x : C => Ce x x) x, D x xe)
                         (refl C)
                         (Sym
                            (Funext C Type
                                    (fun c0 : C => forall x : (fun x : C => Ce x x) c0, D c0 x)
                                    (fun c0 : C => forall x : (fun x : C => Ae x x) c0, B c0 x)
                                    (fun c0 : C =>
                                       SPiext ((fun x : C => Ce x x) c0)
                                              ((fun x : C => Ae x x) c0)
                                              (D c0) (B c0)
                                              (f_Sequal eq2 c0)
                                              (Df_Sequal _
                                                         (fun x : C => ((fun x : C => Ce x x) x)
                                                                       -> Type) _
                                                         (fun (x : C)
                                                              (xe : (fun x : C => Ce x x) x) =>
                                                            B x (SRw ((fun x : C => Ce x x) x)
                                                                     ((fun x : C => Ae x x) x)
                                                                     (f_Sequal eq2 x) xe))
                                                         eq3 c0)))))
                  f c ce)
               (Rw
                  (Sym
                     (SDf_Sequal ((fun x : C => Ce x x) c)
                                 (fun _ : (fun x : C => Ce x x) c => Type) (D c)
                                 (fun xe : (fun x : C => Ce x x) c =>
                                    B c (SRw ((fun x : C => Ce x x) c)
                                             ((fun x : C => Ae x x) c)
                                             (f_Sequal eq2 c) xe))
                                 (Df_Sequal C
                                            (fun x : C => (fun x : C => Ce x x) x -> Type) D
                                            (fun (x : C) (xe : (fun x : C => Ce x x) x) =>
                                               B x (SRw ((fun x : C => Ce x x) x)
                                                        ((fun x : C => Ae x x) x)
                                                        (f_Sequal eq2 x) xe)) eq3 c) ce))
                  (f c (SRw ((fun x : C => Ce x x) c)
                            ((fun x : C => Ae x x) c)
                            (f_Sequal eq2 c) ce)))).

  change (forall x : C, (fun x => Ae x x) x -> Type) in B.
  change (eqn D
          (fun (x : C) (xe : (fun x : C => Ce x x) x) =>
             B x
               (SRw ((fun x0 : C => Ce x0 x0) x)
                    ((fun x0 : C => Ae x0 x0) x)
                    (f_Sequal eq2 x) xe))) in eq3.
  destruct eq2.
  change (eqn D B) in eq3.
  now destruct eq3.
Defined.


Lemma Rw_Bifun2 (A : Type) (Ae : A -> A -> SProp) (B : forall (x : A) (xe : Ae x x), Type)
      (f : forall (x : A) (xe : Ae x x), B x xe)
      (C : Type)
      (Ce : C -> C -> SProp)
      (D : forall (x : C) (xe : Ce x x), Type)
      (eq1 : eqn A C)
      (eq2 : forall x : A, eqn (Ae x x) (Ce (Rw eq1 x) (Rw eq1 x)))
      (eq3 : forall (x : A) (xe : Ae x x), eqn (B x xe) (D (Rw eq1 x) (SRw _ _ (eq2 x) xe)))
      (eq : eqn (forall (x : A) (xe : Ae x x), B x xe) (forall (x : C) (xe : Ce x x), D x xe))
      (a : A) (ae : Ae a a):
  eqn (Rw eq f (Rw eq1 a) (SRw _ _ (eq2 a) ae))
      (Rw (eq3 a ae) (f a ae)).
Proof.
  simpl.
  refine (Trans (@Rw_Bifun A Ae B f C Ce D (Sym eq1) (Rw_app_sym2 (fun x => Sym (eq2 x)))
                               (BiRw_app_sym1 (g:= D) (eq:= eq1) (eq2:= Funext _ _ _ _ eq2) eq3) eq
                               (Rw eq1 a) (SRw (Ae a a) (Ce (Rw eq1 a) (Rw eq1 a)) (eq2 a) ae)) _).
  simpl.
  clear eq.
  generalize (((BiRw_app_sym1 (g:= D) (eq:= eq1) (eq2:= Funext _ _ _ _ eq2) eq3) _ (SRw (Ae a a) (Ce (Rw eq1 a) (Rw eq1 a)) (eq2 a) ae))).
  intro e.
  destruct eq1.
  simpl in *.
  change (eqn (B a (SRw (Ce a a) (Ae a a)
                        (Sym (eq2 a)) (SRw (Ae a a) (Ce a a) (eq2 a) ae)))
              (D a (SRw (Ae a a) (Ce a a) (eq2 a) ae))) in e.
  revert e.
  generalize (eq3 a ae).
  clear eq3.
  generalize (eq2 a).
  clear eq2.
  intros eq1.
  generalize (D a (SRw (Ae a a) (Ce a a) eq1 ae)).
  clear D.
  intros D eq2 eq3.
  destruct eq1.
  simpl.
  reflexivity.
Defined.  
Arguments SRw [_ _] _.
Arguments SDf_Sequal [_ _ _ _ ] _.
Arguments Df_Sequal [_ _ _ _ ] _.
Arguments Piext [_ _ _ _] _.
Arguments SPiext [_ _ _ _] _.
Arguments PiSext [_ _ _ _] _.
Arguments SPiSext [_ _ _ _] _.
Arguments Funext [_ _ _ _] _.

Record BranchingTYPE@{v} := {
  car : Type@{v} ;
  ask : forall (i : I) (f : O -> car), car ;
  rel : car -> car -> SProp;
  ask_rel : forall (i : I) (x : car) (f : O -> car)
                   (fe : rel x (f (alpha i))),
      rel x (ask i f);
                       }.
Arguments ask _ : clear implicits.
Arguments ask_rel _ : clear implicits.
Arguments rel _ : clear implicits.

Definition Eq_car (A B : BranchingTYPE) (e : eqn A B) : eqn A.(car) B.(car) :=
  match e with | refl _ => refl _ end.


Definition Eq_rel (A B : BranchingTYPE) (e : eqn A B) (x y : B.(car)) :
  eqn (A.(rel) (Rw (Sym (Eq_car e)) x) (Rw (Sym (Eq_car e)) y))
      (B.(rel) x y).
Proof.
  now destruct e.
Defined.

Definition Eq_rel2 (A B : BranchingTYPE) (e : eqn A B) (x y : A.(car)) :
  eqn (A.(rel) x y)
      (B.(rel) (Rw (Eq_car e) x) (Rw (Eq_car e) y)).
Proof.
  now destruct e.
Defined.
Lemma Eq_rel3 (A B : BranchingTYPE) (e : eqn A B)
      (a1 a2 : A.(car)) (b1 b2 : B.(car))
      (eq1 : eqn (Rw (Eq_car e) a1) b1)
      (eq2 : eqn (Rw (Eq_car e) a2) b2) :
  eqn (A.(rel) a1 a2)
      (B.(rel) b1 b2).
Proof.
  intros.
  destruct e.
  destruct eq1; destruct eq2.
  reflexivity.
Defined.

Definition Eq_ask (A B : BranchingTYPE) (i : I) (f : O -> A.(car)) (g : O -> B.(car))
                        (eq1 : eqn A B)
                        (eq2 : forall (o: O), eqn (Rw (Eq_car eq1) (f o)) (g o)) :
                    eqn (Rw (Eq_car eq1) (ask A i f)) (ask B i g).
Proof.
  intros.
  destruct eq1.
  now destruct (Funext eq2).
Defined.





About Rw_fun.
Lemma BRw_fun (A : Type) (B : A -> BranchingTYPE)
      (f : forall x : A, (B x).(car))
      (C : Type)
      (D : C -> BranchingTYPE)
      (eq1 : eqn C A)
      (eq2 : forall x : C, eqn (B (Rw eq1 x)) (D x))
      (eq : eqn (forall x : A, (B x).(car)) (forall x : C, (D x).(car)))
      (c : C) :
  eqn (Rw eq f c) (Rw (Eq_car (eq2 c)) (f (Rw eq1 c))).
Proof.
  change (eqn (Rw (@Piext _ _ _ _ (Sym eq1) (Funext (Rw_app_sym2 (fun x => Eq_car (eq2 x))))) f c) (Rw (f_Sequal (Funext (fun x => Eq_car (eq2 x))) c) (f (Rw eq1 c)))).
  clear eq.
  destruct eq1 ; simpl in *.
  now destruct (Funext eq2).
Defined.

Lemma BRw_Bifun (A : Type) (Ae : A -> A -> SProp)
      (B : forall (x : A) (xe : Ae x x), BranchingTYPE)
      (f : forall (x : A) (xe : Ae x x), (B x xe).(car))
      (C : Type)
      (Ce : C -> C -> SProp)
      (D : forall (x : C) (xe : Ce x x), BranchingTYPE)
      (eq1 : eqn C A)
      (eq2 : forall x : C, eqn (Ce x x) (Ae (Rw eq1 x) (Rw eq1 x)))
      (eq3 : forall (x : C) (xe : Ce x x), eqn (B (Rw eq1 x) (SRw (eq2 x) xe)) (D x xe))
      (eq : eqn (forall (x : A) (xe : Ae x x), (B x xe).(car))
                (forall (x : C) (xe : Ce x x), (D x xe).(car)))
      (c : C) (ce : Ce c c):
  eqn (Rw eq f c ce) (Rw (Eq_car (eq3 c ce)) (f (Rw eq1 c) (SRw (eq2 c) ce))).
Proof.
  exact (@Rw_Bifun A Ae (fun x xe => (B x xe).(car)) f C Ce
       (fun x xe => (D x xe).(car)) eq1 eq2 (fun x xe => Eq_car (eq3 x xe)) eq c ce).
Defined.


Lemma Rw_Biask : (forall (A : Type) (Ae : A -> A -> SProp)
                      (B : forall (a : A) (ae : Ae a a), BranchingTYPE)
                      (C : Type) (Ce : C -> C -> SProp)
                      (D : forall (c : C) (ce : Ce c c), BranchingTYPE)
                      (i : I)
                      (f : O -> forall (a : A) (ae : Ae a a), car (B a ae))
                      (e : eqn (forall (c : C) (ce : Ce c c), car (D c ce))
                               (forall (a : A) (ae : Ae a a), car (B a ae)))
                      (eqCA : eqn C A)
                      (eqAeCe : forall (c1 c2 : C), eqn (Ce c1 c2) (Ae (Rw eqCA c1) (Rw eqCA c2)))
                      (he : forall (c : C) (ce : Ce c c),
                          eqn (B (Rw eqCA c) (SRw ((eqAeCe c c)) ce))
                              (D c ce)),
                  eqn (Rw e (fun (c : C) (ce : Ce c c) =>
                               ask (D c ce) i (fun o => (Rw (Sym e) (f o)) c ce)))
                      (fun (a : A) (ae : Ae a a) => ask (B a ae) i (fun o => f o a ae))).
Proof.
  intros.
  refine (DFunext (fun a => _)).
  refine (SDFunext (fun ae => _)).
  generalize ((Sym (@BiRw_app_sym2 _ _ _
                                   (fun x => Ae x x) (fun y => Ce y y) B D
                                   (Sym eqCA)
                                   (Funext (@Rw_app_sym2 _ _ _ (fun a => Ae a a) _ (Sym eqCA) (fun x => Sym (eqAeCe x x)))) he a ae))) ; intro eq.
  unshelve refine (@Trans _ _ _ _ (@BRw_Bifun C Ce D
                                              (fun (c : C) (ce : Ce c c) =>
                                                 ask (D c ce) i
                                                     (fun o : O => Rw (Sym e) (f o) c ce))
                                              A Ae B (Sym eqCA)
                                              (@Rw_app_sym2 _ _ _ (fun a => Ae a a) _
                                                            (Sym eqCA)
                                                            (fun x => Sym (eqAeCe x x)))
                                              (fun c ce => Sym
                                                             (@BiRw_app_sym2 _ _ _
                                                                             (fun x => Ae x x) (fun y => Ce y y) B D
                                                                             (Sym eqCA)
                                                                             (Funext (@Rw_app_sym2 _ _ _ (fun a => Ae a a) _ (Sym eqCA) (fun x => Sym (eqAeCe x x)))) he c ce)) e a ae) _).

  refine (@Eq_ask (D (Rw (Sym eqCA) a)
                     (SRw
                            (@Rw_app_sym2 _ _ _ _ _ (Sym eqCA)
                                         (fun x : C => Sym (eqAeCe x x)) a) ae))
                         (B a ae) i _ _ eq _).
  destruct eqCA ; simpl in *.
  generalize  (Sym (eqAeCe a a)).
  intro e0.
  change (eqn ((D a (SRw e0 ae))) ((B a ae))) in eq.
  intro o.
  refine (@Rw_syme_e _ _ (Eq_car eq) _ _ _).
  refine (Trans (@BRw_Bifun C Ae B (f o) C Ce D (refl C) (fun x => eqAeCe x x) he
                            (Sym e) a (SRw e0 ae)) _).
  clear e.
  now destruct (he a (SRw e0 ae)).
Defined.

Lemma PiDomCodomExt :
  forall (A1 A2 : BranchingTYPE)
         (B1 : forall (x : A1.(car)) (xe : A1.(rel) x x), BranchingTYPE)
         (B2 : forall (x : A2.(car)) (xe : A2.(rel) x x), BranchingTYPE)
         (eq : eqn (forall (x0 : car A1) (xe0 : rel A1 x0 x0), car (B1 x0 xe0))
                   (forall (x0 : car A2) (xe0 : rel A2 x0 x0), car (B2 x0 xe0)))
         (eq1 : eqn A1 A2)
         (eq2 : eqn (fun x xe => B1 (Rw (Sym (Eq_car eq1)) x)
                                    (SRw (Sym (Eq_rel eq1 x x)) xe)) B2)
         (f : forall (x : A1.(car)) (xe : A1.(rel) x x), (B1 x xe).(car)),
    eqn (Rw eq f) (fun (x : A2.(car)) (xe : A2.(rel) x x) =>
                     (Rw (Eq_car (SDf_Sequal (Df_Sequal eq2 x) xe)) (f (Rw (Sym (Eq_car eq1)) x) (SRw (Sym (Eq_rel eq1 x x)) xe)))).
Proof.
  intros.
  destruct eq2.
  destruct eq1.
  simpl.
  revert f.
  unfold Rw.
  destruct eq.
  exact (fun f => refl _ ).
Defined.



Definition BT_eq (A B : BranchingTYPE)
           (eq_car : eqn A.(car) B.(car))
           (eq_ask : eqn (fun i f => Rw eq_car (A.(ask) i (fun o => Rw (Sym eq_car) (f o))))
                         B.(ask))
           (eq_rel : eqn A.(rel)
                         (fun x y => B.(rel) (Rw eq_car x) (Rw eq_car y))) :
  eqn A B.
Proof.
  simpl.
  destruct A ; destruct B ; simpl in *.
  destruct eq_ask.
  destruct eq_car.
  destruct (Sym eq_rel).
  simpl.
  reflexivity.
Defined.

Record BCtx@{u v} : Type@{v} := {
  K : Type@{u};
  R : forall (t1 t2 : K), SProp;
                               }.
Coercion K : BCtx >-> Sortclass.
Arguments R _ : clear implicits.


Record BTy (Gamma : BCtx) : Type :=  {
  typ : forall (gamma : Gamma) (gammae : Gamma.(R) gamma gamma), BranchingTYPE;
  eps : forall (g1 g2 : Gamma) (g1e : Gamma.(R) g1 g1) (g2e : Gamma.(R) g2 g2)
               (e : Gamma.(R) g1 g2),
      eqn (typ g1e) (typ g2e);
  }.
Arguments eps [_] _.


(*Definition AskU (i : I) (F : O -> BranchingTYPE) : BranchingTYPE.
Proof.
  unshelve econstructor.
  - refine ((F (alpha i)).(car)).
  - intros j h.
    refine ((F (alpha i)).(ask) j h).
  - refine ((F (alpha i)).(rel)).
  - simpl.
    intros j f g e.
    refine ((F (alpha i)).(ask_rel) _ _ _ _).
    exact e.
Defined.

Definition Bunit : BranchingTYPE.
Proof.
  unshelve econstructor.
  - exact unit.
  - exact (fun _ _ => tt).
  - exact (fun  _ _ => True).
  - exact (fun _ _ _ _ => T).
Defined.*)

Definition BU_aux@{u v | u < v} : BranchingTYPE@{v}.
Proof.
  unshelve refine (Build_BranchingTYPE@{v} _).
  - exact BranchingTYPE@{u}.
  - exact (fun i F => F (alpha i)).
  - exact (@eqn _).
  - simpl.
    exact (fun _ _ _ x => x).
Defined.

Definition BU (Gamma : BCtx) : BTy Gamma.
Proof.
  unshelve econstructor.
  - exact (fun _ _ => BU_aux). 
  - simpl.
    exact (fun _ _ _ _ _  => refl _ ).
Defined.


Record Trm (Gamma : BCtx) (A : BTy Gamma) :=
  {
  elt : forall (g : Gamma) (ge : Gamma.(R) g g),
      (A.(typ) ge).(car);
  par : forall (g1 g2 : Gamma)
               (g1e : Gamma.(R) g1 g1)
               (g2e : Gamma.(R) g2 g2)
               (e : Gamma.(R) g1 g2),
      (A.(typ) g2e).(rel) (Rw (Eq_car (A.(eps) g1 g2 g1e g2e e)) (elt g1e))
                          (elt g2e);
  }.
     
Arguments elt [_] [_] _  .
Arguments par [_] [_] _ .

(*  par : forall (i : I) (f : O -> Gamma),
      A.(pred) (Gamma.(ask) i f) i
               (fun u : I -> O => A.(eps) i f (fun p => elt (f p))) (elt (Gamma.(ask) i f))}
.*)
Definition EL (Gamma : BCtx) (A : Trm (BU Gamma)) : BTy Gamma.
Proof.
  unshelve econstructor.
  - exact (fun g ge => (A.(elt) g ge)).
  - unshelve refine (fun g1 g2 g1e g2e e => _).
    exact ((A.(par) g1 g2 g1e g2e e)).
Defined.


Definition BU_trm (Gamma : BCtx) : Trm (BU Gamma).
Proof.
  unshelve econstructor.
  - exact (fun _ _ => BU_aux).
  - exact (fun _ _ _ _ _ => refl _). 
Defined.







(*A type is a function from contexts to BranchingType*)
Record Ext_car (A : Type) (B : A -> SProp) (C : forall (a : A) (b : B a), Type) : Type :=
  exist3 { one : A;  two : B one ; three : C one two }. 

Print tsig.
Record Ssig (A : SProp) (B : A -> SProp) : SProp := Sexist { Sone : A;  Stwo : B Sone }.


Definition Ext (Gamma : BCtx)
           (A : BTy Gamma) : BCtx.
Proof.
  unshelve econstructor.
  - unshelve refine (Ext_car _).
    + exact Gamma.
    + exact (fun x => Gamma.(R) x x).
    + simpl.
      exact (fun g ge => (A.(typ) ge).(car)).
  - intros g1 g2.
    unshelve refine (Ssig _).
    + exact (Gamma.(R) g1.(one) g2.(one)).
    + refine (fun e => (A.(typ) g2.(two)).(rel) _ g2.(three)).
      unshelve refine (Rw (Eq_car _) _).
      * exact (A.(typ) g1.(two)).
      * exact (A.(eps) _ _ _ _ e).
      * exact (g1.(three)).
Defined.      


Notation "Γ · A" := (@Ext Γ A) (at level 50, left associativity).


Definition Cns (Gamma : BCtx)
           (A : BTy Gamma) (t : Trm A)
           (gamma : Gamma) (gammae : Gamma.(R) gamma gamma) : (Gamma · A).
Proof.
  unshelve econstructor; trivial.
  exact (t.(elt) gamma gammae).
Defined.



Definition Lft (Gamma : BCtx)
           (A B : BTy Gamma) : BTy (Gamma · A).
Proof.
  unshelve econstructor.
  - intros g ge.
    exact (B.(typ) g.(two)).
  - simpl.
    unshelve refine (fun g1 g2 g1e g2e e => _).
    exact (B.(eps) _ _ _ _ e.(Sone)).
Defined.


Notation "⇑ A" := (@Lft _ _ A) (at level 10).

Definition Var (Gamma : BCtx)
           (A : BTy Gamma) : @Trm (Ext A) (⇑ A).
Proof.
  unshelve econstructor.
  - unshelve refine (fun gamma gammae => gamma.(three)).
  - unshelve refine (fun g1 g2 g1e g2e e => e.(Stwo)).
Defined.


Definition Wkn (Gamma : BCtx)
           (A B : BTy Gamma) (a : Trm A) :
  @Trm (Ext B) (⇑ A).
Proof.
  unshelve econstructor.
  - refine (fun gamma gammae => a.(elt) gamma.(one) gamma.(two)).
  - unshelve refine (fun g1 g2 g1e g2e e => _).
    exact (a.(par) _ _ _ _ _).
Defined.    


Definition BSub (Gamma : BCtx)
           (A : BTy Gamma) (B : BTy (Gamma · A)) (a : Trm A) : BTy Gamma.
Proof.
  unshelve econstructor.
  - unshelve refine (fun gamma gammae => B.(typ) (Sexist _)).
    + unshelve refine (exist3 _).
      * exact (gamma).
      * exact gammae.
      * exact (a.(elt) gamma gammae).
    + exact gammae.
    + exact (a.(par) _ _ _ _ _).
  - simpl.
    unshelve refine (fun g1 g2 g1e g2e e => _).
    unshelve refine (B.(eps) _ _ _ _ _).
    unshelve refine (Sexist _).
    + exact e.
    + exact (a.(par) _ _ _ _ _).
Defined.

Definition Prd (Gamma : BCtx)
           (A : BTy Gamma) (B : BTy (Gamma · A)) : BTy Gamma.
Proof.
  unshelve econstructor.
  - unshelve refine (fun gamma gammae => _).
    unshelve econstructor.
    + refine (forall (a : (A.(typ) gammae).(car)) (ae : (A.(typ) gammae).(rel) a a), _).
        unshelve refine ((B.(typ) _).(car)).
        * exists gamma gammae.
          exact a.
        * exists gammae.
          exact ae.
    + unshelve refine (fun i f a ae => _).
      refine ((B.(typ) _).(ask) i _).
      exact (fun o => f o a ae).
    + intros f g.
      refine (forall (a1 a2 : car (typ A gammae))
                     (a1e : rel (typ A gammae) a1 a1)
                     (a2e : rel (typ A gammae) a2 a2)
                     (ae : rel (typ A gammae) a1 a2),
                 _: SProp).
      unshelve refine ((B.(typ) _ ).(rel) _ _).
      * exists gamma gammae.
        exact a2.
      * exists gammae.
        exact a2e.
      * unshelve refine (Rw (Eq_car _) _).
        -- unshelve refine (typ B _).
           exists gamma gammae ; exact a1.
           exists gammae ; exact a1e.
        -- refine (B.(eps) _ _ _ _ _).
           exists gammae.
           exact ae.
        -- exact (f a1 a1e).
      * exact (g a2 a2e).
    + simpl.
      intros.
      refine ((typ B _).(ask_rel) _ _ _ _).
      refine (fe a1 a2 a1e a2e ae).
  - simpl.
    intros.
    unshelve refine (BT_eq _ _) ; simpl.
    + simpl.
      unshelve refine (Piext _ _).
      * exact (Eq_car (A.(eps) g1 g2 g1e g2e e)).
      * apply Funext.
        intro x.
        unshelve refine (SPiext _ _).
        -- exact (Eq_rel2 (A.(eps) g1 g2 g1e g2e e) _ _).
        -- simpl.
           apply SFunext.
           intro xe.
           refine (Eq_car _).
           refine (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e x)
                  (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e
                           (Rw (Eq_car (A.(eps) g1 g2 g1e g2e e)) x))
                            {| Sone := g1e; Stwo := xe |}
                            _ _).
           unshelve refine (Sexist _).
           ++ exact e.
           ++ simpl.
              refine (SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) _ _) _).
              exact xe.
    + simpl.
      refine (Funext _) ; intro i.
      refine (Funext _) ; intro f.
      unshelve refine (Rw_Biask i f _ _).
      * exact (Eq_car (eps A g1 g2 g1e g2e e)).
      * refine (fun x y => Eq_rel2 ((eps A g1 g2 g1e g2e e)) x y).
      * intros a ae.
        refine (Sym (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e a)
                          (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e
                                   (Rw (Eq_car (eps A g1 g2 g1e g2e e)) a))
                          {| Sone := g1e; Stwo := ae |}
                          {| Sone := g2e;
                             Stwo := SRw ((Eq_rel2 ((eps A g1 g2 g1e g2e e)) a a)) ae |}
                          _)).
        exists e.
        exact (SRw ((Eq_rel2 ((eps A g1 g2 g1e g2e e)) a a)) ae).
    + refine (Funext _) ; intro f.
      refine (Funext _) ; intro g.
      refine (PiSext (Eq_car (eps A g1 g2 g1e g2e e)) _).
      refine (Funext _) ; intro a1.
      refine (PiSext (Eq_car (eps A g1 g2 g1e g2e e)) _).
      refine (Funext _) ; intro a2.
      refine (SPiSext (Eq_rel2 (eps A g1 g2 g1e g2e e) a1 a1) _).
      refine (SFunext _) ; intro a1e.
      refine (SPiSext (Eq_rel2 (eps A g1 g2 g1e g2e e) a2 a2) _).
      refine (SFunext _) ; intro a2e.
      refine (SPiSext (Eq_rel2 (eps A g1 g2 g1e g2e e) a1 a2) _).
      refine (SFunext _) ; intro ae.
      unshelve refine (@Eq_rel3 _ _ _ _ _ _ _ _ _).
      * refine (B.(eps) _ _ _ _ _).
         exists e.
         exact (SRw (Eq_rel2 _ _ _) a2e).
      * simpl.
        unshelve refine (Trans _ _).
        -- unshelve refine (Rw (Eq_car (eps B _ _ _ _ _)) _).
           ++ exists g1 g1e.
              exact a1.
           ++ exists g1e.
              exact a1e.
           ++ exists e.
              exact (SRw (Eq_rel2 _ _ _) ae).
           ++ exact (f a1 a1e).
        -- exact (Rw_trans _ _ _).
        -- simpl.
           unshelve refine (Trans _ _).
           ++ unshelve refine (Rw (Eq_car (eps B _ _ _ _ _)) _).
              ** exists g2 g2e ; exact (Rw (Eq_car (eps A g1 g2 g1e g2e e)) a1).
              ** exact {| Sone := g2e;
                          Stwo := SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) a1 a1) a1e |}.
              ** exact {| Sone := g2e;
                          Stwo := SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) a1 a2) ae |}.
              ** unshelve refine (Rw (Eq_car (eps B _ _ _ _ _)) _).
                 --- exists g1 g1e.
                     exact a1.
                 --- exists g1e ; exact a1e.
                 --- exists e.
                     exact (SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) a1 a1) a1e).
                 --- exact (f a1 a1e).
           ++ simpl.
              exact (Sym (Rw_trans _ _ _)).
           ++ refine (Ap _ _).
              refine (Sym
                        (Rw_Bifun2 f
                                   (D:= fun (a : (A.(typ) g2e).(car))
                                            (ae : (A.(typ) g2e).(rel) a a) =>
                                          car (typ B (gamma:=
                                                        (@exist3 _ (fun x => Gamma.(R) x x) _
                                                                 g2 g2e a))
                                                   {| Sone := g2e; Stwo := ae |}))
                                   (eq2:= fun x => Eq_rel2
                                                     ((A.(eps) g1 g2 g1e g2e e))
                                                     x x)
                                   (fun x xe =>
                                      Eq_car (B.(eps)
                                                  (@exist3 _
                                                           (fun x => Gamma.(R) x x)
                                                           _ g1 g1e x)
                                                  (@exist3 _
                                                           (fun x => Gamma.(R) x x)
                                                           _ g2 g2e
                                                           (Rw (Eq_car (eps A g1 g2 g1e g2e e)) x))
                                                  {| Sone := g1e; Stwo := xe |}
                                                  {| Sone := g2e;
                                                     Stwo := SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) x x) xe |}
                                                  {| Sone := e;
                                                     Stwo := SRw (Eq_rel2 _ _ _) xe |}))
                                   _ (a:= a1) a1e)).
      * simpl.
        exact (Sym (Rw_Bifun2 g (D:= fun (a : (A.(typ) g2e).(car))
                                      (ae : (A.(typ) g2e).(rel) a a) =>
                                    car (typ B (gamma:=
                                                  (@exist3 _ (fun x => Gamma.(R) x x) _
                                                           g2 g2e a))
                                             {| Sone := g2e; Stwo := ae |}))
                               (eq2:= fun x => Eq_rel2
                                               ((A.(eps) g1 g2 g1e g2e e))
                                               x x)
                               (fun x xe =>
                                  Eq_car (B.(eps)
                                              (@exist3 _
                                                       (fun x => Gamma.(R) x x)
                                                       _ g1 g1e x)
                                              (@exist3 _
                                                       (fun x => Gamma.(R) x x)
                                                       _ g2 g2e
                                                       (Rw (Eq_car (eps A g1 g2 g1e g2e e)) x))
                                              {| Sone := g1e; Stwo := xe |}
                                              {| Sone := g2e;
                                                 Stwo := SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) x x) xe |}
                                              {| Sone := e;
                                                 Stwo := SRw (Eq_rel2 _ _ _) xe |}))
               _ _ )).
Defined.        
                       

Definition App (Gamma : BCtx) (A : BTy Gamma) (B : BTy (Gamma · A))
           (f : Trm (Prd B)) (a : Trm A) : Trm (BSub B a).
Proof.
  unshelve econstructor.
  - refine (fun gamma gammae => (f.(elt) gamma gammae) (a.(elt) gamma gammae) _).
  - simpl.
    intros.
    unshelve refine (SRw (@Ap _ SProp (fun x => rel (typ B _) x
                                           (elt f g2 g2e (elt a g2 g2e) (par a g2 g2 g2e g2e g2e)))
                              _ _ _) _).
    + unshelve refine (Rw (Eq_car (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e
                                 (Rw (Eq_car (eps A g1 g2 g1e g2e e)) (elt a g1 g1e)))
                                  (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e (elt a g2 g2e))
                                  _
                                  _
                                  _)) _).
      * exists g2e.
        refine (SRw (Eq_rel2 (A.(eps) g1 g2 g1e g2e e) (elt a g1 g1e) (elt a g1 g1e)) _).
        exact (a.(par) g1 g1 g1e g1e g1e).
      * exists g2e.
        exact (a.(par) g1 g2 g1e g2e e).
      * unshelve refine (Rw (Eq_car (B.(eps)
                                         (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e (elt a g1 g1e))
                                         (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e
                                                  (Rw (Eq_car (eps A g1 g2 g1e g2e e)) (elt a g1 g1e)))
                                         {| Sone := g1e; Stwo := par a g1 g1 g1e g1e g1e |}
                                         {| Sone := g2e; Stwo := _ |}
                                         {| Sone := e; Stwo := _ |})) _).
        -- refine (SRw (Eq_rel2 (A.(eps) g1 g2 g1e g2e e) (elt a g1 g1e) (elt a g1 g1e)) _).
           exact (a.(par) g1 g1 g1e g1e g1e).
        -- exact (elt f g1 g1e (elt a g1 g1e) (par a g1 g1 g1e g1e g1e)).
    + exact (Rw_trans _ _ _).
    + generalize ((Eq_car (eps (Prd B) g1 g2 g1e g2e e))) ;intro eq.
      unshelve refine (SRw (@Eq_rel3 _ _ (refl _) _ _ _ _ _ _) _).
      * unshelve refine (Rw (Eq_car (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e
                                         (Rw (Eq_car (eps A g1 g2 g1e g2e e)) (elt a g1 g1e)))
                                (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e (elt a g2 g2e))
                                {| Sone := g2e; Stwo := _ |}
                                {| Sone := g2e; Stwo := par a g2 g2 g2e g2e g2e |}
                                {| Sone := g2e; Stwo := par a g1 g2 g1e g2e e |})) _).
        -- refine (SRw (Eq_rel2 (A.(eps) g1 g2 g1e g2e e) (elt a g1 g1e) (elt a g1 g1e)) _).
           exact (a.(par) g1 g1 g1e g1e g1e).
        -- exact (Rw eq (elt f g1 g1e)
                     (Rw (Eq_car (eps A g1 g2 g1e g2e e)) (elt a g1 g1e))
                     (SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) (elt a g1 g1e) (elt a g1 g1e))
                          (a.(par) g1 g1 g1e g1e g1e))).
      * refine (elt f g2 g2e (elt a g2 g2e) (par a g2 g2 g2e g2e g2e)).
      * simpl.
        refine (Ap _ _).
        exact (Rw_Bifun2 (elt f g1 g1e) (C:= (A.(typ) g2e).(car))
                            (D:= fun a (ae : (A.(typ) g2e).(rel) a a) =>
                                   car (B.(typ) (gamma := @exist3 _ (fun x => Gamma.(R) x x)
                                                                  _ g2 g2e a)
                                                {| Sone := g2e; Stwo := ae |}))
                            (eq1:= Eq_car (eps A g1 g2 g1e g2e e))
                            (eq2:= fun x : car (typ A g1e) => Eq_rel2 (A.(eps) g1 g2 g1e g2e e) x x)
                            (fun (x : car (typ A g1e)) (xe : rel (typ A g1e) x x) =>
                              Eq_car (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e x)
                                       (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e
                                                (Rw (Eq_car (eps A g1 g2 g1e g2e e)) x))
                                       {| Sone := g1e; Stwo := xe |}
                                       {| Sone := g2e;
                                          Stwo := SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) x x) xe |}
                                       {| Sone := e;
                                          Stwo := SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) x x) xe |}))
                            (Eq_car (eps (Prd B) g1 g2 g1e g2e e))
                            ((par a g1 g1 g1e g1e g1e))).
      * reflexivity.
      * simpl.
        exact (f.(par) g1 g2 g1e g2e e (Rw (Eq_car (eps A g1 g2 g1e g2e e)) (elt a g1 g1e))
                          (elt a g2 g2e)
                          (SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) (elt a g1 g1e) (elt a g1 g1e))
                               (par a g1 g1 g1e g1e g1e))
                          (par a g2 g2 g2e g2e g2e) _).
Defined.


Definition Lam (Gamma : BCtx) (A : BTy Gamma) (B : BTy (Gamma · A))
            (f : Trm B) : Trm (Prd B).
Proof.
  simpl.
  unshelve econstructor.
  - simpl ; intros g ge a ae.
    refine (f.(elt) _ _).
  - cbv beta iota.
    intros.
    intros a1 a2 a1e a2e ae.
    generalize (Eq_car (eps (Prd B) g1 g2 g1e g2e e)) ; intro eq ; simpl.
    pose (q:= Eq_rel3).
    pose (r:= Rw_Bifun).
    simpl in eq.
    pose (eq1 := Sym (A.(eps) g1 g2 g1e g2e e)).
    pose (eq2 := fun (x : (A.(typ) g2e).(car)) => Eq_rel2 (Sym (A.(eps) g1 g2 g1e g2e e)) x x).
    Print Ap.
    pose (eq3 := fun (x : (A.(typ) g2e).(car)) (xe : (A.(typ) g2e).(rel) x x) =>
                   Eq_car (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e
                                  (Rw (Sym (Eq_car (A.(eps) g1 g2 g1e g2e e))) x))
                         (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e x)
                         {| Sone := g1e; Stwo := SRw (Eq_rel2 eq1 x x) xe |}
                         {| Sone := g2e; Stwo := xe |}
                         {| Sone := e;
                            Stwo := SRw (Ap (fun z => rel (typ A g2e) z x)
                                            (Sym (Rw_sym2 (Eq_car (eps A g1 g2 g1e g2e e)) x))) xe |})).
    unshelve refine (SRw (@Eq_rel3 _ _ (refl _) _ _ _ _ _ _) _).
    + unshelve refine (Rw (Eq_car (B.(eps) _ _ _ _ _)) _).
      * exists g2 g2e.
        exact a1.
      * exists g2e.
        exact a1e.
      * exists g2e.
        exact ae.
      * unshelve refine (Rw (Eq_car (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e
                                  (Rw (Sym (Eq_car (A.(eps) g1 g2 g1e g2e e))) a1))
                         (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e a1)
                         {| Sone := g1e; Stwo := SRw (Eq_rel2 eq1 a1 a1) a1e |}
                         {| Sone := g2e; Stwo := a1e |}
                         {| Sone := e;
                            Stwo := SRw (Ap (fun z => rel (typ A g2e) z a1)
                                            (Sym (Rw_sym2 (Eq_car (eps A g1 g2 g1e g2e e)) a1)))
                                        a1e |})) _).
        refine (f.(elt) _ _).
    + exact (elt f (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e a2)
                 {| Sone := g2e; Stwo := a2e |}).
    + simpl.
      refine (Ap _ _).
      refine (Sym _).
      exact (Rw_Bifun _ (fun (x : (A.(typ) g2e).(car)) (xe : (A.(typ) g2e).(rel) x x) =>
                           Eq_car (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e
                                                    (Rw (Sym (Eq_car (A.(eps) g1 g2 g1e g2e e))) x))
                                           (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e x)
                                           {| Sone := g1e; Stwo := SRw (Eq_rel2 eq1 x x) xe |}
                                           {| Sone := g2e; Stwo := xe |}
                                           {| Sone := e;
                                              Stwo := SRw (Ap (fun z => rel (typ A g2e) z x)
                                                              (Sym (Rw_sym2 (Eq_car (eps A g1 g2 g1e g2e e)) x))) xe |}))
                      eq a1e).
    + reflexivity.
    + pose (t:= f.(par) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e
                                 (Rw (Sym (Eq_car (eps A g1 g2 g1e g2e e))) a1))
                      (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e a2)
                      {| Sone := g1e; Stwo := SRw (Eq_rel2 eq1 a1 a1) a1e |}
                      {| Sone := g2e; Stwo := a2e |}
                      {| Sone := e;
                         Stwo := SRw (Ap (fun z => rel (typ A g2e) z a2)
                                         (Sym (Rw_sym2 (Eq_car (eps A g1 g2 g1e g2e e)) a1))) ae |}).
      simpl in t.
      unshelve refine (SRw (@Eq_rel3 _ _ (refl _) _ _ _ _ _ _)
                           (f.(par) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e
                                             (Rw (Sym (Eq_car (eps A g1 g2 g1e g2e e))) a1))
                                    (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e a2)
                                    {| Sone := g1e; Stwo := SRw (Eq_rel2 eq1 a1 a1) a1e |}
                                    {| Sone := g2e; Stwo := a2e |}
                                    {| Sone := e;
                                       Stwo := SRw (Ap (fun z => rel (typ A g2e) z a2)
                                                       (Sym (Rw_sym2 (Eq_car (eps A g1 g2 g1e g2e e))
                                                                     a1))) ae |})).
      * simpl.
        exact (Sym (Rw_trans _ _ _)).
      * reflexivity.
Defined.





Record BranchingTYPE@{v} := {
  car : Type@{v} ;
  pred : car -> SProp;
  rel : forall (x y : car), SProp;
  rel_const : forall (x : car) (xe : pred x), @rel x x;
  ask : forall (i : I) (f : O -> car), car ;
  aske : forall (i : I) (f : O -> car)
                (fe : forall o, pred (f o)),
      pred (ask i f);
  ask_rel : forall (i : I) (f g : O -> car)
                   (fe : forall o, rel (f o) (g o)),
      rel (ask i f) (ask i g);
                       }.
Coercion car : BranchingTYPE >-> Sortclass.
Arguments rel _ : clear implicits.
Arguments rel_const : clear implicits.
Arguments ask _ : clear implicits.
Arguments pred _ : clear implicits.
Arguments aske _ : clear implicits.
Arguments ask_rel _ : clear implicits.
Print BranchingTYPE.

Definition Bunit : BranchingTYPE.
Proof.
  unshelve econstructor.
  - exact unit.
  - exact (fun _ => True).
  - exact (fun  _ _ => True).
  - exact (fun _ _ => tt).
  - exact (fun _ _ => T).
  - exact (fun _ _ _ => T).
  - exact (fun _ _ _ _ => T).
Defined.


(*Inductive BCtx : Type -> Type :=
| Nil : BCtx unit
| Cons : forall (A : Type)
                (Ae : BCtx A)
                (B : A -> BranchingTYPE),
    BCtx (tsig B).

Print BranchingTYPE.

Definition Predicate_Type (Gamma : Type) (Gammae : BCtx Gamma)
           (A : Gamma -> BranchingTYPE) : Type.
Proof.
  revert A.
  refine (match Gammae as Ge in BCtx G return (G -> BranchingTYPE) -> Type with
          | Nil => fun _ => unit
          | @Cons G Ge T  => fun A =>
                               forall (gamma : G)
                                      (t1 t2 : T gamma)
                               (te : (T gamma).(rel) t1 t2), _
          end).
  exact (forall a : A (@exist _ _ gamma t1),
            { x : A (@exist _ _ gamma t2) & (A (@exist _ _ gamma t2).(rel))
  exact (A (@exist _ _ gamma t1) -> A (@exist _ _ gamma t2)).
Defined.  *)
Record BCtx := {
  K : Type;
  P : K -> Type;
  R : forall (t1 t2 : K) (te1 : P t1) (te2 : P t2), SProp;
  R_const : forall t te, @R t t te te;
  }.
Coercion K : BCtx >-> Sortclass.
Arguments P _ : clear implicits .
Arguments R_const _ : clear implicits.
Arguments R _ : clear implicits.
Print tsig.
Record Ssigt (A : Type) (B : A -> SProp) : Type := Sexistt { Sfst : A;  Ssnd : B Sfst }.
Definition Elt (A : BranchingTYPE) := Ssigt (fun a => A.(pred) a).
  
Record BTy (Gamma : BCtx) :=  {
  typ : forall (gamma : Gamma) (gammae : Gamma.(P) gamma), BranchingTYPE;
  eps : forall (g1 g2 : Gamma) (ge1 : Gamma.(P) g1) (ge2 : Gamma.(P) g2)
               (e : Gamma.(R) g1 g2 ge1 ge2),
      eqn (typ ge1) (typ ge2);
  }.
                                                          
Arguments typ [_] _  .
Arguments eps [_] _ .

Definition AskU (F : O -> BranchingTYPE) : BranchingTYPE.
Proof.
  unshelve econstructor.
  - refine (forall o, F o).
  - exact (fun f => forall o, (F o).(pred) (f o)).
  - intros a b.
    refine (forall o, (F o).(rel) (a o) (b o)).
  - intros i h o.
    refine ((F o).(ask) i (fun p => h p o)).
  - simpl.
    intros f fe o.
    exact ((F o).(rel_const) _ (fe o)).
  - simpl.
    intros i f fe o.
    refine ((F o).(aske) _ _ _).
    intro u.
    exact (fe u o).
  - simpl.
    refine (fun i f g e o => _).
    refine ((F o).(ask_rel) _ _ _ _).
    exact (fun u => e u o).
Defined.

Print BranchingTYPE.
Definition BU_aux@{u v | u < v} : BranchingTYPE@{v}.
Proof.
  Print Build_BranchingTYPE.
  unshelve refine (Build_BranchingTYPE@{v} _ _ _).
  - exact BranchingTYPE@{u}.
  - exact (fun A => True).
  - exact (fun A B => eqn A B).
  - simpl.
    exact (fun _ _ => Bunit).
  - simpl.
    exact (fun _ _ => refl _).
  - simpl.
    exact (fun _ _ _ => T).
  - simpl.
    exact (fun _ _ _ _ => refl _).
Defined.
Print BU_aux.
    
Definition BU (Gamma : BCtx) : BTy Gamma.
Proof.
  unshelve econstructor.
  - refine (fun gamma gammae => BU_aux).
  - simpl.
    exact (fun _ _ _ _ _  => refl _).
Defined.

Print BTy.
(*Definition Predicate_Term (Gamma : Type) (Gammae : BCtx Gamma)
           (A : Gamma -> BranchingTYPE)
           (a : forall (gamma : Gamma), A gamma): Type.
Proof.
  revert A a.
  refine (match Gammae as Ge in BCtx G return forall (A: G -> BranchingTYPE)
                                                     (a : forall g, A g), Type with
          | Nil => fun _ _ => unit
          | @Cons G Ge T  => fun A a =>
                               forall (gamma : G)
                                      (t1 t2 : T gamma)
                               (te : (T gamma).(rel) t1 t2), _
          end).
  exact (A (@exist _ _ gamma t1) -> A (@exist _ _ gamma t2)).
Defined.  *)
Print BranchingTYPE.
Print BTy.
Definition BRw (A B : BranchingTYPE) (e : eqn A B) (x : A) : B :=
  match e with | refl _ => x end.

Record Trm (Gamma : BCtx) (A : BTy Gamma) :=
  {
  elt : forall (gamma : Gamma) (gammae : Gamma.(P) gamma),
      Ssigt (fun (a : A.(typ) gamma gammae) => (A.(typ) gamma gammae).(pred) a) ;
  par : forall (g1 g2 : Gamma) (ge1 : Gamma.(P) g1) (ge2 : Gamma.(P) g2)
               (e : Gamma.(R) g1 g2 ge1 ge2),
      (A.(typ) g2 ge2).(rel) (@BRw _ _ (A.(eps) _ _ _ _ e) (elt ge1).(Sfst))
                             (elt ge2).(Sfst);
  }.
     

Arguments elt [_] [_] _  .
Arguments par [_] [_] _ .

(*  par : forall (i : I) (f : O -> Gamma),
      A.(pred) (Gamma.(ask) i f) i
               (fun u : I -> O => A.(eps) i f (fun p => elt (f p))) (elt (Gamma.(ask) i f))}
.*)
Print Trm.
Print BU.
Definition EL (Gamma : BCtx) (A : Trm (BU Gamma)) : BTy Gamma.
Proof.
  unshelve econstructor.
  - exact (fun g ge => (A.(elt) g ge).(Sfst)).
  - simpl.
    unshelve refine (fun g1 g2 ge1 ge2 e => _).
    exact ((A.(par) g1 g2 ge1 ge2 e)). 
Defined.

Print BU.
Definition BU_trm (Gamma : BCtx) : Trm (BU Gamma).
Proof.
  Print BU_aux.
  unshelve econstructor ; simpl.
  - refine (fun _ _ => Sexistt _).
    + exact BU_aux.
    + exact T.
  - simpl.
    exact (fun _ _ _ _ _ => refl _).
Defined.


(*A type is a function from contexts to BranchingType*)
Record Ext_car (A : Type) (B : A -> Type) (C : forall (a : A) (b : B a), Type) : Type :=
  exist3 { one : A;  two : B one ; three : C one two }. 

Print tsig.
Record Ssig (A : SProp) (B : A -> SProp) : SProp := Sexist { Sone : A;  Stwo : B Sone }.


Definition Ext (Gamma : BCtx)
           (A : BTy Gamma) : BCtx.
Proof.
  unshelve econstructor.
  - unshelve refine (Ext_car _).
    + exact Gamma.
    + exact Gamma.(P).
    + exact (fun g ge => Elt (A.(typ) g ge)).
  - exact (fun _ => unit).
  - simpl.
    unshelve refine (fun g1 g2 _ _ => Ssig _).
    + exact (Gamma.(R) g1.(one) g2.(one) g1.(two) g2.(two)).
    + intro e.
      unshelve refine ((A.(typ) g2.(one) g2.(two)).(rel) _ _).
      * refine (@BRw _ _ (A.(eps) _ _ g1.(two) g2.(two) e) g1.(three).(Sfst)).
      * exact g2.(three).(Sfst).
  - simpl.
    unshelve refine (fun t te => Sexist _).
    + exact (Gamma.(R_const) _ _).
    + unshelve refine ((A.(typ) _ _).(rel_const) _ _).
      exact (t.(three).(Ssnd)).
Defined.      


Notation "Γ · A" := (@Ext Γ A) (at level 50, left associativity).


Definition Cns (Gamma : BCtx)
           (A : BTy Gamma) (t : Trm A)
           (gamma : Gamma) (gammae : Gamma.(P) gamma) : (Ext A).
Proof.
  unshelve econstructor; trivial.
  exact (t.(elt) gamma gammae).
Defined.



Definition Lft (Gamma : BCtx)
           (A B : BTy Gamma) : BTy (Gamma · A).
Proof.
  unshelve econstructor.
  - intros g h ; simpl in h.
    exact (B.(typ) g.(one) g.(two)).
  - simpl.
    unshelve refine (fun g1 g2 h1 h2 e => _).
    refine (B.(eps) _ _ _ _ _).
    exact e.(Sone).
Defined.

Notation "⇑ A" := (@Lft _ _ A) (at level 10).

Definition Var (Gamma : BCtx)
           (A : BTy Gamma) : @Trm (Ext A) (⇑ A).
Proof.
  unshelve econstructor.
  - unshelve refine (fun gamma h => gamma.(three)).
  - simpl.
    unshelve refine (fun g1 g2 h1 h2 e => _).
    refine e.(Stwo).
Defined.


Definition Wkn (Gamma : BCtx)
           (A B : BTy Gamma) (a : Trm A) :
  @Trm (Ext B) (⇑ A).
Proof.
  unshelve econstructor.
  - simpl.
    refine (fun gamma gammae => a.(elt) gamma.(one) gamma.(two)).
  - simpl.
    unshelve refine (fun g1 g2 h1 h2 e => _).
    refine (a.(par) _ _ _ _ _).
Defined.    


Definition BSub (Gamma : BCtx)
           (A : BTy Gamma) (B : BTy (Gamma · A)) (a : Trm A) : BTy Gamma.
Proof.
  unshelve econstructor.
  - unshelve refine (fun gamma gammae => B.(typ) (exist3 _) _).
    + exact (gamma).
    + exact gammae.
    + exact (a.(elt) gamma gammae).
    + exact tt.
  - simpl.
    unshelve refine (fun g1 g2 g1e g2e e => _).
    unshelve refine (B.(eps) _ _ _ _ _).
    unshelve econstructor.
    + exact e.
    + simpl.
      exact (a.(par) _ _ _ _ _).
Defined.

Definition Prd (Gamma : BCtx)
           (A : BTy Gamma) (B : BTy (Gamma · A)) : BTy Gamma.
Proof.
  unshelve econstructor.
  - unshelve refine (fun gamma gammae => _).
    unshelve econstructor.
    + unshelve refine (forall (a : Elt (A.(typ) gamma gammae)), Elt (B.(typ) _ tt)).
      exists gamma gammae.
      exact a.
    + unshelve refine (fun f => forall (x y : Elt (A.(typ) gamma gammae))
                                       (e : (A.(typ) gamma gammae).(rel) x.(Sfst) y.(Sfst)), _ : SProp).
      unshelve refine ((B.(typ) _ tt).(rel) _ (f y).(Sfst)).
      * unshelve refine (@BRw _ _ (B.(eps) _ _ _ _ _) _).
        -- exact (@exist3 _ _ _ gamma gammae x).
        -- exact tt.
        -- unshelve refine (Sexist _).
           ++ exact (Gamma.(R_const) _ _).
           ++ simpl.
              exact e.
        -- exact (f x).(Sfst).
    + refine (fun f g => _).
      refine (forall (a1 a2 : Elt (typ A gamma gammae))
                     (e : (A.(typ) gamma gammae).(rel) a1.(Sfst) a2.(Sfst)), _ : SProp).
      refine ((B.(typ) _ tt).(rel) _ (g a2).(Sfst)).
      * unshelve refine (@BRw _ _ (B.(eps) _ _ _ _ _) _).
        -- exact (@exist3 _ _ _ gamma gammae a1).
        -- exact tt.
        -- unshelve refine (Sexist _).
           ++ exact (Gamma.(R_const) _ _).
           ++ simpl.
              exact e.
        -- exact (f a1).(Sfst).
    + unshelve refine (fun i h a => Sexistt _).
      * refine ((B.(typ) _ _).(ask) i _).
        refine (fun o => (h o a).(Sfst)).
      * refine ((B.(typ) _ _).(aske) i _ _).
        refine (fun o => (h o a).(Ssnd)).
    + simpl.
      refine (fun f fe a1 a2 e => _).
      apply fe.
    + simpl.
      refine (fun i f fe x y e => _).
      assert (forall a b (eq : eqn (typ B a tt)
                                   (typ B b tt))
                     g h
                     (ge : forall o, rel (typ B b tt)
                                         (BRw eq (g o)) (h o)),
                 (typ B b tt).(rel)
                     (BRw eq (ask (typ B a tt) i g))
                     (ask (typ B b tt) i h)).
      * intros a b eq.
        destruct eq ; simpl.
        intros g h ge.
        refine ((typ B a tt).(ask_rel) _ _ _ ge).
      * apply H.
        exact (fun o => fe o x y e).
    + simpl.
      unshelve refine (fun i f g H x y e => _).
      assert (forall a b (eq : eqn (typ B a tt)
                                   (typ B b tt))
                     g h
                     (ge : forall o, rel (typ B b tt)
                                         (BRw eq (g o)) (h o)),
                 (typ B b tt).(rel)
                     (BRw eq (ask (typ B a tt) i g))
                     (ask (typ B b tt) i h)) as K.
      * intros a b eq.
        destruct eq ; simpl.
        intros h1 h2 he.
        refine ((typ B a tt).(ask_rel) _ _ _ he).
      * apply K.
        exact (fun o => H o x y e).
  - simpl.
    admit.
Defined.

Definition App (Gamma : BCtx) (A : BTy Gamma) (B : BTy (Gamma · A))
           (f : Trm (Prd B)) (a : Trm A) : Trm (BSub B a).
Proof.
  unshelve econstructor.
  - refine (fun gamma gammae => (f.(elt) gamma gammae).(Sfst) (a.(elt) gamma gammae)).
  - simpl.
    intros.
    admit.
Defined.

  
Inductive BranchingNat : Type :=
    B0 : BranchingNat 
  | BS : BranchingNat  -> BranchingNat 
  | Bêtanat : I -> (O -> BranchingNat) -> BranchingNat.

Inductive BNatE : BranchingNat -> BranchingNat -> SProp :=
| B0e : BNatE B0 B0
| BSe : forall (n m : BranchingNat) (e : BNatE n m),
    BNatE (BS n) (BS m)
| Bêtae : forall (i : I)  (f g : O -> BranchingNat)
                 (e : forall o, BNatE (f o) (g o)),
    BNatE (Bêtanat i f) (Bêtanat i g).


Definition BNat : BranchingTYPE.
Proof.
  unshelve econstructor.
  - exact BranchingNat.
  - exact (fun _ => True).
  - exact BNatE.
  - exact Bêtanat.
  - intros x xe ; clear xe ; induction x.
    + exact B0e.
    + now apply BSe.
    + now apply Bêtae.
  - exact (fun _ _ _ => T).
  - simpl.
    now apply Bêtae.
Defined.

Definition Nat (Gamma : BCtx) : BTy Gamma.
Proof.
  unshelve econstructor.
  - exact (fun _ _ => BNat).
  - exact (fun _ _ _ _ _ => refl _).
Defined.

Definition Zero {Gamma : BCtx} : Trm (Nat Gamma).
Proof.
  unshelve econstructor.
  unshelve refine (fun _ _ => Sexistt _).
  + exact B0.
  + exact T.
  + simpl.
    exact (fun _ _ _ _ _ => B0e).
Defined.
Axiom o : O.
Lemma Nat_rect (Gamma : BCtx) (F : Trm (@Prd Gamma (Nat _) (BU _)))
      (n : Trm (Nat Gamma))
      (p0 : Trm (EL (App F Zero))) : Trm (EL (App F n)).
Proof.
  unshelve econstructor.
  - intros gamma gammae.
    simpl.
    destruct n as [n npar] ; simpl in *.
    destruct (n gamma gammae) as [m me].
    change (Ssigt
              (fun a : Sfst (Sfst (elt F gamma gammae) {| Sfst := m; Ssnd := T|}) =>
                 pred (Sfst (Sfst (elt F gamma gammae) {| Sfst := m; Ssnd := T |})) a)).
    induction m.
    + destruct p0 ; simpl in *.
      exact ((elt0 _ _)).
    + admit.
    + assert (BNatE (b o) (Bêtanat i b)).
      * admit.
      * refine (match ((elt F gamma gammae).(Ssnd) {| Sfst := b o; Ssnd := T |}
                                                   {| Sfst := Bêtanat i b; Ssnd := T |} H) with
                | refl _ => (X o)
                end).
        exact T.
  - simpl.
    induction ((n gamma gammae).(Sfst)).
    assert (BNat.(rel) (n gamma gammae).(Sfst) (n gamma gammae).(Sfst)).
    + admit.
    + simpl in H.
      induction H.


Record BranchingTYPE@{v} := {
  car : Type@{v} ;
  pred : car -> Type@{v};
  rel : forall (x y : car) (xe : pred x) (ye : pred y), Type@{v};
  rel_const : forall x xe, @rel x x xe xe;
  ask : forall (i : I) (f : O -> car), car ;
  aske : forall (i : I) (f : O -> car)
                (fe : forall o, pred (f o)),
      pred (ask i f);
  ask_rel : forall (i : I) (f : O -> car)
                   (fe : forall o, pred (f o))
                   (o : O),
      rel (fe o) (aske i fe);
                       }.
Coercion car : BranchingTYPE >-> Sortclass.
Arguments rel _ : clear implicits.
Arguments rel_const : clear implicits.
Arguments ask _ : clear implicits.
Arguments pred _ : clear implicits.
Arguments aske _ : clear implicits.
Arguments ask_rel _ : clear implicits.
Print BranchingTYPE.

Definition Bunit : BranchingTYPE.
Proof.
  unshelve econstructor.
  - exact unit.
  - exact (fun _ => unit).
  - exact (fun _ _ _ _ => unit).
  - exact (fun _ _ => tt).
  - exact (fun _ _ _ => tt).
  - exact (fun _ _ => tt).
  - exact (fun _ _ _ _ => tt).
Defined.


(*Inductive BCtx : Type -> Type :=
| Nil : BCtx unit
| Cons : forall (A : Type)
                (Ae : BCtx A)
                (B : A -> BranchingTYPE),
    BCtx (tsig B).

Print BranchingTYPE.

Definition Predicate_Type (Gamma : Type) (Gammae : BCtx Gamma)
           (A : Gamma -> BranchingTYPE) : Type.
Proof.
  revert A.
  refine (match Gammae as Ge in BCtx G return (G -> BranchingTYPE) -> Type with
          | Nil => fun _ => unit
          | @Cons G Ge T  => fun A =>
                               forall (gamma : G)
                                      (t1 t2 : T gamma)
                               (te : (T gamma).(rel) t1 t2), _
          end).
  exact (forall a : A (@exist _ _ gamma t1),
            { x : A (@exist _ _ gamma t2) & (A (@exist _ _ gamma t2).(rel))
  exact (A (@exist _ _ gamma t1) -> A (@exist _ _ gamma t2)).
Defined.  *)
Record BCtx := {
  T : Type;
  P : T -> Type;
  R : forall (t1 t2 : T) (te1 : P t1) (te2 : P t2), Type;
  R_const : forall t te, @R t t te te;
  }.
Coercion T : BCtx >-> Sortclass.
Arguments P _ : clear implicits .
Arguments R_const _ : clear implicits.
Arguments R _ : clear implicits.

Definition Elt (A : BranchingTYPE) := tsig (fun a => A.(pred) a).
  
Record BTy (Gamma : BCtx) :=  {
  typ : forall (gamma : Gamma) (gammae : Gamma.(P) gamma), BranchingTYPE;
  eps : forall (g1 g2 : Gamma) (ge1 : Gamma.(P) g1) (ge2 : Gamma.(P) g2)
               (e : Gamma.(R) g1 g2 ge1 ge2),
      Elt (typ ge1) -> Elt (typ ge2);}.
                                                          
Arguments typ [_] _  .
Arguments eps [_] _ .

Definition AskU (F : O -> BranchingTYPE) : BranchingTYPE.
Proof.
  unshelve econstructor.
  - refine (forall o, F o).
  - exact (fun f => forall o, (F o).(pred) (f o)).
  - intros a b ae be.
    refine (forall o, (F o).(rel) (a o) (b o) (ae o) (be o)).
  - intros i h o.
    refine ((F o).(ask) i (fun p => h p o)).
  - simpl.
    intros i f fe o.
    refine ((F o).(aske) _ _ _).
    intro u.
    exact (fe u o).
  - simpl.
    intros f fe o.
    exact ((F o).(rel_const) _ _).
  - intros i f fe o u.
    refine ((F u).(ask_rel) _ _ _ _).
Defined.

Print BranchingTYPE.
Definition BU_aux@{u v | u < v} : BranchingTYPE@{v}.
Proof.
  Print Build_BranchingTYPE.
  unshelve refine (Build_BranchingTYPE@{v} _ _).
  - exact BranchingTYPE@{u}.
  - exact (fun A => unit).
  - exact (fun A B Ae Be =>
             tsig@{u u} (fun (f : Elt@{u} A -> Elt@{u} B) =>
                     forall (x y : Elt@{u} A), A.(rel) _ _ x.(snd) y.(snd) ->
                                           B.(rel) _ _ (f x).(snd) (f y).(snd))).
  - simpl.
    exact (fun _ _ => Bunit).
  - simpl.
    exact (fun _ _ _ => tt).
  - simpl.
    unshelve refine (fun _ _ => exist _).
    + exact (fun x => x).
    + exact (fun _ _ e => e).
  - simpl.
    unshelve refine (fun _ _ _ _ => exist _).
    + exact (fun _ => @exist _ _ tt tt).
    + exact (fun _ _ _ => tt).
Defined.
Print BU_aux.
    
Definition BU (Gamma : BCtx) : BTy Gamma.
Proof.
  unshelve econstructor.
  - refine (fun gamma gammae => BU_aux).
  - simpl.
    exact (fun _ _ _ _ _ A => A).
Defined.

Print BTy.
(*Definition Predicate_Term (Gamma : Type) (Gammae : BCtx Gamma)
           (A : Gamma -> BranchingTYPE)
           (a : forall (gamma : Gamma), A gamma): Type.
Proof.
  revert A a.
  refine (match Gammae as Ge in BCtx G return forall (A: G -> BranchingTYPE)
                                                     (a : forall g, A g), Type with
          | Nil => fun _ _ => unit
          | @Cons G Ge T  => fun A a =>
                               forall (gamma : G)
                                      (t1 t2 : T gamma)
                               (te : (T gamma).(rel) t1 t2), _
          end).
  exact (A (@exist _ _ gamma t1) -> A (@exist _ _ gamma t2)).
Defined.  *)
Print BTy.

Record Trm (Gamma : BCtx) (A : BTy Gamma) :=
  {
  elt : forall (gamma : Gamma) (gammae : Gamma.(P) gamma),
      tsig (fun (a : A.(typ) gamma gammae) => (A.(typ) gamma gammae).(pred) a) ;
  par : forall (g1 g2 : Gamma) (ge1 : Gamma.(P) g1) (ge2 : Gamma.(P) g2)
               (e : Gamma.(R) g1 g2 ge1 ge2),
      (A.(typ) g2 ge2).(rel) (A.(eps) g1 g2 ge1 ge2 e (elt ge1)).(fst) (elt ge2).(fst)
                                                                                   (A.(eps) g1 g2 ge1 ge2 e (elt ge1)).(snd) (elt ge2).(snd) ;
  }.

Arguments elt [_] [_] _  .
Arguments par [_] [_] _ .

(*  par : forall (i : I) (f : O -> Gamma),
      A.(pred) (Gamma.(ask) i f) i
               (fun u : I -> O => A.(eps) i f (fun p => elt (f p))) (elt (Gamma.(ask) i f))}
.*)
Print Trm.
Print BU.
Definition EL (Gamma : BCtx) (A : Trm (BU Gamma)) : BTy Gamma.
Proof.
  unshelve econstructor.
  - exact (fun g ge => (A.(elt) g ge).(fst)).
  - simpl.
    unshelve refine (fun g1 g2 ge1 ge2 e a => _).
    exact ((A.(par) g1 g2 ge1 ge2 e).(fst) a). 
Defined.

Print BU.
Definition BU_trm (Gamma : BCtx) : Trm (BU Gamma).
Proof.
  Print BU_aux.
  unshelve econstructor ; simpl.
  - refine (fun _ _ => exist _).
    + exact BU_aux.
    + exact tt.
  - simpl.
    unshelve refine (fun g1 g2 ge1 ge2 e => exist _).
    + exact (fun A => A).
    + unshelve refine (fun A B f => f).
Defined.


(*A type is a function from contexts to BranchingType*)
Record Ext_car (A : Type) (B : A -> Type) (C : forall (a : A) (b : B a), Type) : Type :=
  exist3 { one : A;  two : B one ; three : C one two }. 


Definition Ext (Gamma : BCtx)
           (A : BTy Gamma) : BCtx.
Proof.
  unshelve econstructor.
  - unshelve refine (Ext_car _).
    + exact Gamma.
    + exact Gamma.(P).
    + exact (fun g ge => Elt (A.(typ) g ge)).
  - exact (fun _ => unit).
  - simpl ; unshelve refine (fun g1 g2 g1e g2e => tsig _).
    + exact (Gamma.(R) g1.(one) g2.(one) g1.(two) g2.(two)).
    + intro e.
      unshelve refine ((A.(typ) g2.(one) g2.(two)).(rel) _ _ _ _).
      * refine (A.(eps) g1.(one) g2.(one) g1.(two) g2.(two) e _).(fst).
        exact g1.(three).
      * refine (A.(eps) g2.(one) g2.(one) g2.(two) g2.(two) _ _).(fst).
        -- exact (Gamma.(R_const) _ _).
        --exact g2.(three).
      * refine ((A.(eps) _ _ _ _ _ _).(snd)).
      * refine ((A.(eps) _ _ _ _ _ _).(snd)).  
  - simpl.
    unshelve refine (fun t te => exist _).
    + exact (Gamma.(R_const) _ _).
    + exact ((A.(typ) _ _).(rel_const) _ _).
Defined.      


Notation "Γ · A" := (@Ext Γ A) (at level 50, left associativity).


Definition Cns (Gamma : BCtx)
           (A : BTy Gamma) (t : Trm A)
           (gamma : Gamma) (gammae : Gamma.(P) gamma) : (Ext A).
Proof.
  unshelve econstructor; trivial.
  exact (t.(elt) gamma gammae).
Defined.



Definition Lft (Gamma : BCtx)
           (A B : BTy Gamma) : BTy (Gamma · A).
Proof.
  unshelve econstructor.
  - intros g h ; simpl in h.
    exact (B.(typ) g.(one) g.(two)).
  - simpl.
    unshelve refine (fun g1 g2 h1 h2 e b => _).
    refine (B.(eps) _ _ _ _ _ b).
    exact e.(fst).
Defined.

Notation "⇑ A" := (@Lft _ _ A) (at level 10).

Definition Var (Gamma : BCtx)
           (A : BTy Gamma) : @Trm (Ext A) (⇑ A).
Proof.
  unshelve econstructor.
  - unshelve refine (fun gamma h => A.(eps) _ _ _ _ _ _).
    + exact gamma.(one).
    + exact gamma.(two).
    + exact (Gamma.(R_const) _ _).
    + exact gamma.(three).
  - simpl.
    unshelve refine (fun g1 g2 h1 h2 e => _).
    refine e.(snd).
Defined.

Definition Wkn (Gamma : BCtx)
           (A B : BTy Gamma) (a : Trm A) :
  @Trm (Ext B) (⇑ A).
Proof.
  unshelve econstructor.
  - simpl.
    refine (fun gamma gammae => a.(elt) gamma.(one) gamma.(two)).
  - simpl.
    unshelve refine (fun g1 g2 h1 h2 e => _).
    refine (a.(par) _ _ _ _ _).
Defined.    


Definition BSub (Gamma : BCtx)
           (A : BTy Gamma) (B : BTy (Gamma · A)) (a : Trm A) : BTy Gamma.
Proof.
  unshelve econstructor.
  - unshelve refine (fun gamma gammae => B.(typ) (exist3 _) _).
    + exact (gamma).
    + exact gammae.
    + exact (a.(elt) gamma gammae).
    + exact tt.
  - simpl.
    unshelve refine (fun g1 g2 g1e g2e e b => _).
    unshelve refine (B.(eps) _ _ _ _ _ b).
    unshelve econstructor.
    + exact e.
    + simpl.
      exact (a.(par) _ _ _ _ _).
Defined.

Definition Prd (Gamma : BCtx)
           (A : BTy Gamma) (B : BTy (Gamma · A)) : BTy Gamma.
Proof.
  unshelve econstructor.
  - unshelve refine (fun gamma gammae => _).
    unshelve econstructor.
    + unshelve refine (forall (a : Elt (A.(typ) gamma gammae)), Elt (B.(typ) _ tt)).
      exists gamma gammae.
      exact a.
    + unshelve refine (fun f => forall (x y : Elt (A.(typ) gamma gammae))
                                       (e : (A.(typ) gamma gammae).(rel) _ _ x.(snd) y.(snd)), _).
      unshelve refine ((B.(typ) _ tt).(rel) _ _ _ (f y).(snd)).
      * unshelve refine (B.(eps) _ _ _ _ _ (f x)).(fst).
        simpl.
