Require Import Dialogue.
Require Import Streams.

Definition Nucleus : Type := {J : Type & (nat -> J) & ((nat -> J) -> J -> J)}.

Definition TNucl (J : Nucleus) := let (x, _, _) := J in x.

Definition Bnucl : Nucleus.
Proof.
  unfold Nucleus.
  exists (Dialogue nat nat nat).
  apply (@êta nat nat).
  apply Bind.
Defined.

Fixpoint NTransty (T : Nucleus) (sigma : ty)  : Type :=
  match sigma with
  | tnat => (projT1 (sigT_of_sigT2 T))
  | ro → tau => (NTransty T ro) -> (NTransty T tau)
  | ro # tau => (prod (NTransty T ro) (NTransty T tau))
  | stream ro => Stream (NTransty T ro)
  end.


Fixpoint GensharpN {T : Nucleus} {sigma : ty} : (nat -> (NTransty T sigma)) -> (TNucl T) -> (NTransty T  sigma) :=
  match sigma as sigma0
        return ((nat -> NTransty T sigma0) -> TNucl T -> NTransty T sigma0)
  with
  | tnat => projT3 T
  | ro → tau => fun f d x => GensharpN (fun y => f y x) d
  | ro # tau => fun f d => (GensharpN (fun y => fst (f y)) d, GensharpN (fun y => snd (f y) ) d)
  | stream ro => fun (f : nat -> Stream (NTransty T ro)) d =>
                   Make (fun n => (S n, GensharpN (fun x => Str_nth n (f x)) d)) 0
  end.


Definition NRec {T : Nucleus} {sigma : ty} (f : (NTransty T sigma) -> (NTransty T sigma)) (x : NTransty T sigma) : (TNucl T -> (NTransty T sigma)) := GensharpN (Rec f x).


Fixpoint NTranste (T : Nucleus) (sigma : ty) (t : te sigma) : (NTransty T sigma) :=
  let êta :=(projT2 (sigT_of_sigT2 T)) in
  let kappa := projT3 T in
  match t as t0 in te sig0 return NTransty T sig0 with
  | z => êta 0
  | s =>  kappa (fun x => êta (S x))
  | rec tau => @NRec T tau
  | tapp ro tau f g => (NTranste T (ro → tau) f) (NTranste T ro g)
  | Kte ro tau => @Kcomb (NTransty T ro) (NTransty T tau)
  | Ste ro sigma tau => @Scomb (NTransty T ro) (NTransty T sigma) (NTransty T tau)
  | make ro tau => @Make (NTransty T ro) (NTransty T tau)
  | hdte ro => @hd (NTransty T ro)
  | tlte ro => @tl (NTransty T ro)
  end.

Fixpoint GenR {T : Nucleus} {sigma : ty} (R : nat -> (NTransty T tnat) -> Type) : (Transty sigma) -> (NTransty T sigma) -> Type :=
  match sigma as sig0 return (Transty sig0) -> (NTransty T sig0) -> Type  with
  | tnat => R
  | ro → tau => fun f f' => (forall (x : Transty ro) (x' : NTransty T ro), @GenR T ro R x x' -> @GenR T tau R (f x) (f' x'))
  | ro # tau => fun x x' => (prod (@GenR T ro R (fst x) (fst x')) (@GenR T tau R (snd x) (snd x')))
  | stream ro => fun s s' => ((forall (n : nat), @GenR T ro R (Str_nth n s) (Str_nth n s')))
  end.
  


Theorem FondLogical (T : Nucleus) (sigma : ty) (t : te sigma) : forall (R :  nat -> (NTransty T tnat) -> Type), (forall (n : nat), R n ((projT2 (sigT_of_sigT2 T)) n)) -> (forall (f : nat -> nat) (g : nat -> (NTransty T tnat)), ((forall n : nat, R (f n) (g n)) -> @GenR T (tnat → tnat) R f (projT3 T g))) -> GenR R (Transte sigma t) (NTranste T sigma t).
Proof.
  induction t ; intros.
  apply (X 0).
  destruct T as [J êta kappa].
  simpl ; simpl in X ; simpl in X0 ; simpl in R.
  apply (X0 S (fun n => êta (S n))).
  intro n ; exact (X (S n)).
  assert (forall (h : nat -> (Transty sigma)) (h' : nat -> (NTransty T sigma)), (forall (n : nat), GenR R (h n) (h' n)) -> @GenR T (tnat → sigma) R h (GensharpN h')).
  induction sigma.
  destruct T as [J êta kappa].
  simpl.
  intros.
  simpl ; simpl in X ; simpl in X0 ; simpl in R.
  apply X0 ; assumption.
  simpl ; simpl in IHsigma1 ; simpl in IHsigma2.
  intros.
  apply (IHsigma2 (fun n => h n x0) (fun n => h' n x'0)).
  intros ; apply (X1 n x0 x'0).
  assumption.
  assumption.
  simpl ; intros.
  split.
  apply (IHsigma1 (fun x => fst (h x)) (fun x => fst (h' x))) ; [apply X1 | assumption].
  apply (IHsigma2 (fun x => snd (h x)) (fun x => snd (h' x))) ; [apply X1 | assumption].
  simpl.
  intros.
  Print Make_Str_nth.
  assert (HID := Make_Str_nth n 0 (NTransty T sigma) (fun n0 => GensharpN (fun x0 : nat => Str_nth n0 (h' x0)) x')) ; simpl in HID.
  assert (GenR R (Str_nth n (h x))
               (GensharpN (fun x0 : nat => Str_nth (n + 0) (h' x0)) x')).
  assert (aux : n + 0 = n) ; auto ; rewrite aux.
  simpl in IHsigma.
  apply (IHsigma (fun x => Str_nth n (h x)) (fun x => Str_nth n (h' x))).
  intro ; apply (X1 n0 n).
  assumption.
  rewrite <- HID in X3.
  assumption.
  simpl.
  intros x x' x0 x'0 H1 H2.
  unfold NRec.
  apply (X1 (fun x1 => Rec x x'0 x1) (fun x'1 => Rec x' H1 x'1)).
  induction n ; simpl ; [assumption | apply x0 ; assumption].
  apply IHt1 ; [assumption | assumption | ].
  apply IHt2 ; assumption.
  simpl ; intros ; assumption.
  simpl ; intros.
  apply X1 ; [ apply X3 | apply X2 ; apply X3].
  simpl.
  assert (RS : forall n : nat, forall (x : Transty ro -> Transty ro * Transty tau)
    (x' : NTransty T ro -> NTransty T ro * NTransty T tau),
  (forall (x0 : Transty ro) (x'0 : NTransty T ro),
   GenR R x0 x'0 ->
   GenR R (fst (x x0)) (fst (x' x'0)) * GenR R (snd (x x0)) (snd (x' x'0))) ->
  forall (x0 : Transty ro) (x'0 : NTransty T ro),
  GenR R x0 x'0 ->
  GenR R (Str_nth n (Make x x0)) (Str_nth n (Make x' x'0))).
  induction n ; intros ; unfold Str_nth ; simpl.
  apply X1 ; assumption.
  apply (IHn x x' X1).
  apply X1 ;  assumption.
  intros ; apply RS ; assumption.
  simpl ; unfold Str_nth ; intros.
  apply (X1 0).
  simpl ; unfold Str_nth ; intros.
  apply (X1  (S n)).
Defined.


