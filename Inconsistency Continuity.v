Require Import Streams.

Print eq.
Print comparison.

Fixpoint eqb n m : bool :=
  match n, m with
    | 0, 0 => true
    | 0, S _ => false
    | S _, 0 => false
    | S n', S m' => eqb n' m'
  end.

Definition eqn (n : nat) (a b : Stream nat) : Type :=
  let fix eqn_aux n a b :=
      let x := Nat.compare (hd a) (hd b) in
      match x with
      | Eq => match n with
              | 0 => True
              | S k => eqn_aux k (tl a) (tl b)
              end
      | _ => False
      end
      in (eqn_aux n a b).


Fixpoint prefix0 (n r : nat) : Stream nat :=
  match n with
  | 0 => const r
  | S k => Cons 0 (prefix0 k r)
  end.

Lemma nthprefix0 : forall n r : nat, Str_nth n (prefix0 n r) = r.
Proof.
  induction n ; auto.
Qed.

Lemma prefix_eq0 : forall (n r : nat), eqn n (const 0) (prefix0 (S n) r).
Proof.
  intros.
  unfold eqn ; unfold Nat.compare.
  induction n ; simpl ; auto.
Qed.

Lemma nthconst : forall (n : nat), Str_nth n (const 0) = 0.
Proof.
  induction n ; simpl ; auto.
Qed.


Definition non_Brouwer : (forall (f : (Stream nat) -> nat) (alpha : Stream nat),
                             {n : nat & (forall (bêta :Stream nat), (eqn n alpha bêta) -> (f alpha) = (f bêta) ) }) -> False.
Proof.
  intros.
  remember (H (fun a => 0) (const 0)) as s ; remember (projT1 s) as m.
  (*destruct s as [m e]*)
  remember (fun (bêta : Stream nat) => H (fun alpha => Str_nth (Str_nth (S m) alpha) bêta) (const 0)) as f ; simpl in f.
  remember (H (fun bêta => projT1 (f bêta)) (const 0)) as r ; simpl in r.
  (**destruct r as [r].**)
  remember (prefix0 (S m) (S (projT1 r))) as alpha.
  remember (prefix0 (S (projT1 r)) 1) as bêta.
  assert ((Str_nth (Str_nth (S m) alpha) bêta) = 1).
  rewrite Heqalpha ; rewrite Heqbêta.
  rewrite (nthprefix0 (S m) (S (projT1 r))) ; rewrite (nthprefix0 (S (projT1 r)) 1) ; auto.
  assert (projT1 (f (const 0)) = projT1 (f bêta)).
  apply (projT2 r) ; rewrite Heqbêta ; apply(prefix_eq0 (projT1 r) 1).
  assert ((Str_nth (Str_nth (S m) (const 0)) bêta) = 0).
  rewrite (nthconst (S m)).
  rewrite Heqbêta.
  unfold Str_nth ; auto.
  assert (0 = (Str_nth (Str_nth (S m) alpha) bêta)).
  rewrite <- H2.
  apply (projT2 (f bêta) alpha).
  assert (projT1 (f (const 0)) = m).
  assert ((fun alpha0 : Stream nat => Str_nth (Str_nth (S m) alpha0) (const 0)) = (fun x => 0)).
  unfold Str_nth.
  simpl (nthconst _).
  hnf.
  rewrite Heqf.
  unfold Str_nth.
  simpl.
  
  rewrite Heqf ; simpl.
  rewrite Heqf in H1.
  rewrite <- H1.
  auto.
  rewrite nthconst.
  rewrite Heqf in H3.
  rewrite H3.
  rewrite Heqalpha.
  apply prefix_eq0.
  rewrite <- H3 in H0.
  discriminate H0.
  rewrite Heqf.
  destruct s.
  rewrite Heqm.
  simpl.
  replace (m) with (projT1 s) in Heqalpha.
  rewrite Heqs.


  rewrite <- (nthconst (S m)).
  specialize f with bêta.
  apply (e1 alpha).
  specialize e0 with bêta.
  
  destruct (f 


  
  assert (H := H').