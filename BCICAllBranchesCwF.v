Set Definitional UIP.
Set Primitive Projections.
Require Import Dialogue.
Section BranchingTranslation.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Printing Width 90.
Variables (I : Set).
Variable (O : Set).
Variable (alpha : I -> O).

Section BlablaSProp.

Inductive True : SProp := T.
Inductive False : SProp :=.
Inductive and (A : SProp) (B : SProp) : SProp := conj : A -> B -> and A B.

Inductive eqn (A : Type) (x : A) : A -> SProp := refl : @eqn A x x.

Definition Sym (A : Type) (x y : A) (e : eqn x y) : eqn y x :=
  match e with | refl _ => refl _ end.

Definition Trans (A : Type) (x y z : A) (e : eqn x y) (e' : eqn y z) : eqn x z :=
  match e, e' with |refl _, refl _ => refl _ end.


Definition Rw (A B : Type) (e : eqn A B) (x : A) : B :=
  match e with | refl _ => x end.


Definition SRw (A B : SProp) (e : eqn A B) (x : A) : B :=
  match e with | refl _ => x end.

Definition Ap (A B : Type) (f : A -> B) (x y : A) (e : eqn x y) :
  eqn (f x) (f y) :=
  match e with
  | refl _ => refl (f x)
  end.

Definition ApD (A : Type) (B : A -> Type) (f : forall x, B x) (x y : A) (e : eqn x y) :
  eqn (Rw (Ap B e) (f x)) (f y) :=
  match e with
  | refl _ => refl _
  end.

Definition SAp (A : Type) (f : A -> SProp) (x y : A) (e : eqn x y) :
  eqn (f x) (f y) :=
  match e with
  | refl _ => refl (f x)
  end.


Definition Rw_sym1 (A B : Type) (e : eqn A B) (x: A):  (eqn (Rw (Sym e) (Rw e x)) x) :=
  match e with | refl _ => refl _ end.


Definition Rw_sym2 (A B : Type) (e : eqn A B) (x: B):  (eqn (Rw e (Rw (Sym e) x)) x).
Proof.
  now destruct e.
Defined.

Definition Rw_e_syme (A B : Type) (eq1 : eqn A B) (a : A) (b : B) (eq2 : eqn (Rw eq1 a) b) :
  eqn a (Rw (Sym eq1) b).
Proof.
  now destruct eq1.
Defined.

Definition Rw_syme_e (A B : Type) (eq1 : eqn A B) (a : A) (b : B) (eq2 : eqn a (Rw (Sym eq1) b)) :
  eqn (Rw eq1 a) b .
Proof.
  now destruct eq1.
Defined.

Definition Rw_trans (A B C : Type) (eq1 : eqn A B) (eq2 : eqn B C) (x : A) :
  eqn (Rw eq2 (Rw eq1 x)) (Rw (Trans eq1 eq2) x).
Proof.
  destruct eq1.
  now destruct eq2.
Defined.
  

Lemma Rw_app_sym1 (A B C : Type) (f : A -> C) (g : B -> C)
      (eq : eqn A B)
      (e : forall x, eqn (f x) (g (Rw eq x))) :
  forall x , eqn (f (Rw (Sym eq) x)) (g x).
Proof.
  intro x.
  destruct eq.
  exact (e x).
Defined.

Lemma Rw_app_sym2 (A B C : Type) (f : A -> C) (g : B -> C)
      (eq : eqn A B)
      (e : forall x , eqn (f (Rw (Sym eq) x)) (g x)) :
  forall x, eqn (f x) (g (Rw eq x)).
Proof.
  intro x.
  destruct eq.
  exact (e x).
Defined.


Definition f_Sequal (A B : Type) (f g : A -> B) (e : eqn f g)
           (x : A) : eqn (f x) (g x) :=
  match e in (eqn _ h) return (eqn (f x) (h x)) with
  | refl _ => refl (f x)
  end.

Definition Df_Sequal (A : Type) (B : A -> Type)
           (f g : forall x : A, B x) (e : eqn f g)
           (x : A) : eqn (f x) (g x) :=
  match e in (eqn _ h) return (eqn (f x) (h x)) with
  | refl _ => refl (f x)
  end.

Definition SDf_Sequal (A : SProp) (B : A -> Type)
           (f g : forall x : A, B x) (e : eqn f g)
           (x : A) : eqn (f x) (g x) :=
  match e in (eqn _ h) return (eqn (f x) (h x)) with
  | refl _ => refl (f x)
  end.



Record tsig (A : Type) (B : A -> Type) :=
  exist {fst : A ; snd : B fst}.
Arguments fst [_ _] _.


Definition Eq_tsig (A : Type) (B : A -> Type)
           (x y : tsig B)
           (efst : eqn x.(fst) y.(fst))
           (esnd : eqn (Rw (Ap B efst) x.(snd)) y.(snd)) :
  eqn x y.
Proof.
  destruct x ; destruct y ; simpl in *.
  destruct esnd.
  destruct efst.
  reflexivity.
Defined.


Lemma BiRw_app_sym1 (A B C : Type)
      (Ae : A -> SProp)
      (Be : B -> SProp)
      (f : forall (x : A), Ae x -> C)
      (g : forall (x : B), Be x -> C)
      (eq : eqn A B)
      (eq2 : eqn Ae (fun x => Be (Rw eq x)))
      (e : forall x xe, eqn (f x xe) (g (Rw eq x) (SRw (f_Sequal eq2 x) xe))) :
  forall (x : B) (xe : Be x),
    eqn (f (Rw (Sym eq) x) (SRw (Sym (Rw_app_sym1 (g:= Be) (eq:= eq) (f_Sequal eq2) x)) xe))
        (g x xe).
Proof.
  intros.
  destruct eq.
  simpl in *.
  change (eqn Ae Be) in eq2.
  destruct eq2.
  exact (e x xe).
Defined.

Lemma BiRw_app_sym2 (A B C : Type)
      (Ae : A -> SProp)
      (Be : B -> SProp)
      (f : forall (x : A), Ae x -> C)
      (g : forall (x : B), Be x -> C)
      (eq : eqn A B)
      (eq2 : eqn Ae (fun x => Be (Rw eq x)))
      (e:  forall (x : B) (xe : Be x),
    eqn (f (Rw (Sym eq) x) (SRw (Sym (Rw_app_sym1 (g:= Be) (eq:= eq) (f_Sequal eq2) x)) xe))
        (g x xe)) :
  forall x xe, eqn (f x xe) (g (Rw eq x) (SRw (f_Sequal eq2 x) xe)).
Proof.
  intros.
  destruct eq.
  simpl in *.
  change (eqn Ae Be) in eq2.
  destruct eq2.
  exact (e x xe).
Defined.

  
Axiom F : False.
Ltac admit := case F.


Axiom Funext :
  forall (A B : Type) (f g : A -> B) (e : forall x, eqn (f x) (g x)), eqn f g.

Axiom SFunext :
  forall (A : SProp) (B : Type) (f g : A -> B) (e : forall x, eqn (f x) (g x)), eqn f g.

Axiom DFunext :
  forall (A : Type) (B : A -> Type)
         (f g : forall x : A, B x)
         (e : forall x, eqn (f x) (g x)), eqn f g.

Axiom SDFunext :
  forall (A : SProp) (B : A -> Type)
         (f g : forall x : A, B x)
         (e : forall x, eqn (f x) (g x)), eqn f g.

Lemma GFunext1 : forall (A: Type) (B1 B2 : A->Type)
                        (eq2 : eqn B1 B2)
                        (f : forall x : A, B1 x) (g : forall x : A, B2 x)
                        (eq3: forall x : A, eqn (Rw (f_Sequal eq2 x) (f x)) (g x)),
    eqn (fun x => (Rw (f_Sequal eq2 x) (f x))) g.
Proof.
  intros.
  destruct eq2.
  simpl in *.
  exact (DFunext eq3).
Qed.

Lemma GFunext2 : forall (A1 A2 : Type) (B1 : A1->Type) (B2 : A2 -> Type)
          (eq1 : eqn A1 A2) (eq2 : eqn (fun x => B1 (Rw (Sym eq1) x)) B2)
          (f : forall x : A1, B1 x) (g : forall x : A2, B2 x)
          (eq3: forall x : A2, eqn (Rw (f_Sequal eq2 x) (f (Rw (Sym eq1) x))) (g x)),
    eqn (fun x => (Rw (f_Sequal eq2 x) (f (Rw (Sym eq1) x)))) g.
Proof.
  intros.
  destruct eq2.
  destruct eq1.
  simpl in *.
  exact (DFunext eq3).
Qed.



Lemma Piext :
  forall (A1 A2 : Type) (B1 : A1 -> Type) (B2 : A2 -> Type)
         (eq1 : eqn A1 A2) (eq2 : eqn B1 (fun x => B2 (Rw eq1 x))),
           eqn (forall x : A1, B1 x) (forall x : A2, B2 x).
Proof.
  intros.
  destruct (Sym eq2).
  now destruct eq1.
Defined.

Lemma PiSext :
  forall (A1 A2 : Type) (B1 : A1 -> SProp) (B2 : A2 -> SProp)
         (eq1 : eqn A1 A2) (eq2 : eqn B1 (fun x => B2 (Rw eq1 x))),
           eqn (forall x : A1, B1 x) (forall x : A2, B2 x).
Proof.
  intros.
  destruct (Sym eq2).
  now destruct eq1.
Defined.

Lemma SPiext :
  forall (A1 A2 : SProp) (B1 : A1 -> Type) (B2 : A2 -> Type)
         (eq1 : eqn A1 A2) (eq2 : eqn B1 (fun x => B2 (SRw eq1 x))),
           eqn (forall x : A1, B1 x) (forall x : A2, B2 x).
Proof.
  intros.
  destruct (Sym eq2).
  now destruct eq1.
Defined.

Lemma SPiSext :
  forall (A1 A2 : SProp) (B1 : A1 -> SProp) (B2 : A2 -> SProp)
         (eq1 : eqn A1 A2) (eq2 : eqn B1 (fun x => B2 (SRw eq1 x))),
           eqn (forall x : A1, B1 x) (forall x : A2, B2 x).
Proof.
  intros.
  destruct (Sym eq2).
  now destruct eq1.
Defined.

Lemma BiPiext (A : Type) (Ae : A -> A -> SProp) (B : forall (x : A) (xe : Ae x x), Type)
      (f : forall (x : A) (xe : Ae x x), B x xe)
      (C : Type)
      (Ce : C -> C -> SProp)
      (D : forall (x : C) (xe : Ce x x), Type)
      (eq1 : eqn A C)
      (eq2 : forall x : A, eqn (Ae x x) (Ce (Rw eq1 x) (Rw eq1 x)))
      (eq3 : forall (x : A) (xe : Ae x x), eqn (B x xe) (D (Rw eq1 x) (SRw (eq2 x) xe))) :
  eqn (forall (x : A) (xe : Ae x x), B x xe) (forall (x : C) (xe : Ce x x), D x xe).
Proof.
  unshelve refine (Piext _).
  - exact eq1.
  - refine (Funext (fun x => _)).
    unshelve refine (SPiext _).
    + exact (eq2 x).
    + refine (SFunext (fun xe => _)).
      exact (eq3 x xe).
Defined.  
  
  

Lemma Rw_fun (A : Type) (B : A -> Type)
      (f : forall x : A, B x)
      (C : Type)
      (D : C -> Type)
      (eq1 : eqn C A)
      (eq2 : forall x : C, eqn (B (Rw eq1 x)) (D x))
      (eq : eqn (forall x : A, B x) (forall x : C, D x))
      (c : C) :
  eqn (Rw eq f c) (Rw (eq2 c) (f (Rw eq1 c))).
Proof.
  change (eqn (Rw (@Piext _ _ _ _ (Sym eq1) (Funext (Rw_app_sym2 eq2))) f c)
              (Rw (f_Sequal (Funext eq2) c) (f (Rw eq1 c)))).
  clear eq.
  destruct eq1 ; simpl in *.
  now destruct (Funext eq2).
Defined.
Print Funext.




Lemma Rw_Bifun (A : Type) (Ae : A -> A -> SProp) (B : forall (x : A) (xe : Ae x x), Type)
      (f : forall (x : A) (xe : Ae x x), B x xe)
      (C : Type)
      (Ce : C -> C -> SProp)
      (D : forall (x : C) (xe : Ce x x), Type)
      (eq1 : eqn C A)
      (eq2 : forall x : C, eqn (Ce x x) (Ae (Rw eq1 x) (Rw eq1 x)))
      (eq3 : forall (x : C) (xe : Ce x x), eqn (B (Rw eq1 x) (SRw (eq2 x) xe)) (D x xe))
      (eq : eqn (forall (x : A) (xe : Ae x x), B x xe) (forall (x : C) (xe : Ce x x), D x xe))
      (c : C) (ce : Ce c c):
  eqn (Rw eq f c ce) (Rw (eq3 c ce) (f (Rw eq1 c) (SRw (eq2 c) ce))).
Proof.
  change (forall (x : C) (xe : Ce x x), eqn (B (Rw eq1 x)
                                               (SRw ((f_Sequal (Funext eq2)) x) xe))
                                            (D x xe)) in eq3.
  change (eqn (Rw (@Piext _ _ (fun x : A => forall xe : Ae x x, B x xe)
                          (fun x : C => forall xe : Ce x x, D x xe) (Sym eq1)
                          ((Funext (@Rw_app_sym2 _ _ _ (fun x : A => forall xe : Ae x x, B x xe)
                          (fun x : C => forall xe : Ce x x, D x xe) (Sym eq1)
                          (fun c => Sym (SPiext (Df_Sequal (DFunext (fun x : C => (Sym (SFunext (fun xe : Ce x x => eq3 x xe))))) c)))))))
                  f c ce)
              (Rw (Sym (SDf_Sequal (Df_Sequal (DFunext
                                                 (fun (x : C) =>
                                                    (Sym (SFunext (fun xe => eq3 x xe))))) c) ce))
                  (f (Rw eq1 c) (SRw ((f_Sequal (Funext eq2)) c) ce)))).
  revert eq3.
  generalize (Funext eq2) ; clear eq2 ; intros eq2 eq3.
  generalize (DFunext (fun (x : C) => (Sym (SFunext (fun xe => eq3 x xe))))).
  clear eq3 ; intro eq3.
  destruct eq1 ; simpl in *.
  clear eq.
  revert f.
  change (eqn D
              (fun (x : C) (xe : Ce x x) => B x (@SRw ((fun x : C => Ce x x) x)
                                                     ((fun x : C => Ae x x) x)
                                                     (f_Sequal eq2 x) xe)))
         in eq3.
  change (forall f : forall (x : C) (xe : (fun x : C => Ae x x) x), B x xe,
             eqn
               (Rw
                  (@Piext C C (fun x : C => forall xe : (fun x : C => Ae x x) x,
                                  B x xe)
                         (fun x : C => forall xe : (fun x : C => Ce x x) x, D x xe)
                         (refl C)
                         (Sym
                            (@Funext C Type
                                    (fun c0 : C => forall x : (fun x : C => Ce x x) c0, D c0 x)
                                    (fun c0 : C => forall x : (fun x : C => Ae x x) c0, B c0 x)
                                    (fun c0 : C =>
                                       @SPiext ((fun x : C => Ce x x) c0)
                                              ((fun x : C => Ae x x) c0)
                                              (D c0) (B c0)
                                              (f_Sequal eq2 c0)
                                              (@Df_Sequal _
                                                         (fun x : C => ((fun x : C => Ce x x) x)
                                                                       -> Type) _
                                                         (fun (x : C)
                                                              (xe : (fun x : C => Ce x x) x) =>
                                                            B x (@SRw ((fun x : C => Ce x x) x)
                                                                     ((fun x : C => Ae x x) x)
                                                                     (f_Sequal eq2 x) xe))
                                                         eq3 c0)))))
                  f c ce)
               (Rw
                  (Sym
                     (@SDf_Sequal ((fun x : C => Ce x x) c)
                                 (fun _ : (fun x : C => Ce x x) c => Type) (D c)
                                 (fun xe : (fun x : C => Ce x x) c =>
                                    B c (@SRw ((fun x : C => Ce x x) c)
                                             ((fun x : C => Ae x x) c)
                                             (f_Sequal eq2 c) xe))
                                 (@Df_Sequal C
                                            (fun x : C => (fun x : C => Ce x x) x -> Type) D
                                            (fun (x : C) (xe : (fun x : C => Ce x x) x) =>
                                               B x (@SRw ((fun x : C => Ce x x) x)
                                                        ((fun x : C => Ae x x) x)
                                                        (f_Sequal eq2 x) xe)) eq3 c) ce))
                  (f c (@SRw ((fun x : C => Ce x x) c)
                            ((fun x : C => Ae x x) c)
                            (f_Sequal eq2 c) ce)))).
  change (forall x : C, (fun x => Ae x x) x -> Type) in B.
  change (eqn D
          (fun (x : C) (xe : (fun x : C => Ce x x) x) =>
             B x
               (@SRw ((fun x0 : C => Ce x0 x0) x)
                    ((fun x0 : C => Ae x0 x0) x)
                    (f_Sequal eq2 x) xe))) in eq3.
  destruct eq2.
  change (eqn D B) in eq3.
  now destruct eq3.
Defined.


Lemma Rw_Bifun2 (A : Type) (Ae : A -> A -> SProp) (B : forall (x : A) (xe : Ae x x), Type)
      (f : forall (x : A) (xe : Ae x x), B x xe)
      (C : Type)
      (Ce : C -> C -> SProp)
      (D : forall (x : C) (xe : Ce x x), Type)
      (eq1 : eqn A C)
      (eq2 : forall x : A, eqn (Ae x x) (Ce (Rw eq1 x) (Rw eq1 x)))
      (eq3 : forall (x : A) (xe : Ae x x), eqn (B x xe) (D (Rw eq1 x) (SRw (eq2 x) xe)))
      (eq : eqn (forall (x : A) (xe : Ae x x), B x xe) (forall (x : C) (xe : Ce x x), D x xe))
      (a : A) (ae : Ae a a):
  eqn (Rw eq f (Rw eq1 a) (SRw (eq2 a) ae))
      (Rw (eq3 a ae) (f a ae)).
Proof.
  simpl.
  refine (Trans (@Rw_Bifun A Ae B f C Ce D (Sym eq1) (Rw_app_sym2 (fun x => Sym (eq2 x)))
                               (BiRw_app_sym1 (g:= D) (eq:= eq1) (eq2:= Funext eq2) eq3) eq
                               (Rw eq1 a) (@SRw (Ae a a) (Ce (Rw eq1 a) (Rw eq1 a)) (eq2 a) ae)) _).
  simpl.
  clear eq.
  generalize (((BiRw_app_sym1 (g:= D) (eq:= eq1) (eq2:= Funext eq2) eq3) _ (SRw (eq2 a) ae))).
  intro e.
  destruct eq1.
  simpl in *.
  change (eqn (B a (SRw (Sym (eq2 a)) (SRw (eq2 a) ae)))
              (D a (SRw (eq2 a) ae))) in e.
  revert e.
  generalize (eq3 a ae).
  clear eq3.
  generalize (eq2 a).
  clear eq2.
  intros eq1.
  generalize (D a (SRw eq1 ae)).
  clear D.
  intros D eq2 eq3.
  destruct eq1.
  simpl.
  reflexivity.
Defined.

Arguments SRw [_ _] _.
Arguments SDf_Sequal [_ _ _ _ ] _.
Arguments Df_Sequal [_ _ _ _ ] _.
Arguments Piext [_ _ _ _] _.
Arguments SPiext [_ _ _ _] _.
Arguments PiSext [_ _ _ _] _.
Arguments SPiSext [_ _ _ _] _.
Arguments Funext [_ _ _ _] _.

Record BCtx : Type := {
  L : Type ;
  R : Type;
  P : forall (tl : L) (tr : R), Type;
  SP : forall (l1 l2 : L) (r1 r2 : R) (e1 : P l1 r1) (e2 : P l2 r2), SProp;
  }.


Arguments P _ : clear implicits.
Arguments SP _ [_ _ _ _] _.


Record Sub (Gamma Delta : BCtx) := {
  subL : forall (gl : Gamma.(L)) (gr : Gamma.(R)) (ge : Gamma.(P) gl gr)
                                    (se : Gamma.(SP) ge ge), Delta.(L);
  subR : forall (gl : Gamma.(L)) (gr : Gamma.(R)) (ge : Gamma.(P) gl gr)
                (se : Gamma.(SP) ge ge), Delta.(R);
  subP : forall (gl : Gamma.(L)) (gr : Gamma.(R)) (ge : Gamma.(P) gl gr)
                (se : Gamma.(SP) ge ge), Delta.(P) (subL se) (subR se);
  subSP : forall (gl1 gl2 : Gamma.(L)) (gr1 gr2 : Gamma.(R))
                 (ge1 : Gamma.(P) gl1 gr1) (ge2 : Gamma.(P) gl2 gr2) 
                 (se1 : Gamma.(SP) ge1 ge1) (se2 : Gamma.(SP) ge2 ge2)
                 (se : Gamma.(SP) ge1 ge2),
      Delta.(SP) (subP se1) (subP se2);
  
  }.

Arguments subL {_ _}.
Arguments subR {_ _}.
Arguments subP {_ _} _.
Arguments subSP {_ _} _ [_ _ _ _ _ _].

Definition Idn {Γ : BCtx} : Sub Γ Γ.
Proof.
  unshelve econstructor.
  + exact (fun gl gr ge se => gl).
  + exact (fun gl gr ge se => gr).
  + exact (fun gl gr ge se => ge).
  + exact (fun gl1 gl2 gr1 gr2 ge1 ge2 se1 se2 se => se).
Defined.

Definition Cmp {G D X : BCtx} (s : Sub G D) (r : Sub D X) : Sub G X.
Proof.
  unshelve econstructor.
  - unshelve refine (fun g ge e se => r.(subL) _ _ _ _).
    + exact (s.(subL) g ge e se).
    + exact (s.(subR) g ge e se).
    + exact (s.(subP) g ge e se).
    + exact (s.(subSP) _ _ se).
  - unshelve refine (fun g ge e se => r.(subR) _ _ _ _).
    + exact (s.(subL) g ge e se).
    + exact (s.(subR) g ge e se).
    + exact (s.(subP) g ge e se).
    + exact (s.(subSP) _ _ se).
  - unshelve refine (fun g ge e se => r.(subP) _ _ _ _).
  - unshelve refine (fun gl1 gl2 gr1 gr2 ge1 ge2 se1 se2 se => r.(subSP) _ _ _).
    + exact (s.(subSP) _ _ se).
Defined.

Lemma Idn_Cmp : forall {Γ Δ} (σ : Sub Γ Δ), Cmp Idn σ = σ.
Proof.
reflexivity.
Qed.

Lemma Cmp_Idn : forall {Γ Δ} (σ : Sub Γ Δ), Cmp σ Idn = σ.
Proof.
reflexivity.
Qed.

Lemma Cmp_Cmp : forall {Γ Δ Ξ Ω} (σ : Sub Γ Δ) (ρ : Sub Δ Ξ) (θ : Sub Ξ Ω),
  Cmp (Cmp σ ρ) θ = Cmp σ (Cmp ρ θ).
Proof.
reflexivity.
Qed.


Record Ssig (A : SProp) (B : A -> SProp) : SProp :=
  Sexist { Sone : A;  Stwo : B Sone }.

Record Ssig2 (A : SProp) (B : SProp) (C : A -> B -> SProp) : SProp :=
  Sexist3 { Sone3 : A;  Stwo3 : B ; Sthree3 : C Sone3 Stwo3 }.

Definition Threeqn (A B C D : Type)
           (P1 : tsig (fun (P : A -> C -> Type) =>
                         forall (a1 a2 : A) (c1 c2 : C) (p1 : P a1 c1) (p2 : P a2 c2),
                           SProp))
           (P2 : tsig (fun (P : B -> D -> Type) =>
                         forall (b1 b2 : B) (d1 d2 : D) (p1 : P b1 d1) (p2 : P b2 d2),
                           SProp)) : SProp.
Proof.
  unshelve refine (Ssig2 _).
  - exact (eqn A B).
  - exact (eqn C D).
  - intros e1 e2.
    refine (eqn P1 _).
    unshelve econstructor.
    + intros a b.
      exact (P2.(fst) (Rw e1 a) (Rw e2 b)).
    + simpl.
      intros.
      exact (P2.(snd) _ _ _ _ p1 p2).
Defined.    
  
Record BTy (Gamma : BCtx) : Type := {
  typL : forall (gl : Gamma.(L)) (gr : Gamma.(R)) (ge : Gamma.(P) gl gr)
                                     (sge : Gamma.(SP) ge ge), Type;
  typR : forall (gl : Gamma.(L)) (gr : Gamma.(R)) (ge : Gamma.(P) gl gr)
                                     (sge : Gamma.(SP) ge ge), Type;
  typP : forall (gl : Gamma.(L)) (gr : Gamma.(R)) (ge : Gamma.(P) gl gr)
                                     (sge : Gamma.(SP) ge ge),
      tsig (fun (P : typL sge -> typR sge -> Type) =>
            forall (x1 y1 : typL sge) (x2 y2 : typR sge)
                   (xe : P x1 x2) (ye : P y1 y2),
              SProp);
  eps : forall (gl1 gl2 : Gamma.(L)) (gr1 gr2 : Gamma.(R))
               (ge1 : Gamma.(P) gl1 gr1) (ge2 : Gamma.(P) gl2 gr2) 
               (sge1 : Gamma.(SP) ge1 ge1) (sge2 : Gamma.(SP) ge2 ge2)
               (se : Gamma.(SP) ge1 ge2),
      @Threeqn (typL sge1) (typL sge2) (typR sge1) (typR sge2) (typP sge1) (typP sge2);
  }.
  

Arguments typL [_] _.
Arguments typR [_] _.
Arguments typP [_] _.
Arguments eps [_] _ [_ _ _ _ _ _] _.


Record SsigT (A : Type) (B : A -> SProp) : Type := SexisT { SoneT : A;  StwoT : B SoneT }.

Definition BU (Gamma : BCtx) : BTy Gamma.
Proof.
  unshelve econstructor.
  - exact (fun _ _ _ _ => Type). 
  - exact (fun _ _ _ _ => Type). 
  - simpl; unshelve refine (fun _ _ _ _ => _).
    unshelve econstructor.
    + intros A B.
      unshelve refine (tsig _).
      * exact (A -> B -> Type).
      * intro P.
        exact (forall (a1 a2 : A) (b1 b2 : B) (xe : P a1 b1) (ye : P a2 b2),
                  SProp).
    + simpl.
      intros A B C D P1 P2.
      unshelve refine (@Threeqn A B C D P1 P2).
  - simpl.
    intros.
    unshelve econstructor.
    + reflexivity.
    + reflexivity.
    + simpl.
      reflexivity.
Defined.


Definition Typ_subs {G D : BCtx} (s : Sub D G) (A : BTy G) : BTy D.
Proof.
  unshelve econstructor.
  - unshelve refine (fun dl dr de sde => A.(typL) _ _ _ _).
    + exact (s.(subL) dl dr de sde).
    + exact (s.(subR) dl dr de sde).
    + exact (s.(subP) dl dr de sde).
    + exact (s.(subSP) sde sde sde).
  - unshelve refine (fun dl dr de sde => A.(typR) _ _ _ _).
    + exact (s.(subL) dl dr de sde).
    + exact (s.(subR) dl dr de sde).
    + exact (s.(subP) dl dr de sde).
    + exact (s.(subSP) sde sde sde).
  - exact (fun dl dr de sde => A.(typP) _ _ _ _).
  - simpl.
    unshelve refine (fun dl1 dl2 dr1 dr2 de1 de2 sde1 sde2 sde => A.(eps) _ _ _).
    exact (s.(subSP) sde1 sde2 sde).
Defined.


Record Trm (Gamma : BCtx) (A : BTy Gamma) :=
  {
  eltL :  forall (gl : Gamma.(L)) (gr : Gamma.(R)) (ge : Gamma.(P) gl gr)
                                     (sge : Gamma.(SP) ge ge),
      A.(typL) gl gr ge sge ;
  eltR :  forall (gl : Gamma.(L)) (gr : Gamma.(R)) (ge : Gamma.(P) gl gr)
                                     (sge : Gamma.(SP) ge ge),
      A.(typR) gl gr ge sge ;
  eltP :  forall (gl : Gamma.(L)) (gr : Gamma.(R)) (ge : Gamma.(P) gl gr)
                                     (sge : Gamma.(SP) ge ge),
      (A.(typP) gl gr ge sge).(fst) (eltL sge) (eltR sge) ;
  eltSP : forall (gl1 gl2 : Gamma.(L)) (gr1 gr2 : Gamma.(R))
               (ge1 : Gamma.(P) gl1 gr1) (ge2 : Gamma.(P) gl2 gr2) 
               (sge1 : Gamma.(SP) ge1 ge1) (sge2 : Gamma.(SP) ge2 ge2)
               (se : Gamma.(SP) ge1 ge2),
      (A.(typP) _ _ _ sge2).(snd) _ _ _ _
                                  (Rw (f_Sequal (f_Sequal (Ap (@fst _ _) (A.(eps) _ _ se).(Sthree3)) _) _)
                                      (eltP sge1))
                                  (eltP sge2);
  }.
        
Arguments Trm _ : clear implicits.
Arguments eltL [_] [_] _ [_ _ _] _  .
Arguments eltR [_] [_] _ [_ _ _] _  .
Arguments eltP [_] [_] _  [_ _ _] _ .
Arguments eltSP [_] [_] _  [_ _ _ _ _ _] _.


Definition Trm_subs {G D : BCtx} (s : Sub D G) {A : BTy G}
  (t : @Trm G A) : @Trm D (Typ_subs s A).
Proof.
  unshelve econstructor.
  - simpl; refine (fun dl dr de sde => _).
    exact (t.(eltL) _).
  - simpl; refine (fun dl dr de sde => _).
    exact (t.(eltR) _).
  - simpl; refine (fun dl dr de sde => _).
    exact (t.(eltP) _).
  - simpl.
    refine (fun dl1 dl2 dr1 dr2 de1 de2 sde1 sde2 se => _).
    exact (t.(eltSP) _ _ _).
Defined.


Definition EL {Gamma : BCtx} (A : Trm Gamma (BU Gamma)) : BTy Gamma.
Proof.
  unshelve econstructor.
  - exact A.(eltL).
  - exact (A.(eltR)).
  - exact (A.(eltP)).
  - exact (A.(eltSP)).
Defined.

Notation " [ A ] " := (EL A).


Definition Rfl {Γ : BCtx} (A : BTy Γ) : Trm Γ (BU _).
Proof.
  unshelve econstructor; simpl.
  - exact A.(typL). 
  - exact A.(typR). 
  - exact (A.(typP)).
  - exact (A.(eps)).
Defined.

Lemma Rfl_EL : forall (Γ : BCtx) (A : BTy Γ), EL (Rfl A) = A.
Proof.
reflexivity.
Qed.


Definition Nil : BCtx.
Proof.
  unshelve econstructor.
  - refine unit.
  - refine unit.
  - exact (fun _ _ => unit).
  - exact (fun _ _ _ _ _ _ => True).
Defined.


Record Ext_car
       (A : Type) (B : Type) (C : A -> B  -> Type)
       (D : forall (l1 l2 : A) (r1 r2 : B) (e1 : C l1 r1) (e2 : C l2 r2), SProp)
       (E : forall (a : A) (b : B) (c : C a b) (d : D a a b b c c), Type) : Type :=
  exist4 { one : A;  two : B ; three : C one two ; four : D one one two two three three ;
           five : E one two three four}. 

Print tsig.

Record Prod (A B : Type) : Type := { pi1 : A ; pi2 : B }.
Record SProd (A B : SProp) : SProp := { spi1 : A ; spi2 : B }.



Definition Ext (Gamma : BCtx)
           (A : BTy Gamma) : BCtx.
Proof.
  unshelve econstructor.
  - unshelve refine (Ext_car _).
    + exact Gamma.(L).
    + exact Gamma.(R).
    + exact Gamma.(P).
    + exact Gamma.(SP).
    + exact A.(typL).
  - unshelve refine (Ext_car _).
    + exact Gamma.(L).
    + exact Gamma.(R).
    + exact Gamma.(P).
    + exact Gamma.(SP).
    + exact A.(typR).
  - intros.
    unshelve refine (tsig _).
    + unshelve refine (SsigT _).
      * unshelve refine (SsigT _).
       -- exact (Gamma.(P) tl.(one) tr.(two)).
       -- exact (fun p => Gamma.(SP) p p).
      * refine (fun p => SProd _ _).
        -- exact (Gamma.(SP) tl.(three) p.(SoneT)).
        -- exact (Gamma.(SP) tr.(three) p.(SoneT)).
    + 
  (*    * unshelve refine (SsigT _).
        -- unshelve refine (SsigT _).
           ++ exact (Gamma.(P) tr.(one) tl.(two)).
           ++ exact (fun p => Gamma.(SP) p p).
        -- exact (fun p => Gamma.(SP) tr.(three) p.(SoneT)).*)
      intro p.
      unshelve refine ((A.(typP) _ _ _ p.(SoneT).(StwoT)).(fst) _ _).
      -- refine (Rw _ tl.(five)).
         refine ((A.(eps) (four tl) (StwoT p.(SoneT)) _).(Sone3)).
         exact p.(StwoT).(spi1).
      -- refine (Rw _ tr.(five)).
         refine (A.(eps) (four tr) (StwoT p.(SoneT)) p.(StwoT).(spi2)).(Stwo3).
  - simpl.
    intros.
    unshelve refine (Ssig _).
    + refine (Gamma.(SP) e1.(fst).(SoneT).(SoneT) e2.(fst).(SoneT).(SoneT)).
    + intro se.
      unshelve refine ((A.(typP) _ _ _ e2.(fst).(SoneT).(StwoT)).(snd) _ _ _ _ _ e2.(snd)).
      * refine (Rw (A.(eps) (StwoT (SoneT e1.(fst))) (StwoT (SoneT e2.(fst))) se).(Sone3) _).
        refine (Rw (A.(eps) (four l1) (StwoT (SoneT e1.(fst))) (spi1 (StwoT e1.(fst)))).(Sone3) _).
        exact l1.(five).
      * refine (Rw (A.(eps) (StwoT (SoneT e1.(fst))) (StwoT (SoneT e2.(fst))) se).(Stwo3) _).
        refine (Rw (A.(eps) (four r1) (StwoT (SoneT e1.(fst))) (spi2 (StwoT e1.(fst)))).(Stwo3) _).
        exact r1.(five).
      * refine (Rw _ e1.(snd)).
        exact ((f_Sequal (f_Sequal (Ap (@fst _ _) (A.(eps) (StwoT (SoneT e1.(fst))) (StwoT (SoneT e2.(fst))) se).(Sthree3)) _) _)).
Defined.

Notation "Γ · A" := (@Ext Γ A) (at level 50, left associativity).



Definition Cns (Γ : BCtx) (A : BTy Γ) (t : Trm Γ A) : Sub Γ (@Ext Γ A).
Proof.
  unshelve econstructor; simpl.
  - refine (fun gl gr ge se => _).
    unshelve econstructor.
    + exact gl.
    + exact gr.
    + exact ge.
    + exact se.
    + exact (t.(eltL) _).
  - refine (fun gl gr ge se => _).
    unshelve econstructor.
    + exact gl.
    + exact gr.
    + exact ge.
    + exact se.
    + exact (t.(eltR) _).
  - refine (fun gl gr ge se => _).
    unshelve econstructor ; simpl.
    + unshelve econstructor.
      * exists ge.
        exact se.
      * unshelve econstructor ; assumption.
    + simpl.
      exact (t.(eltP) _).
  - simpl.
    intros.
    unshelve econstructor.
    + assumption.
    + exact (t.(eltSP) se1 se2 se).
Defined.

Definition Wkn (Γ : BCtx) (A : BTy Γ) : Sub (@Ext Γ A) Γ.
Proof.
  unshelve econstructor; simpl.
  - intros.
    exact gl.(one).
  - intros.
    exact gr.(two).
  - intros.
    simpl in *.
    exact ge.(fst).(SoneT).(SoneT).
  - simpl.
    intros.
    exact se.(Sone).
Defined.


Definition Lft {G D : BCtx} (s : Sub G D) (A : BTy D) :
  Sub (@Ext G (Typ_subs s A)) (@Ext D A).
Proof.
  unshelve econstructor.
  - simpl.
    intros ; unshelve econstructor.
    + refine (s.(subL) _ _ _ ge.(fst).(SoneT).(StwoT)).
    + refine (s.(subR) _ _ _ ge.(fst).(SoneT).(StwoT)).
    + refine (s.(subP) _ _ _ _ ).
    + refine (s.(subSP) _ _ _).
      refine ge.(fst).(SoneT).(StwoT).
    + refine (Rw _ gl.(five)).
      refine ((A.(eps) _ _ _).(Sone3)).
      refine (s.(subSP) _ _ _).
      refine (ge.(fst).(StwoT).(spi1)).
  - simpl.
    intros ; unshelve econstructor.
    + refine (s.(subL) _ _ _ ge.(fst).(SoneT).(StwoT)).
    + refine (s.(subR) _ _ _ ge.(fst).(SoneT).(StwoT)).
    + refine (s.(subP) _ _ _ _ ).
    + refine (s.(subSP) _ _ _).
      refine ge.(fst).(SoneT).(StwoT).
    + refine (Rw _ gr.(five)).
      refine ((A.(eps) _ _ _).(Stwo3)).
      refine (s.(subSP) _ _ _).
      refine (ge.(fst).(StwoT).(spi2)).
  - simpl.
    intros ; unshelve econstructor.
    + unshelve econstructor.
      * unshelve econstructor.
        -- refine (s.(subP) _ _ _ _).
        -- refine (s.(subSP) _ _ _).
           refine ge.(fst).(SoneT).(StwoT).
      * unshelve econstructor.
        -- refine (s.(subSP) _ _ _).
           refine ge.(fst).(SoneT).(StwoT).
        -- refine (s.(subSP) _ _ _).
           refine ge.(fst).(SoneT).(StwoT).
    + simpl.
      exact ge.(snd).
  - simpl.
    intros.
    unshelve econstructor.
    + refine (s.(subSP) _ _ _).
      refine se.(Sone).
    + simpl.
      refine se.(Stwo).
Defined.      


Definition Var {Γ : BCtx} {A : BTy Γ} : Trm (@Ext Γ A) (Typ_subs (@Wkn Γ A) A).
Proof.
  unshelve econstructor.
  - intros gl gr ge sge.
    refine (Rw _ gl.(five)).
    refine (A.(eps) _ _ _).(Sone3).
    exact ge.(fst).(StwoT).(spi1).
  - intros gl gr ge sge.
    refine (Rw _ gr.(five)).
    refine (A.(eps) _ _ _).(Stwo3).
    exact ge.(fst).(StwoT).(spi2).
  - intros gl gr ge sge.
    exact ge.(snd).
  - intros.
    exact se.(Stwo).
Defined.    


(*Definition BSub (Gamma : BCtx)
           (A : BTy Gamma) (B : BTy (Gamma · A)) (a : Trm A) : BTy Gamma.
Proof.
  unshelve econstructor.
  - unshelve refine (fun gamma gammae => B.(typ) (Sexist _)).
    + unshelve refine (exist3 _).
      * exact (gamma).
      * exact gammae.
      * exact (a.(elt) gamma gammae).
    + exact gammae.
    + exact (a.(par) _ _ _ _ _).
  - simpl.
    unshelve refine (fun g1 g2 g1e g2e e => _).
    unshelve refine (B.(eps) _ _ _ _ _).
    unshelve refine (Sexist _).
    + exact e.
    + exact (a.(par) _ _ _ _ _).
Defined.*)

Lemma SEq_Fun_App (A1 A2 : SProp) (B : A1 -> Type) (f : forall a : A1, B a)
                 (e1 : eqn A1 A2)
                 (e2 : eqn (forall a : A1, B a) (forall a : A2, B (SRw (Sym e1) a)))
                 (e3 : forall a : A1, eqn (B a) (B (SRw (Sym e1) (SRw e1 a)))) 
                 (a : A1):
             eqn (Rw (e3 a) (f a)) (Rw e2 f (SRw e1 a)).
Proof.
  intros.
  now destruct e1.
Defined.

Lemma Eq_Fun_App (A1 A2 : Type) (B : A1 -> Type) (f : forall a : A1, B a)
                 (e1 : eqn A1 A2)
                 (e2 : eqn (forall a : A1, B a) (forall a : A2, B (Rw (Sym e1) a)))
                 (e3 : forall a : A1, eqn (B a) (B (Rw (Sym e1) (Rw e1 a)))) 
                 (a : A1):
             eqn (Rw (e3 a) (f a)) (Rw e2 f (Rw e1 a)).
Proof.
  intros.
  now destruct e1.
Defined.

Lemma Prd_eq_help : (forall (A1 A2 B1 B2 : Type) (C1 : A1 -> B1 -> Type) (C2 : A2 -> B2 -> Type)
                       (Ce1 : forall (a1 a2 : A1) (b1 b2 : B1), C1 a1 b1 -> C1 a2 b2 -> SProp)
                       (Ce2 : forall (a1 a2 : A2) (b1 b2 : B2), C2 a1 b1 -> C2 a2 b2 -> SProp)
                       (eA : eqn A1 A2) (eB : eqn B1 B2)
                       (e : @eqn (tsig (fun (P : A1 -> B1 -> Type) => forall a1 a2 b1 b2,
                                            P a1 b1 -> P a2 b2 -> SProp)) (exist Ce1)
                                 (@exist _ _ (fun a b => C2 (Rw eA a) (Rw eB b))
                                         (fun a1 a2 b1 b2 c1 c2 =>
                                           Ce2 (Rw eA a1) (Rw eA a2) (Rw eB b1) (Rw eB b2) c1 c2)))
                       (a : A1) (b : B1) (ae : C1 a b),
                   eqn (Ce1 a a b b ae ae)
                       ((Rw (Ap (fun t => forall (a1 a2 : A1) (b1 b2 : B1),
                                     t.(fst) a1 b1 -> t.(fst) a2 b2 -> SProp)
                                e) Ce1) _ _ _ _
                          (Rw (f_Sequal (f_Sequal (Ap (@fst _ _) e) a) b) ae)
                          (Rw (f_Sequal (f_Sequal (Ap (@fst _ _) e) a) b) ae))).
Proof.
  intros.
  destruct e.
  simpl.
  reflexivity.
Defined.

Lemma DPrd_eq_help (A1 A2 B1 B2 : Type) (C1 : A1 -> B1 -> Type)
      (C2 : A2 -> B2 -> Type)
      (Ce1 : forall (a1 a2 : A1) (b1 b2 : B1), C1 a1 b1 -> C1 a2 b2 -> SProp)
      (Ce2 : forall (a1 a2 : A2) (b1 b2 : B2), C2 a1 b1 -> C2 a2 b2 -> SProp)
      (eA : eqn A1 A2) (eB : eqn B1 B2)
      (e : @eqn (tsig (fun (P : A1 -> B1 -> Type) => forall a1 a2 b1 b2,
                           P a1 b1 -> P a2 b2 -> SProp)) (exist Ce1)
                (@exist _ _ (fun a b => C2 (Rw eA a) (Rw eB b))
                        (fun a1 a2 b1 b2 c1 c2 =>
                           Ce2 (Rw eA a1) (Rw eA a2) (Rw eB b1) (Rw eB b2) c1 c2)))
      (a1 a2 : A1) (b1 b2 : B1) (ae1 : C1 a1 b1) (ae2 : C1 a2 b2) :
  eqn (Ce1 a1 a2 b1 b2 ae1 ae2)
      ((Rw (Ap (fun t => forall (a1 a2 : A1) (b1 b2 : B1),
                    t.(fst) a1 b1 -> t.(fst) a2 b2 -> SProp)
               e) Ce1) _ _ _ _
                       (Rw (f_Sequal (f_Sequal (Ap (@fst _ _) e) a1) b1) ae1)
                       (Rw (f_Sequal (f_Sequal (Ap (@fst _ _) e) a2) b2) ae2)).
Proof.
  intros.
  destruct e.
  simpl.
  reflexivity.
Defined.



Lemma Prd_eq_help2 : forall (A1 A2 B1 B2 : Type) (C1 : A1 -> B1 -> Type) (C2 : A2 -> B2 -> Type)
                       (Ce1 : forall (a1 a2 : A1) (b1 b2 : B1), C1 a1 b1 -> C1 a2 b2 -> SProp)
                       (Ce2 : forall (a1 a2 : A2) (b1 b2 : B2), C2 a1 b1 -> C2 a2 b2 -> SProp)
                       (D1 : forall (a1 : A1) (b1 : B1) (c1 : C1 a1 b1) (ce1 : Ce1 a1 a1 b1 b1 c1 c1),
                           Type)
                       (D2 : forall (a2 : A2) (b2 : B2) (c2 : C2 a2 b2) (ce2 : Ce2 a2 a2 b2 b2 c2 c2),
                           Type)
                       (eA : eqn A1 A2) (eB : eqn B1 B2)
                       (eC : forall a b,
                           eqn (C1 a b)
                               (C2 (Rw eA a) (Rw eB b)))
                       (eCe : forall a1 b1 c1,
                           eqn (Ce1 a1 a1 b1 b1 c1 c1)
                               (Ce2 (Rw eA a1) (Rw eA a1) (Rw eB b1) (Rw eB b1)
                                    (Rw (eC a1 b1) c1)
                                    (Rw (eC a1 b1) c1)))
                       (ePi : eqn (forall (a1 : A1) (b1 : B1) (c1 : C1 a1 b1) (ce1 : Ce1 a1 a1 b1 b1 c1 c1),
                                      D1 a1 b1 c1 ce1)
                                   (forall (a2 : A2) (b2 : B2) (c2 : C2 a2 b2) (ce2 : Ce2 a2 a2 b2 b2 c2 c2),
                                       D2 a2 b2 c2 ce2))
                       (eD : forall a1 b1 c1 ce1,
                           eqn (D1 a1 b1 c1 ce1)
                               (D2 (Rw eA a1) (Rw eB b1) (Rw (eC a1 b1) c1)
                                   (SRw (eCe a1 b1 c1) ce1)))
                       (f : forall (a1 : A1) (b1 : B1) (c1 : C1 a1 b1) (ce1 : Ce1 a1 a1 b1 b1 c1 c1),
                           D1 a1 b1 c1 ce1)
                       (a1 : A1) (b1 : B1) (c1 : C1 a1 b1) (ce1 : Ce1 a1 a1 b1 b1 c1 c1),
                         eqn (Rw (eD a1 b1 c1 ce1) (f a1 b1 c1 ce1))
                             ((Rw ePi f) (Rw eA a1) (Rw eB b1) (Rw (eC a1 b1) c1)
                                         (SRw (eCe a1 b1 c1) ce1)).
Proof.
  intros.
  generalize (Sym (DFunext (fun x1 => DFunext (fun x2 => DFunext (fun y1 => SDFunext (fun y2 => eD x1 x2 y1 y2)))))) as eD2 ; intros.
  destruct eD2.
  simpl in *.
  change (forall (a1 : A1) (b1 : B1) (c1 : C1 a1 b1),
             eqn ((fun (x1 : A1) (y1 : B1) (z1 : C1 x1 y1) => Ce1 x1 x1 y1 y1 z1 z1) a1 b1 c1)
                 ((fun (x1 : A2) (y1 : B2) (z1 : C2 x1 y1) => Ce2 x1 x1 y1 y1 z1 z1) (Rw eA a1) (Rw eB b1) (Rw (eC a1 b1) c1))) in eCe.
  generalize (DFunext (fun x1 => DFunext (fun y1 => DFunext (fun z1 => eCe x1 y1 z1)))) as eCe2.
  generalize (DFunext (fun a => DFunext (fun b => eC a b))) as eC2 ; intros.
  destruct eA.
  destruct eB.
  change (eqn C1 C2) in eC2.
  destruct eC2.
  simpl in *.
  change (eqn ((f a1 b1 c1 ce1))
              (Rw ePi f a1 b1 c1 (SRw (eCe a1 b1 c1) ce1))).
  + unshelve refine
             (@Trans _ _
                     (@Rw _ (forall (b2 : B1) (c2 : C1 a1 b2) (ce2 : Ce2 a1 a1 b2 b2 c2 c2),
                                D2 a1 b2 c2 ce2)
                          _ (f a1) b1 c1 (SRw (eCe a1 b1 c1) ce1)) _ _ _).
    * simpl.
      refine (Piext (refl B1) (DFunext (fun b => _))).
      refine (Piext (refl _) (DFunext (fun c => _))).
      unshelve refine (SPiext _ _).
      -- exact (eCe _ _ _).
      -- reflexivity.
    * unshelve refine
               (@Trans _ _
                       (@Rw _ (forall (c2 : C1 a1 b1) (ce2 : Ce2 a1 a1 b1 b1 c2 c2),
                                  D2 a1 b1 c2 ce2)
                            _ (f a1 b1) c1 (SRw (eCe a1 b1 c1) ce1)) _ _ _).
      -- refine (Piext (refl _) (DFunext (fun c => _))).
           unshelve refine (SPiext _ _).
         ++ exact (eCe _ _ _).
         ++ reflexivity.
        -- unshelve refine
                 (@Trans _ _
                         (@Rw _ (forall (ce2 : Ce2 a1 a1 b1 b1 c1 c1),
                                    D2 a1 b1 c1 ce2)
                              _ (f a1 b1 c1) (SRw (eCe a1 b1 c1) ce1)) _ _ _).
           ++ refine (SPiext (eCe _ _ _) (refl _)).
           ++ generalize (eCe a1 b1 c1).
              generalize (f a1 b1 c1) ; intro g.
              intro eqC.
              exact (@SEq_Fun_App (Ce1 a1 a1 b1 b1 c1 c1) (Ce2 a1 a1 b1 b1 c1 c1) _ g eqC
                                     (SPiext eqC (refl (fun x : Ce1 a1 a1 b1 b1 c1 c1 => D2 a1 b1 c1 (SRw eqC x))))
                   (fun _ => refl _) ce1).
           ++ refine (SDf_Sequal _ _).
              exact (Sym (@Rw_fun (C1 _ _) _ (f a1 b1) (C1 _ _) (fun x => forall ce1, D2 a1 b1 x ce1)
                                  (refl _)
                                  (fun y => SPiext (eCe _ _ _) (refl _))
                                  (Piext (refl (C1 a1 b1))
                                         (DFunext
                                            (fun c : C1 a1 b1 =>
                                               SPiext (eCe a1 b1 c)
                                                      (refl
                                                         (fun x : Ce1 a1 a1 b1 b1 c c =>
                                                            D2 a1 (Rw (refl B1) b1) (Rw (refl (C1 a1 b1)) c)
                                                               (SRw (eCe a1 b1 c) x))))))
                                  c1)).
        -- refine (SDf_Sequal _ _).
           refine (Df_Sequal _ _).
           refine (Sym (@Rw_fun B1 _ (f a1) B1 (fun x => forall c1 ce1, D2 a1 x c1 ce1)
                                (refl _)
                       (fun x => Piext (refl _) (DFunext (fun y => SPiext (eCe _ _ _) (refl _))))
                       (Piext (refl B1)
                              (DFunext
                                 (fun b : B1 =>
                                    Piext (refl (C1 a1 b))
                                          (DFunext
                                             (fun c : C1 a1 b =>
                                                SPiext (eCe a1 b c)
                                                       (refl
                                                          (fun x : Ce1 a1 a1 b b c c =>
                                                             D2 a1 (Rw (refl B1) b) (Rw (refl (C1 a1 b)) c)
                                                                (SRw (eCe a1 b c) x))))))))
                  b1)).
      * simpl.
        refine (SDf_Sequal _ _).
        refine (Df_Sequal _ _).
        refine (Df_Sequal _ _).
        refine (Sym (@Rw_fun A1 _ f A1 (fun x => forall b c ce, D2 x b c ce)
                        (refl _)
                        (fun a => Piext (refl B1)
                                        (DFunext
                                           (fun b : B1 =>
                                              Piext (refl (C1 a b))
                                                    (DFunext
                                                       (fun c : C1 a b =>
                                                          SPiext (eCe a b c)
                                                                 (refl (fun x : Ce1 a a b b c c => D2 a b c (SRw (eCe a b c) x))))))))
                        ePi a1)).
Defined.


Lemma Eq_helpHL {Gamma : BCtx} (A : BTy Gamma) : forall gl1 gl2 gr1 gr2 ge1 ge2 sge1 sge2 se xl xr,
    eqn (fst (typP A gl1 gr1 ge1 sge1) xl xr)
        (fst (typP A gl2 gr2 ge2 sge2) (Rw (Sone3 (eps A sge1 sge2 se)) xl)
             (Rw (Stwo3 (eps A sge1 sge2 se)) xr)).
Proof.
  intros.
  exact (f_Sequal (f_Sequal (Ap (@fst _ _) (A.(eps) sge1 sge2 se).(Sthree3)) xl) xr).
Defined.

Definition Eq_helpHAE {Gamma : BCtx} (A : BTy Gamma)
           (gl1 gl2 : Gamma.(L)) (gr1 gr2 : Gamma.(R))
           (ge1 : Gamma.(P) gl1 gr1)
           (ge2 : Gamma.(P) gl2 gr2)
           (sge1 : Gamma.(SP) ge1 ge1)
           (sge2 : Gamma.(SP) ge2 ge2)
           (se : Gamma.(SP) ge1 ge2) :
  forall al1 al2 ar1 ar2 ae1 ae2,
  eqn (Rw (Ap (fun t : tsig
                         (fun P : typL A gl1 gr1 ge1 sge1 -> typR A gl1 gr1 ge1 sge1 -> Type
                          =>
                            forall (a1 a2 : typL A gl1 gr1 ge1 sge1)
                                   (c1 c2 : typR A gl1 gr1 ge1 sge1), 
                              P a1 c1 -> P a2 c2 -> SProp) =>
                 (fun P : typL A gl1 gr1 ge1 sge1 -> typR A gl1 gr1 ge1 sge1 -> Type =>
                    forall (a1 a2 : typL A gl1 gr1 ge1 sge1)
                           (c1 c2 : typR A gl1 gr1 ge1 sge1), P a1 c1 -> P a2 c2 -> SProp)
                   (fst t)) (Sthree3 (eps A sge1 sge2 se)))
          (snd (typP A gl1 gr1 ge1 sge1)) al1 al2 ar1 ar2 ae1 ae2)
      (snd
         {|
           fst :=
             fun (a : typL A gl1 gr1 ge1 sge1) (b : typR A gl1 gr1 ge1 sge1) =>
               fst (typP A gl2 gr2 ge2 sge2) (Rw (Sone3 (eps A sge1 sge2 se)) a)
                   (Rw (Stwo3 (eps A sge1 sge2 se)) b);
           snd :=
             fun (a1 a2 : typL A gl1 gr1 ge1 sge1) (c1 c2 : typR A gl1 gr1 ge1 sge1)
                 (p1 : fst (typP A gl2 gr2 ge2 sge2)
                           (Rw (Sone3 (eps A sge1 sge2 se)) a1)
                           (Rw (Stwo3 (eps A sge1 sge2 se)) c1))
                 (p2 : fst (typP A gl2 gr2 ge2 sge2)
                           (Rw (Sone3 (eps A sge1 sge2 se)) a2)
                           (Rw (Stwo3 (eps A sge1 sge2 se)) c2)) =>
               snd (typP A gl2 gr2 ge2 sge2) (Rw (Sone3 (eps A sge1 sge2 se)) a1)
                   (Rw (Sone3 (eps A sge1 sge2 se)) a2)
                   (Rw (Stwo3 (eps A sge1 sge2 se)) c1)
                   (Rw (Stwo3 (eps A sge1 sge2 se)) c2) p1 p2
            |} al1 al2 ar1 ar2 ae1 ae2).
Proof.
  exact (fun al1 al2 ar1 ar2 ae1 ae2 =>
    (Df_Sequal (Df_Sequal (Df_Sequal (Df_Sequal (Df_Sequal (Df_Sequal (ApD (@snd _ _) (A.(eps) sge1 sge2 se).(Sthree3)) al1) al2) ar1) ar2) ae1)) ae2).
Defined.

Print Eq_helpHAE.

Definition Prd_helpL {Gamma : BCtx} (A : BTy Gamma) (B : BTy (@Ext Gamma A)) :
  forall gl gr ge se (al : A.(typL) gl gr ge se) (ar : A.(typR) gl gr ge se)
         (ae : (A.(typP) gl gr ge se).(fst) al ar)
         (ase : (A.(typP) gl gr ge se).(snd) _ _ _ _ ae ae), Type. 
Proof.
  intros.
  unshelve refine (B.(typL) _ _ _ _).
    + exists gl gr ge se.
      exact al.
    + exists gl gr ge se.
      exact ar.
    + unshelve econstructor.
      * unshelve econstructor.
        -- exists ge.
           exact se.
        -- split ; exact se.
      * simpl.
        exact ae.
    + simpl.
      unshelve econstructor.
      * exact se.
      * exact ase.
Defined.


Definition Prd_helpR {Gamma : BCtx} (A : BTy Gamma) (B : BTy (@Ext Gamma A)) :
  forall gl gr ge se (al : A.(typL) gl gr ge se) (ar : A.(typR) gl gr ge se)
         (ae : (A.(typP) gl gr ge se).(fst) al ar)
         (ase : (A.(typP) gl gr ge se).(snd) _ _ _ _ ae ae), Type. 
Proof.
  intros.
  unshelve refine (B.(typR) _ _ _ _).
    + exists gl gr ge se.
      exact al.
    + exists gl gr ge se.
      exact ar.
    + unshelve econstructor.
      * unshelve econstructor.
        -- exists ge.
           exact se.
        -- split ; exact se.
      * simpl.
        exact ae.
    + simpl.
      unshelve econstructor.
      * exact se.
      * exact ase.
Defined.


Definition Prd_helpP {Gamma : BCtx} (A : BTy Gamma) (B : BTy (@Ext Gamma A)) :
  forall gl gr ge se (al : A.(typL) gl gr ge se) (ar : A.(typR) gl gr ge se)
         (ae : (A.(typP) gl gr ge se).(fst) al ar)
         (ase : (A.(typP) gl gr ge se).(snd) _ _ _ _ ae ae)
         (f : forall (al : typL A gl gr ge se) (ar : typR A gl gr ge se)
                     (ae : fst (typP A gl gr ge se) al ar)
                     (ase : snd (typP A gl gr ge se) al al ar ar ae ae), Prd_helpL B ase)
         (g : forall (al : typL A gl gr ge se) (ar : typR A gl gr ge se)
                     (ae : fst (typP A gl gr ge se) al ar)
                     (ase : snd (typP A gl gr ge se) al al ar ar ae ae), Prd_helpR B ase),
    Type. 
Proof.
  intros.
  unshelve refine ((B.(typP) _ _ _ _).(fst) _ _).
    + exists gl gr ge se.
      exact al.
    + exists gl gr ge se.
      exact ar.
    + unshelve econstructor.
      * unshelve econstructor.
        -- exists ge.
           exact se.
        -- split ; exact se.
      * simpl.
        exact ae.
    + simpl.
      unshelve econstructor.
      * exact se.
      * exact ase.
    + exact (f _ _ _ _).
    + exact (g _ _ _ _).
Defined.




Definition Prd_helpSP {Gamma : BCtx} (A : BTy Gamma) (B : BTy (@Ext Gamma A)) :
  forall gl gr ge se
         (al1 al2 : A.(typL) gl gr ge se) (ar1 ar2 : A.(typR) gl gr ge se)
         (ae1 : (A.(typP) gl gr ge se).(fst) al1 ar1)
         (ae2 : (A.(typP) gl gr ge se).(fst) al2 ar2)
         (ase1 : (A.(typP) gl gr ge se).(snd) _ _ _ _ ae1 ae1)
         (ase2 : (A.(typP) gl gr ge se).(snd) _ _ _ _ ae2 ae2)
         (ase : (A.(typP) gl gr ge se).(snd) _ _ _ _ ae1 ae2)
         (fl hl : forall (al : typL A gl gr ge se) (ar : typR A gl gr ge se)
                         (ae : fst (typP A gl gr ge se) al ar)
                         (ase : snd (typP A gl gr ge se) al al ar ar ae ae), 
             Prd_helpL B ase)
         (fr hr : forall (al : typL A gl gr ge se) (ar : typR A gl gr ge se)
                         (ae : fst (typP A gl gr ge se) al ar)
                         (ase : snd (typP A gl gr ge se) al al ar ar ae ae), 
             Prd_helpR B ase)
         (fe : forall (al : typL A gl gr ge se) (ar : typR A gl gr ge se)
                      (ae : fst (typP A gl gr ge se) al ar)
                      (ase : snd (typP A gl gr ge se) al al ar ar ae ae), 
             Prd_helpP ase fl fr)
         (he : forall (al : typL A gl gr ge se) (ar : typR A gl gr ge se)
                      (ae : fst (typP A gl gr ge se) al ar)
                      (ase : snd (typP A gl gr ge se) al al ar ar ae ae), 
             Prd_helpP ase hl hr),
    SProp. 
Proof.
  intros.
  unshelve refine ((B.(typP) _ _ _ _).(snd) _ _ _ _ _ _).
  * exists gl gr ge se.
    exact al2.
  * exists gl gr ge se.
    exact ar2.
  * unshelve econstructor.
    -- unshelve econstructor.
       ++ exists ge.
          exact se.
       ++ split ; exact se.
    -- simpl.
       exact ae2.
  * simpl.
    unshelve econstructor.
    -- exact se.
    -- exact ase2.
  * unshelve refine (Rw _ (fl _ _ _ ase1)).
    unshelve refine (B.(eps) _ _ _).(Sone3).
    simpl.
    exists se.
    exact ase.
  * exact (hl _ _ _ _).
  * unshelve refine (Rw _ (fr _ _ _ ase1)).
    unshelve refine (B.(eps) _ _ _).(Stwo3).
    simpl.
    exists se.
    exact ase.
  * exact (hr _ _ _ _).
  * unshelve refine (Rw _ (fe _ _ _ ase1)).
    unshelve refine (Df_Sequal (Df_Sequal (Ap (@fst _ _) ((B.(eps) _ _ _).(Sthree3))) _) _).
  * exact (he _ _ _ _).
Defined.

Definition Prd_eq_L {Gamma : BCtx} (A : BTy Gamma) (B : BTy (@Ext Gamma A)) :
  forall gl1 gl2 gr1 gr2 ge1 ge2 sge1 sge2 (se : SP Gamma ge1 ge2),
    eqn (forall (al : typL A gl1 gr1 ge1 sge1) (ar : typR A gl1 gr1 ge1 sge1)
         (ae : fst (typP A gl1 gr1 ge1 sge1) al ar)
         (ase : snd (typP A gl1 gr1 ge1 sge1) al al ar ar ae ae), 
    Prd_helpL B ase)
    (forall (al : typL A gl2 gr2 ge2 sge2) (ar : typR A gl2 gr2 ge2 sge2)
       (ae : fst (typP A gl2 gr2 ge2 sge2) al ar)
       (ase : snd (typP A gl2 gr2 ge2 sge2) al al ar ar ae ae), 
        Prd_helpL B ase).
Proof.
  intros.
  unshelve refine (Piext ((A.(eps) _ _ se).(Sone3)) _).
  apply DFunext ; intro al.
  unshelve refine (Piext ((A.(eps) _ _ se).(Stwo3)) _).
  apply DFunext ; intro ar.
  unshelve refine (Piext (Eq_helpHL _ _ _ _) _).
  apply DFunext ; intro ae.
  unshelve refine (SPiext _ _).
  * refine (Trans _ (Eq_helpHAE _ _)).
    exact (@Prd_eq_help _ _ _ _
                        (fst (typP A gl1 gr1 ge1 sge1))
                        (fst (typP A gl2 gr2 ge2 sge2))
                        (snd (typP A gl1 gr1 ge1 sge1))
                        (snd (typP A gl2 gr2 ge2 sge2))
                        _ _ _ al ar ae).
  * simpl.
    apply SDFunext ; intro sae.
    refine ((B.(eps) _ _ _).(Sone3)).
    simpl.
    unshelve econstructor.
    -- exact se.
    -- refine (SRw _ sae).
       refine (Trans _ (@Eq_helpHAE _ A _ _ _ _ _ _ _ _ se al al ar ar _ _)).
       exact (@Prd_eq_help _ _ _ _
                           (fst (typP A gl1 gr1 ge1 sge1))
                           (fst (typP A gl2 gr2 ge2 sge2))
                           (snd (typP A gl1 gr1 ge1 sge1))
                           (snd (typP A gl2 gr2 ge2 sge2))
                           _ _ _ al ar ae).
Defined.

Definition Prd_eq_R {Gamma : BCtx} (A : BTy Gamma) (B : BTy (@Ext Gamma A)) :
  forall gl1 gl2 gr1 gr2 ge1 ge2 sge1 sge2 (se : SP Gamma ge1 ge2),
    eqn (forall (al : typL A gl1 gr1 ge1 sge1) (ar : typR A gl1 gr1 ge1 sge1)
         (ae : fst (typP A gl1 gr1 ge1 sge1) al ar)
         (ase : snd (typP A gl1 gr1 ge1 sge1) al al ar ar ae ae), 
    Prd_helpR B ase)
    (forall (al : typL A gl2 gr2 ge2 sge2) (ar : typR A gl2 gr2 ge2 sge2)
       (ae : fst (typP A gl2 gr2 ge2 sge2) al ar)
       (ase : snd (typP A gl2 gr2 ge2 sge2) al al ar ar ae ae), 
        Prd_helpR B ase).
Proof.
  intros.
  unshelve refine (Piext ((A.(eps) _ _ se).(Sone3)) _).
  apply DFunext ; intro al.
  unshelve refine (Piext ((A.(eps) _ _ se).(Stwo3)) _).
  apply DFunext ; intro ar.
  unshelve refine (Piext (Eq_helpHL _ _ _ _) _).
  apply DFunext ; intro ae.
  unshelve refine (SPiext _ _).
  * refine (Trans _ (@Eq_helpHAE _ A _ _ _ _ _ _ _ _ se al al ar ar _ _)).
    exact (@Prd_eq_help _ _ _ _
                        (fst (typP A gl1 gr1 ge1 sge1))
                        (fst (typP A gl2 gr2 ge2 sge2))
                        (snd (typP A gl1 gr1 ge1 sge1))
                        (snd (typP A gl2 gr2 ge2 sge2))
                        _ _ _ al ar ae).
  * simpl.
    apply SDFunext ; intro sae.
    refine ((B.(eps) _ _ _).(Stwo3)).
    simpl.
    unshelve econstructor.
    -- exact se.
    -- refine (SRw _ sae).
       refine (Trans _ (@Eq_helpHAE _ A _ _ _ _ _ _ _ _ se al al ar ar _ _)).
       exact (@Prd_eq_help _ _ _ _
                           (fst (typP A gl1 gr1 ge1 sge1))
                           (fst (typP A gl2 gr2 ge2 sge2))
                           (snd (typP A gl1 gr1 ge1 sge1))
                           (snd (typP A gl2 gr2 ge2 sge2))
                           _ _ _ al ar ae).
Defined.

Definition Prd_eq_L2 {Gamma : BCtx} (A : BTy Gamma) (B : BTy (@Ext Gamma A)) :
  forall gl1 gl2 gr1 gr2 ge1 (ge2 : P Gamma gl2 gr2) sge1 sge2 (se : SP Gamma ge1 ge2)
         (al : typL A gl1 gr1 ge1 sge1) (ar : typR A gl1 gr1 ge1 sge1)
         (ae : fst (typP A gl1 gr1 ge1 sge1) al ar)
         (sae : snd (typP A gl1 gr1 ge1 sge1) al al ar ar ae ae),
             eqn (Prd_helpL B sae)
               (Prd_helpL B
                  (SRw
                     (Trans (Prd_eq_help (Sthree3 (eps A sge1 sge2 se)) ae)
                        (Eq_helpHAE (Rw (Eq_helpHL sge2 se al ar) ae) _)) sae)).
Proof.
  intros.
  refine ((B.(eps) _ _ _).(Sone3)).
  exists se.
  refine (SRw _ sae).
  refine ((Trans (Prd_eq_help (Sthree3 (eps A sge1 sge2 se)) ae) _)).
  exact (@Eq_helpHAE _ _ gl1 gl2 gr1 gr2 ge1 ge2 sge1 sge2 se al al ar ar _ _).
Defined.

Definition Prd_eq_R2 {Gamma : BCtx} (A : BTy Gamma) (B : BTy (@Ext Gamma A)) :
  forall gl1 gl2 gr1 gr2 ge1 (ge2 : P Gamma gl2 gr2) sge1 sge2 (se : SP Gamma ge1 ge2)
         (al : typL A gl1 gr1 ge1 sge1) (ar : typR A gl1 gr1 ge1 sge1)
         (ae : fst (typP A gl1 gr1 ge1 sge1) al ar)
         (sae : snd (typP A gl1 gr1 ge1 sge1) al al ar ar ae ae),
             eqn (Prd_helpR B sae)
               (Prd_helpR B
                  (SRw
                     (Trans (Prd_eq_help (Sthree3 (eps A sge1 sge2 se)) ae)
                        (Eq_helpHAE (Rw (Eq_helpHL sge2 se al ar) ae) _)) sae)).
Proof.
  intros.
  refine ((B.(eps) _ _ _).(Stwo3)).
  exists se.
  refine (SRw _ sae).
  refine ((Trans (Prd_eq_help (Sthree3 (eps A sge1 sge2 se)) ae) _)).
  exact (@Eq_helpHAE _ _ gl1 gl2 gr1 gr2 ge1 ge2 sge1 sge2 se al al ar ar _ _).
Defined.

Definition BiAp : forall (A B C : Type) (F : A -> B -> C) (x1 x2 : A) (y1 y2 : B)
                             (ex : eqn x1 x2) (ey : eqn y1 y2),
    eqn (F x1 y1) (F x2 y2).
Proof.
  intros.
  destruct ex.
  now destruct ey.
Defined.

Inductive Teq (A : Type) (x : A) : A -> Type := | Teq_refl : @Teq A x x.

Inductive ListP (A B : Type) (C : A -> B -> Type) : list A -> list B -> Type :=
| Pnil : ListP _ nil nil
| Pcons : forall (a : A) (b : B) (c : C a b) (la : list A) (lb : list B),
    ListP C la lb -> ListP C (cons a la) (cons b lb).
Arguments Pnil {_ _ _}.

Lemma Rw_ListP : forall (A1 A2 B1 B2 : Type) (C : A2 -> B2 -> Type)
                    (eqA : eqn A1 A2) (eqB : eqn B1 B2),
                      eqn (ListP (fun a b => C (Rw eqA a) (Rw eqB b)))
                          (fun la lb => ListP C (Rw (Ap list eqA) la) (Rw (Ap list eqB) lb)).
Proof.
  intros.
  destruct eqA.
  now destruct eqB.
Defined.
  
Inductive ListSP (A B : Type) (C : A -> B -> Type)
          (SC : forall a1 a2 b1 b2, C a1 b1 -> C a2 b2 -> SProp) :
  forall (la1 la2 : list A) (lb1 lb2 : list B), ListP C la1 lb1 -> ListP C la2 lb2 -> SProp :=
| SPnil : ListSP SC Pnil Pnil
| SPcons : forall (a1 a2 : A) (b1 b2 : B) (c1 : C a1 b1) (c2 : C a2 b2)
                  (sc : SC a1 a2 b1 b2 c1 c2)
                  (la1 la2 : list A) (lb1 lb2 : list B)
                  (lp1 : ListP C la1 lb1)
                  (lp2 : ListP C la2 lb2)
                  (lsp : ListSP SC lp1 lp2),
    ListSP SC (Pcons c1 lp1) (Pcons c2 lp2).
    


Definition List {Gamma : BCtx} : BTy (Gamma · BU Gamma).
Proof.
  unshelve econstructor.
  - intros.
    exact (@list gl.(five)).
  - intros.
    exact (@list gr.(five)).
  - intros.
    unshelve econstructor.
    + refine (ListP _).
      exact ge.(snd).(fst).
    + exact (ListSP ge.(snd).(snd)).
  - intros.
    unshelve econstructor.
    + refine (Ap list _).
      exact (se.(Stwo).(Sone3)).
    + simpl in se.
      refine (Ap list _).
      exact (se.(Stwo).(Stwo3)).
    + simpl.
      unshelve refine (Eq_tsig _).
      * simpl.
        unshelve refine (@Trans _ _ (ListP (fun (a : five gl1) (b : five gr1) =>
                                              fst (snd ge2) (Rw (Sone3 (Stwo se)) a) (Rw (Stwo3 (Stwo se)) b))) _ _ _).
        -- refine (Ap (@ListP _ _) _).
           exact (Ap (@fst _ _) se.(Stwo).(Sthree3)).
        -- refine (Rw_ListP _ _ _).
      * simpl.
        generalize ((Ap
          (fun P0 : list (five gl1) -> list (five gr1) -> Type =>
           forall (a1 a2 : list (five gl1)) (c1 c2 : list (five gr1)),
           P0 a1 c1 -> P0 a2 c2 -> SProp)
          (Trans
             (Ap (ListP (B:=five gr1))
                (Ap
                   (fst
                      (B:=fun P0 : five gl1 -> five gr1 -> Type =>
                          forall (a1 a2 : five gl1) (c1 c2 : five gr1),
                          P0 a1 c1 -> P0 a2 c2 -> SProp)) (Sthree3 (Stwo se))))
             (Rw_ListP (fst (snd ge2)) (Sone3 (Stwo se)) (Stwo3 (Stwo se)))))).
        simpl.
        intro e.
        change (eqn (Rw e (ListSP (snd (snd ge1))))
                    (fun (a1 a2 : list (five gl1)) (c1 c2 : list (five gr1))
                         (p1 : ListP (fst (snd ge2)) (Rw (Ap list (Sone3 (Stwo se))) a1)
                                     (Rw (Ap list (Stwo3 (Stwo se))) c1))
                         (p2 : ListP (fst (snd ge2)) (Rw (Ap list (Sone3 (Stwo se))) a2)
                                     (Rw (Ap list (Stwo3 (Stwo se))) c2)) => ListSP (snd (snd ge2)) p1 p2)).
        assert (forall (a1 a2 : list (five gl1)) (c1 c2 : list (five gr1)),
                   eqn (ListP (fst (snd ge1)) a1 c1 -> ListP (fst (snd ge1)) a2 c2 -> SProp)
                       (ListP (fst (snd ge2)) (Rw (Ap list (Sone3 (Stwo se))) a1)
                              (Rw (Ap list (Stwo3 (Stwo se))) c1) ->
                        ListP (fst (snd ge2)) (Rw (Ap list (Sone3 (Stwo se))) a2)
                              (Rw (Ap list (Stwo3 (Stwo se))) c2) -> SProp)) as H.
        -- intros.
           exact (Ap (fun P0 : list (five gl1) -> list (five gr1) -> Type =>
                         P0 a1 c1 -> P0 a2 c2 -> SProp)
                      (Trans
             (Ap (ListP (B:=five gr1))
                (Ap
                   (fst
                      (B:=fun P0 =>
                          forall (a1 a2 : five gl1) (c1 c2 : five gr1),
                          P0 a1 c1 -> P0 a2 c2 -> SProp)) (Sthree3 (Stwo se))))
             (Rw_ListP (fst (snd ge2)) (Sone3 (Stwo se)) (Stwo3 (Stwo se))))).
        -- refine (@Trans _ _ (fun (a1 a2 : list (five gl1)) (c1 c2 : list (five gr1)) =>
                                 Rw (H a1 a2 c1 c2) (ListSP (snd (snd ge1))
                                                            (la1:= a1)
                                                            (la2:= a2)
                                                            (lb1 := c1)
                                                            (lb2:= c2))) _ _ _).
           ++ pose (t:= (ApD (@snd _ _) se.(Stwo).(Sthree3))).
              simpl in t.

             eqn (ListP (fst (snd ge1)))
    (fun (a : list (five gl1)) (b : list (five gr1)) =>
     ListP (fst (snd ge2)) (Rw (Ap list (Sone3 (Stwo se))) a)
       (Rw (Ap list (Stwo3 (Stwo se))) b))

    + intros ll lr.
      unshelve refine (@list _).
      simpl in ge.
      refine (
    exact (@list gl.(five)).


Definition Eq {Gamma : BCtx} :
  BTy (Gamma · BU Gamma · [@Var Gamma (BU _)] · [(Trm_subs (Wkn _) (@Var Gamma (BU _)))]).
Proof.
  unshelve econstructor.
  - intros.
    refine (@eqn _ gl.(one).(five) gl.(five)).
  - intros.
    refine (@eqn _ gr.(two).(five) gr.(five)).
  - intros.
    unshelve econstructor.
    + intros e1 e2.
      destruct ge.
      simpl in snd0.
      destruct fst0.
      destruct SoneT0.
      simpl in snd0.
      destruct SoneT0.
      simpl in snd0.
      simpl in snd1.
      simpl in StwoT1.
      exact True.
    + simpl.
      exact (fun _ _ _ _ _ _ => True).
  - intros.
    unshelve econstructor.
    + 
    simpl.
      refine (@eqn _ _ ge.(five)).
      refine (@Teq _ e1 _).
    
    destruct gl.
    destruct one0.
    simpl in five1.
    simpl in five0.
  pose (t:= [@Var Gamma (BU _)]).

Definition Prd {Gamma : BCtx} (A : BTy Gamma) (B : BTy (@Ext Gamma A)) : BTy Gamma.
Proof.
  unshelve econstructor.
  - unshelve refine (fun gl gr ge se => _).
    refine (forall (al : A.(typL) gl gr ge se) (ar : A.(typR) gl gr ge se)
                   (ae : (A.(typP) gl gr ge se).(fst) al ar)
                   (ase : (A.(typP) gl gr ge se).(snd) _ _ _ _ ae ae), _).
    exact (Prd_helpL B ase).
  - unshelve refine (fun gl gr ge se => _).
    refine (forall (al : A.(typL) gl gr ge se) (ar : A.(typR) gl gr ge se)
                   (ae : (A.(typP) gl gr ge se).(fst) al ar)
                   (ase : (A.(typP) gl gr ge se).(snd) _ _ _ _ ae ae), _).
    exact (Prd_helpR B ase).
  - unshelve refine (fun gl gr ge se => _).
    unshelve econstructor.
    + intros f g.
      refine (forall (al : A.(typL) gl gr ge se) (ar : A.(typR) gl gr ge se)
                   (ae : (A.(typP) gl gr ge se).(fst) al ar)
                   (ase : (A.(typP) gl gr ge se).(snd) _ _ _ _ ae ae), _).
      intros.
      exact (Prd_helpP ase f g).
    + simpl.
      intros fl hl fr hr fe he.
      refine (forall (al1 al2 : A.(typL) gl gr ge se)
                     (ar1 ar2 : A.(typR) gl gr ge se)
                     (ae1 : (A.(typP) gl gr ge se).(fst) al1 ar1)
                     (ae2 : (A.(typP) gl gr ge se).(fst) al2 ar2)
                     (ase1 : (A.(typP) gl gr ge se).(snd) _ _ _ _ ae1 ae1)
                     (ase2 : (A.(typP) gl gr ge se).(snd) _ _ _ _ ae2 ae2)
                     (ase : (A.(typP) gl gr ge se).(snd) _ _ _ _ ae1 ae2), _ : SProp).
      refine (Prd_helpSP ase1 ase2 ase fe he).
  - intros.
    simpl.
    unshelve econstructor.
    + refine (Prd_eq_L _ _ _ se).
    + refine (Prd_eq_R _ _ _ se).
    + simpl.
      unshelve refine (Eq_tsig _).
      * simpl.
        simpl.
        apply DFunext.
        intro f.
        apply DFunext.
        intro g.
        unshelve refine (Piext ((A.(eps) _ _ se).(Sone3)) _).
        apply DFunext ; intro al.
        unshelve refine (Piext ((A.(eps) _ _ se).(Stwo3)) _).
        apply DFunext ; intro ar.
        unshelve refine (Piext (Eq_helpHL _ _ _ _ ) _).
        apply DFunext ; intro ae.
        unshelve refine (SPiext _ _).
        -- refine (Trans _ (Eq_helpHAE _ _)).
           exact (@Prd_eq_help _ _ _ _
                               (fst (typP A gl1 gr1 ge1 sge1))
                               (fst (typP A gl2 gr2 ge2 sge2))
                               (snd (typP A gl1 gr1 ge1 sge1))
                               (snd (typP A gl2 gr2 ge2 sge2))
                               _ _ _ al ar ae).
        -- simpl.
           apply SDFunext ; intro sae.
           unshelve refine (Trans (Df_Sequal (Df_Sequal (Ap (@fst _ _) ((B.(eps) _ _ _).(Sthree3))) _) _) _).
           ++ exists gl2 gr2 ge2 sge2.
              refine (Rw _ al).
              exact (A.(eps) _ _ se).(Sone3).
           ++ exists gl2 gr2 ge2 sge2.
              refine (Rw _ ar).
              exact (A.(eps) _ _ se).(Stwo3).
           ++ unshelve econstructor.
              ** unshelve econstructor.
                 --- exists ge2.
                     exact sge2.
                 --- split ; assumption.
              ** simpl.
                 refine (Rw _ ae).
                 exact (@Eq_helpHL _ _ gl1 gl2 gr1 gr2 ge1 ge2 sge1 sge2 se al ar).
           ++ simpl.
              exists sge2.
              refine (SRw _ sae).
              simpl.
              refine (Trans _ (@Eq_helpHAE _ _ _ _ _ _ _ _ sge1 sge2 se al al ar ar _ _)).
              exact (@Prd_eq_help _ _ _ _
                               (fst (typP A gl1 gr1 ge1 sge1))
                               (fst (typP A gl2 gr2 ge2 sge2))
                               (snd (typP A gl1 gr1 ge1 sge1))
                               (snd (typP A gl2 gr2 ge2 sge2))
                               _ _ _ al ar ae).
           ++ simpl.
              exists se. 
              refine (SRw _ sae).
              refine (Trans _ (@Eq_helpHAE _ _ _ _ _ _ _ _ sge1 sge2 se al al ar ar _ _)).
              exact (@Prd_eq_help _ _ _ _
                               (fst (typP A gl1 gr1 ge1 sge1))
                               (fst (typP A gl2 gr2 ge2 sge2))
                               (snd (typP A gl1 gr1 ge1 sge1))
                               (snd (typP A gl2 gr2 ge2 sge2))
                               _ _ _ al ar ae).
           ++ simpl.
              refine (@BiAp _ _ Type (fst (typP B _ _ _ _)) _ _ _ _ _ _).
                 ** exact (@Prd_eq_help2 _ _ _ _
                                           (fst (typP A gl1 gr1 ge1 sge1))
                                           (fst (typP A gl2 gr2 ge2 sge2))
                                           (snd (typP A gl1 gr1 ge1 sge1))
                                           (snd (typP A gl2 gr2 ge2 sge2))
                                           (fun al ar ae ase => Prd_helpL B ase)
                                           (fun al ar ae ase => Prd_helpL B ase)
                                           (Sone3 (eps A sge1 sge2 se))
                                           (Stwo3 (eps A sge1 sge2 se))
                                           (@Eq_helpHL _ _ _ _ _ _ _ _ _ _ _)
                                           _
                                           (Prd_eq_L _ _ _ se)
                                           (Prd_eq_L2 _ _ _) f al ar ae sae).
                 ** exact (@Prd_eq_help2 _ _ _ _
                                          (fst (typP A gl1 gr1 ge1 sge1))
                                          (fst (typP A gl2 gr2 ge2 sge2))
                                          (snd (typP A gl1 gr1 ge1 sge1))
                                          (snd (typP A gl2 gr2 ge2 sge2))
                                          (fun al ar ae ase => Prd_helpR B ase)
                                          (fun al ar ae ase => Prd_helpR B ase)
                                          (Sone3 (eps A sge1 sge2 se))
                                          (Stwo3 (eps A sge1 sge2 se))
                                          (@Eq_helpHL _ _ _ _ _ _ _ _ _ _ _)
                                          (fun al ar ae =>
                                             Trans (Prd_eq_help (Sthree3 (eps A sge1 sge2 se)) ae)
                                                   (Eq_helpHAE (Rw (Eq_helpHL _ _ _ _) ae)
                                                               (Rw (Eq_helpHL _ _ _ _) ae)))
                                          (Prd_eq_R _ _ _ se)
                                          (Prd_eq_R2 _ _ _)
                                          g al ar ae sae).
      * simpl.
        apply DFunext.
        intro fl.
        apply DFunext.
        intro gl.
        apply DFunext.
        intro fr.
        apply DFunext.
        intro gr.
        apply DFunext.
        intro fe.
        apply DFunext.
        intro ge.
        simpl.
        pose (t:= Ap (fun
             P0 : (forall (al : typL A gl1 gr1 ge1 sge1) (ar : typR A gl1 gr1 ge1 sge1)
                     (ae : fst (typP A gl1 gr1 ge1 sge1) al ar)
                     (ase : snd (typP A gl1 gr1 ge1 sge1) al al ar ar ae ae),
                   Prd_helpL B ase) ->
                  (forall (al : typL A gl1 gr1 ge1 sge1) (ar : typR A gl1 gr1 ge1 sge1)
                     (ae : fst (typP A gl1 gr1 ge1 sge1) al ar)
                     (ase : snd (typP A gl1 gr1 ge1 sge1) al al ar ar ae ae),
                   Prd_helpR B ase) -> Type =>
           forall
             (a1
              a2 : forall (al : typL A gl1 gr1 ge1 sge1) (ar : typR A gl1 gr1 ge1 sge1)
                     (ae : fst (typP A gl1 gr1 ge1 sge1) al ar)
                     (ase : snd (typP A gl1 gr1 ge1 sge1) al al ar ar ae ae),
                   Prd_helpL B ase)
             (c1
              c2 : forall (al : typL A gl1 gr1 ge1 sge1) (ar : typR A gl1 gr1 ge1 sge1)
                     (ae : fst (typP A gl1 gr1 ge1 sge1) al ar)
                     (ase : snd (typP A gl1 gr1 ge1 sge1) al al ar ar ae ae),
                 Prd_helpR B ase), P0 a1 c1 -> P0 a2 c2 -> SProp)).
        simpl in t.
        assert (H : forall (fl1 fl2 : forall (al : typL A gl1 gr1 ge1 sge1)
                                             (ar : typR A gl1 gr1 ge1 sge1)
                                             (ae : fst (typP A gl1 gr1 ge1 sge1) al ar)
                                             (ase : snd (typP A gl1 gr1 ge1 sge1) al al ar ar ae ae),
                               Prd_helpL B ase)
                           (fr1 fr2 : forall (al : typL A gl1 gr1 ge1 sge1) (ar : typR A gl1 gr1 ge1 sge1)
                                             (ae : fst (typP A gl1 gr1 ge1 sge1) al ar)
                                             (ase : snd (typP A gl1 gr1 ge1 sge1) al al ar ar ae ae),
                               Prd_helpR B ase),
                   forall (x y :
                             (forall (al : typL A gl1 gr1 ge1 sge1) (ar : typR A gl1 gr1 ge1 sge1)
                                     (ae : fst (typP A gl1 gr1 ge1 sge1) al ar)
                                     (ase : snd (typP A gl1 gr1 ge1 sge1) al al ar ar ae ae), 
                                 Prd_helpL B ase) ->
                             (forall (al : typL A gl1 gr1 ge1 sge1) (ar : typR A gl1 gr1 ge1 sge1)
                                     (ae : fst (typP A gl1 gr1 ge1 sge1) al ar)
                                     (ase : snd (typP A gl1 gr1 ge1 sge1) al al ar ar ae ae), 
                                 Prd_helpR B ase) -> Type),
                     eqn ((x fl1 fr1) -> (x fl2 fr2) -> SProp)
                         ((y fl1 fr1) -> (y fl2 fr2) -> SProp)).
        -- admit.
        -- unshelve refine (@Trans _ _ (forall (al1 al2 : typL A gl1 gr1 ge1 sge1) (ar1 ar2 : typR A gl1 gr1 ge1 sge1)
          (ae1 : fst (typP A gl1 gr1 ge1 sge1) al1 ar1)
          (ae2 : fst (typP A gl1 gr1 ge1 sge1) al2 ar2)
          (ase1 : snd (typP A gl1 gr1 ge1 sge1) al1 al1 ar1 ar1 ae1 ae1)
          (ase2 : snd (typP A gl1 gr1 ge1 sge1) al2 al2 ar2 ar2 ae2 ae2)
          (ase : snd (typP A gl1 gr1 ge1 sge1) al1 al2 ar1 ar2 ae1 ae2), _ : SProp) _ _ _).
           3:{ refine (PiSext (Sone3 (eps A sge1 sge2 se))
                             (DFunext
                                (fun al1 : typL A gl1 gr1 ge1 sge1 => _))).
               refine (PiSext (Sone3 (eps A sge1 sge2 se))
                             (DFunext
                                (fun al2 : typL A gl1 gr1 ge1 sge1 => _))).
               refine (PiSext (Stwo3 (eps A sge1 sge2 se))
                             (DFunext
                                (fun ar1 : typR A gl1 gr1 ge1 sge1 => _))).
               refine (PiSext (Stwo3 (eps A sge1 sge2 se))
                             (DFunext
                                (fun ar2 : typR A gl1 gr1 ge1 sge1 => _))).
               refine (PiSext (Eq_helpHL sge2 se al1 ar1)
                             (DFunext
                                (fun ae1 : fst (typP A gl1 gr1 ge1 sge1) al1 ar1 => _))).
               refine (PiSext (Eq_helpHL sge2 se al2 ar2)
                             (DFunext
                                (fun ae2 : fst (typP A gl1 gr1 ge1 sge1) al2 ar2 => _))).
               refine (SPiSext (Trans (Prd_eq_help (Sthree3 (eps A sge1 sge2 se)) ae1)
                                      (Eq_helpHAE (Rw (Eq_helpHL sge2 se al1 ar1) ae1) _))
                               (SDFunext
                                  (fun sae1 : snd (typP A gl1 gr1 ge1 sge1) al1 al1 ar1
                                                 ar1 ae1 ae1 => _))).
               refine (SPiSext (Trans (Prd_eq_help (Sthree3 (eps A sge1 sge2 se)) ae2)
                                      (Eq_helpHAE (Rw (Eq_helpHL sge2 se al2 ar2) ae2) _))
                               (SDFunext
                                  (fun sae2 : snd (typP A gl1 gr1 ge1 sge1) al2 al2 ar2
                                                  ar2 ae2 ae2 => _))).
               unshelve refine
                        (SPiSext (Trans (DPrd_eq_help (Sthree3 (eps A sge1 sge2 se)) ae1 ae2) _)
                                 (SDFunext
                                    (fun sae => _))).
               2:{ refine (Eq_helpHAE _ _). }
               - intro sae.
                 unshelve refine ((Rw (H fl gl fr gr (Prd_helpP sae1) (Prd_helpP sae2)) _) _ _).
                 ** refine ((B.(typP) _ _ _ _).(snd) _ _ _ _).
                 ** simpl.
                    unfold Prd_helpP.
                    unfold Prd_helpP in fe.
                    admit.
                 ** unfold Prd_helpP.
                    admit.
               - simpl.
                 unfold Prd_helpSP.
                 simpl.
                 simpl.
               admit.
               }
                   pose (q:= Eq_helpHAE (Gamma:= Gamma)).
               
               
                             Piext (Stwo3 (eps A sge1 sge2 se))
                        (DFunext
                           (fun ar : typR A gl1 gr1 ge1 sge1 =>
                            Piext (Eq_helpHL sge2 se al ar)
                              (DFunext
                                 (fun ae : fst (typP A gl1 gr1 ge1 sge1) al ar =>
                                  SPiext
                                    (Trans
                                       (Prd_eq_help (Sthree3 (eps A sge1 sge2 se)) ae)
                                       (Eq_helpHAE (Rw (Eq_helpHL sge2 se al ar) ae)))
                                    (SDFunext
                                       (fun
                                          sae : snd (typP A gl1 gr1 ge1 sge1) al al ar ar
                                                  ae ae => _ ))))))))). }.
           ++ unshelve refine ((Rw (H fl gl fr gr (Prd_helpP ase1) (Prd_helpP ase2)) _) _ _).
              ** refine ((B.(typP) _ _ _ _).(snd) _ _ _ _).
              ** simpl.
                 unfold Prd_helpP.
                 unfold Prd_helpP in fe.
                 refine (Rw (Sym (Df_Sequal (Df_Sequal (Ap (@fst _ _) ((B.(eps) _ _ _).(Sthree3))) (fl al2 ar2 ae2 ase2)) (fr al2 ar2 ae2 ase2))) _).
                 simpl.
                 unfold Prd_helpP in fe.
                 

                 (fe _ _ _ _).
                 pose (Prd_helpSP (B:= B) ase1 ase1 ase1).
                         *** refine (Trans _ (Eq_helpHAE _)).
                             exact (@Prd_eq_help _ _ _ _
                                                 (fst (typP A gl1 gr1 ge1 sge1))
                                                 (fst (typP A gl2 gr2 ge2 sge2))
                                                 (snd (typP A gl1 gr1 ge1 sge1))
                                                 (snd (typP A gl2 gr2 ge2 sge2))
                                                 _ _ _ al ar ae).
                         *** 
              ** exact (fun fe ge => Prd_helpP ase2 fe ge).

        (Rw
       (Ap
          ()
          (DFunext
             (DFunext
                (Piext
                   (DFunext
                      (Piext (Stwo3 (eps A sge1 sge2 se))
                         (DFunext
                            (Piext (Eq_helpHL sge2 se al ar)
                               (DFunext
                                  (SPiext
                                     (SDFunext
                                        (
                                         Trans
                                           ))))))))))))
       (fun
          (fl0
           hl : forall (al : typL A gl1 gr1 ge1 sge1) (ar : typR A gl1 gr1 ge1 sge1)
                  (ae : fst (typP A gl1 gr1 ge1 sge1) al ar)
                  (ase : snd (typP A gl1 gr1 ge1 sge1) al al ar ar ae ae),
                Prd_helpL B ase)
          (fr0
           hr : forall (al : typL A gl1 gr1 ge1 sge1) (ar : typR A gl1 gr1 ge1 sge1)
                  (ae : fst (typP A gl1 gr1 ge1 sge1) al ar)
                  (ase : snd (typP A gl1 gr1 ge1 sge1) al al ar ar ae ae),
                Prd_helpR B ase)
          (fe0 : forall (al : typL A gl1 gr1 ge1 sge1) (ar : typR A gl1 gr1 ge1 sge1)
                   (ae : fst (typP A gl1 gr1 ge1 sge1) al ar)
                   (ase : snd (typP A gl1 gr1 ge1 sge1) al al ar ar ae ae),
                 Prd_helpP ase fl0 fr0)
          (he : forall (al : typL A gl1 gr1 ge1 sge1) (ar : typR A gl1 gr1 ge1 sge1)
                  (ae : fst (typP A gl1 gr1 ge1 sge1) al ar)
                  (ase : snd (typP A gl1 gr1 ge1 sge1) al al ar ar ae ae),
                Prd_helpP ase hl hr) =>
        forall (al1 al2 : typL A gl1 gr1 ge1 sge1) (ar1 ar2 : typR A gl1 gr1 ge1 sge1)
          (ae1 : fst (typP A gl1 gr1 ge1 sge1) al1 ar1)
          (ae2 : fst (typP A gl1 gr1 ge1 sge1) al2 ar2)
          (ase1 : snd (typP A gl1 gr1 ge1 sge1) al1 al1 ar1 ar1 ae1 ae1)
          (ase2 : snd (typP A gl1 gr1 ge1 sge1) al2 al2 ar2 ar2 ae2 ae2)
          (ase : snd (typP A gl1 gr1 ge1 sge1) al1 al2 ar1 ar2 ae1 ae2),
        Prd_helpSP ase1 ase2 ase fe0 he)





        
        refine (Piext _ _).
        admit.
Defined.             

Arguments Prd [_] _.

Definition Lam {Gamma : BCtx} (A : BTy Gamma) {B : BTy (Gamma · A)}
  (f : Trm (Gamma · A) B) : Trm Gamma (Prd A B).
Proof.
  unshelve econstructor.
  - simpl.
    intros.
    refine (f.(eltL) _).
  - simpl.
    intros.
    refine (f.(eltR) _).
  - simpl.
    intros.
    refine (f.(eltP) _).
  - cbn beta iota.
    intros.
    generalize (eps (Prd A B) sge1 sge2 se).
    intro e.
    pose (q:= e.(Sthree3)).
    unshelve refine (SRw (Df_Sequal (Df_Sequal (Df_Sequal (Df_Sequal (Df_Sequal (Df_Sequal (ApD (@snd _ _) (B.(eps) _ _ _).(Sthree3)) _) _) _) _) _) _) _).
    simpl.
    intros.
    unfold Prd_helpSP.
    simpl.
    pose (t:= f.(eltSP)
                  (gl1:= {| one := gl2; two := gr2; three := ge2; four := sge2; five := al1 |} : L (Gamma · A))
                  (gl2 := {| one := gl2; two := gr2; three := ge2; four := sge2; five := al2 |} : L (Gamma · A))
                  (gr1:= {| one := gl2; two := gr2; three := ge2; four := sge2; five := ar1 |} : R (Gamma · A))
                  (gr2 := {| one := gl2; two := gr2; three := ge2; four := sge2; five := ar2 |} : R (Gamma · A))).
    cbn beta iota in t.





     snd
       (typP B {| one := gl2; two := gr2; three := ge2; four := sge2; five := al2 |}
          {| one := gl2; two := gr2; three := ge2; four := sge2; five := ar2 |} ge3 sge3)
       (Rw (Sone3 (eps B sge1 sge3 se)) (eltL f sge1)) (eltL f sge3)
       (Rw (Stwo3 (eps B sge1 sge3 se)) (eltR f sge1)) (eltR f sge3)
       (Rw
          (f_Sequal
             (f_Sequal
                (Ap
                   (@fst _ _ )
                   (Sthree3 (eps B sge1 sge3 se))) (eltL f sge1)) (eltR f sge1)) 
          (eltP f sge1)) (eltP f sge3)





    
    simpl in t.
    pose (q:= t ({| fst := {| SoneT := {| SoneT := ge2; StwoT := sge2 |};
                                                                                                                                     StwoT := {| spi1 := sge2; spi2 := sge2 |} |};
                                                                                                                           snd := ae1 |} : P (Gamma · A)
                              ({| one := gl2; two := gr2; three := ge2; four := sge2; five := al1 |}
                               : L (Gamma · A))
                              ({| one := gl2; two := gr2; three := ge2; four := sge2; five := ar1 |}
                               : R (Gamma · A)))). )).
    pose (a:= {| one := gl2; two := gr2; three := ge2; four := sge2; five := al2 |} : L (Gamma · A)).
    snd
    (typP B {| five := al2 |} {| five := ar2 |}
       {| snd := ae2 |} {|Stwo := ase2 |})
    (Rw
       (Sone3
          (eps B {| Sone := sge2; Stwo := ase1 |} {| Sone := sge2; Stwo := ase2 |}
             {| Sone := sge2; Stwo := ase |})) (eltL f {| Sone := sge2; Stwo := ase1 |}))
    (eltL f {| Sone := sge2; Stwo := ase2 |})
    (Rw
       (Stwo3
          (eps B {| Sone := sge2; Stwo := ase1 |} {| Sone := sge2; Stwo := ase2 |}
             {| Sone := sge2; Stwo := ase |})) (eltR f {| Sone := sge2; Stwo := ase1 |}))
    (eltR f {| Sone := sge2; Stwo := ase2 |})
    (Rw
       (f_Sequal
          (f_Sequal
             (Ap
                (fst
                   (B:=fun
                         P : typL B
                               {|
                                 one := gl2; two := gr2; three := ge2; four := sge2; five := al1
                               |}
                               {|
                                 one := gl2; two := gr2; three := ge2; four := sge2; five := ar1
                               |}
                               {|
                                 fst :=
                                   {|
                                     SoneT := {| SoneT := ge2; StwoT := sge2 |};
                                     StwoT := {| spi1 := sge2; spi2 := sge2 |}
                                   |};
                                 snd := ae1
                               |} {| Sone := sge2; Stwo := ase1 |} ->
                             typR B
                               {|
                                 one := gl2; two := gr2; three := ge2; four := sge2; five := al1
                               |}
                               {|
                                 one := gl2; two := gr2; three := ge2; four := sge2; five := ar1
                               |}
                               {|
                                 fst :=
                                   {|
                                     SoneT := {| SoneT := ge2; StwoT := sge2 |};
                                     StwoT := {| spi1 := sge2; spi2 := sge2 |}
                                   |};
                                 snd := ae1
                               |} {| Sone := sge2; Stwo := ase1 |} -> Type =>
                       forall
                         (a1
                          a2 : typL B
                                 {|
                                   one := gl2;
                                   two := gr2;
                                   three := ge2;
                                   four := sge2;
                                   five := al1
                                 |}
                                 {|
                                   one := gl2;
                                   two := gr2;
                                   three := ge2;
                                   four := sge2;
                                   five := ar1
                                 |}
                                 {|
                                   fst :=
                                     {|
                                       SoneT := {| SoneT := ge2; StwoT := sge2 |};
                                       StwoT := {| spi1 := sge2; spi2 := sge2 |}
                                     |};
                                   snd := ae1
                                 |} {| Sone := sge2; Stwo := ase1 |})
                         (c1
                          c2 : typR B
                                 {|
                                   one := gl2;
                                   two := gr2;
                                   three := ge2;
                                   four := sge2;
                                   five := al1
                                 |}
                                 {|
                                   one := gl2;
                                   two := gr2;
                                   three := ge2;
                                   four := sge2;
                                   five := ar1
                                 |}
                                 {|
                                   fst :=
                                     {|
                                       SoneT := {| SoneT := ge2; StwoT := sge2 |};
                                       StwoT := {| spi1 := sge2; spi2 := sge2 |}
                                     |};
                                   snd := ae1
                                 |} {| Sone := sge2; Stwo := ase1 |}),
                       P a1 c1 -> P a2 c2 -> SProp))
                (Sthree3
                   (eps B {| Sone := sge2; Stwo := ase1 |} {| Sone := sge2; Stwo := ase2 |}
                      {| Sone := sge2; Stwo := ase |})))
             (eltL f {| Sone := sge2; Stwo := ase1 |})) (eltR f {| Sone := sge2; Stwo := ase1 |}))
       (eltP f {| Sone := sge2; Stwo := ase1 |})) (eltP f {| Sone := sge2; Stwo := ase2 |})


    
    refine (f.(eltSP) _ _ _).

    

Definition App {Gamma : BCtx} {A : BTy Gamma} {B : BTy (@Ext Gamma A)}
  (f : Trm Gamma (Prd A B)) (a : Trm Gamma A) : Trm Gamma (Typ_subs (Cns a) B).
Proof.
  unshelve econstructor.
  - refine (fun gl gr ge sge => (f.(eltL) sge) (a.(eltL) sge) (a.(eltR) sge) (a.(eltP) sge) _).
  - refine (fun gl gr ge sge => (f.(eltR) sge) (a.(eltL) sge) (a.(eltR) sge) (a.(eltP) sge) _).
  - refine (fun gl gr ge sge => (f.(eltP) sge) (a.(eltL) sge) (a.(eltR) sge) (a.(eltP) sge) _).
  - intros.
    pose (t:= f.(eltSP) sge1 sge2 se (a.(eltL) sge2) (a.(eltR) sge2) (a.(eltP) sge2)
                        (a.(eltSP) sge2 sge2 sge2)).
    simpl.
    unfold Prd_helpSP in t ; unfold Prd_eq_L in t ; unfold Prd_eq_R in t ; simpl in t.

 snd
    (typP B {| one := gl2; two := gr2; three := ge2; four := sge2; five := eltL a sge2 |}
       {| one := gl2; two := gr2; three := ge2; four := sge2; five := eltR a sge2 |}
       {|
         fst :=
           {|
             SoneT := {| SoneT := ge2; StwoT := sge2 |};
             StwoT := {| spi1 := sge2; spi2 := sge2 |}
           |};
         snd := eltP a sge2
       |} {| Sone := sge2; Stwo := eltSP a sge2 sge2 sge2 |})
    (Rw
       (Sone3
          (eps B {| Sone := sge1; Stwo := eltSP a sge1 sge1 sge1 |}
             {| Sone := sge2; Stwo := eltSP a sge2 sge2 sge2 |}
             {| Sone := se; Stwo := eltSP a sge1 sge2 se |}))
       (eltL f sge1 (eltL a sge1) (eltR a sge1) (eltP a sge1) (eltSP a sge1 sge1 sge1)))
    (eltL f sge2 (eltL a sge2) (eltR a sge2) (eltP a sge2) (eltSP a sge2 sge2 sge2))
    (Rw
       (Stwo3
          (eps B {| Sone := sge1; Stwo := eltSP a sge1 sge1 sge1 |}
             {| Sone := sge2; Stwo := eltSP a sge2 sge2 sge2 |}
             {| Sone := se; Stwo := eltSP a sge1 sge2 se |}))
       (eltR f sge1 (eltL a sge1) (eltR a sge1) (eltP a sge1) (eltSP a sge1 sge1 sge1)))
    (eltR f sge2 (eltL a sge2) (eltR a sge2) (eltP a sge2) (eltSP a sge2 sge2 sge2))
    (Rw
       (f_Sequal
          (f_Sequal
             (Ap
                (fst
                   (B:=fun
                         P0 : typL B
                                {|
                                  one := gl1;
                                  two := gr1;
                                  three := ge1;
                                  four := sge1;
                                  five := eltL a sge1
                                |}
                                {|
                                  one := gl1;
                                  two := gr1;
                                  three := ge1;
                                  four := sge1;
                                  five := eltR a sge1
                                |}
                                {|
                                  fst :=
                                    {|
                                      SoneT := {| SoneT := ge1; StwoT := sge1 |};
                                      StwoT := {| spi1 := sge1; spi2 := sge1 |}
                                    |};
                                  snd := eltP a sge1
                                |} {| Sone := sge1; Stwo := eltSP a sge1 sge1 sge1 |} ->
                              typR B
                                {|
                                  one := gl1;
                                  two := gr1;
                                  three := ge1;
                                  four := sge1;
                                  five := eltL a sge1
                                |}
                                {|
                                  one := gl1;
                                  two := gr1;
                                  three := ge1;
                                  four := sge1;
                                  five := eltR a sge1
                                |}
                                {|
                                  fst :=
                                    {|
                                      SoneT := {| SoneT := ge1; StwoT := sge1 |};
                                      StwoT := {| spi1 := sge1; spi2 := sge1 |}
                                    |};
                                  snd := eltP a sge1
                                |} {| Sone := sge1; Stwo := eltSP a sge1 sge1 sge1 |} ->
                              Type =>
                       forall
                         (a1
                          a2 : typL B
                                 {|
                                   one := gl1;
                                   two := gr1;
                                   three := ge1;
                                   four := sge1;
                                   five := eltL a sge1
                                 |}
                                 {|
                                   one := gl1;
                                   two := gr1;
                                   three := ge1;
                                   four := sge1;
                                   five := eltR a sge1
                                 |}
                                 {|
                                   fst :=
                                     {|
                                       SoneT := {| SoneT := ge1; StwoT := sge1 |};
                                       StwoT := {| spi1 := sge1; spi2 := sge1 |}
                                     |};
                                   snd := eltP a sge1
                                 |} {| Sone := sge1; Stwo := eltSP a sge1 sge1 sge1 |})
                         (c1
                          c2 : typR B
                                 {|
                                   one := gl1;
                                   two := gr1;
                                   three := ge1;
                                   four := sge1;
                                   five := eltL a sge1
                                 |}
                                 {|
                                   one := gl1;
                                   two := gr1;
                                   three := ge1;
                                   four := sge1;
                                   five := eltR a sge1
                                 |}
                                 {|
                                   fst :=
                                     {|
                                       SoneT := {| SoneT := ge1; StwoT := sge1 |};
                                       StwoT := {| spi1 := sge1; spi2 := sge1 |}
                                     |};
                                   snd := eltP a sge1
                                 |} {| Sone := sge1; Stwo := eltSP a sge1 sge1 sge1 |}),
                       P0 a1 c1 -> P0 a2 c2 -> SProp))
                (Sthree3
                   (eps B {| Sone := sge1; Stwo := eltSP a sge1 sge1 sge1 |}
                      {| Sone := sge2; Stwo := eltSP a sge2 sge2 sge2 |}
                      {| Sone := se; Stwo := eltSP a sge1 sge2 se |})))
             (eltL f sge1 (eltL a sge1) (eltR a sge1) (eltP a sge1)
                (eltSP a sge1 sge1 sge1)))
          (eltR f sge1 (eltL a sge1) (eltR a sge1) (eltP a sge1) (eltSP a sge1 sge1 sge1)))
       (eltP f sge1 (eltL a sge1) (eltR a sge1) (eltP a sge1) (eltSP a sge1 sge1 sge1)))
    (eltP f sge2 (eltL a sge2) (eltR a sge2) (eltP a sge2) (eltSP a sge2 sge2 sge2))
    

    
  - simpl.
    intros.
    unshelve refine (SRw (@Ap _ SProp (fun x => rel (typ B _) x
                                           (elt f g2 g2e (elt a g2 g2e) (par a g2 g2 g2e g2e g2e)))
                              _ _ _) _).
    + unshelve refine (Rw (Eq_car (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e
                                 (Rw (Eq_car (eps A g1 g2 g1e g2e e)) (elt a g1 g1e)))
                                  (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e (elt a g2 g2e))
                                  _
                                  _
                                  _)) _).
      * exists g2e.
        refine (SRw (Eq_rel2 (A.(eps) g1 g2 g1e g2e e) (elt a g1 g1e) (elt a g1 g1e)) _).
        exact (a.(par) g1 g1 g1e g1e g1e).
      * exists g2e.
        exact (a.(par) g1 g2 g1e g2e e).
      * unshelve refine (Rw (Eq_car (B.(eps)
                                         (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e (elt a g1 g1e))
                                         (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e
                                                  (Rw (Eq_car (eps A g1 g2 g1e g2e e)) (elt a g1 g1e)))
                                         {| Sone := g1e; Stwo := par a g1 g1 g1e g1e g1e |}
                                         {| Sone := g2e; Stwo := _ |}
                                         {| Sone := e; Stwo := _ |})) _).
        -- refine (SRw (Eq_rel2 (A.(eps) g1 g2 g1e g2e e) (elt a g1 g1e) (elt a g1 g1e)) _).
           exact (a.(par) g1 g1 g1e g1e g1e).
        -- exact (elt f g1 g1e (elt a g1 g1e) (par a g1 g1 g1e g1e g1e)).
    + exact (Rw_trans _ _ _).
    + generalize ((Eq_car (eps (Prd A B) g1 g2 g1e g2e e))) ;intro eq.
      unshelve refine (SRw (@Eq_rel3 _ _ (refl _) _ _ _ _ _ _) _).
      * unshelve refine (Rw (Eq_car (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e
                                         (Rw (Eq_car (eps A g1 g2 g1e g2e e)) (elt a g1 g1e)))
                                (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e (elt a g2 g2e))
                                {| Sone := g2e; Stwo := _ |}
                                {| Sone := g2e; Stwo := par a g2 g2 g2e g2e g2e |}
                                {| Sone := g2e; Stwo := par a g1 g2 g1e g2e e |})) _).
        -- refine (SRw (Eq_rel2 (A.(eps) g1 g2 g1e g2e e) (elt a g1 g1e) (elt a g1 g1e)) _).
           exact (a.(par) g1 g1 g1e g1e g1e).
        -- exact (Rw eq (elt f g1 g1e)
                     (Rw (Eq_car (eps A g1 g2 g1e g2e e)) (elt a g1 g1e))
                     (SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) (elt a g1 g1e) (elt a g1 g1e))
                          (a.(par) g1 g1 g1e g1e g1e))).
      * refine (elt f g2 g2e (elt a g2 g2e) (par a g2 g2 g2e g2e g2e)).
      * simpl.
        refine (Ap _ _).
        exact (Rw_Bifun2 (elt f g1 g1e) (C:= (A.(typ) g2e).(car))
                            (D:= fun a (ae : (A.(typ) g2e).(rel) a a) =>
                                   car (B.(typ) (gamma := @exist3 _ (fun x => Gamma.(R) x x)
                                                                  _ g2 g2e a)
                                                {| Sone := g2e; Stwo := ae |}))
                            (eq1:= Eq_car (eps A g1 g2 g1e g2e e))
                            (eq2:= fun x : car (typ A g1e) => Eq_rel2 (A.(eps) g1 g2 g1e g2e e) x x)
                            (fun (x : car (typ A g1e)) (xe : rel (typ A g1e) x x) =>
                              Eq_car (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e x)
                                       (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e
                                                (Rw (Eq_car (eps A g1 g2 g1e g2e e)) x))
                                       {| Sone := g1e; Stwo := xe |}
                                       {| Sone := g2e;
                                          Stwo := SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) x x) xe |}
                                       {| Sone := e;
                                          Stwo := SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) x x) xe |}))
                            (Eq_car (eps (Prd A B) g1 g2 g1e g2e e))
                            ((par a g1 g1 g1e g1e g1e))).
      * reflexivity.
      * simpl.
        exact (f.(par) g1 g2 g1e g2e e (Rw (Eq_car (eps A g1 g2 g1e g2e e)) (elt a g1 g1e))
                          (elt a g2 g2e)
                          (SRw (Eq_rel2 (eps A g1 g2 g1e g2e e) (elt a g1 g1e) (elt a g1 g1e))
                               (par a g1 g1 g1e g1e g1e))
                          (par a g2 g2 g2e g2e g2e) _).
Defined.

Print Prd.


Definition Lam {Gamma : BCtx} (A : BTy Gamma) {B : BTy (Gamma · A)}
  (f : Trm (Gamma · A) B) : Trm Gamma (Prd A B).
Proof.
  unshelve econstructor.
  - simpl ; intros g ge a ae.
    refine (f.(elt) _ _).
  - cbv beta iota.
    intros.
    intros a1 a2 a1e a2e ae.
    generalize (Eq_car (eps (Prd A B) g1 g2 g1e g2e e)) ; intro eq ; simpl.
    unshelve refine (SRw (@Eq_rel3 _ _ (refl _) _ _ _ _ _ _) _).
    + unshelve refine (Rw (Eq_car (B.(eps) _ _ _ _ _)) _).
      * exists g2 g2e.
        exact a1.
      * exists g2e.
        exact a1e.
      * exists g2e.
        exact ae.
      * unshelve refine (Rw (Eq_car (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e
                                  (Rw (Sym (Eq_car (A.(eps) g1 g2 g1e g2e e))) a1))
                         (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e a1)
                         {| Sone := g1e; Stwo := SRw (Eq_rel2 (Sym (A.(eps) g1 g2 g1e g2e e))
                                                              a1 a1) a1e |}
                         {| Sone := g2e; Stwo := a1e |}
                         {| Sone := e;
                            Stwo := SRw (Ap (fun z => rel (typ A g2e) z a1)
                                            (Sym (Rw_sym2 (Eq_car (eps A g1 g2 g1e g2e e)) a1)))
                                        a1e |})) _).
        refine (f.(elt) _ _).
    + exact (elt f (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e a2)
                 {| Sone := g2e; Stwo := a2e |}).
    + simpl.
      refine (Ap _ _).
      refine (Sym _).
      exact (Rw_Bifun _ (fun (x : (A.(typ) g2e).(car)) (xe : (A.(typ) g2e).(rel) x x) =>
                           Eq_car (B.(eps) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e
                                                    (Rw (Sym (Eq_car (A.(eps) g1 g2 g1e g2e e))) x))
                                           (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e x)
                                           {| Sone := g1e;
                                              Stwo := SRw (Eq_rel2 (Sym (A.(eps) g1 g2 g1e g2e e))
                                                                   x x) xe |}
                                           {| Sone := g2e; Stwo := xe |}
                                           {| Sone := e;
                                              Stwo := SRw (Ap (fun z => rel (typ A g2e) z x)
                                                              (Sym (Rw_sym2 (Eq_car (eps A g1 g2 g1e g2e e)) x))) xe |}))
                      eq a1e).
    + reflexivity.
    + unshelve refine (SRw (@Eq_rel3 _ _ (refl _) _ _ _ _ _ _)
                           (f.(par) (@exist3 _ (fun x => Gamma.(R) x x) _ g1 g1e
                                             (Rw (Sym (Eq_car (eps A g1 g2 g1e g2e e))) a1))
                                    (@exist3 _ (fun x => Gamma.(R) x x) _ g2 g2e a2)
                                    {| Sone := g1e; Stwo := SRw (Eq_rel2 (Sym (A.(eps) g1 g2 g1e g2e e)) a1 a1) a1e |}
                                    {| Sone := g2e; Stwo := a2e |}
                                    {| Sone := e;
                                       Stwo := SRw (Ap (fun z => rel (typ A g2e) z a2)
                                                       (Sym (Rw_sym2 (Eq_car (eps A g1 g2 g1e g2e e))
                                                                     a1))) ae |})).
      * simpl.
        exact (Sym (Rw_trans _ _ _)).
      * reflexivity.
Defined.

Lemma Lam_App_eqn :
  forall (Γ : BCtx) (A : BTy Γ) (B : BTy (@Ext Γ A)) (t : Trm (@Ext Γ A) B) (u : Trm Γ A),
  App (Lam t) u = Trm_subs (Cns u) t.
Proof.
  reflexivity.
Qed.


Lemma App_Lam_eqn :
  forall (Γ : BCtx) (A : BTy Γ) (B : BTy (@Ext Γ A))
  (t : Trm Γ (Prd A B)),
    Lam (@App (@Ext Γ A) (Typ_subs (@Wkn Γ A) A) (Typ_subs (Lft (@Wkn Γ A) A) B)
              (Trm_subs (@Wkn Γ A) t) Var) = t.
Proof.
reflexivity.
Qed.

Inductive BranchingBool :=
| Btrue : BranchingBool
| Bfalse : BranchingBool
| BêtaBool : I -> (O -> BranchingBool) -> BranchingBool.

Inductive EBool : BranchingBool -> BranchingBool -> SProp :=
| EBtrue : EBool Btrue Btrue
| EBfalse : EBool Bfalse Bfalse
| EBêtaBool1 : forall (b : BranchingBool) (i : I) (f : O -> BranchingBool)
                      (H : EBool b (f (alpha i))),
    EBool b (BêtaBool i f)
| EBêtaBool2 : forall (b : BranchingBool) (i : I) (f : O -> BranchingBool)
                  (H : EBool (f (alpha i)) b),
    EBool (BêtaBool i f) b.

Lemma BoolRefl: forall (b : BranchingBool),
    EBool b b.
Proof.
  refine (fix f b := match b with
                     | Btrue => EBtrue
                     | Bfalse => EBfalse
                     | BêtaBool i k => EBêtaBool1 (EBêtaBool2 (f (k (alpha i))))
                     end).
Defined.  

Definition EBool_Branch (i : I) (k : O -> BranchingBool) :
    EBool (k (alpha i)) (BêtaBool i k) :=
  EBêtaBool1 (BoolRefl (k (alpha i))).


Definition BBool : BranchingTYPE.
Proof.
  unshelve econstructor.
  - exact BranchingBool.
  - exact BêtaBool.
  - exact EBool.
  - exact EBêtaBool1.
Defined.

Definition Bool {Gamma : BCtx} : BTy Gamma.
Proof.
  unshelve econstructor.
  - exact (fun _ _ => BBool).
  - exact (fun _ _ _ _ _ => refl _).
Defined.

Definition BTrue {Gamma : BCtx} : Trm Gamma Bool.
Proof.
  unshelve econstructor.
  - exact (fun _ _ => Btrue).
  - exact (fun _ _ _ _ _ => EBtrue).
Defined.

Definition BFalse {Gamma : BCtx} : Trm Gamma Bool.
Proof.
  unshelve econstructor.
  - exact (fun _ _ => Bfalse).
  - exact (fun _ _ _ _ _ => EBfalse).
Defined.

Print Typ_subs.

Notation "⇑ B" := (@Typ_subs _ _ (Wkn _) B) (at level 10).

Notation "A → B" := (Prd A (⇑ B))
                        (at level 99, right associativity, B at level 200).
Print Rw.

Definition Prd_subst : forall Γ Δ A B σ,
  @Typ_subs Γ Δ σ (@Prd Γ A B) =
  @Prd Δ (@Typ_subs Γ Δ σ A) (@Typ_subs (@Ext Γ A) (@Ext Δ (@Typ_subs Γ Δ σ A)) (@Lft Δ Γ σ A) B).
Proof.
reflexivity.
Defined.

(*
>>>>>>> 557e6a7f69285419fd60762c65e0daa4caa22d07
Definition Bool_rect_aux {Gamma : BCtx} :
  forall (gamma : Gamma) (gammae : Gamma.(R) gamma gamma),
    ((Prd (Bool → (BU _))
         ([@App _ Bool (BU _) (@Var Gamma (Prd Bool (BU _))) BTrue]
          → [@App _ Bool (BU _) (@Var Gamma (Prd Bool (BU _))) BFalse]
          → Prd Bool [@App _ Bool (BU _)
                           (Trm_subs (Wkn Bool) (@Var Gamma (Bool → (BU _))))
                           (@Var _ Bool)])).(typ) gammae).(car) :=
  fun (g : Gamma) (ge : Gamma.(R) g g)
      (P :  forall a : BranchingBool, EBool a a -> BranchingTYPE)
      (Pe : forall (a1 a2 : BranchingBool)
                   (a1e : EBool a1 a1) (a2e : EBool a2 a2),
          EBool a1 a2 -> eqn (P a1 a1e) (P a2 a2e))
      (ptrue : car (P Btrue EBtrue))
      (ptruee : rel (P Btrue EBtrue) ptrue ptrue)
      (pfalse : car (P Bfalse EBfalse))
      (pfalsee : rel (P Bfalse EBfalse) pfalse pfalse) =>
    fix f (b : BranchingBool) :=
    match b return (forall (be : EBool b b), car (P b be))
    with
    | Btrue => fun _ => ptrue
    | Bfalse => fun _ => pfalse
    | BêtaBool i h =>
      fun be =>
        @Rw (P (h (alpha i)) (BoolRefl (h (alpha i)))).(car) _
                                                             (Eq_car (Pe _ _ _ _ (EBool_Branch i h)))
                                                             (f (h (alpha i))
                                                                (BoolRefl (h (alpha i))))
    end.
  change (forall (a : BranchingBool) (ae : EBool a a), car (P a ae)).
  unshelve refine (fix f b := match b with
                              | Btrue => fun _ => ptrue
                              | Bfalse => fun _ => pfalse
                              | BêtaBool i h => fun be => _
                              end).
  unshelve refine (@Rw (P (h (alpha i)) (BoolRefl (h (alpha i)))).(car) _ _ _).
  + refine (Eq_car (Pe _ _ _ _ _)).
    exact (EBool_Branch i h).
  + exact (f (h (alpha i)) (BoolRefl (h (alpha i)))).
Defined.
*)

Ltac prd_subst :=
match goal with [ |- context [@Typ_subs _ ?Δ ?σ (@Prd ?Γ ?A ?B)] ] =>
  change (@Typ_subs Γ Δ σ (@Prd Γ A B)) with
  (@Prd Δ (@Typ_subs Γ Δ σ A) (@Typ_subs (@Ext Γ A) (@Ext Δ (@Typ_subs Γ Δ σ A)) (@Lft Δ Γ σ A) B))
end.

Definition Bool_rect {Gamma : BCtx} :
  Trm Gamma (Prd (Bool → (BU _))
                  ([@App _ Bool (BU _) (@Var Gamma (Prd Bool (BU _))) BTrue]
                   → [@App _ Bool (BU _) (@Var Gamma (Prd Bool (BU _))) BFalse]
                   → Prd Bool [@App _ Bool (BU _)
                                    (Trm_subs (Wkn Bool) (@Var Gamma (Bool → (BU _))))
                                    (@Var _ Bool)])).
Proof.
refine (Lam _).
refine (Lam _).
match goal with [ |- context [@Typ_subs _ ?Δ ?σ (@Prd ?Γ ?A ?B)] ] =>
  rewrite (@Prd_subst Γ Δ A B σ)
end.
refine (Lam _).
match goal with [ |- context [@Typ_subs _ ?Δ ?σ (@Prd ?Γ ?A ?B)] ] =>
  rewrite (@Prd_subst Γ Δ A B σ)
end.
match goal with [ |- context [@Typ_subs _ ?Δ ?σ (@Prd ?Γ ?A ?B)] ] =>
  rewrite (@Prd_subst Γ Δ A B σ)
end.

rewrite (Prd_subst _ _).

  unshelve econstructor.
  - cbv beta iota.
    intros g ge P Pe ptrue ptruee pfalse pfalsee.
    cbn in *.
    unshelve refine (fix f b := match b with
                                   | Btrue => fun _ => ptrue
                                   | Bfalse => fun _ => pfalse
                                   | BêtaBool i h => fun be => _
                                   end).
    unshelve refine (@Rw (P (h (alpha i)) (BoolRefl (h (alpha i)))).(car) _ _ _).
    + refine (Eq_car (Pe _ _ _ _ _)).
      exact (EBool_Branch i h).
    + exact (f (h (alpha i)) (BoolRefl (h (alpha i)))).
  - unshelve refine (fun (g1 : Gamma) (g2 : Gamma)
                         (g1e : Gamma.(R) g1 g1)
                         (g2e : Gamma.(R) g2 g2)
                         (e : Gamma.(R) g1 g2)
                         (P1 : forall a : BranchingBool, EBool a a -> BranchingTYPE)
                         (P2 : forall a : BranchingBool, EBool a a -> BranchingTYPE)
                         (P1e : forall (a1 a2 : BranchingBool)
                                       (a1e : EBool a1 a1) (a2e : EBool a2 a2),
                             EBool a1 a2 -> eqn (P1 a1 a1e) (P1 a2 a2e))
                         (P2e : forall (a1 a2 : BranchingBool)
                                       (a1e : EBool a1 a1) (a2e : EBool a2 a2),
                             EBool a1 a2 -> eqn (P2 a1 a1e) (P2 a2 a2e))
                         (Pe : forall (a1 a2 : BranchingBool)
                                      (a1e : EBool a1 a1) (a2e : EBool a2 a2),
                             EBool a1 a2 -> eqn (P1 a1 a1e) (P2 a2 a2e))
                         (ptt1 : car (P2 Btrue EBtrue))
                         (ptt2 : car (P2 Btrue EBtrue))
                         (ptt1e : rel (P2 Btrue EBtrue) ptt1 ptt1)
                         (ptt2e : rel (P2 Btrue EBtrue) ptt2 ptt2)
                         (ptte : rel (P2 Btrue EBtrue) ptt1 ptt2)
                         (pff1 : car (P2 Bfalse EBfalse))
                         (pff2 : car (P2 Bfalse EBfalse))
                         (pff1e : rel (P2 Bfalse EBfalse) pff1 pff1)
                         (pff2e : rel (P2 Bfalse EBfalse) pff2 pff2)
                         (pffe : rel (P2 Bfalse EBfalse) pff1 pff2)
                         (b1 : BranchingBool)
                         (b2 : BranchingBool)
                         (b1e : EBool b1 b1) 
                         (b2e : EBool b2 b2) 
                         (be : EBool b1 b2) => _).
   intros g1 g2 g1e g2e e P1 P2 P1e P2e Pe ptt1 ptt2 ptt1e ptt2e ptte
           pff1 pff2 pff1e pff2e pffe.
    simpl in pff1 ; simpl in pff2 ; simpl in pff1e ; simpl in pff2e ; simpl in pffe.
    simpl in ptt1 ; simpl in ptt2 ; simpl in ptt1e ; simpl in ptt2e ; simpl in ptte.
    simpl in P1 ; simpl in P2 ; simpl in P1e ; simpl in P2e ; simpl in Pe.
    simpl in Pe.
    unshelve refine (fun (g1 : Gamma) (g2 : Gamma)
                         (g1e : Gamma.(R) g1 g1)
                         (g2e : Gamma.(R) g2 g2)
                         (e : Gamma.(R) g1 g2)
                         (P1 : forall a : BranchingBool, EBool a a -> BranchingTYPE)
                         (P2 : forall a : BranchingBool, EBool a a -> BranchingTYPE)
                         (P1e : forall (a1 a2 : BranchingBool)
                                       (a1e : EBool a1 a1) (a2e : EBool a2 a2),
                             EBool a1 a2 -> eqn (P1 a1 a1e) (P1 a2 a2e))
                         (P2e : forall (a1 a2 : BranchingBool)
                                       (a1e : EBool a1 a1) (a2e : EBool a2 a2),
                             EBool a1 a2 -> eqn (P2 a1 a1e) (P2 a2 a2e))
                         (Pe : forall (a1 a2 : BranchingBool)
                                      (a1e : EBool a1 a1) (a2e : EBool a2 a2),
                             EBool a1 a2 -> eqn (P1 a1 a1e) (P2 a2 a2e))
                         (ptt1 : car (P2 Btrue EBtrue))
                         (ptt2 : car (P2 Btrue EBtrue))
                         (ptt1e : rel (P2 Btrue EBtrue) ptt1 ptt1)
                         (ptt2e : rel (P2 Btrue EBtrue) ptt2 ptt2)
                         (ptte : rel (P2 Btrue EBtrue) ptt1 ptt2)
                         (pff1 : car (P2 Bfalse EBfalse))
                         (pff2 : car (P2 Bfalse EBfalse))
                         (pff1e : rel (P2 Bfalse EBfalse) pff1 pff1)
                         (pff2e : rel (P2 Bfalse EBfalse) pff2 pff2)
                         (pffe : rel (P2 Bfalse EBfalse) pff1 pff2) => _).
                         
    intros b1 b2 b1e b2e be.
    simpl in b1 ; simpl in b2 ; simpl in b1e ; simpl in b2e ; simpl in be.





    simpl.
    cbv beta iota.
    simpl.
    simpl in pff1 ; simpl in pff2.
      exact (BoolRefl _).
    +


      induction (h (alpha i)).
      * exact EBtrue.
      * exact EBfalse.
      * refine (EBêtaBool _).
        exact (H (alpha i)).
    
    cbn beta iota in *.
    simpl in *.

    simpl.
  refine (Lam _).
  refine (Lam _).
  refine (Trm_subs (Wkn _) (Lam _)).
  unshelve econstructor.
  cbv beta iota.
  refine (fun P => EL (App P _)).
