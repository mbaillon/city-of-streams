Require Import ssreflect.
Set Universe Polymorphism.
Set Primitive Projections.


Axiom pi : forall (P : Prop) (p q : P), p = q.

Axiom funext : forall (A : Type) (B : A -> Type)
                      (f g : forall x : A, B x), (forall x : A, f x = g x) -> f = g.

Axiom Todo : forall (P : Type), P.

CoInductive ℙ := mkℙ { hd : bool; tl : ℙ}.

Axiom ℙ_eta : forall s, s = mkℙ s.(hd) s.(tl).


Record ssig (A:Type) (P: A -> SProp) : Type := Sexist { spr1 : A; spr2 : P spr1 }.


Arguments spr1 {_ _} _.
Arguments spr2 {_ _} _.
Set Definitional UIP.
Inductive Seq  {A : Type} (x : A) : A -> SProp :=  Seq_refl : Seq x x.

Definition Rw (A B : Type) : Seq A B -> A -> B.
Proof.
  intros eq a.
  refine (match eq as eq0 in Seq _ B
          with | Seq_refl _ => a end).
Defined.

Definition Sym (A B : Type) : Seq A B -> Seq B A :=
  fun x => match x as eq in Seq _ K with
           | Seq_refl _ => Seq_refl _
           end.

Definition SRw (A B : SProp) : Seq A B -> A -> B.
Proof.
  intros eq a.
  refine (match eq as eq0 in Seq _ B
          with | Seq_refl _ => a end).
Defined.


Definition Sf_equal := 
  fun (A B : Type) (f : A -> B) (x y : A) (H : Seq x y) =>
    match H in (Seq _ y0) return (Seq (f x) (f y0)) with
    | Seq_refl _ => Seq_refl _
    end.

Fixpoint take (n : nat) (α : ℙ) : list bool :=
match n with
| 0 => nil
| S n => cons α.(hd) (take n α.(tl))
end.

Fixpoint skip (n : nat) (α : ℙ) : ℙ :=
match n with
| 0 => α
| S n => skip n α.(tl)
end.

Fixpoint nth (α : ℙ) (n : nat) : bool :=
match n with
| 0 => α.(hd)
| S n => nth α.(tl) n
end.

Fixpoint app (s : list bool) (α : ℙ) : ℙ :=
match s with
| nil => α
| cons b s => mkℙ b (app s α)
end.

Inductive eqn (α β : ℙ) : nat -> Prop :=
| eqn_0 : eqn α β 0
| eqn_S : forall n, α.(hd) = β.(hd) -> eqn α.(tl) β.(tl) n -> eqn α β (S n).

Lemma eqn_le : forall α β n m, m <= n -> eqn α β n -> eqn α β m.
Proof.
intros α β n m H He; revert m H.
induction He; intros m Hle.
+ inversion Hle; constructor.
+ destruct m as [|m].
  - constructor.
  - constructor; [assumption|apply IHHe, le_S_n; assumption].
Qed.

Lemma length_take : forall n s, length (take n s) = n.
Proof.
induction n; intros s; simpl; intuition eauto.
Qed.

Lemma eqn_refl : forall n p, eqn p p n.
Proof.
  induction n.
  - now constructor.
  - intro p ; simpl.
    now constructor.
Defined.


Lemma eqn_nth : forall α β n, eqn α β (S n) -> nth α n = nth β n.
Proof.
fix F 4; intros α β n H.
refine (
match H in eqn _ _ n return
  match n with O => True | S n => nth α n = nth β n end
with
| eqn_0 _ _ => I
| eqn_S _ _ n h t => _
end).
destruct n as [|n].
+ assumption.
+ simpl; apply F; assumption.
Qed.

Lemma eqn_take : forall α β n, eqn α β n -> take n α = take n β.
Proof.
induction 1; simpl.
+ constructor.
+ f_equal; assumption.
Qed.

Lemma take_eqn : forall α β n, take n α = take n β -> eqn α β n.
Proof.
intros α β n; revert α β; induction n as [|n IHn]; intros α β H; simpl in *.
+ constructor.
+ constructor.
  - congruence.
  - apply IHn; congruence.
Qed.

Lemma take_skip : forall n s, app (take n s) (skip n s) = s.
Proof.
induction n; intros s; simpl.
+ reflexivity.
+ rewrite -> (IHn (tl s)), ℙ_eta; reflexivity.
Qed.

Lemma take_app : forall s α, take (length s) (app s α) = s.
Proof.
induction s as [|b s IHs]; intros α; simpl.
+ reflexivity.
+ f_equal; apply IHs.
Qed.


Definition act {X : Type} (p : ℙ -> ℙ) (x : ℙ -> X) : ℙ -> X := fun α => x (p α).

Notation "p · x" := (act p x) (at level 30, right associativity).

Definition merge {X : Type} (x y : ℙ -> X) : ℙ -> X :=
  fun α => if α.(hd) then x α.(tl) else y α.(tl).

Definition lam₀ : ℙ -> ℙ := fun α => mkℙ true α.
Definition ρ₀ : ℙ -> ℙ := fun α => mkℙ false α.

Inductive cover {X} (P : (ℙ -> X) -> Prop) (x : ℙ -> X) : Prop :=
| cov_ret : P x -> cover P x
| cov_bnd : cover P (act lam₀ x) -> cover P (act ρ₀ x) -> cover P x.

Definition isContinuous (p : ℙ -> ℙ) :=
  forall m : nat, exists n : nat, forall α β : ℙ, eqn α β n -> eqn (p α) (p β) m.

Inductive extends : nat -> (ℙ -> ℙ) -> Prop :=
| extends_refl : forall p, isContinuous p -> extends 0 p
| extends_lam : forall n p, extends n p -> extends (S n) (act lam₀ p)
| extends_ρ : forall n p, extends n p -> extends (S n) (act ρ₀ p).

Definition isContinuous' (p : ℙ -> ℙ) :=
  forall m : nat, exists n : nat,
    forall s : list bool, length s = n ->
      exists p₀ : ℙ -> ℙ, exists s₀ : list bool,
        isContinuous p₀ /\ length s₀ = m /\ act (app s) p = act p₀ (app s₀).

Lemma merge_LR : forall {X : Type} (x : ℙ -> X), merge (act lam₀ x) (act ρ₀ x) = x.
Proof.
intros X x.
apply funext; intros α.
unfold merge, lam₀, ρ₀; simpl.
rewrite (ℙ_eta α); simpl.
destruct (hd α); reflexivity.
Qed.

Lemma L_merge : forall {X : Type} (x y : ℙ -> X), act lam₀ (merge x y) = x.
Proof.
reflexivity.
Qed.

Lemma R_merge : forall {X : Type} (x y : ℙ -> X), act ρ₀ (merge x y) = y.
Proof.
reflexivity.
Qed.


Lemma isContinuous_act : forall p q,
  isContinuous p -> isContinuous q -> isContinuous (act p q).
Proof.
intros p q Hp Hq m; unfold act.
destruct (Hq m) as [n Hn].
destruct (Hp n) as [l Hl].
exists l; intros α β H.
apply Hn, Hl, H.
Qed.

Lemma isContinuous_app : forall s, isContinuous (app s).
Proof.
intros s m; exists m.
revert m; induction s as [|b s]; intros m α β H; simpl.
+ assumption.
+ destruct m as [|m]; constructor; simpl; try reflexivity.
  apply IHs; eapply eqn_le; [|eassumption].
  repeat constructor.
Qed.

Lemma isContinuous_skip : forall n, isContinuous (skip n).
Proof.
intros s m.
exists (s + m); intros α β H.
revert m α β H; induction s as [|s IHs]; intros m α β H; simpl in *.
+ assumption.
+ inversion H; subst.
  apply IHs; assumption.
Qed.

Lemma isContinuous_1 : forall p, isContinuous p -> isContinuous' p.
Proof.
intros p Hp m.
destruct (Hp m) as [n Hn].
exists n; intros s Hs.
exists (fun α => skip m (p (app s α))).
exists (take m (p (app s (cofix ω := mkℙ true ω)))).
split; [|split].
+ change (isContinuous (act (act (app s) p) (skip m))).
  apply isContinuous_act; [apply isContinuous_act|].
  - apply isContinuous_app.
  - assumption.
  - apply isContinuous_skip.
+ apply length_take.
+ apply funext; intros α.
  unfold act.
  replace (take m (p (app s (cofix ω := mkℙ true ω)))) with (take m (p (app s α))).
  - rewrite take_skip; reflexivity.
  - apply eqn_take, Hn.
    rewrite <- Hs.
    apply take_eqn; rewrite !take_app; reflexivity.
Qed.

Lemma isContinuous_2 : forall p, isContinuous' p -> isContinuous p.
Proof.
intros p Hp m.
destruct (Hp m) as [n Hn].
exists n; intros α β H.
specialize (Hn (take n α) (length_take _ _)).
destruct Hn as [p₀ [s₀ [Hp₀ [Hs₀ He]]]].
apply take_eqn.
unfold act in He.
rewrite <- (take_skip n α).
rewrite <- (take_skip n β).
replace (take n β) with (take n α) by (apply eqn_take; assumption).
pose (Heα := f_equal (fun f => f (skip n α)) He).
pose (Heβ := f_equal (fun f => f (skip n β)) He).
simpl in *.
rewrite -> Heα, Heβ, <- Hs₀, !take_app; reflexivity.
Qed.

Record M := {
  hom : ℙ -> ℙ;
  spc : isContinuous hom;
}.

Coercion hom : M >-> Funclass.

Inductive D (A : Type) : Type :=
| η : A -> D A
| ask : forall (m : nat), D A -> D A -> D A.

Arguments η {_}.
Arguments ask {_}.

Definition one : M.
Proof.
exists (fun α => α).
intros m; exists m; intros α β h; exact h.
Defined.

Definition dot (p q : M) : M.
Proof.
exists (fun α => q (p α)).
apply isContinuous_act; apply spc.
Defined.

Definition lam : M.
Proof.
exists lam₀.
change (isContinuous (app (cons true nil))).
apply isContinuous_app.
Defined.

Definition ρ : M.
Proof.
exists ρ₀.
change (isContinuous (app (cons false nil))).
apply isContinuous_app.
Defined.

Definition const (p : ℙ) : M.
Proof.
  exists (fun _ => p).
  intro m ; exists 0.
  intros alpha beta eq0.
  exact (eqn_refl m p).
Defined.


(*Record S := {
  car : Type;
  rel : (ℙ -> car) -> Prop;
  beta : forall x y : car, car ;
  beta_nat : forall (x y : ℙ -> car) (xe : rel (act lam x)) (ye : rel (act ρ y)),
      rel (fun p => beta (x p) (y p));
  rel_hom : forall x (p : M), rel x -> rel (p · x);
}.
  beta : forall x y : car, car ;
  beta_nat : forall (x y : ℙ -> car) (xe : forall p : M, rel (act lam (act p x)) (ye : rel y),
      rel (fun p => beta (x p) (y p));


 *)

Record tsig (A : Type) (B : A -> Type) : Type :=
  exist { fst : A; snd : B fst}.

Arguments fst {_ _} _.
Arguments snd {_ _} _.
Notation "{ x : A & P }" := (@tsig A (fun x => P)) : type_scope.

Record Psh := {
  car : Type;
  rel : (ℙ -> car) -> Type;
  ret : forall (a : car), rel (fun _ => a);
  rel_hom : forall x (p : M), rel x -> rel (p · x);
}.

Record Nat (X Y : Psh) := {
  fun_fun : X.(car) -> Y.(car);
  fun_rel : forall (x : ℙ -> X.(car)), X.(rel) x -> Y.(rel) (fun α => fun_fun (x α));
}.

Arguments fun_fun {_ _}.
Arguments fun_rel {_ _}.

Coercion fun_fun : Nat >-> Funclass.

Definition Act : M -> Psh -> Psh.
Proof.
  intros m A.
  unshelve refine (Build_Psh _ _ _ _).
  - exact A.(car).
  - exact (fun x => forall p : M, A.(rel) (m · p · x)).
  - simpl.
    unfold act ; simpl.
    intros a p.
    now apply A.(ret).
  - simpl.
    intros x p H.
    exact (fun q => H (dot q p)).
Defined.

Definition PshProd : Psh -> Psh -> Psh.
Proof.
  intros P Q.
  unshelve refine (Build_Psh _ _ _ _).
  - exact (prod P.(car) Q.(car)).
  - exact (fun f => prod (P.(rel) (fun alpha => match (f alpha) with (p, q) => p end))
                        (Q.(rel) (fun alpha => match (f alpha) with (p,q) => q end))).
  - intro a ; destruct a as [p q] ; simpl.
    split.
    + now apply P.(ret).
    + now apply Q.(ret).
  - simpl ; intros x p H ; destruct H as [HP HQ].
    split.
    + assert ((fun alpha => let (p0, _) := (p · x) alpha in p0) =
              p · (fun alpha : ℙ => let (p, _) := x alpha in p)).
      unfold act ; simpl ; now apply funext.
      rewrite H.
      now apply P.(rel_hom).
    +  assert ((fun alpha => let (_ , q) := (p · x) alpha in q) =
               p · (fun alpha : ℙ => let (_, q) := x alpha in q)).
       unfold act ; simpl ; now apply funext.
       rewrite H.
       now apply Q.(rel_hom).
Defined.


Inductive BranchingSum (A B : Type) :=
| Binl : A -> BranchingSum A B
| Binr : B -> BranchingSum A B
| BêtaSum : BranchingSum A B -> BranchingSum A B -> BranchingSum A B.

Inductive EParaSum (A B : Psh) :
  (ℙ -> BranchingSum A.(car) B.(car)) -> Type :=
| EParaBinl : forall (f : ℙ -> A.(car)) (fe : A.(rel) f),
    EParaSum A B (fun alpha => Binl A.(car) B.(car) (f alpha))
| EParaBinr : forall (f : ℙ -> B.(car)) (fe : B.(rel) f),
    EParaSum A B (fun alpha => Binr A.(car) B.(car) (f alpha))
| EParaBêtaSum : forall (x y : ℙ -> BranchingSum A.(car) B.(car))
                         (Hx : forall p : M, EParaSum A B (lam · p · x))
                         (Hy : forall p : M, EParaSum A B (ρ · p · y)),
    EParaSum A B (fun alpha => BêtaSum A.(car) B.(car) (x alpha) (y alpha)). 

Definition PshSum : Psh -> Psh -> Psh.
Proof.
  intros P Q.
  unshelve refine (Build_Psh _ _ _ _).
  - exact (BranchingSum P.(car) Q.(car)).
  - exact (EParaSum P Q).
  - refine (fix f s := match s with
            | Binl _ _ a => _
            | Binr _ _ b => _
            | BêtaSum _ _ a b => _
            end).
    + refine (EParaBinl _ _ (fun _ => a) _).
      now apply P.(ret).
    + refine (EParaBinr _ _ (fun _ => b) _).
      now apply Q.(ret).
    + refine (EParaBêtaSum _ _ (fun _ => a) (fun _ => b) _ _).
      * intro p.
        unfold act.
        exact (f a).
      * intro p ; unfold act.
        exact (f b).
  - intros s p se.
    induction se as [ | | f g fe fe2 ge ge2].
    * refine (EParaBinl _ _ (fun alpha => f (p alpha)) _).
      now apply P.(rel_hom).
    * refine (EParaBinr _ _ (fun alpha => f (p alpha)) _).
      now apply Q.(rel_hom).
    * unshelve refine (EParaBêtaSum _ _ _ _ _ _).
        -- intro q ; exact (fe (dot q p)).
        -- intro q ; exact (ge (dot q p)).
Defined.


Definition BShf :=
  {P : Psh & Nat (PshProd (Act lam P) (Act ρ P)) P}.



Definition isLocal {X} (x : ℙ -> X) :=
  exists m : nat, forall α β, eqn α β m -> x α = x β.

Definition Discᶠ (A : Type) : BShf.
Proof.
  unshelve econstructor.
  - unshelve econstructor.
    + exact A.
    + refine isLocal.
    + exists 0.
      reflexivity.
    + unfold isLocal ; unfold act ; simpl ; intros x [p I] [m H] ; simpl.
      destruct (I m) as [n C].
      exists n.
      intros alpha beta eqab.
      erewrite (H (p alpha) (p beta) _).
      reflexivity.
  - simpl.
    unshelve econstructor.
    unfold PshProd ; unfold Act ; simpl.
    exact (fun x => match x with (a,b) => a end).
    unfold PshProd ; simpl.
    intros x [H1 H2].
Abort.


Definition BShProd (A B : BShf) : BShf. 
Proof.
  unshelve econstructor.
  - refine (PshProd A.(fst) B.(fst)).
  - unshelve econstructor.
    + simpl.
      intros [[A1 B1] [A2 B2]].
      split.
      * apply A.(snd).
        simpl.
        split.
        -- exact A1.
        -- exact A2.
      * apply B.(snd).
        split.
        -- exact B1.
        -- exact B2.
    + simpl ; intros f [fe ge].
      apply Todo.
Defined.

Inductive BranchingBool :=
| Btrue : BranchingBool
| Bfalse : BranchingBool
| Bêtabool : BranchingBool -> BranchingBool -> BranchingBool.

Inductive EParabool :
  (ℙ -> BranchingBool) -> Type :=
| EParatrue : EParabool (fun _ => Btrue)
| EParafalse : EParabool (fun _ => Bfalse)
| EParabêtabool : forall (x y : ℙ -> BranchingBool)
                         (Hx : forall p : M, EParabool (lam · p · x))
                         (Hy : forall p : M, EParabool (ρ · p · y)),
    EParabool (fun alpha => Bêtabool (x alpha) (y alpha)).

Definition Bool : BShf.
Proof.
  unshelve econstructor.
  - unshelve econstructor.
    + exact BranchingBool.
    + exact EParabool.
    + induction a as [ | | x IHx y IHy].
      * exact EParatrue.
      * exact EParafalse.
      * now unshelve refine (EParabêtabool (fun _ => x)
                                           (fun _ => y) _ _) ; intros.
    + intros b p be.
      induction be as [ | | x y xe xe2 ye ye2].
      * unfold act ; simpl ; exact EParatrue.
      * unfold act ; simpl ; exact EParafalse.
      * unfold act ; simpl.
        unshelve refine (EParabêtabool _ _ _ _).
        -- intro q ; exact (xe (dot q p)).
        -- intro q ; exact (ye (dot q p)).
  - unshelve econstructor ; simpl.
    + exact (fun p => let (a, b) := p in Bêtabool a b).
    + intros b [Hl Hr].
      assert ((fun α : ℙ => Bêtabool (let (p0, _) := b α in p0) (let (_, q) := b α in q)) =
              (fun α : ℙ => let (a, b0) := b α in Bêtabool a b0)).
      * apply funext.
        intro x.
        now destruct (b x).
      * rewrite <- H.
        now unshelve refine (EParabêtabool (fun alpha : ℙ => let (p0, _) := b alpha in p0)
                                       (fun alpha : ℙ => let (_, q) := b alpha in q) _ _).
Defined.        

Inductive Ub :=
| Etau : BShf -> Ub
| Bêtau : Ub -> Ub -> Ub.

Inductive EParaUb :
  (ℙ -> Ub) -> Type :=
| EParaetau : forall (A : BShf), EParaUb (fun _ => Etau A)
| EParabêtau : forall (A B : ℙ -> Ub)
                      (HA : forall p : M, EParaUb (lam · p · A))
                      (HB : forall p : M, EParaUb (ρ · p · B)),
    EParaUb (fun alpha => Bêtau (A alpha) (B alpha)).



Definition U : BShf.
Proof.
  unshelve econstructor.
  - unshelve econstructor.
    + exact Ub.
    + exact EParaUb.
    + simpl ; intro A.
      induction A.
      * exact (EParaetau b).
      * unshelve refine (EParabêtau (fun _ => A1)
                                    (fun _ => A2) _ _).
        -- unfold act ; simpl ; intro p.
           assumption.
        -- unfold act ; simpl ; intro p.
           assumption.
    + intros A p Ae.
      induction Ae as [ | A B Ae Ae2 Be Be2].
      * unfold act ; simpl ; now apply EParaetau.
      * unfold act ; simpl.
        unshelve refine (EParabêtau _ _ _ _).
        -- intro q ; exact (Ae (dot q p)).
        -- intro q ; exact (Be (dot q p)).
  - unshelve econstructor ; simpl.
    + exact (fun P => let (A,B):= P in Bêtau A B).
    + intros A [Hl Hr].
      assert ((fun α : ℙ => Bêtau (let (p0, _) := A α in p0) (let (_, q) := A α in q)) =
              (fun α : ℙ =>let (A0, B) := A α in Bêtau A0 B)).
      * apply funext.
        intro x.
        now destruct (A x).
      * rewrite <- H.
        now unshelve refine (EParabêtau (fun alpha : ℙ => let (p0, _) := A alpha in p0)
                                       (fun alpha : ℙ => let (_, q) := A alpha in q) _ _).
Defined.



Definition Dummy : BShf.
Proof.
  unshelve econstructor.
  - unshelve econstructor.
    + exact unit.
    + exact (fun _ => True).
    + simpl ; intro A.
      exact I.
    + simpl ; intros.
      exact I.
  - unshelve econstructor.
    + simpl.
      exact (fun _ => tt).
    + simpl.
      intros.
      exact I.
Defined.    

Definition El (A : Ub) : BShf := match A with
                                 | Etau B => B
                                 | Bêtau _ _ => Dummy
                                 end.


Definition PArr (X Y : Psh) : Psh.
Proof.
  unshelve econstructor.
    + refine (Nat X Y).
    + refine (fun f =>
                forall (p : M) (x : ℙ -> X.(car)), X.(rel) x -> Y.(rel)
                                                              (fun α => (f (p α)) (x α))).
    + intros [f fe] p.
      exact fe.
    + simpl ; intros f p fe q x xe.
      refine (fe (dot q p) x xe).
Defined.

  
Definition Arr (X Y : BShf) : BShf.
Proof.
  unshelve econstructor.
  - exact (PArr X.(fst) Y.(fst)).
  - unshelve econstructor ; simpl.
    + intros [f g].
      unshelve econstructor.
      * refine (fun x => Y.(snd) _).
        split.
        -- exact (f x).
        -- exact (g x).
      * simpl ; intros x xe.
        unshelve refine (Y.(snd).(fun_rel) _ _).
        split.
        -- intro p.
           refine (Y.(fst).(rel_hom) (fun alpha : ℙ => f (x alpha)) (dot lam p) _).
           now apply f.(fun_rel).
        -- intro p.
           refine (Y.(fst).(rel_hom) (fun alpha : ℙ => g (x alpha)) (dot ρ p) _).
           now apply g.(fun_rel).
    + intros h [Hl Hr] p x xe.
      simpl.
      assert ((fun α : ℙ =>
                 (let (f, g) := h (p α) in
                  {|
                    fun_fun := fun x0 : (fst X).(car) => snd Y (f x0, g x0);
                    fun_rel :=
                      fun (x0 : ℙ -> (fst X).(car)) (xe0 : rel (fst X) x0) =>
                        fun_rel (snd Y) (fun α0 : ℙ => (f (x0 α0), g (x0 α0)))
                                (fun p0 : M =>
                                   rel_hom (fst Y) (fun alpha : ℙ => f (x0 alpha)) (dot lam p0) (fun_rel f x0 xe0),
                                   fun p0 : M =>
                                     rel_hom (fst Y) (fun alpha : ℙ => g (x0 alpha)) (dot ρ p0) (fun_rel g x0 xe0))
                  |}) (x α)) =
              fun α => snd Y (let (f, g) := h (p α) in (f (x α), g (x α)))).
      * apply funext.
        intro α.
        now destruct (h (p α)).
      * rewrite H.
        clear H.
        unshelve refine (Y.(snd).(fun_rel) _ _).
        unfold PshProd ; unfold Act ; simpl.
        split.
        -- intro q.
           apply (Y.(fst).(rel_hom) _ lam) ; apply (Y.(fst).(rel_hom)).
           assert ((fun alpha : ℙ =>
                      let (p0, _) := let (f, g) := h (p alpha) in (f (x alpha), g (x alpha)) in p0) =
                   (fun α : ℙ => (let (p, _) := h (p α) in p) (x α))) as H.
           ++ apply funext.
              intro alpha ; now destruct (h (p alpha)).
           ++ rewrite H.
              exact (Hl (dot (Build_M (skip 1) (isContinuous_skip 1)) p) one x xe).
        -- intro q.
           apply (Y.(fst).(rel_hom) _ ρ) ; apply (Y.(fst).(rel_hom)).
           assert ((fun alpha : ℙ =>
                      let (_, q0) := let (f, g) := h (p alpha) in (f (x alpha), g (x alpha))
                      in q0) =
                   (fun α : ℙ => (let (_, q) := h (p α) in q) (x α))) as H.
           ++ apply funext.
              intro alpha ; now destruct (h (p alpha)).
           ++ rewrite H.
              exact (Hr (dot (Build_M (skip 1) (isContinuous_skip 1)) p) one x xe).
Defined.


Definition BShfSum : (Arr U (Arr U U)).(fst).(car).
Proof.
  unshelve econstructor.
  - intro A ; unshelve econstructor ; intro B. 
    + simpl in A ; simpl in B.
      induction A as [A | A1 IHA1 A2 IHA2].
      * induction B as [B | B1 IHB1 B2 IHB2].
        -- apply Etau.
           unshelve econstructor.
           exact (PshSum A.(fst) B.(fst)).
           unshelve econstructor ; simpl.
           ++ exact (fun C => let (P,Q) := C in BêtaSum _ _ P Q).
           ++ intros h [fe ge].
              assert ((fun α : ℙ => let (P, Q) := h α in BêtaSum (car (fst A)) (car (fst B)) P Q) =
                      (fun alpha => BêtaSum (car (fst A)) (car (fst B))
                                            (let (p, _) := h alpha in p) (let (_, q) := h alpha in q)))
                as eq.
              ** apply funext.
                 intro alpha.
                 f_equal.
                 now induction (h alpha).
              ** rewrite eq.
                 now apply (EParaBêtaSum A.(fst) B.(fst)
                                                     (fun alpha => let (p, _) := h alpha in p)
                                                     (fun alpha => let (_, q) := h alpha in q)).
      -- refine (Bêtau _ _).
        ++ exact IHB1. 
        ++ exact IHB2.
      * refine (Bêtau _ _).
        -- exact IHA1.
        -- exact IHA2.
    + intro Be.
      simpl in *.
      induction A.
      * induction Be.
        -- now apply EParaetau.
        -- now apply EParabêtau.
      * apply EParabêtau.
        -- intro p.
           refine (U.(fst).(rel_hom) _ lam _).
           now refine (U.(fst).(rel_hom) _ p _).
        -- intro p.
           refine (U.(fst).(rel_hom) _ ρ _).
           now refine (U.(fst).(rel_hom) _ p _).
  - simpl ; intros A Ae.
    induction Ae as [A | A1 A2 A1e IHAL A2e IHAR] ; simpl ; intros p B Be.
    + induction Be ; simpl.
      * now apply EParaetau.
      * now apply EParabêtau.
    + apply EParabêtau.
      * intro q.
        refine (IHAL ((dot q p)) one ((dot lam q) · B) _).
        now refine (U.(fst).(rel_hom) _ _ _).
      * intro q.
        refine (IHAR ((dot q p)) one ((dot ρ q) · B) _).
        now refine (U.(fst).(rel_hom) _ _ _).
Defined.        


Lemma ParaUbinversion : forall (A : Psh) (B : car A -> car (fst U))
                    (x : ℙ -> car A) (xe : rel A x)
                    (Be : EParaUb (fun a => B (x a))),
    sum (ssig _ (fun C : BShf => Seq (fun _ : ℙ => Etau C) (fun α : ℙ => B (x α))))
        (exists (X Y : ℙ -> Ub) (Xe : forall p : M, EParaUb (lam · p · X))
                (Ye : forall p : M, EParaUb (ρ · p · Y)),
            (fun alpha : ℙ => Bêtau (X alpha) (Y alpha)) = (fun α : ℙ => B (x α))).
Proof.
  intros.
  inversion Be.
  - left.
    exists A0 ; reflexivity.
  - right.
    exists A0 ; exists B0 ; exists HA ; exists HB.
    done.
Defined.
    
Definition PiNat (A : Psh) (B : (PArr A U.(fst)).(car)) : Type.
Proof.
  destruct B as [B Be].
    simpl in Be.
    unshelve refine (tsig _ _).
    + simpl in B.
      refine (forall x : A.(car), _).
      refine (match B x with
              | Etau C => (C.(fst)).(car)
              | Bêtau _ _ => unit
              end).
    + intro f.
      refine (forall (x : ℙ -> A.(car)) (xe : A.(rel) x), _).
      case (ParaUbinversion A B x xe (Be x xe)).
      * intros [C eqC].
        refine (C.(fst).(rel) _).
        refine (Rw _ ( ℙ -> car (fst C)) _ (fun α => f (x α))).
        set (t:= fun alpha => B (x alpha)).
        assert (Seq (forall α : ℙ, match B (x α) with
                                   | Etau C => (fst C).(car)
                                   | Bêtau _ _ => unit
                                   end)
                    (forall α : ℙ, match t α with
                                   | Etau C => (fst C).(car)
                                   | Bêtau _ _ => unit
                                   end)) as H.
        -- reflexivity.
        -- assert (Seq (fun _ => Etau C)  t) as I.
           ++ now destruct eqC.
           ++ revert H.
              now destruct I.
      * intros ; refine unit.
Defined.



Definition PPi (A : Psh) (B : (PArr A U.(fst)).(car)) : Psh.
Proof.
  unshelve econstructor.
  - exact (PiNat A B).
  - intro f.
    destruct B as [B Be].
    unfold PiNat in f.
    refine (forall (p : M) (x : ℙ -> A.(car)) (xe: A.(rel) x), _).
    case (ParaUbinversion A B x xe (Be x xe)).
    + intros [C eqC].
      refine (C.(fst).(rel) _).
      refine (Rw _ ( ℙ -> car (fst C)) _ (fun α => (f (p α)).(fst) (x α))).
        set (t:= fun alpha => B (x alpha)).
        assert (Seq (forall α : ℙ, match B (x α) with
                               | Etau C => (fst C).(car)
                               | Bêtau _ _ => unit
                               end)
                (forall α : ℙ, match t α with
                               | Etau C => (fst C).(car)
                               | Bêtau _ _ => unit
                              end)) as H.
        -- reflexivity.
        -- assert (Seq (fun _ => Etau C) t) as I.
           ++ now destruct eqC.
           ++ now destruct I.
    + intros.
      exact unit.
  - now intros [f fe] p.
  - simpl ; intros f p fe q x xe.
    refine (fe (dot q p) x xe).
Defined.

Definition Pi (A : BShf) (B : (Arr A U).(fst).(car)) : BShf.
Proof.
  unshelve econstructor.
  - exact (PPi A.(fst) B).
  - unshelve econstructor ; simpl.
    + intros [[f fe] [g ge]].
      unshelve econstructor.
      * clear fe.
        clear ge.
        intro x.
        pose (t := f x) ; pose (u := g x).
        clearbody t u.
        revert t ; revert u.
        refine (match B x as K return (match K with
                                        | Etau C => car (fst C)
                                        | Bêtau _ _ => unit
                                       end) ->
                                      (match K with
                                        | Etau C => car (fst C)
                                        | Bêtau _ _ => unit
                                       end) ->
                                      (match K with
                                        | Etau C => car (fst C)
                                        | Bêtau _ _ => unit
                                       end)
                with
                | Etau C => _
                | Bêtau _ _=> fun _ _ => tt
                end).
        intros u t.
        refine (C.(snd) _).
        unfold PshProd ; unfold Act ; simpl.
        split.
        -- exact t.
        -- exact u.
      * simpl ; intros x xe.
        apply Todo.
    + apply Todo.
Defined.
(*        case_eq (ParaUbinversion (fst A) B x xe (fun_rel B x xe)).
        -- intros [C eqC] eqinv.
           rewrite eqinv.
           set (t:= fun alpha => B (x alpha)).
           assert (Seq (forall α : ℙ, match B (x α) with
                               | Etau C => (fst C).(car)
                               | Bêtau _ => unit
                               end)
                   (forall α : ℙ, match t α with
                              | Etau C => (fst C).(car)
                              | Bêtau _ => unit
                                 end)) as H.
           ++ reflexivity.
           ++ assert (Seq (fun _ => Etau C) t) as I.
              ** clear eqinv. now destruct eqC.
              ** unshelve refine (Rw _ _ _ (C.(snd).(fun_rel) _ _)).
                 --- unfold Act ; unfold PshProd ; simpl.
                     intro α.
                     assert (Seq (match t α with
                                      | Etau C0 => (fst C0).(car)
                                      | Bêtau _ => unit
                                      end)
                                     (match B (x α) with
                                      | Etau C0 => (fst C0).(car)
                                      | Bêtau _ => unit
                                      end)) as H1.
                     +++ reflexivity.
                     +++ split.
                         *** unshelve refine (Rw _ _ _ (f (x α))).
                             refine (match H1 in Seq _ K return Seq K (car (fst C)) with
                                     | Seq_refl _ => _
                                     end).
                             refine (match I in Seq _ K return Seq (match K  α with
                                                                    | Etau C0 => car (fst C0)
                                                                    | Bêtau _ => unit
                                                                    end) _
                                     with
                                     | Seq_refl _ => Seq_refl _
                                     end).
                         *** unshelve refine (Rw _ _ _ (g (x α))).
                             refine (match H1 in Seq _ K return Seq K (car (fst C)) with
                                     | Seq_refl _ => _
                                     end).
                             refine (match I in Seq _ K return Seq (match K  α with
                                                                    | Etau C0 => car (fst C0)
                                                                    | Bêtau _ => unit
                                                                    end) _
                                     with
                                     | Seq_refl _ => Seq_refl _
                                     end).
                 --- clear fe ; clear ge.
                     unfold Rw.
                     refine (match I in Seq _ K return Seq _ _
                             with
                             | Seq_refl _ => _
                             end).
                     refine (match H in Seq _ K return Seq _ _
                             with
                             | Seq_refl _ => _
                             end).
                     refine (match eqC in Seq _ K return Seq _ _
                             with
                             | Seq_refl _ => _
                             end).
                     apply Todo.
                 --- unfold PshProd ; unfold Act ; simpl.
                     split.
                     +++ intro q.
                         apply (C.(fst).(rel_hom) _ lam) ; apply (C.(fst).(rel_hom)).
                         specialize fe with x xe.
                         rewrite eqinv in fe ; simpl in fe.
                         revert fe.
                         clear g ge.
                         unfold Rw.
                         refine (match I in Seq _ K return _
                                 with
                                 | Seq_refl _ => _
                                 end).
                         refine (match H in Seq _ K return _
                             with
                             | Seq_refl _ => _
                             end).
                         refine (match eqC in Seq _ K return  _
                                 with
                                 | Seq_refl _ => _
                                 end).
                         simpl.
                         intro fe.
                         apply Todo.
                     +++ apply Todo.
        -- intros e eqinv.
           rewrite eqinv ; simpl ; exact tt.
    + intros f fe p x xe.
      apply Todo. 
Definition Pi2 (A : BShf) (B : (Arr A U).(fst).(car)) : BShf.
Proof.
  unshelve econstructor.
  - unshelve econstructor.
    + refine (forall x : A.(fst).(car), _).
      refine (match (B x) with
              | Etau C => C.(fst).(car)
              | Bêtau _ _ => unit
              end).
    + refine (fun f =>
                forall (p : M) (x : ℙ -> A.(fst).(car)), A.(fst).(rel) x -> B.(fst).(rel)
                                                                                (fun α => (f (p α)) (x α))).

      
    pose (t:= B x).
    cbv in t.

    
    + refine (Nat X.(fst) Y.(fst)).
    + refine (fun f =>
                forall (p : M) (x : ℙ -> X.(fst)), X.(fst).(rel) x -> Y.(fst).(rel)
                                                                                (fun α => (f (p α)) (x α))).
    + intros [f fe] p.
      exact fe.
    + simpl ; intros f p fe q x xe.
      refine (fe (dot q p) x xe).
  - unshelve econstructor ; simpl.
    + intros [f g].
      unshelve econstructor.
      * refine (fun x => Y.(snd) _).
        unfold PshProd ; unfold Act ; simpl.
        split.
        -- exact (f x).
        -- exact (g x).
      * simpl ; intros x xe.
        unshelve refine (Y.(snd).(fun_rel) _ _).
        unfold Act ; unfold PshProd ; simpl.
        split.
        -- intro p.
           refine (Y.(fst).(rel_hom) (fun alpha : ℙ => f (x alpha)) (dot lam p) _).
           now apply f.(fun_rel).
        -- intro p.
           refine (Y.(fst).(rel_hom) (fun alpha : ℙ => g (x alpha)) (dot ρ p) _).
           now apply g.(fun_rel).
    + intros h [Hl Hr] p x xe.
      simpl.
      assert ((fun α : ℙ =>
                 (let (f, g) := h (p α) in
                  {|
                    fun_fun := fun x0 : fst X => snd Y (f x0, g x0);
                    fun_rel :=
                      fun (x0 : ℙ -> fst X) (xe0 : rel (fst X) x0) =>
                        fun_rel (snd Y) (fun α0 : ℙ => (f (x0 α0), g (x0 α0)))
                                (conj
                                   (fun p0 : M =>
                                      rel_hom (fst Y) (fun alpha : ℙ => f (x0 alpha)) (dot lam p0) (fun_rel f x0 xe0))
                                   (fun p0 : M =>
                                      rel_hom (fst Y) (fun alpha : ℙ => g (x0 alpha)) (dot ρ p0) (fun_rel g x0 xe0)))
                  |}) (x α)) =
              fun α => snd Y (let (f, g) := h (p α) in (f (x α), g (x α)))).
      * apply funext.
        intro α.
        now destruct (h (p α)).
      * rewrite H.
        clear H.
        unshelve refine (Y.(snd).(fun_rel) _ _).
        unfold PshProd ; unfold Act ; simpl.
        split.
        -- intro q.
           apply (Y.(fst).(rel_hom) _ lam) ; apply (Y.(fst).(rel_hom)).
           assert ((fun alpha : ℙ =>
                      let (p0, _) := let (f, g) := h (p alpha) in (f (x alpha), g (x alpha)) in p0) =
                   (fun α : ℙ => (let (p, _) := h (p α) in p) (x α))) as H.
           ++ apply funext.
              intro alpha ; now destruct (h (p alpha)).
           ++ rewrite H.
              exact (Hl (dot (Build_M (skip 1) (isContinuous_skip 1)) p) one x xe).
        -- intro q.
           apply (Y.(fst).(rel_hom) _ ρ) ; apply (Y.(fst).(rel_hom)).
           assert ((fun alpha : ℙ =>
                      let (_, q0) := let (f, g) := h (p alpha) in (f (x alpha), g (x alpha))
                      in q0) =
                   (fun α : ℙ => (let (_, q) := h (p α) in q) (x α))) as H.
           ++ apply funext.
              intro alpha ; now destruct (h (p alpha)).
           ++ rewrite H.
              exact (Hr (dot (Build_M (skip 1) (isContinuous_skip 1)) p) one x xe).
Defined.*)
  

Inductive BranchingNat :=
| B0 : BranchingNat
| BS : BranchingNat -> BranchingNat
| Bêtanat : BranchingNat -> BranchingNat -> BranchingNat.


Inductive EParanat :
  (ℙ -> BranchingNat) -> Type :=
| EPara0 : EParanat (fun _ => B0)
| EParaS : forall (n :  ℙ -> BranchingNat)
                  (ne : EParanat n),
    EParanat (fun alpha => BS (n alpha))                        
| EParabêtanat  : forall (x y : ℙ -> BranchingNat)
                         (Hx : forall p : M, EParanat (lam · p · x))
                         (Hx : forall p : M, EParanat (ρ · p · y)),
    EParanat (fun alpha => Bêtanat (x alpha) (y alpha)).


Definition BNat : BShf.
Proof.
  unshelve econstructor.
  - unshelve econstructor.
    + exact BranchingNat.
    + refine EParanat.
    + induction a as [ | | x IHx y IHy].
      * exact EPara0.
      * now apply EParaS.
      * unshelve refine (EParabêtanat (fun _ => x)
                                      (fun _ => y) _ _).
        -- now unfold act ; simpl ; intro p. 
        -- now unfold act ; simpl ; intro p.
    + intros n p ne.
      induction ne as [ | | x y xe xe2 ye ye2].
      * unfold act ; simpl ; exact EPara0.
      * unfold act ; simpl ; now apply EParaS.
      * unfold act ; simpl.
        unshelve refine (EParabêtanat _ _ _ _).
        -- intro q ; exact (xe (dot q p)).
        -- intro q ; exact (ye (dot q p)).
  - unshelve econstructor ; simpl.
    + exact (fun p => let (a, b) := p in Bêtanat a b).
    + intros n [Hl Hr].
      assert ((fun α : ℙ => Bêtanat (let (p0, _) := n α in p0) (let (_, q) := n α in q)) =
              (fun α : ℙ => let (a, b) := n α in Bêtanat a b)).
      * apply funext.
        intro x.
        now destruct (n x).
      * rewrite - H.
        now unshelve refine (EParabêtanat (fun alpha : ℙ => let (p0, _) := n alpha in p0)
                                       (fun alpha : ℙ => let (_, q) := n alpha in q) _ _).
Defined.


Fixpoint Bêta_nth (n : nat) (u : ℙ) : BranchingBool :=
  match n with
  | 0 => Bêtabool Btrue Bfalse
  | S k => Bêtabool (Bêta_nth k u) (Bêta_nth k u)
  end.

Fixpoint inj_aux (n: nat) (f: ℙ) (b: BranchingNat) : BranchingBool:=
  match b with
  | B0 => Bêta_nth n f
  | BS k => inj_aux (S n) f k
  | Bêtanat i j => Bêtabool (inj_aux n f i) (inj_aux n f j)
  end.

  
Lemma inj_car : ℙ -> (Arr BNat Bool).(fst).(car).
Proof.
  intros f; unshelve econstructor; simpl.
  - exact (inj_aux 0 f).
  - intros n ne.
    generalize 0.
    induction ne.
    + intro m ; simpl.
      induction m ; simpl.
      * refine (EParabêtabool _ _ (fun _ => EParatrue) (fun _ => EParafalse)).
      * refine (EParabêtabool _ _ (fun _ => IHm) (fun _ => IHm)).
    + intro m ; simpl.
      exact (IHne (S m)).
    + intro m ; simpl.
      apply EParabêtabool.
      * refine (fun p => H p m).
      * refine (fun p => H0 p m).
Defined.

Lemma inj_stream_par :
  (forall (p : M) (x : ℙ -> BranchingNat),
      EParanat x -> EParabool (fun α : ℙ => inj_car (p α) (x α))).
Proof.
  simpl.
  intros p x xe.
  generalize 0.
  revert p.
  induction xe.
  - simpl.
    intros p n ; simpl ; revert p.
    induction n.
    + intro p ; refine (EParabêtabool _ _ (fun _ => EParatrue) (fun _ => EParafalse)).
    + simpl ; intro p.
      refine (EParabêtabool _ _ _ _ ).
      * exact (fun q => IHn (dot lam (dot q p))). 
      * exact (fun q => IHn (dot ρ (dot q p))).
  - simpl ; intros p m.
    exact (IHxe p (S m)).
  - simpl ; intros p m.
    apply EParabêtabool.
    + intro q.
      refine (H q (dot lam (dot q p)) m).
    + intro q.
      refine (H0 q (dot ρ (dot q p)) m).
Defined.



Definition Gen : (Arr (Arr BNat Bool) (Arr BNat Bool)).(fst).(car).
Proof.
  unshelve econstructor.
  - unfold PArr ; simpl.
    intros [u ue] ; simpl in *.
    unshelve econstructor.
    + simpl ; intro n.
      refine (let fix f (b: BranchingBool) := match b with
                                          | Btrue => Bêtabool Btrue Bfalse
                                          | Bfalse => Bêtabool Btrue Bfalse
                                          | Bêtabool x y => Bêtabool (f x) (f y)
                                          end
              in f (u n)).
    + simpl.
      intros x xe.
      specialize ue with x ; apply ue in xe ; clear ue.
      set (t:= fun α : ℙ => u (x α)).
      assert (Seq (EParabool (fun α : ℙ =>
                                (fix f (b : BranchingBool) : BranchingBool :=
                                   match b with
                                   | Bêtabool x0 y => Bêtabool (f x0) (f y)
                                   | _ => Bêtabool Btrue Bfalse
                                   end) (t α) ))
                  (EParabool (fun α : ℙ =>
                                (fix f (b : BranchingBool) : BranchingBool :=
                          match b with
                          | Bêtabool x0 y => Bêtabool (f x0) (f y)
                          | _ => Bêtabool Btrue Bfalse
                          end) (u (x α))))) as H ; try reflexivity.
      refine (Rw _ _ H _) ; clear H.
      revert t.
      induction xe ; simpl.
      * apply EParabêtabool ; intros.
        -- exact EParatrue.
        -- exact EParafalse. 
      * apply EParabêtabool ; intros.
        -- exact EParatrue.
        -- exact EParafalse.
      * now apply EParabêtabool.
  - simpl.
    intros x xe p n ne.
    specialize xe with p n.
    apply xe in ne ; clear xe.
    set (t:= fun α : ℙ => (x (p α) (n α))).
    assert (Seq (EParabool (fun α : ℙ =>
                              (fix f (b : BranchingBool) : BranchingBool :=
                                 match b with
                                 | Bêtabool x0 y => Bêtabool (f x0) (f y)
                                 | _ => Bêtabool Btrue Bfalse
                                 end) (t α) ))
                (EParabool (fun α : ℙ =>
                              (fix f (b : BranchingBool) : BranchingBool :=
                                 match b with
                                 | Bêtabool x0 y => Bêtabool (f x0) (f y)
                                 | _ => Bêtabool Btrue Bfalse
                                 end) (x (p α) (n α))))) as H ; try reflexivity.
    refine (Rw _ _ H _) ; clear H.
    revert t.
    induction ne ; simpl.
    + apply EParabêtabool ; intros.
      * exact EParatrue.
      * exact EParafalse.
    + apply EParabêtabool ; intros.
      * exact EParatrue.
      * exact EParafalse.
    + apply EParabêtabool ; intros.
      * now apply H.
      * now apply H0.
Defined.


    
Definition fan : (Arr (Arr (Arr BNat Bool) BNat) BNat).(fst).(car). 
Proof.
  simpl.
  unshelve econstructor ; simpl.
  - intros [f fe].
    simpl in *.
    unshelve refine
             (let fix h _ (t : EParanat _) :=
              match t with
              | EPara0 => B0
              | EParaS b e => h b e
              | EParabêtanat x y xe ye => Bêtanat (h (lam · x) (xe one))
                                                  (h (ρ · y) (ye one))
              end in h (fun α : ℙ => f (inj_car α))
                       (fe inj_car inj_stream_par)).
  - simpl.
    intros f fe.
    assert (forall alpha,
               (fun α : ℙ => f alpha (inj_car α)) =
               (fun α : ℙ => f α (inj_car α))) as H.
    + intro alpha.
      generalize (eq_refl (fun α : ℙ => f alpha (inj_car α))).
      generalize (fun α : ℙ => f alpha (inj_car α)) at 1 ; simpl.
      intros c1 e1.
      assert (H1:= f_equal (fun k => k alpha) e1) ; simpl in H1.
      generalize (eq_refl (fun α : ℙ => f α (inj_car α))).
      generalize (fun α : ℙ => f α (inj_car α)) at 1 ; simpl.
      intros c2 e2.
      assert (H2:= f_equal (fun k => k alpha) e2) ; simpl in H2.
      rewrite -H1 in H2 ; clear H1.
      generalize (fe (const alpha) inj_car inj_stream_par) ; intro r ; simpl in r.
      generalize (fe one inj_car inj_stream_par) ; intro q ; simpl in q.
      revert c1 c2 e1 e2 H2 r q.
      generalize  (fun α : ℙ => f alpha (inj_car α)) ;
      generalize  (fun α : ℙ => f α (inj_car α)) ; simpl.
      intros c1 c2 x y e1 e2 H2 r q.
      rewrite - e1 ; rewrite - e2.
      rewrite - e1 in r ; rewrite -e2 in q.
      clear c1 c2 e1 e2.
      revert x y r q H2.
      refine (fix h x y r q := match r as r0 in EParanat x0 return (_ = x0 alpha) -> x0 = y with
                           | EPara0 => match q as q0 in EParanat y0 return (_ = _) -> _ with
                                       | EPara0 => _
                                       | EParaS _ _ => _
                                       | EParabêtanat _ _ _ _ => _
                                       end
                           | EParaS _ _  => match q as q0 in EParanat y0
                                                  return (_ = _) -> _
                                            with
                                            | EPara0 => _
                                            | EParaS _ _ => _
                                            | EParabêtanat _ _ _ _ => _
                                            end
                           | EParabêtanat _ _ _ _ =>
                             match q as q0 in EParanat y0
                                                  return (_ = _) -> _
                                            with
                                            | EPara0 => _
                                            | EParaS _ _ => _
                                            | EParabêtanat _ _ _ _ => _
                                            end
                           end) ; trivial ; intro H1 ; try discriminate H1.
      * rewrite (h b b0 e e0 _).
        -- now inversion H1.
        -- reflexivity.
      * admit.
    + assert (tsig _ (fun ne => forall alpha,
                          (fun_rel (f alpha) inj_car inj_stream_par) =
                          (Rw (EParanat (fun α : ℙ => f α (inj_car α))) _
                              ((Sf_equal _ _ _ _ _ (match H alpha with | eq_refl _ => Seq_refl _ end))) ne))).
      * exists (Rw _ (EParanat n)
                   ((Sf_equal _ _ _ _ _ (match H ((cofix ω := mkℙ true ω)) with
                                         | eq_refl _ => Seq_refl _ end)))
                   (fun_rel (f ((cofix ω := mkℙ true ω))) inj_car inj_stream_par)).
        simpl.


    
    assert ((let k := (fun α : ℙ =>
     (fix h (H : ℙ -> BranchingNat) (t0 : EParanat H) {struct t0} : BranchingNat :=
        match t0 with
        | EPara0 => B0
        | EParaS b e => h b e
        | EParabêtanat x y xe ye => Bêtanat (h (lam₀ · x) (xe one)) (h (ρ₀ · y) (ye one))
        end) (fun α0 : ℙ => f α (inj_car α0))
             (fun_rel (f α) inj_car inj_stream_par)) in
             ((k = fun alpha => B0)))).
    + apply funext.
      intro x.
      generalize (fun_rel (f x) inj_car inj_stream_par) ; simpl.
      induction r.
      * reflexivity.
      * 
      pose (r:= fe (const x) inj_car inj_stream_par) ; simpl in r.
      generalize (Seq_refl (fun α : ℙ => f x (inj_car α))).
      generalize (fun α : ℙ => f x (inj_car α)) at 2.
      intros c e.
      refine (Rw (((fix h (H : ℙ -> BranchingNat) (t0 : EParanat H) {struct t0} : BranchingNat :=
                      match t0 with
                      | EPara0 => B0
                      | EParaS b e0 => h b e0
                      | EParabêtanat x y xe ye => Bêtanat (h (lam₀ · x) (xe one))
                                                          (h (ρ₀ · y) (ye one))
                      end) c
                           (Rw _ (EParanat c)
                               ((Sf_equal _ _ _ _ _ (match e with | Seq_refl _ => Seq_refl _ end)))
                               (fun_rel (f x) inj_car inj_stream_par))) = B0) _ _ _).
      * destruct e.
        reflexivity.
      * generalize ((Rw _ (EParanat c)
                               ((Sf_equal _ _ _ _ _ (match e with | Seq_refl _ => Seq_refl _ end)))
                               (fun_rel (f x) inj_car inj_stream_par))) at 1.

        


    
    rewrite - e.
    induction r.
    * simpl.
    
    assert (


    
    simpl in t.
    generalize (Seq_refl (fun alpha α : ℙ => f alpha (inj_car α))).
    generalize (fun alpha α : ℙ => f alpha (inj_car α)) at 2.
    intros c e.
    change (EParanat
              (fun α : ℙ =>
                 (fix h (H : ℙ -> BranchingNat) (t0 : EParanat H) {struct t0} : BranchingNat :=
                    match t0 with
                    | EPara0 => B0
                    | EParaS b e0 => h b e0
                    | EParabêtanat x y xe ye => Bêtanat (h (lam₀ · x) (xe one)) (h (ρ₀ · y) (ye one))
                    end) ((fun alpha α => f alpha (inj_car α)) α)
                         (fun_rel (f α) inj_car inj_stream_par))).
    Print Rw.
    refine (Rw (EParanat
                (fun α : ℙ =>
                   (fix h (H : ℙ -> BranchingNat) (t0 : EParanat H) {struct t0} : BranchingNat :=
                      match t0 with
                      | EPara0 => B0
                      | EParaS b e0 => h b e0
                      | EParabêtanat x y xe ye => Bêtanat (h (lam₀ · x) (xe one)) (h (ρ₀ · y) (ye one))
                      end) (c α)
                           (Rw _ (EParanat (c α)) ((Sf_equal _ _ _ _ _ (match e with | Seq_refl _ => Seq_refl _ end))) (fun_rel (f α) inj_car inj_stream_par)))) _ _ _).
    + simpl.
      now destruct e.
    + simpl.
      specialize t with ((cofix ω := mkℙ true ω)).
      * 

          
    refine (Rw (EParanat
                  (fun α : ℙ =>
                     (fix h (H : ℙ -> BranchingNat) (t0 : EParanat H) {struct t0} : BranchingNat :=
                        match t0 with
                        | EPara0 => B0
                        | EParaS b e0 => h b e0
                        | EParabêtanat x y xe ye => Bêtanat (h (lam₀ · x) (xe one)) (h (ρ₀ · y) (ye one))
                        end) c
                             ((fun_rel (f α) inj_car inj_stream_par)))) e).
               
    
    destruct e.
              

    
    induction t.
    +

    
    pose (q:= (fun_rel (f (cofix ω := mkℙ true ω)) inj_car inj_stream_par)).
    simpl in q.

    
    apply fe.
    apply Gen.
    simpl.
    unshelve econstructor.
    * simpl.
      refine (fun _ => Btrue).
    * simpl.
      refine (fun _ _ => EParatrue).
  - simpl.
    intros f fe.
      
    Print Nat.
    
    destruct H as [K | J | P].
   unshelve econstructor.
  - 

  

Lemma fan_spec : forall (f : arrᶠ (arrᶠ natᶠ boolᶠ) natᶠ),
  isLocal (fun α => f (inj (nth α))).
Proof.
intros [f fε]; simpl in *.
apply (fε (fun α => (inj (nth α)))).
intros p n [m Hm].
destruct (p.(spc) m) as [l Hl].
exists (Nat.max m l); intros α β H; simpl.
rewrite (Hm α β); [|eapply eqn_le; [|eassumption]; lia].
apply eqn_nth.
eapply eqn_le.
2:{ apply Hl. admit. }
Admitted.

Definition fanᶠ : arrᶠ (arrᶠ (arrᶠ natᶠ boolᶠ) natᶠ) natᶠ.
Proof.
unshelve econstructor; simpl.
+ intros [f fε]; simpl in *.
  specialize (fε (fun α => (inj (nth α)))); cbn in *.
  admit.
+ simpl.
intros.
apply fan_spec.
Abort.

Definition El (A : BShf) : Type.
Proof.
  unshelve refine (@sig _ _).
  - exact (ℙ -> A.(fst).(car)).
  - exact A.(fst).(rel).
Defined.    
    
Definition PI (A : BShf) (B : (Arr A U).(fst)) : BShf.
Proof.
  unfold Arr in B.
  simpl in B.
  destruct B as [B Be] ; simpl in B ; simpl in Be.
  unshelve econstructor.
  - unshelve econstructor.
    + exact (forall a : fst A, (B a).(fst)).
    + Print Nat.
      intro f.
      exact (forall a : ℙ -> fst A, rel A.(fst) a ->
                                    rel (B a).(fst) (fun alpha => f alpha (a alpha))).
      
  
(*Record Fun (X Y : S) := {
  fun_fun : X -> Y;
  fun_rel : forall (x : ℙ -> X), X.(rel) x -> Y.(rel) (fun α => fun_fun (x α));
}.
Arguments fun_fun {_ _}.
Arguments fun_rel {_ _}.

Coercion fun_fun : Fun >-> Funclass.

Definition Fun_beta {X Y : S} (f g : ℙ -> (ℙ -> X) -> Y) : ℙ -> (ℙ -> X) -> Y :=
  fun p x => Y.(beta) (fun q => f q x) (fun q => g q x) p.

Definition appL {X Y : S} (f : ℙ -> X -> Y) (x : ℙ -> X) : ℙ -> Y :=
  fun alpha => f alpha (x alpha).

Definition appL_L {X Y : S} (f : ℙ -> X -> Y) (x : ℙ -> X) :
  (fun alpha => act lam₀ f alpha (act lam₀ x alpha)) =
  fun alpha => act lam₀ (appL f x) alpha.
Proof.
  reflexivity.
Defined.

Definition appL_nat {X Y : S} (f : ℙ -> X -> Y)(x : ℙ -> X) (m : M) :
  act m (appL f x) = appL (act m f) (act m x).
Proof.
  reflexivity.
Defined.



Definition Fun_bnd {X Y : S}
           (f : ℙ -> Fun X Y)
           (fe : forall (p : M) (x : ℙ -> X),
               rel X x -> rel Y (fun α : ℙ => (f (p α)) x))
           (g : ℙ -> Fun X Y)
           (ge : forall (p : M) (x : ℙ -> X),
               rel X x -> rel Y (fun α : ℙ => (g (p α))  x)) :
  forall (p : M) (x : ℙ -> X), X.(rel) x ->
                               Y.(rel) (Y.(beta) (fun q => (f (p q))  x)
                                                 (fun q => (g (p q))  x)).
Proof.
  unfold Fun_beta ; simpl.
  intros p x xe.
  apply Y.(rel_bnd).
  + now apply fe.
  + now apply ge.
Defined.
*)
(*Definition tran {X Y : S} {u v : Fun X Y} (p : u.(fun_fun) = v.(fun_fun)) : 
  (forall x : ℙ -> X, rel X x -> rel Y (fun _ : ℙ => u.(fun_fun) x)) ->
  (forall x : ℙ -> X, rel X x -> rel Y (fun _ : ℙ => v.(fun_fun) x)).
Proof.
induction p.
auto.
Defined.

Lemma equal_foo {X Y : S} (u v : Fun X Y) (p : (u.(fun_fun)) = (v.(fun_fun))) :
  tran p u.(fun_rel) = v.(fun_rel) -> u = v.
Proof.
intro H.
destruct u as [x y].
destruct v as [s t].
simpl in * |- *.
destruct p.
simpl in H.
rewrite H.
reflexivity.
Qed.*)

Definition arrᶠ (X Y : S) : S.
Proof.
  unshelve econstructor.
  - exact (Fun X Y).
  - refine (fun f =>
              forall (p : M) (x : ℙ -> X), X.(rel) x -> Y.(rel) (fun α => (f (p α)) (x α))).
  - intros f g.
    unshelve refine (Build_Fun _ _ _ _).
    + intro x.
      unshelve refine (Y.(beta) _ _).
      * exact (f x).
      * exact (g x).
    + simpl.
      intros x xe.
      apply (Y.(beta_nat) (act (skip 1) (fun alpha => f (x alpha)))
                          (act (skip 1) (fun alpha => g (x alpha)))).
      * apply (Y.(rel_hom) _ (Build_M (skip 1) (isContinuous_skip 1))).
        now apply fun_rel.
      * apply (Y.(rel_hom) _ (Build_M (skip 1) (isContinuous_skip 1))).
        now apply fun_rel.
  - simpl.
    intros f g fe ge p x xe.
    pose (q:= Y.(beta_nat) (fun alpha => f (p alpha) (act (skip 1) x alpha) )
                           (fun alpha => g (p alpha) (act (skip 1) x alpha))).
    unfold act in q ; unfold skip in q ; simpl in q.
    unfold act ; simpl.
    apply q.
    refine (Y.(beta_nat) _ _ _ _).


    refine (Y.(beta_nat) _ _ _ _).
      * apply fun_rel.
        now refine (X.(rel_hom) x lam xe).
      * apply fun_rel.
        now refine (X.(rel_hom) x ρ xe).
  - simpl.
    intros f g fe ge p x xe.
    refine (Y.(beta_nat) _ _ _ _).      
    + unfold act ; simpl. assert (Hp : isContinuous' p).
    { apply isContinuous_1; destruct p; assumption. }
    destruct (Hp 1) as [m Hm]; clear Hp.
    assert (Hs : forall s, length s = m -> exists (p₀ : ℙ -> ℙ),
                 isContinuous p₀ /\ (app s · p = p₀ · lam₀ \/ app s · p = p₀ · ρ₀)).
    { intros s Hs. destruct (Hm s Hs) as [p₀ [s₀ [Hp₀ [Hs₀ He]]]].
      exists p₀; split; [assumption|].
      destruct s₀ as [|b s₀]; [exfalso; simpl in Hs₀; congruence|].
      destruct s₀ as [|? s₀]; [|exfalso; simpl in Hs₀; congruence].
      rewrite He; destruct b; simpl.
      * left; reflexivity.
      * right; reflexivity. }
    clear Hm; revert p x xe Hs.
    induction m as [|m IHm]; intros p x xe Hs.
      * specialize (Hs nil eq_refl).
        destruct Hs as [p₀ [s₀ [He|He]]]; change (app nil · p) with (hom p) in He; rewrite He.
        -- apply (hl(dot lam (Build_M p₀ s₀)) (lam · x)).
           now apply rel_hom.
        -- unfold act ; simpl.
           
          apply (fe (dot lam (Build_M p₀ s₀)) (lam · x)).
         apply rel_hom ; assumption.
        -- unfold act ; simpl.
           unfold act in fe ; simpl.
           
          pose (q:= fe (dot ρ (Build_M p₀ s₀)) (ρ · x)).
          unfold dot in q ; unfold act in q ; simpl in q.
           apply rel_hom ; assumption.
        
           unfold dot in q ; unfold act in q ; simpl in q.
           apply q.
           apply (fe (Build_M p₀ s₀) x xe).
           ; assumption.
      * apply (fr (Build_M p₀ s₀)); assumption.
  - apply rel_bnd; unfold act.
  * apply (IHm (dot lam p) (act lam₀ x)); [apply (rel_hom _ _ lam); assumption|].
    intros s Hs'.
    destruct (Hs (cons true s)) as [p₀ [Hp₀ He]]; [simpl; congruence|].
    exists p₀; split; [assumption|].
    { change (app s · dot lam p) with (app (true :: s) · p); assumption. }
  * apply (IHm (dot ρ p) (act ρ₀ x)); [apply (rel_hom _ _ ρ); assumption|].
    intros s Hs'.
    destruct (Hs (cons false s)) as [p₀ [Hp₀ He]]; [simpl; congruence|].
    exists p₀; split; [assumption|].
    { change (app s · dot ρ p) with (app (false :: s) · p); assumption. }
      
      pose (q:= fe one x xe).
      unfold one in q ; simpl in q.
    
      
    refine (fun x => Y.(beta) (fun q => (f q) x) (fun q => (g q) x) p).
   (* simpl.
    intros x xe.
    pose (q:= Y.(rel_hom) (fun alpha : ℙ => beta Y
                                                 (fun q : ℙ => fun_fun (f q) x)
                                                 (fun q : ℙ => fun_fun (g q) x)
                          alpha)
                          (const p)).
    unfold act in q ; simpl in q.
    apply q.
    apply Y.(rel_bnd).
    * *) 
  + simpl ; intros f g.
    apply funext ; intro q ; apply funext ; intro x.
    now rewrite Y.(beta_nat).
  (*unfold act ; simpl.
    pose (u :=  {|
    fun_fun :=
      fun x : ℙ -> X =>
      beta Y (fun q0 : ℙ => fun_fun (f q0) x) (fun q0 : ℙ => fun_fun (g q0) x) q;
    fun_rel :=
      fun (x : ℙ -> X) (_ : rel X x) =>
      rel_hom Y
        (fun alpha : ℙ =>
         beta Y (fun q0 : ℙ => fun_fun (f q0) x) (fun q0 : ℙ => fun_fun (g q0) x) alpha)
        (const q)
        (rel_bnd Y (fun q0 : ℙ => fun_fun (f q0) x) (fun q0 : ℙ => fun_fun (g q0) x)
           (Todo (rel Y (fun q0 : ℙ => fun_fun (f q0) x)))
           (Todo (rel Y (fun q0 : ℙ => fun_fun (g q0) x))))
  |}).
    pose (v := {|
    fun_fun :=
      fun x : ℙ -> X =>
      beta Y (fun q0 : ℙ => fun_fun (f (lam₀ q0)) x) (fun q0 : ℙ => fun_fun (g (ρ₀ q0)) x) q;
    fun_rel :=
      fun (x : ℙ -> X) (_ : rel X x) =>
      rel_hom Y
        (fun alpha : ℙ =>
         beta Y (fun q0 : ℙ => fun_fun (f (lam₀ q0)) x) (fun q0 : ℙ => fun_fun (g (ρ₀ q0)) x) alpha)
        (const q)
        (rel_bnd Y (fun q0 : ℙ => fun_fun (f (lam₀ q0)) x) (fun q0 : ℙ => fun_fun (g (ρ₀ q0)) x)
           (Todo (rel Y (fun q0 : ℙ => fun_fun (f (lam₀ q0)) x)))
           (Todo (rel Y (fun q0 : ℙ => fun_fun (g (ρ₀ q0)) x))))
  |}).
    assert (u = v).
    * unshelve refine (@equal_foo _ _ _ _ _ _).
      -- apply funext.
         intro x.
         exact (f_equal (fun k => k q) (Y.(beta_nat) (fun q0 : ℙ => fun_fun (f q0) x)
                                 (fun q0 : ℙ => fun_fun (g q0) x))).
      -- admit.
    * assumption.*)
  + simpl ; intros f p fe q x xe.
    exact (fe (dot q p) x xe).
  + simpl.
    intros f g fe ge p x xe.
    apply Y.(rel_hom).
    apply Y.(rel_bnd).
    * exact (fe one x xe).
    * exact (ge one x xe).
Defined. 

(*Definition Fun_bnd {X Y : S} (f : ℙ -> Fun X Y)
  (fl : forall (p : M) (x : ℙ -> X),
   rel X x -> rel Y (fun α : ℙ => (act lam₀ f) (p α) (x α)))
  (fr : forall (p : M) (x : ℙ -> X),
   rel X x -> rel Y (fun α : ℙ => (act ρ₀ f) (p α) (x α)))
  (p : M) (x : ℙ -> X) (xε : rel X x) :
  rel Y (fun α : ℙ => f (p α) (x α)).
Proof.
assert (Hp : isContinuous' p).
{ apply isContinuous_1; destruct p; assumption. }
destruct (Hp 1) as [m Hm]; clear Hp.
assert (Hs : forall s, length s = m -> exists (p₀ : ℙ -> ℙ),
  isContinuous p₀ /\ (app s · p = p₀ · lam₀ \/ app s · p = p₀ · ρ₀)).
{ intros s Hs. destruct (Hm s Hs) as [p₀ [s₀ [Hp₀ [Hs₀ He]]]].
  exists p₀; split; [assumption|].
  destruct s₀ as [|b s₀]; [exfalso; simpl in Hs₀; congruence|].
  destruct s₀ as [|? s₀]; [|exfalso; simpl in Hs₀; congruence].
  rewrite He; destruct b; simpl.
  + left; reflexivity.
  + right; reflexivity. }
clear Hm; revert p x xε Hs.
induction m as [|m IHm]; intros p x xε Hs.
- specialize (Hs nil eq_refl).
  destruct Hs as [p₀ [s₀ [He|He]]]; change (app nil · p) with (hom p) in He; rewrite He.
  * apply (fl (Build_M p₀ s₀)); assumption.
  * apply (fr (Build_M p₀ s₀)); assumption.
- apply rel_bnd; unfold act.
  * apply (IHm (dot lam p) (act lam₀ x)); [apply (rel_hom _ _ lam); assumption|].
    intros s Hs'.
    destruct (Hs (cons true s)) as [p₀ [Hp₀ He]]; [simpl; congruence|].
    exists p₀; split; [assumption|].
    { change (app s · dot lam p) with (app (true :: s) · p); assumption. }
  * apply (IHm (dot ρ p) (act ρ₀ x)); [apply (rel_hom _ _ ρ); assumption|].
    intros s Hs'.
    destruct (Hs (cons false s)) as [p₀ [Hp₀ He]]; [simpl; congruence|].
    exists p₀; split; [assumption|].
    { change (app s · dot ρ p) with (app (false :: s) · p); assumption. }
Qed.

Definition arrᶠ (X Y : S) : S.
Proof.
unshelve econstructor.
+ exact (Fun X Y).
+ refine (fun f => forall (p : M) (x : ℙ -> X), X.(rel) x -> Y.(rel) (fun α => f (p α) (x α))).
+ intros [f hf] _ x xε ; simpl.
  refine (hf x xε).
+ simpl; intros f p fε q x xε.
  refine (fε (dot q p) x xε).
+ simpl. apply Fun_bnd.
Defined.*)

Definition isLocal {X} (x : ℙ -> X) :=
  exists m : nat, forall α β, eqn α β m -> x α = x β.

Definition Discᶠ (A : Type) : S.
Proof.
unshelve econstructor.
+ exact A.
+ refine isLocal.
+ exact merge.
+ unfold act ; unfold merge ; simpl.




    
    



  admit.
+ simpl; intros n p [m Hm].
  destruct (p.(spc) m) as [l Hl].
  exists l; intros α β H; apply Hm, Hl, H.
+ simpl. intros x y [ml Hml] [mr Hmr].
  exists (Datatypes.S (Nat.max ml mr)); intros α β H.
  rewrite -> (ℙ_eta α), (ℙ_eta β).
  replace (hd β) with (hd α).
  2:{ inversion H; assumption. }
  set (b := α.(hd)); set (α' := tl α); set (β' := tl β).
  assert (eqn α' β' (Nat.max ml mr)) by (inversion H; assumption).
  clearbody b α' β'.
  destruct b; [apply Hml|apply Hmr]; (eapply eqn_le; [|eassumption]); lia.
Defined.

Definition natᶠ : S := Discᶠ nat.

Definition boolᶠ : S := Discᶠ bool.

Lemma inj : (nat -> bool) -> arrᶠ natᶠ boolᶠ.
Proof.
intros f; unshelve econstructor; simpl.
+ exact f.
+ intros n [m Hm].
  exists m; intros α β H.
  f_equal; apply Hm, H.
Defined.

Lemma fan_spec : forall (f : arrᶠ (arrᶠ natᶠ boolᶠ) natᶠ),
  isLocal (fun α => f (inj (nth α))).
Proof.
intros [f fε]; simpl in *.
apply (fε (fun α => (inj (nth α)))).
intros p n [m Hm].
destruct (p.(spc) m) as [l Hl].
exists (Nat.max m l); intros α β H; simpl.
rewrite (Hm α β); [|eapply eqn_le; [|eassumption]; lia].
apply eqn_nth.
eapply eqn_le.
2:{ apply Hl. admit. }
Admitted.

Definition fanᶠ : arrᶠ (arrᶠ (arrᶠ natᶠ boolᶠ) natᶠ) natᶠ.
Proof.
unshelve econstructor; simpl.
+ intros [f fε]; simpl in *.
  specialize (fε (fun α => (inj (nth α)))); cbn in *.
  admit.
+ simpl.
intros.
apply fan_spec.
Abort.
