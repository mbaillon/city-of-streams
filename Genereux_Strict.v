Set Primitive Projections.
Set Universe Polymorphism.
Require Import Dialogue.
Require Import Base.
Require Import ssreflect.


(*Section Translation3.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Variables (X Y : Type).
Definition ℙ := list (X -> Y).

Record ssig (A:Type) (P: A -> SProp) : Type := Sexist { spr1 : A; spr2 : P spr1 }.

Record Ssig (A : SProp) (P: A -> SProp) : SProp := Sexist2 { Spr1 : A; Spr2 : P Spr1 }.


Arguments spr1 {_ _} _.
Arguments spr2 {_ _} _.
Set Definitional UIP.
Inductive Seq (A : Type) (x : A) : A -> SProp :=  Seq_refl : Seq x x.

Definition Rw (A B : Type) : Seq A B -> A -> B.
Proof.
  intros eq a.
  refine (match eq as eq0 in Seq _ B
          with | Seq_refl _ => a end).
Defined.

Definition SRw (A B : SProp) : Seq A B -> A -> B.
Proof.
  intros eq a.
  refine (match eq as eq0 in Seq _ B
          with | Seq_refl _ => a end).
Defined.

Definition Sf_equal := 
  fun (A B : Type) (f : A -> B) (x y : A) (H : Seq x y) =>
    match H in (Seq _ y0) return (Seq (f x) (f y0)) with
    | Seq_refl _ => Seq_refl _
    end.

Definition Sf_equal2 (A : Type) (B : A -> Type)
         (a1 a2 : A) (b1 : B a1)
         (Ha : Seq a1 a2) :
    B a2 :=
  match Ha in (Seq _ x) return B x with
  | Seq_refl _ => b1
  end.

Definition SSf_equal2 (A : Type) (B : A -> SProp)
         (a1 a2 : A) (b1 : B a1)
         (Ha : Seq a1 a2) :
    B a2 :=
  match Ha in (Seq _ x) return B x with
  | Seq_refl _ => b1
  end.


Inductive sUnit : SProp := stt.

Print List.incl.

Notation "a <= b" := (List.incl a b).

Definition id {A : Type} (l : list A) : l <= l.
Proof.
  now unfold List.incl.
Defined.
Arguments id {A}.

Definition comp {A : Type} (l m n : list A) : l <= m -> m <= n -> l <= n.
Proof.
  now unfold List.incl ; auto.
Defined.

Definition Ctx (p : ℙ) :=  forall (q : ℙ), p <= q -> Type.

Definition Ctx_El (p : ℙ) (Γ : Ctx p) : Type :=
  forall (q : ℙ) h, Γ p h.

  
(*Definition Ty_aux (alpha : X -> Y) : Type :=
  (tsig (fun (A : ℙ -> Type) => ((forall p, A p) -> SProp))).*)

Definition Ty_aux (p : ℙ) : Type.
Proof.
  unshelve refine (ssig _).
  - unshelve refine (forall (alpha : ℙ) (h : p <= alpha), tsig _).
    + exact (forall (beta : ℙ) (i : alpha <= beta) , Type).
    + refine (fun A => _).
      refine ((forall (beta : ℙ) (i : alpha <= beta), A beta i) -> SProp).
  - intro A.
    exact (forall (alpha : ℙ) (h : p <= alpha),
          Seq ((A alpha h).(fst) alpha (id alpha)) ((A p (id p)).(fst) alpha h)).
Defined.

Definition Ty (p : ℙ) (Γ : Ctx p) : Type.
Proof.
  refine (forall (γ : Ctx_El Γ) (q : ℙ) (h : p <= q), Ty_aux q).
Defined.      


Definition Elt (p : ℙ) (Γ : Ctx p) (A : Ty Γ) : Type.
Proof.
  unshelve refine (forall  (γ : Ctx_El Γ), ssig _).
  - unfold Ty in A ; unfold Ty_aux in A ; simpl in A.
    refine (forall (q : ℙ) (h : p <= q),
               ((A γ p (id p)).(spr1) p (id p)).(fst) q h).
  - intro a.
    refine (forall (q : ℙ) (h : p <= q),
               ((A γ q h).(spr1) q (id q)).(snd) _).
    intros r i.
    unfold Ty in A ; unfold Ty_aux in A ; simpl in A.
    

    
    pose t:= ((A.(spr1) alpha (Γ.(snd) p alpha h γ)).(spr2) beta (i)).
    pose v := (Sf_equal fst (A.(spr2) p beta (comp h i) γ)).
    pose q:= (Rw (Sf_equal (fun f => f beta (id beta)) (Sf_equal fst (A.(spr2) p beta (comp h i) γ)))).
    pose u := ((A.(spr1) p γ).(spr2) beta (comp h i)).
    apply (Rw (Sf_equal (fun f => f beta i) (Sf_equal fst (A.(spr2) p alpha h γ)))).
    apply (Rw t).

    
Definition Elt_aux (p : ℙ) (A : Ty_aux p) : Type.
Proof.
  unshelve refine (ssig _).
  - unfold Ty_aux in A ; simpl in A.
    refine (forall (alpha : ℙ) (h : p <= alpha),
             ((A.(spr1) p (id p)).(fst) alpha h)).
  - intro a.
    unfold Ty_aux in A ; simpl in A.
    refine (forall (alpha : ℙ) (h : p <= alpha),
               (A.(spr1) p (id p)).(snd) _).
    intros beta i.
    pose t:= (A.(spr2) beta (i)).
    refine (fun beta i => Rw (A.(spr2) beta (i)) _).
Defined.                                           

Definition Elt (Γ : Ctx) (A : Ty Γ) : Type :=
  forall (γ : Γ), Elt_aux (A γ).


Definition U {Γ : Ctx} : Ty Γ.
Proof.
  unshelve refine (fun _ => Sexist _).
  - unshelve refine (fun alpha => exist _).
    + unshelve refine (fun beta => tsig _).
      * exact (forall (alpha : ℙ), Type).
      * exact (fun A => (forall beta, A beta) -> SProp).
    + cbv. intros  A.
      exact (forall (beta : ℙ),
          Seq ((A beta).(fst) beta) ((A alpha).(fst) beta)).
  - exact (fun _ _ => Seq_refl _).
Defined.

Definition SU {Γ : Ctx} : Ty Γ.
Proof.
  unshelve refine (fun _ => Sexist _).
  - unshelve refine (fun alpha => exist _).
    + unshelve refine (fun beta => tsig _).
      * exact (forall (alpha : ℙ), SProp).
      * exact (fun A => (forall beta, A beta) -> SProp).
    + cbv. intros  A.
      exact (forall (beta : ℙ),
          Seq ((A beta).(fst) beta) ((A alpha).(fst) beta)). 
  - exact (fun _ _ => Seq_refl _).
Defined.

Check (SU : @Elt (unit) U).

Definition SElt {Γ : Ctx} (A : @Elt Γ SU) : SProp.
Proof.
  unshelve refine (forall (γ : Γ), @Ssig _ _).
  - exact (forall (alpha : ℙ) ,
              (((A γ).(spr1) alpha).(fst) alpha)).
  - exact (fun a => forall (alpha : ℙ),
               ((A γ).(spr1) alpha).(snd)
                                      (fun beta => SRw ((A γ).(spr2) alpha beta) (a beta))).
Defined.       


(*Lemma ibvf : True.
Proof.
  pose q := @Elt (unit) U.
  cbv in q.
  pose v := @Ty (unit).
  cbv in v.
  Check (U : @Elt (unit) U).
*)  
Definition Ty_inst {Γ : Ctx} (A : Ty Γ)
           (γ : Γ) : Type.
Proof.
  unshelve refine (ssig _).
  - refine (forall α : ℙ, ((A γ).(spr1) α).(fst) α).
  - refine (fun a => forall α, ((A γ).(spr1) α).(snd) _).
    refine (fun beta => Rw ((A γ).(spr2) α beta) (a beta)).
Defined.


Definition Inst {Γ : Ctx} {A : Ty Γ} (a : Elt A)
           (γ : Γ) : Ty_inst A γ :=  (a γ).

Notation "r ® γ" := (@Inst _ _ r γ) (at level 10, left associativity).


Definition Ctx_nil : Ctx :=  unit.

Definition ctx_nil : Ctx_nil := tt.

Notation "()" := ctx_nil.


Definition Ctx_cons (Γ : Ctx) (R : Ty Γ) : Ctx.
Proof.
  unshelve refine (@tsig Γ _).
  unshelve refine (fun gamma => Ty_inst R gamma).
Defined.

Notation "Γ · A" := (@Ctx_cons Γ A) (at level 50, left associativity).


Definition ctx_cons {Γ : Ctx} {A : Ty Γ}
           (γ : Γ) (a : Ty_inst A γ) :
  (Γ · A).
Proof.
  exists γ.
  assumption.
Defined.

Notation "γ ·· xe" := (@ctx_cons _ _ γ xe)
                          (at level 50, left associativity).


Definition Ctx_tl {Γ : Ctx} {A : Ty Γ}
           (γ : (Γ · A)) : Γ.
Proof.
  exact (γ.(fst)).
Defined.


Notation "¿ g" := (Ctx_tl g) (at level 10).


Definition Ctx_hd {Γ : Ctx} {A : Ty Γ}
           (γ : (Γ · A)) : Ty_inst A (¿ γ).
Proof.
  exact γ.(snd).
Defined.


Definition Lift {Γ : Ctx} {A : Ty Γ}
           (B : Ty Γ) : Ty (Γ · A) :=
  fun γ => B (¿ γ).

Notation "⇑ A" := (@Lift _ _ A) (at level 10).


Definition Var {Γ : Ctx} (A : Ty Γ) :
  @Elt (Γ · A) (⇑ A).
Proof.
  unshelve refine  (fun γ => _).
  exact (γ.(snd)). 
Defined.


Notation "#" := (@Var _ _) (at level 0).


Definition Weaken {Γ : Ctx} {A B : Ty Γ}
           (r : @Elt Γ A) :
  @Elt (Γ · B) (⇑ A).
Proof.
  unshelve refine (fun γ => _).
  refine ((r (¿ γ))).
Defined.


Notation "↑ M" := (@Weaken _ _ _ M) (at level 10).

Definition Sub {Γ : Ctx} {A : Ty Γ}
           (B : Ty (Γ · A))
           (a : @Elt Γ A) :
           Ty Γ.
Proof.
  unshelve refine (fun γ => _). 
  refine (B (γ ·· (a ® γ))).
Defined.

Notation "S {{ r }} " := (@Sub _ _ S r) (at level 20, left associativity).


Definition Pi {Γ : Ctx} (A : Ty Γ)
           (B : Ty (Γ ·  A)) : Ty Γ.
Proof.
  unshelve refine (fun γ => Sexist _).
  unshelve refine (fun alpha => exist _).
  - refine (fun beta => forall (a : Ty_inst A γ),
               ((B (γ ·· a)).(spr1) beta).(fst) beta).
  - intro f.
    refine (forall  (a : Ty_inst A γ),
               ((B (γ ·· a)).(spr1) alpha).(snd) _).
    cbv in f.
    exact (fun beta => Rw ((B (γ ·· a)).(spr2) alpha beta) (f beta a)).
  - exact (fun _ _ => Seq_refl _).
Defined.

Definition SPi {Γ : Ctx} (A : Ty Γ)
           (B : @Elt (Γ ·  A) SU) : @Elt Γ SU.
Proof.
  unshelve refine (fun γ => Sexist _).
  unshelve refine (fun alpha => exist _).
  - refine (fun beta => forall (a : Ty_inst A γ),
               ((B (γ ·· a)).(spr1) beta).(fst) beta).
  - intro f.
    refine (forall  (a : Ty_inst A γ),
               ((B (γ ·· a)).(spr1) alpha).(snd) _).
    exact (fun beta => SRw ((B (γ ·· a)).(spr2) alpha beta) (f beta a)).
  - exact (fun _ _ => Seq_refl _).
Defined.


Notation "∏" := Pi.

Notation "A → B" := (@Pi _ A (@Lift _ A B)) (at level 99, right associativity, B at level 200).


Definition Lam {Γ : Ctx} {A : Ty Γ}
           (B : Ty (Γ · A))
           (f : @Elt (Γ · A) B)
  : @Elt Γ (Pi B).
Proof.
  unshelve refine (fun γ => Sexist _).
  - refine (fun α a => ((f (γ ·· a)).(spr1) α)).
  - refine (fun beta a => (f (γ ·· a)).(spr2) beta).
Defined.

Notation "'λ'" := Lam.


Definition App {Γ : Ctx} {A : Ty Γ}
           {B : Ty (Γ · A)}
           (f : @Elt Γ (Pi B))
           (x : Elt A) :
  @Elt Γ (B {{ x }}).
Proof.
  unshelve refine (fun γ => Sexist _).
  - refine (fun α => (f γ).(spr1) α (x γ)).
  - refine (fun alpha => (f γ).(spr2) alpha (x γ)).
Defined.

Notation "t • u" := (App t u) (at level 12, left associativity).

Definition AppN {Γ : Ctx} {A B : Ty Γ}
           (f : @Elt Γ (A → B))
           (x : Elt A) :
  @Elt Γ B :=
  @App Γ A (⇑ B) f x.

Notation "t ◊ u" := (AppN t u) (at level 12, left associativity).


Definition EL {Γ : Ctx} (A : @Elt Γ U) : Ty Γ := A.

Notation "[ A ]" := (EL A).

Inductive BranchingBool :=
| Btrue_aux : BranchingBool
| Bfalse_aux : BranchingBool
| Bêtabool : (Y -> BranchingBool) -> X -> BranchingBool.

Fixpoint BoolDialogue (alpha : X -> Y) (b : BranchingBool) : bool :=
  match b with
  | Btrue_aux => true
  | Bfalse_aux => false
  | Bêtabool f x => BoolDialogue alpha (f (alpha x))
  end.


Inductive EParabool :
  ((X -> Y) -> bool) -> SProp :=
| EParatrue : EParabool (fun _ => true)
| EParafalse : EParabool (fun _ => false).
(*| EParabêtabool : forall  (bo : (X -> Y) -> bool) (bb : BranchingBool),
    (forall α, bo α = BoolDialogue α bb) ->
    EParabool bo.*)



Definition Bool {Γ : Ctx} : Ty Γ.
Proof.
  cbv.
  unshelve refine (fun gamma => Sexist _).
  - unshelve refine (fun alpha => exist _).
    + exact (fun _ => bool).
    + simpl.
      exact (EParabool).
  - exact (fun _ _ => Seq_refl _).
Defined.


Definition Etrue {Gamma : Ctx} : Elt (@Bool Gamma).
Proof.
  unshelve refine (fun gamma => Sexist _).
  - exact (fun alpha => true).
  - exact (fun _ => EParatrue).
Defined.


Definition Efalse {Gamma : Ctx} : Elt (@Bool Gamma).
Proof.
  unshelve refine (fun gamma => Sexist _).
  - exact (fun alpha => false).
  - exact (fun _ => EParafalse).
Defined.

Lemma Bool_rect_aux : forall (alpha : X -> Y)
                    (P : forall (bo : (X -> Y) -> bool) (be : EParabool bo), Type)
                    (ptrue : P (fun _ => true) (EParatrue))
                    (pfalse : P (fun _ => false) (EParafalse))
                    (bo : (X -> Y) -> bool) (be : EParabool bo),
    P bo be.
Proof.
  intros.
  case_eq (bo alpha).
  + unshelve refine (fun eq => @Sf_equal2 ((X ->Y) -> bool)
                                          (fun c => forall ce : EParabool c, P c ce)
                                          (fun _ => true) bo (fun _ => ptrue) _ be).
    refine (match be as be0 in EParabool f0 return
                  f0 alpha = true -> Seq (fun _ => true) f0
              with
              | EParatrue => fun _ => Seq_refl _
              | EParafalse => fun eq => _
            end eq).
    exfalso.
    now apply Bool.diff_true_false.
  + unshelve refine (fun eq => @Sf_equal2 ((X ->Y) -> bool)
                                          (fun c => forall ce : EParabool c, P c ce)
                                          (fun _ => false) bo (fun _ => pfalse) _ be).
    refine (match be as be0 in EParabool f0 return
                  f0 alpha = false -> Seq (fun _ => false) f0
            with
            | EParafalse => fun _ => Seq_refl _
            | EParatrue => fun eq => _
            end eq).
    exfalso.
    now apply Bool.diff_true_false.
Defined.

Lemma SBool_rect_aux : forall (alpha : X -> Y)
                    (P : forall (bo : (X -> Y) -> bool) (be : EParabool bo), SProp)
                    (ptrue : P (fun _ => true) (EParatrue))
                    (pfalse : P (fun _ => false) (EParafalse))
                    (bo : (X -> Y) -> bool) (be : EParabool bo),
    P bo be.
Proof.
  intros.
  case_eq (bo alpha).
  + unshelve refine (fun eq => @SSf_equal2 ((X ->Y) -> bool)
                                          (fun c => forall ce : EParabool c, P c ce)
                                          (fun _ => true) bo (fun _ => ptrue) _ be).
    refine (match be as be0 in EParabool f0 return
                  f0 alpha = true -> Seq (fun _ => true) f0
              with
              | EParatrue => fun _ => Seq_refl _
              | EParafalse => fun eq => _
            end eq).
    exfalso.
    now apply Bool.diff_true_false.
  + unshelve refine (fun eq => @SSf_equal2 ((X ->Y) -> bool)
                                          (fun c => forall ce : EParabool c, P c ce)
                                          (fun _ => false) bo (fun _ => pfalse) _ be).
    refine (match be as be0 in EParabool f0 return
                  f0 alpha = false -> Seq (fun _ => false) f0
            with
            | EParafalse => fun _ => Seq_refl _
            | EParatrue => fun eq => _
            end eq).
    exfalso.
    now apply Bool.diff_true_false.
Defined.

Definition Bool_rect (Γ : Ctx) :
  @Elt Γ (@Pi Γ ((Bool → U)) (
                   [@App (Γ · (Bool → U)) Bool U (Var (A:=Bool → U)) Etrue] →
                   [@App (Γ · (Bool → U)) Bool U (Var (A:=Bool → U)) Efalse] →
                   (@Pi (Γ · (Bool → U)) Bool
                        [@App _ _ U (Weaken (Var (A:=Bool → U)))
                              (Var (A := Bool))]
                         ))).
Proof.
  - unshelve refine (fun gamma => Sexist _).
    + unshelve refine (fun alpha P ptrue pfalse b => _).
      destruct b as [bo be].
      change be with (fun (_ : X-> Y) => be alpha).
      simpl in bo.
      simpl in be.
      set be' := be alpha.
      clearbody be'.
      clear be.
      simpl.
      destruct P as [Po Pe].
      cbv in Po.
      cbv.
      exact (@Bool_rect_aux alpha (fun co ce => (Po alpha {| spr1 := co;
                                                  spr2 := fun _ : X -> Y => ce |}).(fst)
                                                                                     alpha)
                 (ptrue.(spr1) alpha)
                 (pfalse.(spr1) alpha)
                 bo
                 be').
    + unshelve refine (fun alpha P ptrue pfalse b => _).
      destruct b as [bo be].
      change be with (fun (_ : X-> Y) => be alpha).
      set be' := be alpha.
      clearbody be'.
      clear be.
      exact (@SBool_rect_aux alpha
                            (fun bo be' => snd (spr1 P alpha {| spr1 := bo; spr2 := fun _ : X -> Y => be' |})
    (fun beta : X -> Y =>
     Rw (spr2 P alpha {| spr1 := bo; spr2 := fun _ : X -> Y => be' |} beta)
       (Bool_rect_aux beta (spr1 ptrue beta) (spr1 pfalse beta) be')))
                            (ptrue.(spr2) alpha) (pfalse.(spr2) alpha) bo
                            be').
Defined.


Definition BNdialogue := @BNdialogue X Y.


Inductive EParanat : ((X -> Y) -> nat) -> SProp :=
| EParabêtanat : forall (no : (X -> Y) -> nat) (nb : BranchingNat X Y)
                        (rel : forall α : X -> Y, Seq (no α) (BNdialogue nb α)),
    EParanat no.


Definition Nat {Γ : Ctx} : Ty Γ.
Proof.
  unshelve refine (fun gamma => Sexist _).
  - unshelve refine (fun alpha => exist _).
    + exact (fun _ => nat).
    + exact (EParanat).
  - exact (fun _ _ => Seq_refl _).
Defined.

Definition Z {Gamma : Ctx} : Elt (@Nat Gamma).
Proof.
  unshelve refine (fun gamma => Sexist _).
  - exact (fun α => 0).
  - unshelve refine (fun α => EParabêtanat _).
    + exact (B0 _ _).
    + exact (fun _ => Seq_refl _).
Defined.

Definition Succ {Gamma : Ctx} : Elt ((@Nat Gamma) → Nat).
Proof.
  unshelve refine (fun gamma => Sexist _).
  - exact (fun α n => S (n.(spr1) α)). 
  - intros α n.
    refine (match (n.(spr2) α) as q0 in EParanat f1
                  return EParanat (fun beta => S (f1 beta))
                                     with
                                  | @EParabêtanat mo mb rel => _
                                   end).
    unshelve refine (EParabêtanat _).
    + exact (BS mb).
    + intro beta.
      simpl.
      refine (match (rel beta) as eq0 in Seq _ dmb
                    return Seq (S (mo beta)) (S dmb)
              with
              | Seq_refl _ => Seq_refl _
              end).
Defined.  

(*Definition Nat_rect (Γ : Ctx) :
  @Elt Γ (@Pi Γ ((Nat → U)) (
                   [@App (Γ · (Nat → U))  Nat  U  #  Z] →
                   (@Pi (Γ · (Nat → U)) Nat
                         ([@App (Γ · (Nat → U) · Nat)  _ U
                                 (↑#)  #] →
                          [@App (Γ · (Nat → U) · Nat)  _ U
                                 (↑#)  (App Succ #)])) →
                   (@Pi (Γ · (Nat → U)) Nat
                        [@App (Γ · (Nat → U) · Nat) _ U
                              (Weaken (Var (A:=Nat → U)))
                              #]))).
Proof.
  unshelve refine (fun gamma => Sexist _).
  - unshelve refine (fun alpha P p0 pS n => _).
    cbv.
*)

Inductive EParalist (A : Ty_aux)
  : ((X -> Y) -> list (Elt_aux A)) -> SProp :=
| EParanil : @EParalist A (fun _ => nil)
| EParacons : forall (l : forall (alpha : X -> Y), list (Elt_aux A))
                     (le : EParalist l)
                     (a : Elt_aux A),
    EParalist (fun alpha => (cons a (l alpha))).

Definition List {Γ : Ctx} : Elt ((@U Γ) → U).
Proof.
  unshelve refine (fun gamma => Sexist _).
  - unshelve refine (fun alpha A => exist _).
    + exact (fun beta => list (Elt_aux A)).
    + exact (@EParalist A).
  -intros alpha A beta.
   reflexivity.
Defined.    



Lemma Arr_aux (A B : Ty_aux) : Ty_aux.
Proof.
  unshelve refine (Sexist _).
  - unshelve refine (fun alpha => exist _).
    + refine (fun beta => Elt_aux A -> fst (spr1 B beta) beta).
    + simpl.
      refine (fun f => forall a : Elt_aux A,
                  snd (spr1 B alpha)
                      (fun beta : X -> Y => Rw (spr2 B alpha beta) (f beta a))).
  - reflexivity.
Defined.

Definition App_aux (A B : Ty_aux) (f :Elt_aux (Arr_aux A B))
           (a : Elt_aux A) : Elt_aux B.
Proof.
  unshelve refine (Sexist _).
  - exact (fun alpha => f.(spr1) alpha a).
  - exact (fun alpha => f.(spr2) alpha a).
Defined.    
  

Inductive Seqfin {A B : Ty_aux} :
  Elt_aux (Arr_aux A B) -> list (Elt_aux A) ->
  Elt_aux (Arr_aux A B) -> Type :=
  | Seqnil : forall (f g : Elt_aux (Arr_aux A B)), Seqfin f nil g
  | Seqcons : forall (f g : Elt_aux (Arr_aux A B))
                     (l : list (Elt_aux A)) (a : Elt_aux A),
      App_aux f a = App_aux g a -> Seqfin f l g -> Seqfin f (a :: l) g.



Inductive ParaSeqfin {A B : Ty_aux} :
  forall (f : Elt_aux (Arr_aux A B)) (l : list (Elt_aux A))
         (g : Elt_aux (Arr_aux A B)),
  ((X -> Y) -> @Seqfin A B f l g) -> SProp :=
| ParaSeqnil :
    forall f g : Elt_aux (Arr_aux A B),
      ParaSeqfin (fun _ => Seqnil f g)
| ParaSeqcons : forall (f g : Elt_aux (Arr_aux A B))
                       (l : list (Elt_aux A))
                       (a : Elt_aux A)
                       (rel : App_aux f a = App_aux g a)
                       (eqo : (X -> Y) -> Seqfin f l g),
    ParaSeqfin eqo ->
    ParaSeqfin (fun alpha => @Seqcons _ _ f g l a rel (eqo alpha)).

End Translation3.*)


Section Translation2.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Variables (X Y : Type).
Definition ℙ := X -> Y.

Record ssig (A:Type) (P: A -> SProp) : Type := Sexist { spr1 : A; spr2 : P spr1 }.

Record Ssig (A : SProp) (P: A -> SProp) : SProp := Sexist2 { Spr1 : A; Spr2 : P Spr1 }.


Arguments spr1 {_ _} _.
Arguments spr2 {_ _} _.
Set Definitional UIP.
Inductive Seq (A : Type) (x : A) : A -> SProp :=  Seq_refl : Seq x x.

Definition Rw (A B : Type) : Seq A B -> A -> B.
Proof.
  intros eq a.
  refine (match eq as eq0 in Seq _ B
          with | Seq_refl _ => a end).
Defined.

Definition SRw (A B : SProp) : Seq A B -> A -> B.
Proof.
  intros eq a.
  refine (match eq as eq0 in Seq _ B
          with | Seq_refl _ => a end).
Defined.

Definition Sf_equal := 
  fun (A B : Type) (f : A -> B) (x y : A) (H : Seq x y) =>
    match H in (Seq _ y0) return (Seq (f x) (f y0)) with
    | Seq_refl _ => Seq_refl _
    end.

Definition Sf_equal2 (A : Type) (B : A -> Type)
         (a1 a2 : A) (b1 : B a1)
         (Ha : Seq a1 a2) :
    B a2 :=
  match Ha in (Seq _ x) return B x with
  | Seq_refl _ => b1
  end.

Definition SSf_equal2 (A : Type) (B : A -> SProp)
         (a1 a2 : A) (b1 : B a1)
         (Ha : Seq a1 a2) :
    B a2 :=
  match Ha in (Seq _ x) return B x with
  | Seq_refl _ => b1
  end.



Definition Ctx := Type.

Inductive sUnit : SProp := stt.



(*Definition Ty_aux (alpha : X -> Y) : Type :=
  (tsig (fun (A : ℙ -> Type) => ((forall p, A p) -> SProp))).*)

Definition Ty_aux : Type.
Proof.
  unshelve refine (ssig _).
  - unshelve refine (forall (alpha : X -> Y), tsig _).
    + exact (forall (beta : X -> Y), Type).
    + exact (fun A =>
                 (forall beta, A beta) -> SProp).
  - intro A.
    exact (forall (alpha : X -> Y) (beta : X -> Y),
          Seq ((A beta).(fst) beta) ((A alpha).(fst) beta)).
Defined.

Definition Ty (Γ : Ctx) : Type := forall (γ : Γ), Ty_aux.

Definition Elt_aux (A : Ty_aux) : Type.
Proof.
  unshelve refine (ssig _).
  - cbv in A.
    refine (forall (alpha : X -> Y) ,
             ((A.(spr1) alpha).(fst) alpha)).
  - intro a.
    cbv in A.
    refine (forall (alpha : X -> Y),
               (A.(spr1) alpha).(snd) _).
    refine (fun beta => Rw (A.(spr2) alpha beta) (a beta)).
Defined.                                           

Definition Elt (Γ : Ctx) (A : Ty Γ) : Type :=
  forall (γ : Γ), Elt_aux (A γ).


Definition U {Γ : Ctx} : Ty Γ.
Proof.
  unshelve refine (fun _ => Sexist _).
  - unshelve refine (fun alpha => exist _).
    + unshelve refine (fun beta => tsig _).
      * exact (forall (alpha : X -> Y), Type).
      * exact (fun A => (forall beta, A beta) -> SProp).
    + cbv. intros  A.
      exact (forall (beta : X -> Y),
          Seq ((A beta).(fst) beta) ((A alpha).(fst) beta)).
  - exact (fun _ _ => Seq_refl _).
Defined.

Definition SU {Γ : Ctx} : Ty Γ.
Proof.
  unshelve refine (fun _ => Sexist _).
  - unshelve refine (fun alpha => exist _).
    + unshelve refine (fun beta => tsig _).
      * exact (forall (alpha : X -> Y), SProp).
      * exact (fun A => (forall beta, A beta) -> SProp).
    + cbv. intros  A.
      exact (forall (beta : X -> Y),
          Seq ((A beta).(fst) beta) ((A alpha).(fst) beta)). 
  - exact (fun _ _ => Seq_refl _).
Defined.

Check (SU : @Elt (unit) U).

Definition SElt {Γ : Ctx} (A : @Elt Γ SU) : SProp.
Proof.
  unshelve refine (forall (γ : Γ), @Ssig _ _).
  - exact (forall (alpha : X -> Y) ,
              (((A γ).(spr1) alpha).(fst) alpha)).
  - exact (fun a => forall (alpha : X -> Y),
               ((A γ).(spr1) alpha).(snd)
                                      (fun beta => SRw ((A γ).(spr2) alpha beta) (a beta))).
Defined.       


(*Lemma ibvf : True.
Proof.
  pose q := @Elt (unit) U.
  cbv in q.
  pose v := @Ty (unit).
  cbv in v.
  Check (U : @Elt (unit) U).
*)  
Definition Ty_inst {Γ : Ctx} (A : Ty Γ)
           (γ : Γ) : Type.
Proof.
  unshelve refine (ssig _).
  - refine (forall α : X -> Y, ((A γ).(spr1) α).(fst) α).
  - refine (fun a => forall α, ((A γ).(spr1) α).(snd) _).
    refine (fun beta => Rw ((A γ).(spr2) α beta) (a beta)).
Defined.


Definition Inst {Γ : Ctx} {A : Ty Γ} (a : Elt A)
           (γ : Γ) : Ty_inst A γ :=  (a γ).

Notation "r ® γ" := (@Inst _ _ r γ) (at level 10, left associativity).


Definition Ctx_nil : Ctx :=  unit.

Definition ctx_nil : Ctx_nil := tt.

Notation "()" := ctx_nil.


Definition Ctx_cons (Γ : Ctx) (R : Ty Γ) : Ctx.
Proof.
  unshelve refine (@tsig Γ _).
  unshelve refine (fun gamma => Ty_inst R gamma).
Defined.

Notation "Γ · A" := (@Ctx_cons Γ A) (at level 50, left associativity).


Definition ctx_cons {Γ : Ctx} {A : Ty Γ}
           (γ : Γ) (a : Ty_inst A γ) :
  (Γ · A).
Proof.
  exists γ.
  assumption.
Defined.

Notation "γ ·· xe" := (@ctx_cons _ _ γ xe)
                          (at level 50, left associativity).


Definition Ctx_tl {Γ : Ctx} {A : Ty Γ}
           (γ : (Γ · A)) : Γ.
Proof.
  exact (γ.(fst)).
Defined.


Notation "¿ g" := (Ctx_tl g) (at level 10).


Definition Ctx_hd {Γ : Ctx} {A : Ty Γ}
           (γ : (Γ · A)) : Ty_inst A (¿ γ).
Proof.
  exact γ.(snd).
Defined.


Definition Lift {Γ : Ctx} {A : Ty Γ}
           (B : Ty Γ) : Ty (Γ · A) :=
  fun γ => B (¿ γ).

Notation "⇑ A" := (@Lift _ _ A) (at level 10).


Definition Var {Γ : Ctx} (A : Ty Γ) :
  @Elt (Γ · A) (⇑ A).
Proof.
  unshelve refine  (fun γ => _).
  exact (γ.(snd)). 
Defined.


Notation "#" := (@Var _ _) (at level 0).


Definition Weaken {Γ : Ctx} {A B : Ty Γ}
           (r : @Elt Γ A) :
  @Elt (Γ · B) (⇑ A).
Proof.
  unshelve refine (fun γ => _).
  refine ((r (¿ γ))).
Defined.


Notation "↑ M" := (@Weaken _ _ _ M) (at level 10).

Definition Sub {Γ : Ctx} {A : Ty Γ}
           (B : Ty (Γ · A))
           (a : @Elt Γ A) :
           Ty Γ.
Proof.
  unshelve refine (fun γ => _). 
  refine (B (γ ·· (a ® γ))).
Defined.

Notation "S {{ r }} " := (@Sub _ _ S r) (at level 20, left associativity).


Definition Pi {Γ : Ctx} (A : Ty Γ)
           (B : Ty (Γ ·  A)) : Ty Γ.
Proof.
  unshelve refine (fun γ => Sexist _).
  unshelve refine (fun alpha => exist _).
  - refine (fun beta => forall (a : Ty_inst A γ),
               ((B (γ ·· a)).(spr1) beta).(fst) beta).
  - intro f.
    refine (forall  (a : Ty_inst A γ),
               ((B (γ ·· a)).(spr1) alpha).(snd) _).
    cbv in f.
    exact (fun beta => Rw ((B (γ ·· a)).(spr2) alpha beta) (f beta a)).
  - exact (fun _ _ => Seq_refl _).
Defined.

Definition SPi {Γ : Ctx} (A : Ty Γ)
           (B : @Elt (Γ ·  A) SU) : @Elt Γ SU.
Proof.
  unshelve refine (fun γ => Sexist _).
  unshelve refine (fun alpha => exist _).
  - refine (fun beta => forall (a : Ty_inst A γ),
               ((B (γ ·· a)).(spr1) beta).(fst) beta).
  - intro f.
    refine (forall  (a : Ty_inst A γ),
               ((B (γ ·· a)).(spr1) alpha).(snd) _).
    exact (fun beta => SRw ((B (γ ·· a)).(spr2) alpha beta) (f beta a)).
  - exact (fun _ _ => Seq_refl _).
Defined.


Notation "∏" := Pi.

Notation "A → B" := (@Pi _ A (@Lift _ A B)) (at level 99, right associativity, B at level 200).


Definition Lam {Γ : Ctx} {A : Ty Γ}
           (B : Ty (Γ · A))
           (f : @Elt (Γ · A) B)
  : @Elt Γ (Pi B).
Proof.
  unshelve refine (fun γ => Sexist _).
  - refine (fun α a => ((f (γ ·· a)).(spr1) α)).
  - refine (fun beta a => (f (γ ·· a)).(spr2) beta).
Defined.

Notation "'λ'" := Lam.


Definition App {Γ : Ctx} {A : Ty Γ}
           {B : Ty (Γ · A)}
           (f : @Elt Γ (Pi B))
           (x : Elt A) :
  @Elt Γ (B {{ x }}).
Proof.
  unshelve refine (fun γ => Sexist _).
  - refine (fun α => (f γ).(spr1) α (x γ)).
  - refine (fun alpha => (f γ).(spr2) alpha (x γ)).
Defined.

Notation "t • u" := (App t u) (at level 12, left associativity).

Definition AppN {Γ : Ctx} {A B : Ty Γ}
           (f : @Elt Γ (A → B))
           (x : Elt A) :
  @Elt Γ B :=
  @App Γ A (⇑ B) f x.

Notation "t ◊ u" := (AppN t u) (at level 12, left associativity).


Definition EL {Γ : Ctx} (A : @Elt Γ U) : Ty Γ := A.

Notation "[ A ]" := (EL A).

Inductive BranchingBool :=
| Btrue_aux : BranchingBool
| Bfalse_aux : BranchingBool
| Bêtabool : (Y -> BranchingBool) -> X -> BranchingBool.

Fixpoint BoolDialogue (alpha : X -> Y) (b : BranchingBool) : bool :=
  match b with
  | Btrue_aux => true
  | Bfalse_aux => false
  | Bêtabool f x => BoolDialogue alpha (f (alpha x))
  end.


Inductive EParabool :
  ((X -> Y) -> bool) -> SProp :=
| EParatrue : EParabool (fun _ => true)
| EParafalse : EParabool (fun _ => false).
(*| EParabêtabool : forall  (bo : (X -> Y) -> bool) (bb : BranchingBool),
    (forall α, bo α = BoolDialogue α bb) ->
    EParabool bo.*)



Definition Bool {Γ : Ctx} : Ty Γ.
Proof.
  cbv.
  unshelve refine (fun gamma => Sexist _).
  - unshelve refine (fun alpha => exist _).
    + exact (fun _ => bool).
    + simpl.
      exact (EParabool).
  - exact (fun _ _ => Seq_refl _).
Defined.


Definition Etrue {Gamma : Ctx} : Elt (@Bool Gamma).
Proof.
  unshelve refine (fun gamma => Sexist _).
  - exact (fun alpha => true).
  - exact (fun _ => EParatrue).
Defined.


Definition Efalse {Gamma : Ctx} : Elt (@Bool Gamma).
Proof.
  unshelve refine (fun gamma => Sexist _).
  - exact (fun alpha => false).
  - exact (fun _ => EParafalse).
Defined.

Lemma Bool_rect_aux : forall (alpha : X -> Y)
                    (P : forall (bo : (X -> Y) -> bool) (be : EParabool bo), Type)
                    (ptrue : P (fun _ => true) (EParatrue))
                    (pfalse : P (fun _ => false) (EParafalse))
                    (bo : (X -> Y) -> bool) (be : EParabool bo),
    P bo be.
Proof.
  intros.
  case_eq (bo alpha).
  + unshelve refine (fun eq => @Sf_equal2 ((X ->Y) -> bool)
                                          (fun c => forall ce : EParabool c, P c ce)
                                          (fun _ => true) bo (fun _ => ptrue) _ be).
    refine (match be as be0 in EParabool f0 return
                  f0 alpha = true -> Seq (fun _ => true) f0
              with
              | EParatrue => fun _ => Seq_refl _
              | EParafalse => fun eq => _
            end eq).
    exfalso.
    now apply Bool.diff_true_false.
  + unshelve refine (fun eq => @Sf_equal2 ((X ->Y) -> bool)
                                          (fun c => forall ce : EParabool c, P c ce)
                                          (fun _ => false) bo (fun _ => pfalse) _ be).
    refine (match be as be0 in EParabool f0 return
                  f0 alpha = false -> Seq (fun _ => false) f0
            with
            | EParafalse => fun _ => Seq_refl _
            | EParatrue => fun eq => _
            end eq).
    exfalso.
    now apply Bool.diff_true_false.
Defined.

Lemma SBool_rect_aux : forall (alpha : X -> Y)
                    (P : forall (bo : (X -> Y) -> bool) (be : EParabool bo), SProp)
                    (ptrue : P (fun _ => true) (EParatrue))
                    (pfalse : P (fun _ => false) (EParafalse))
                    (bo : (X -> Y) -> bool) (be : EParabool bo),
    P bo be.
Proof.
  intros.
  case_eq (bo alpha).
  + unshelve refine (fun eq => @SSf_equal2 ((X ->Y) -> bool)
                                          (fun c => forall ce : EParabool c, P c ce)
                                          (fun _ => true) bo (fun _ => ptrue) _ be).
    refine (match be as be0 in EParabool f0 return
                  f0 alpha = true -> Seq (fun _ => true) f0
              with
              | EParatrue => fun _ => Seq_refl _
              | EParafalse => fun eq => _
            end eq).
    exfalso.
    now apply Bool.diff_true_false.
  + unshelve refine (fun eq => @SSf_equal2 ((X ->Y) -> bool)
                                          (fun c => forall ce : EParabool c, P c ce)
                                          (fun _ => false) bo (fun _ => pfalse) _ be).
    refine (match be as be0 in EParabool f0 return
                  f0 alpha = false -> Seq (fun _ => false) f0
            with
            | EParafalse => fun _ => Seq_refl _
            | EParatrue => fun eq => _
            end eq).
    exfalso.
    now apply Bool.diff_true_false.
Defined.

Definition Bool_rect (Γ : Ctx) :
  @Elt Γ (@Pi Γ ((Bool → U)) (
                   [@App (Γ · (Bool → U)) Bool U (Var (A:=Bool → U)) Etrue] →
                   [@App (Γ · (Bool → U)) Bool U (Var (A:=Bool → U)) Efalse] →
                   (@Pi (Γ · (Bool → U)) Bool
                        [@App _ _ U (Weaken (Var (A:=Bool → U)))
                              (Var (A := Bool))]
                         ))).
Proof.
  - unshelve refine (fun gamma => Sexist _).
    + unshelve refine (fun alpha P ptrue pfalse b => _).
      destruct b as [bo be].
      change be with (fun (_ : X-> Y) => be alpha).
      simpl in bo.
      simpl in be.
      set be' := be alpha.
      clearbody be'.
      clear be.
      simpl.
      destruct P as [Po Pe].
      cbv in Po.
      cbv.
      exact (@Bool_rect_aux alpha (fun co ce => (Po alpha {| spr1 := co;
                                                  spr2 := fun _ : X -> Y => ce |}).(fst)
                                                                                     alpha)
                 (ptrue.(spr1) alpha)
                 (pfalse.(spr1) alpha)
                 bo
                 be').
    + unshelve refine (fun alpha P ptrue pfalse b => _).
      destruct b as [bo be].
      change be with (fun (_ : X-> Y) => be alpha).
      set be' := be alpha.
      clearbody be'.
      clear be.
      exact (@SBool_rect_aux alpha
                            (fun bo be' => snd (spr1 P alpha {| spr1 := bo; spr2 := fun _ : X -> Y => be' |})
    (fun beta : X -> Y =>
     Rw (spr2 P alpha {| spr1 := bo; spr2 := fun _ : X -> Y => be' |} beta)
       (Bool_rect_aux beta (spr1 ptrue beta) (spr1 pfalse beta) be')))
                            (ptrue.(spr2) alpha) (pfalse.(spr2) alpha) bo
                            be').
Defined.


Definition BNdialogue := @BNdialogue X Y.


Inductive EParanat : ((X -> Y) -> nat) -> SProp :=
| EParabêtanat : forall (no : (X -> Y) -> nat) (nb : BranchingNat X Y)
                        (rel : forall α : X -> Y, Seq (no α) (BNdialogue nb α)),
    EParanat no.


Definition Nat {Γ : Ctx} : Ty Γ.
Proof.
  unshelve refine (fun gamma => Sexist _).
  - unshelve refine (fun alpha => exist _).
    + exact (fun _ => nat).
    + exact (EParanat).
  - exact (fun _ _ => Seq_refl _).
Defined.

Definition Z {Gamma : Ctx} : Elt (@Nat Gamma).
Proof.
  unshelve refine (fun gamma => Sexist _).
  - exact (fun α => 0).
  - unshelve refine (fun α => EParabêtanat _).
    + exact (B0 _ _).
    + exact (fun _ => Seq_refl _).
Defined.

Definition Succ {Gamma : Ctx} : Elt ((@Nat Gamma) → Nat).
Proof.
  unshelve refine (fun gamma => Sexist _).
  - exact (fun α n => S (n.(spr1) α)). 
  - intros α n.
    refine (match (n.(spr2) α) as q0 in EParanat f1
                  return EParanat (fun beta => S (f1 beta))
                                     with
                                  | @EParabêtanat mo mb rel => _
                                   end).
    unshelve refine (EParabêtanat _).
    + exact (BS mb).
    + intro beta.
      simpl.
      refine (match (rel beta) as eq0 in Seq _ dmb
                    return Seq (S (mo beta)) (S dmb)
              with
              | Seq_refl _ => Seq_refl _
              end).
Defined.  

(*Definition Nat_rect (Γ : Ctx) :
  @Elt Γ (@Pi Γ ((Nat → U)) (
                   [@App (Γ · (Nat → U))  Nat  U  #  Z] →
                   (@Pi (Γ · (Nat → U)) Nat
                         ([@App (Γ · (Nat → U) · Nat)  _ U
                                 (↑#)  #] →
                          [@App (Γ · (Nat → U) · Nat)  _ U
                                 (↑#)  (App Succ #)])) →
                   (@Pi (Γ · (Nat → U)) Nat
                        [@App (Γ · (Nat → U) · Nat) _ U
                              (Weaken (Var (A:=Nat → U)))
                              #]))).
Proof.
  unshelve refine (fun gamma => Sexist _).
  - unshelve refine (fun alpha P p0 pS n => _).
    cbv.
*)

Inductive EParalist (A : Ty_aux)
  : ((X -> Y) -> list (Elt_aux A)) -> SProp :=
| EParanil : @EParalist A (fun _ => nil)
| EParacons : forall (l : forall (alpha : X -> Y), list (Elt_aux A))
                     (le : EParalist l)
                     (a : Elt_aux A),
    EParalist (fun alpha => (cons a (l alpha))).

Definition List {Γ : Ctx} : Elt ((@U Γ) → U).
Proof.
  unshelve refine (fun gamma => Sexist _).
  - unshelve refine (fun alpha A => exist _).
    + exact (fun beta => list (Elt_aux A)).
    + exact (@EParalist A).
  -intros alpha A beta.
   reflexivity.
Defined.    



Lemma Arr_aux (A B : Ty_aux) : Ty_aux.
Proof.
  unshelve refine (Sexist _).
  - unshelve refine (fun alpha => exist _).
    + refine (fun beta => Elt_aux A -> fst (spr1 B beta) beta).
    + simpl.
      refine (fun f => forall a : Elt_aux A,
                  snd (spr1 B alpha)
                      (fun beta : X -> Y => Rw (spr2 B alpha beta) (f beta a))).
  - reflexivity.
Defined.

Definition App_aux (A B : Ty_aux) (f :Elt_aux (Arr_aux A B))
           (a : Elt_aux A) : Elt_aux B.
Proof.
  unshelve refine (Sexist _).
  - exact (fun alpha => f.(spr1) alpha a).
  - exact (fun alpha => f.(spr2) alpha a).
Defined.    
  

Inductive Seqfin {A B : Ty_aux} :
  Elt_aux (Arr_aux A B) -> list (Elt_aux A) ->
  Elt_aux (Arr_aux A B) -> Type :=
  | Seqnil : forall (f g : Elt_aux (Arr_aux A B)), Seqfin f nil g
  | Seqcons : forall (f g : Elt_aux (Arr_aux A B))
                     (l : list (Elt_aux A)) (a : Elt_aux A),
      App_aux f a = App_aux g a -> Seqfin f l g -> Seqfin f (a :: l) g.



Inductive ParaSeqfin {A B : Ty_aux} :
  forall (f : Elt_aux (Arr_aux A B)) (l : list (Elt_aux A))
         (g : Elt_aux (Arr_aux A B)),
  ((X -> Y) -> @Seqfin A B f l g) -> SProp :=
| ParaSeqnil :
    forall f g : Elt_aux (Arr_aux A B),
      ParaSeqfin (fun _ => Seqnil f g)
| ParaSeqcons : forall (f g : Elt_aux (Arr_aux A B))
                       (l : list (Elt_aux A))
                       (a : Elt_aux A)
                       (rel : App_aux f a = App_aux g a)
                       (eqo : (X -> Y) -> Seqfin f l g),
    ParaSeqfin eqo ->
    ParaSeqfin (fun alpha => @Seqcons _ _ f g l a rel (eqo alpha)).


Definition SEqFin {Γ : Ctx} :
  @Elt Γ (@Pi Γ U
              (@Pi _ U
                   (((Weaken (Var (A:= U))) → (Var (A:= U))) →
                    [App List (Weaken (Var (A:= U)))] →
                    ((Weaken (Var (A:= U))) → (Var (A:= U))) →
         U))).
Proof.
  unshelve refine (fun gamma => Sexist _).
  - unshelve refine (fun alpha A B f l g => exist _).
    + intro beta.
      exact (@Seqfin A B f (l.(spr1) alpha) g).
    + exact (@ParaSeqfin _ _ f (l.(spr1) alpha) g).
  - simpl ; intros alpha A B f l g beta.
    unshelve refine ((fun (eq : Seq (l.(spr1) beta) (l.(spr1) alpha)) => _) _).
    + exact (match eq as eq0 in Seq _ l0 return
                   Seq _ (Seqfin f l0 g)
             with
             | Seq_refl _ => Seq_refl _
             end).
    + exact ((fix aux (qo : (X -> Y) -> list (Elt_aux A))
                  (qe : EParalist qo) : Seq (qo beta) (qo alpha)
                := match qe as q0 in EParalist l0 return
                         (Seq (l0 beta) (l0 alpha)): SProp
                         with
                         | EParanil _ => Seq_refl _
                         | @EParacons _ u ue a =>
                           match (aux u ue) as eq0 in Seq _ u0 with
                           | Seq_refl _ => Seq_refl _
                           end
                   end) l.(spr1) (l.(spr2) alpha)).
Defined.   


 
Inductive continuous {A B C : Ty_aux} :
  Elt_aux (Arr_aux (Arr_aux A B) C) ->
  Elt_aux (Arr_aux A B) -> SProp := 
| cont : forall (f : Elt_aux (Arr_aux (Arr_aux A B) C))
                (alpha : Elt_aux (Arr_aux A B))
                (l : list (Elt_aux A))
                (rel : forall beta :Elt_aux (Arr_aux A B),
                    @Seqfin _ _ alpha l beta -> Seq (App_aux f alpha) (App_aux f beta)),
    continuous f alpha.

 
Inductive Paracontinuous {A B C : Ty_aux} :
  forall (f : Elt_aux (Arr_aux (Arr_aux A B) C))
         (u : Elt_aux (Arr_aux A B)),
    ((X -> Y) -> continuous f u) -> SProp := 
| Paracont : forall (f : Elt_aux (Arr_aux (Arr_aux A B) C))
                (u : Elt_aux (Arr_aux A B))
                (l : list (Elt_aux A))
                (rel : forall v : Elt_aux (Arr_aux A B),
                    @Seqfin _ _ u l v ->
                    Seq (App_aux f u) (App_aux f v)),
    Paracontinuous (fun _ => @cont A B C f u l rel).


Definition Continuous {Γ : Ctx} :
  @Elt Γ (@Pi Γ U
              (@Pi (Γ · U) U
                   (@Pi (Γ · U · U) U
                        ((((Weaken (Weaken (Var (A:= U)))) → (Weaken (Var (A:= U))))
                         → (Var (A := U))) →
                    (((Weaken (Weaken (@Var Γ U))) → (Weaken (Var (A:= U)))) →
                    SU))))).
Proof.
  unshelve refine (fun gamma => Sexist _).
  - simpl.
    unshelve refine (fun alpha A B C f u => exist _).
    + exact (fun _ => @continuous A B C f u).
    + simpl.
      exact (@Paracontinuous A B C f u).
  - now simpl.
Defined.

    
End Translation2.




Section Continuity2.

Notation "r ® γ" := (@Inst nat nat _ _ _ _ r γ) (at level 10, left associativity).
Notation "()" := (@ctx_nil nat nat _).
Notation "Γ · A" := (@Ctx_cons nat nat Γ A) (at level 50, left associativity).
Notation "γ ·· xe" := (@ctx_cons nat nat _ _ _ _ γ _ xe)
                          (at level 50, left associativity).
Notation "¿ g" := (@Ctx_tl nat nat _ _ _ _ g) (at level 10).
Notation "t • u" := (@App nat nat _ _ _ t u) (at level 12, left associativity).
Notation "t ◊ u" := (@AppN nat nat _ _ _ t u) (at level 12, left associativity).
Notation "⇑ A" := (@Lift nat nat _ _ _ _ A) (at level 10).
Notation "#" := (@Var nat nat _ _) (at level 0).
Notation "↑ M" := (@Weaken nat nat _ _ _ M) (at level 10).
Notation "S {{ r }} " := (@Sub nat nat _ _ S r) (at level 20, left associativity).
Notation "∏" := (@Pi nat nat _) .
Notation "A → B" := (@Pi nat nat _ A (@Lift nat nat _ A B))
                        (at level 99, right associativity, B at level 200).
Notation "[ A ]" := (@EL nat nat A).
Notation "'λ'" := (@Lam nat nat _ ). 
Notation "'U'" := (@U nat nat) (at level 10).
Definition Natnat { Γ : Ctx }:= @Nat nat nat Γ.

  
Definition Generic {Γ : Ctx } :
  @Elt nat nat Γ (Natnat → Natnat).
Proof.
  unshelve refine (fun gamma => Sexist _).
  - unshelve refine (fun alpha n => _).
    exact (alpha (n.(spr1) alpha)).
  - unshelve refine (fun alpha n => _).
    destruct n as [no ne].
    refine (match (ne alpha) as ne0 in EParanat f0
                  return
                  EParanat (fun beta => beta (f0 beta))
            with
            | @EParabêtanat _ _ mo mb rel => _
            end).
    unshelve refine (EParabêtanat _).
    + exact (Generic_aux 0 mb).
    + intro beta.
      refine ((fun (e : Seq (BNdialogue mb beta) (mo beta)) =>
                match e as eq0 in Seq _ dmb
                    return
                    Seq (beta (dmb)) _
              with
              | Seq_refl _ => _
              end) _).
      * now rewrite - (NaturGen_aux mb beta 0).
      * refine (match (rel beta) as eq0 in Seq _ dmb
                      return
                      Seq dmb _
                with
                |Seq_refl _ => Seq_refl _
                end).
Defined.

Inductive Seloquent : ((nat -> nat) -> nat) -> SProp :=
| Sdial : forall
    (f : (nat -> nat) -> nat)
    (nb : @BranchingNat nat nat)
    (rel : forall (alpha : nat -> nat),
        Seq (f alpha) (BNdialogue nb alpha)),
    Seloquent f.
Print continuous.
Print App.

  
Theorem Continuity {Γ : Ctx}
        (G := Γ · ((Natnat → Natnat) → Natnat) · (Natnat → Natnat))
        (S := @Pi _ _  (G · (U)) (U)
                  (((Natnat → (↑ (Var (A:= U))))
                    → (Var (A := (U)))) →
                   ((Natnat → (↑ (Var (A:= U)))) →
                    (@SU nat nat _))))
        (T := ((Natnat → Natnat)
               → (Var (Γ := G) (A := (U)))) →
              ((Natnat → Natnat) →
               (@SU nat nat _))) :
  @SElt nat nat Γ (@SPi nat nat Γ ((Natnat → Natnat) → Natnat)
                        (@SPi _ _ _ (Natnat → Natnat)
                              (((@App _ _ G (U) T
                                      (@App _ _ G (U) S
                                            ((@Continuous nat nat _) • Natnat)
                                            Natnat) Natnat) ◊ (↑ #))
                                 ◊ #))).
Proof.
  unshelve refine (fun gamma => Sexist2 _).
  - simpl.
    intros alpha f u.
    pose t:= App_aux f (Inst Generic gamma).
    simpl in t.
    unfold App_aux in t.
    unfold Inst in t.
    unfold Elt_aux in t.
    cbn in t.
    simpl in t.
    unshelve refine (@cont nat nat _ _ _ f u _ _).
    + admit.
    + simpl ; intro beta ; cbv.
      destruct u as [uo ue].
      simpl in uo ; simpl in ue.
      unfold Generic in t ; simpl in t.
      unfold Ty_inst in f.
      simpl in f.
      unfold Ty_inst in f.
      simpl in f.


      
(*Definition upn (n : nat) : @EParanat nat nat (fun _ => n).
Proof.
  induction n.
  - unshelve refine (EParabêtanat _).
    +  exact (@B0 _ _).
    +  reflexivity.
  - refine (match IHn in EParanat f0
                  return
                  EParanat (fun beta => S (f0 beta))
            with
            | @EParabêtanat _ _ mo mb rel => _
            end).
    unshelve refine (EParabêtanat _).
    + exact (BS mb).
    + simpl.
      intro beta.
      refine (match (rel beta) as eq0 in Seq _ dmb
                    return
                    Seq (S (mo beta)) (S dmb)
                with
                |Seq_refl _ => Seq_refl _
                end).
Defined.


Definition continuous (f : (nat -> nat) -> nat) (alpha : nat -> nat) : Type.
Proof.
  Print ssig.
  unshelve refine (ssig _).
  - refine (list nat).
  - exact (fun l => forall beta : nat -> nat, SEqFin alpha l beta -> Seq (f alpha) (f beta) ).
Defined.
*)



Inductive continuous :
  ((nat -> nat) -> nat) -> (nat -> nat) -> SProp :=
| cont : forall (f : (nat -> nat) -> nat)
                (alpha : nat -> nat)
                (l : list nat)
                (rel : forall beta : nat -> nat, SEqFin _ _ alpha l beta -> Seq (f alpha) (f beta)),
    continuous f alpha.


Definition Continuous { Γ: Ctx} :
  @Elt nat nat Γ (((Natnat → Natnat) → Natnat)
                   → (Natnat  → Natnat)  → (@SU nat nat _)).
Proof.
  unshelve refine (fun gamma => Sexist _).
  - intro alpha.
    simpl.
    intros f beta.
    unshelve refine (continuous _ _).
    + pose q:= ((f.(spr1) alpha)).
      cbv in q.
      
    
Definition SBNDialogue_Continuity :
forall (b : BranchingNat nat nat) (alpha : nat -> nat),
  continuous (BNdialogue b) alpha.
Proof.
  intros b alpha.
  induction b.
  - cbv.
    unshelve refine (@cont _ _ _ _).
    + exact nil.
    + intros beta H.
      reflexivity.
  - inversion IHb as [ g lambda  l IHl].
    unshelve refine (@cont _ _ _ _).
    + exact l.
    + intros beta HeqFin.
      refine (Sf_equal S  (IHl beta HeqFin)).
  - specialize H with (alpha x).
    inversion_clear H as [g lambda l Ihl].
    exists (cons x l).
    intros beta HeqFin.
    simpl.
    inversion_clear HeqFin.
    rewrite - H.
    simpl.
    exact (Ihl beta H0).
Defined.

Inductive Seloquent : ((nat -> nat) -> nat) -> SProp :=
| Sdial : forall
    (f : (nat -> nat) -> nat)
    (nb : @BranchingNat nat nat)
    (rel : forall (alpha : nat -> nat),
        Seq (f alpha) (BNdialogue nb alpha)),
    Seloquent f.

Lemma Seloquent_continuous (f : (nat -> nat) -> nat) :
  Seloquent f -> forall alpha, continuous f alpha.
Proof.
  intro H.
  destruct H.
  intro alpha.
  induction (SBNDialogue_Continuity nb alpha). 
  unshelve refine (cont _).
  - exact l.
  - intros beta Eql.
    refine ((fun (e : Seq (f0 alpha) (f alpha)) =>
                 match e as eq0 in Seq _ g0
                  return
                  Seq g0 (f beta)
            with
            | Seq_refl _ => _
            end) _).
    + refine ((fun (e : Seq (f0 beta) (f0 alpha)) =>
                 match e as eq0 in Seq _ g0
                  return
                  Seq g0 (f beta)
            with
            | Seq_refl _ => _
            end) _).
      * refine (match (rel beta) as eq0 in Seq _ g0
                    return Seq g0 (f beta)
              with
              | Seq_refl _ => Seq_refl _
                end).
      * refine (match (rel0 beta Eql) as eq0 in Seq _ g0
                    return Seq g0 (f0 alpha)
              with
              | Seq_refl _ => Seq_refl _
                end).
    + refine (match (rel alpha) as eq0 in Seq _ g0
                    return Seq g0 (f alpha)
              with
              | Seq_refl _ => Seq_refl _
              end).
Defined.


Theorem continuity {Γ : Ctx }
        (f : @Elt nat nat (@Ctx_nil) ((Natnat → Natnat) → Natnat))
        (alpha : nat -> nat) :
  continuous ((App f Generic) (ctx_nil)).(spr1) alpha .
Proof.
  pose g := ((App f Generic) (ctx_nil)).(spr1).
  pose ge := ((App f Generic) (ctx_nil)).(spr2) alpha.
  simpl in g.
  simpl in ge.
  refine (let aux : Seloquent ((App f Generic) (ctx_nil)).(spr1) :=
    match ge as ge0 in EParanat f1
          return Seloquent f1
    with
    | @EParabêtanat _ _ mo mb rel => (@Sdial _ mb rel)
    end in _).
  refine (Seloquent_continuous aux alpha).
Defined.




End Continuity2.



Section Translation.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Variables (X Y : Type).
Definition ℙ := X -> Y.
Implicit Type (A B : Type).

Record ssig (A:Type) (P: A -> SProp) : Type := Sexist { spr1 : A; spr2 : P spr1 }.


Arguments spr1 {_ _} _.
Arguments spr2 {_ _} _.

Definition Ctx := (X -> Y) -> Type.

Inductive sUnit : SProp := stt.

Definition Ty (Γ : Ctx) :=
  forall (α : ℙ) (γ : Γ α),
    tsig (fun (A : Type) => (A -> SProp)).

Definition ETy (Γ : Ctx) (A : Ty Γ) : SProp :=
  forall (α : ℙ) (γ : Γ α), sUnit. 


Definition Elt (Γ : Ctx) (A : Ty Γ) (Ae : ETy A) : Type :=
  forall (α : X -> Y) (gamma : Γ α),
    (A α gamma).(fst).

Definition EElt (Γ : Ctx) (A : Ty Γ) (Ae : ETy A) (a : Elt Ae) : SProp :=
  forall (α : X -> Y) (gamma : Γ α),
    (A α gamma).(snd) (a α gamma).

Definition Ty_inst {Γ : Ctx} {α : ℙ} (A : Ty Γ) (Ae : ETy A)
           (γ : Γ α) : Type :=
  (A α γ).(fst).

Definition ETy_inst {Γ : Ctx} {α : ℙ} (A : Ty Γ) (Ae : ETy A)
           (γ : Γ α) (a : Ty_inst Ae γ) : SProp :=
  (A α γ).(snd) a.


Definition Inst {Γ : Ctx} {A : Ty Γ} {Ae : ETy A} {α : ℙ} (a : Elt Ae)
           (γ : Γ α) : Ty_inst Ae γ :=
  (a α γ).

Notation "r ® γ" := (@Inst _ _ _ _ r γ) (at level 10, left associativity).

Definition EInst {Γ : Ctx} {A : Ty Γ} {Ae : ETy A} {α : ℙ}
           (a : Elt Ae) (ae : EElt a) (γ : Γ α) : ETy_inst (Inst a γ) :=
  (ae α γ).

Notation "r © γ" := (@EInst _ _ _ _ _ r γ) (at level 10, left associativity).

Definition Ctx_nil : Ctx :=  (fun α => unit).

Definition ctx_nil {α : ℙ} : Ctx_nil α := tt.

Notation "()" := ctx_nil.


Definition Ctx_cons (Γ : Ctx) (R : Ty Γ) (Re : ETy R) : Ctx.
Proof.
  unshelve refine (fun α => @tsig (Γ α) _).
  unshelve refine (fun gamma => ssig _).
  - exact (R α gamma).(fst).
  - exact (R α gamma).(snd).
Defined.

Notation "Γ · A" := (@Ctx_cons Γ _ A) (at level 50, left associativity).


Definition ctx_cons {Γ : Ctx} {A : Ty Γ} {Ae : ETy A} {α : ℙ}
           (γ : Γ α) (a : Ty_inst Ae γ) (ae : ETy_inst a) :
  (Γ · Ae) α.
Proof.
  exists γ.
  refine (Sexist ae).
Defined.

Notation "γ ·· xe" := (@ctx_cons _ _ _ _ γ _ xe)
                          (at level 50, left associativity).


Definition Ctx_tl {Γ : Ctx} {A : Ty Γ} (Ae : ETy A) {α : ℙ}
           (γ : (Γ · Ae) α) : Γ α.
Proof.
  exact (γ.(fst)).
Defined.


Notation "¿ g" := (Ctx_tl g) (at level 10).


Definition Ctx_hd {Γ : Ctx} {A : Ty Γ} {Ae : ETy A} {α : ℙ}
           (γ : (Γ · Ae) α) : Ty_inst Ae (¿ γ).
Proof.
  exact γ.(snd).(spr1).
Defined.

Notation "◊ g" := (Ctx_hd g) (at level 10).

Definition Lift {Γ : Ctx} {A : Ty Γ} {Ae : ETy A}
           (B : Ty Γ) (Be : ETy B) : Ty (Γ · Ae) :=
  fun α γ => B α (¿ γ).

Notation "⇑ A" := (@Lift _ _ _ _ A) (at level 10).


Definition ELift {Γ : Ctx} {A : Ty Γ} {Ae : ETy A}
           (B : Ty Γ) (Be : ETy B) : ETy (Lift (Ae:= Ae) Be) :=
  fun α γ => stt.


Definition Var {Γ : Ctx} (A : Ty Γ) (Ae : ETy A) :
  @Elt (Γ · Ae) (⇑ Ae) (@ELift _ _ Ae _ Ae).
Proof.
  unshelve refine  (fun α γ => _).
  + exact (γ.(snd).(spr1)). 
Defined.


Definition EVar {Γ : Ctx} (R : Ty Γ) (Re : ETy R) :
  @EElt (Γ · Re) (⇑ Re) (@ELift _ _ Re _ Re) (@Var _ _ Re).
Proof.
  unshelve refine (fun α γ => _).
  exact (γ.(snd).(spr2)).
Defined.

Notation "#" := (@EVar _ _ _) (at level 0).


Definition Weaken {Γ : Ctx} {A B : Ty Γ} {Ae : ETy A} (Be : ETy B)
           (r : @Elt Γ A Ae) :
  @Elt (Γ · Be) (⇑ Ae) (@ELift _ _ Be _ Ae).
Proof.
  unshelve refine (fun α γ => _).
  refine ((r α (¿ γ))).
Defined.


Definition EWeaken {Γ : Ctx} {R S : Ty Γ} {Re : ETy R} {Se : ETy S}
           (r : @Elt Γ R Re) (re : EElt r) :
  EElt  (Weaken (Be:= Se) r).
Proof.
  exact (fun α γ => re α (¿ γ)).
Defined.

Notation "↑ M" := (@EWeaken _ _ _ _ _ _ M) (at level 10).

Definition Sub {Γ : Ctx} {A : Ty Γ} (Ae : ETy A)
           (B : Ty (Γ · Ae)) (Be : ETy B)
           (a : @Elt Γ A Ae) (ae : EElt a) :
  Ty Γ.
Proof.
  unshelve refine (fun α γ => _). 
  refine (B _ (γ ·· (ae © γ))).
Defined.


Definition ESub {Γ : Ctx} {A : Ty Γ} (Ae : ETy A)
           (B : Ty (Γ · Ae)) (Be : ETy B)
           (a : @Elt Γ A Ae) (ae : EElt a) :
  ETy (Sub Be ae) := fun α γ => stt. 

Notation "S {{ r }} " := (@ESub _ _ _ _ S _ r) (at level 20, left associativity).


Definition Pi {Γ : Ctx} (A : Ty Γ) (Ae : ETy A)
           (B : Ty (Γ ·  Ae))  (Be : ETy B) : Ty Γ.
Proof.
  unshelve refine (fun α γ => exist _).
  - refine (forall (a : Ty_inst Ae γ) (ae : ETy_inst a),
               (B α (γ ·· ae)).(fst)).
  - exact (fun f => forall (a : Ty_inst Ae γ) (ae: ETy_inst a),
            (B α (γ ·· ae)).(snd) (f a ae)).
Defined.

Definition EPi {Γ : Ctx} (A : Ty Γ) (Ae : ETy A)
           (B : Ty (Γ ·  Ae)) (Be : ETy B) : ETy (Pi Be) :=
  fun _ _  => stt.


Notation "∏" := EPi.

Notation "Ae → Be" := (@EPi _ _ Ae _ (@ELift _ _ Ae _ Be)) (at level 99, right associativity, Be at level 200).


Definition Lam {Γ : Ctx} {A : Ty Γ} (Ae : ETy A)
           (B : Ty (Γ · Ae)) (Be : ETy B)
           (f : @Elt (Γ · Ae) B Be) (fe : EElt f)
  : @Elt Γ (Pi Be) (@EPi _ _ Ae _ Be).
Proof.
  unshelve refine (fun α γ => _).
  - refine (fun a ae => (f α (γ ·· ae))).
Defined.


Notation "'λ'" := Lam.

Definition ELam {Γ : Ctx} {A : Ty Γ} (Ae : ETy A)
           (B : Ty (Γ · Ae)) (Be : ETy B)
           (f : @Elt (Γ · Ae) B Be) (fe : EElt f)
  : EElt (Lam (Ae:= Ae) fe).
Proof.
  unshelve refine (fun α γ a ae => _).
  exact (fe α (γ ·· ae)).
Defined.


Definition App {Γ : Ctx} {A : Ty Γ} (Ae : ETy A)
           {B : Ty (Γ · Ae)} (Be : ETy B)
           (f : @Elt Γ (Pi Be) (@EPi _ _ Ae _ Be))
           (x : Elt Ae) (xe : EElt x) :
  @Elt Γ _ (Be {{ xe }}).
Proof.
  unshelve refine (fun α γ => _).
  refine ((f α γ) (x α γ) (xe α γ)).
Defined.

Notation "t • u" := (App t u) (at level 12, left associativity).

Definition EApp {Γ : Ctx} {A : Ty Γ} (Ae : ETy A)
           {B : Ty (Γ · Ae)} (Be : ETy B)
           (f : @Elt Γ (Pi Be) (@EPi _ _ Ae _ Be))
           (fe : EElt f)
           (x : Elt Ae) (xe : EElt x) :
  @EElt Γ _ (Be {{ xe }}) (f • xe).
Proof.
  unshelve refine (fun α γ => _).
  refine ((fe α γ) (x α γ) (xe α γ)).
Defined.

Definition U {Γ : Ctx} : Ty Γ.
Proof.
  unshelve refine (fun α γ => exist _).
  - refine (tsig (fun (A : Type) => (A -> SProp))).
  - refine (fun _ => sUnit).
Defined.

Print Elt.

Definition Ue {Γ : Ctx} : ETy (@U Γ) :=
  fun _ _ => stt.


Definition EL {Γ : Ctx} (A : @Elt Γ U Ue) : Ty Γ := A.

Definition EEL {Γ : Ctx} (A : @Elt Γ U Ue) (Ae: EElt A) : ETy (EL A) :=
  Ae.

Notation "[ A ]" := (EEL A).

Inductive BranchingBool :=
| Btrue_aux : BranchingBool
| Bfalse_aux : BranchingBool
| Bêtabool : (Y -> BranchingBool) -> X -> BranchingBool.

Fixpoint BoolDialogue (alpha : X -> Y) (b : BranchingBool) : bool :=
  match b with
  | Btrue_aux => true
  | Bfalse_aux => false
  | Bêtabool f x => BoolDialogue alpha (f (alpha x))
  end.


Inductive EParabool (α : X -> Y) :
  bool -> SProp :=
| EParatrue : EParabool α true
| EParafalse : EParabool α false
| EParabêtabool : forall  (bo : bool) (bb : BranchingBool),
    bo = BoolDialogue α bb ->
    EParabool α bo.

Definition Bool {Γ : Ctx} : Ty Γ.
Proof.
  unshelve refine (fun alpha gamma => exist _).
  - exact bool.
  - exact (EParabool alpha).
Defined.

Definition EBool {Γ : Ctx} : ETy (@Bool Γ) := fun _ _ => stt.

Definition Etrue {Gamma : Ctx} : Elt (@EBool Gamma) :=
  fun α gamma => true.

Definition EEtrue {Gamma : Ctx} : @EElt Gamma _ _ Etrue :=
  fun alpha _ => EParatrue alpha.
  
Definition Efalse {Gamma : Ctx} : Elt (@EBool Gamma) :=
  fun α gamma => false.

Definition EEfalse {Gamma : Ctx} : @EElt Gamma _ _ Efalse :=
  fun alpha _ => EParafalse alpha.



(*Definition Bool_rect (Γ : Ctx) :
  @Elt Γ _ (@EPi Γ _ ((EBool → Ue)) _ (
                   [@EApp (Γ · (EBool → Ue)) _ EBool _ Ue _ # _ EEtrue] →
                   [@EApp (Γ · (EBool → Ue)) _ EBool _ Ue _ # _ EEfalse] →
                   (@EPi (Γ · (EBool → Ue)) Bool EBool
                         (@App _ _ _ U Ue (Weaken (Var (Ae:=EBool → Ue))) _ #)
                         [@EApp (Γ · (EBool → Ue) · EBool) _ _ U Ue (Weaken (Var (Ae:=EBool → Ue)))
                                (↑#) _ #]))).
Proof.
  unshelve refine (fun alpha gamma P Pe ptrue ptruee pfalse pfalsee b be => _).
  induction b.
  - exact ptrue.
  - exact pfalse.
Defined.

Definition EBool_rect (Γ : Ctx) :
  @EElt Γ _ (@EPi Γ _ ((EBool → Ue)) _ (
                   [@EApp (Γ · (EBool → Ue)) _ EBool _ Ue _ # _ EEtrue] →
                   [@EApp (Γ · (EBool → Ue)) _ EBool _ Ue _ # _ EEfalse] →
                   (@EPi (Γ · (EBool → Ue)) Bool EBool
                         (@App _ _ _ U Ue (Weaken (Var (Ae:=EBool → Ue))) _ #)
                         [@EApp (Γ · (EBool → Ue) · EBool) _ _ U Ue (Weaken (Var (Ae:=EBool → Ue)))
                                (↑#) _ #])))
(@Bool_rect Γ).
Proof.
  unshelve refine (fun alpha gamma P Pe ptrue ptruee pfalse pfalsee b be => _).
  induction be.
  - exact ptruee.
  - exact pfalsee.
  - induction bo.
    + exact ptruee.
    + exact pfalsee.
Defined.    *)

Definition BNdialogue := @BNdialogue X Y.
(*
Fixpoint Natdialogue (oracle : X -> Y) (b : BranchingNat X Y) {struct b} : nat :=
  match b with
  | B0 _ _ => 0
  | BS b' => S (Natdialogue oracle b')
  | Bêtanat f x => Natdialogue oracle (f (oracle x)) 
  end.

Inductive EParanat (α : X -> Y) : nat -> SProp :=
| EPara0 : EParanat α 0
| EParaS : forall (no : nat) (ne : EParanat α no),
    EParanat α (S no)
| EParabêtanat : forall (no : nat) (nb : BranchingNat X Y)
                        (rel : no = Natdialogue α nb)
  (ne : EParanat α no),
    EParanat α no.*)

Inductive EParanat (α : X -> Y) : nat -> SProp :=
| EParabêtanat : forall (no : nat) (nb : BranchingNat X Y)
                        (rel : no = BNdialogue nb α),
    EParanat α no.



Definition Nat {Γ : Ctx} : Ty Γ.
Proof.
  unshelve refine (fun alpha gamma => exist _).
  - exact nat.
  - exact (EParanat alpha).
Defined.

Definition ENat {Γ : Ctx} : ETy (@Nat Γ) := fun _ _ => stt.

Definition Z {Gamma : Ctx} : Elt (@ENat Gamma) :=
  fun α gamma => 0.
Print BranchingNat.
Definition EZ {Gamma : Ctx} : @EElt Gamma _ _ Z.
Proof.
  intros alpha gamma.
  now refine (@EParabêtanat alpha 0 (@B0 X Y) _).
Defined.

Definition Succ {Gamma : Ctx} : Elt ((@ENat Gamma) → ENat) :=
  fun α gamma n ne => S n.

Definition ESucc {Gamma : Ctx} : EElt (@Succ Gamma) :=
  fun alpha gamma n ne => match ne as ne0 in EParanat _ n0
                                return EParanat alpha (S n0)
                          with
                          | @EParabêtanat _ no nb rel =>
                            @EParabêtanat alpha (S no) (@BS X Y nb) (eq_S _ _ rel)
                          end.

Lemma Param_rev (alpha : X -> Y) (n : nat) :
  EParanat alpha (S n) -> EParanat alpha n.
Proof.
  intro ne.
  inversion ne.
  revert rel ; revert H ; revert ne ;  revert no ; revert n.
  induction nb ; intros.
  + exfalso.
    now refine (O_S 0 _).
  + refine (@EParabêtanat alpha n nb _).
    now apply eq_add_S.
  + exact (H (alpha x) _ _ ne H0 rel).
Defined.      
    
    
(*  refine (fix f k ke {struct ke} :=
              let m:= S k in
              let eqm := eq_refl : m = S k in
              match ke as ke0 in EParanat _ k0
                    return m = k0 -> EParanat _ k
              with
              | EPara0 _ => _
              | EParaS me => _
                            | @EParabêtanat _ n _ _ ne =>
                              match n as n0 return EParanat _ n0 -> _
                              with
                              | 0 => _
                              | S p => _
                              end ne
              end eqm
         ).
  - intro eqm0.
    exfalso.
    rewrite  eqm in eqm0.
    now refine (O_S k _).
  - intro eqmSn.
    now rewrite (eq_add_S k n eqmSn).
  - intros ne2 eqm0.
    exfalso.
    rewrite eqm in eqm0.
    now refine (O_S k _).
  - intros ne2 eqmSp.
    rewrite eqm in eqmSp.
    rewrite (eq_add_S k p eqmSp).
    exact (f p ne2).
Defined.    
*)
Lemma Nat_rect_aux (Γ : Ctx) (alpha : X -> Y) :
  forall
    (P : forall a : nat, EParanat alpha a -> {A : Type & A -> SProp})
    (Pe : forall a : nat, EParanat alpha a -> sUnit)
    (p0 : fst (P 0 (@EParabêtanat _ 0 (@B0 X Y) eq_refl)))
    (p0e : snd (P 0 (@EParabêtanat _ 0 (@B0 X Y) eq_refl)) p0)
    (pS : forall (a : nat) (ae : EParanat alpha a) (a0 : fst (P a ae)),
       snd (P a ae) a0 ->
       fst
         (P (S a)
            match ae in (EParanat _ n0) return (EParanat alpha (S n0)) with
            | @EParabêtanat _ no nb rel =>
                @EParabêtanat alpha (S no) (@BS X Y nb)
                  match rel in (_ = y) return (S no = S y) with
                  | eq_refl => eq_refl
                  end
            end))
    (pSe : forall (a : nat) (ae : EParanat alpha a) (a0 : fst (P a ae))
          (ae0 : snd (P a ae) a0),
        snd
          (P (S a)
             match ae in (EParanat _ n0) return (EParanat alpha (S n0)) with
             | @EParabêtanat _ no nb rel =>
                 @EParabêtanat alpha (S no) (@BS X Y nb)
                   match rel in (_ = y) return (S no = S y) with
                   | eq_refl => eq_refl
                   end
             end) (pS a ae a0 ae0))
    (n : nat) (ne : EParanat alpha n),
    ssig (fun x : (P n ne).(fst) => (P n ne).(snd) x).
Proof.
  intros ; induction n.
    + exists p0.
      exact p0e.
    + unshelve refine (Sexist _).
      * exact (pS n (Param_rev ne) (IHn (Param_rev ne)).(spr1) (IHn (Param_rev ne)).(spr2)).
      * exact (pSe n (Param_rev ne) (IHn (Param_rev ne)).(spr1) (IHn (Param_rev ne)).(spr2)).
Defined.


(*Definition Nat_rect (Γ : Ctx) :
  @Elt Γ _ (@EPi Γ _ ((ENat → Ue)) _ (
                   [@EApp (Γ · (ENat → Ue)) _ ENat _ Ue _ # _ EZ] →
                   (@EPi (Γ · (ENat → Ue)) Nat ENat
                         _
                         ([@EApp (Γ · (ENat → Ue) · ENat) _ _ U Ue
                                 (Weaken (Var (Ae:=ENat → Ue)))
                                 (↑#) _ #] →
                          [@EApp (Γ · (ENat → Ue) · ENat) _ _ U Ue
                                 (Weaken (Var (Ae:=ENat → Ue)))
                                 (↑#) _ (EApp ESucc #)])) →
                   (@EPi (Γ · (ENat → Ue)) Nat ENat
                         (@App _ _ _ U Ue (Weaken (Var (Ae:=ENat → Ue))) _ #)
                         [@EApp (Γ · (ENat → Ue) · ENat) _ _ U Ue (Weaken (Var (Ae:=ENat → Ue)))
                                (↑#) _ #]))).
Proof.
  refine (fun alpha gamma P Pe p0 p0e pS pSe n ne =>
                     (@Nat_rect_aux Γ alpha P Pe p0 p0e pS pSe n ne).(spr1)).
Defined.


Definition ENat_rect (Γ : Ctx) :
  @EElt Γ _ (@EPi Γ _ ((ENat → Ue)) _ (
                   [@EApp (Γ · (ENat → Ue)) _ ENat _ Ue _ # _ EZ] →
                   (@EPi (Γ · (ENat → Ue)) Nat ENat
                         _
                         ([@EApp (Γ · (ENat → Ue) · ENat) _ _ U Ue
                                 (Weaken (Var (Ae:=ENat → Ue)))
                                 (↑#) _ #] →
                          [@EApp (Γ · (ENat → Ue) · ENat) _ _ U Ue
                                 (Weaken (Var (Ae:=ENat → Ue)))
                                 (↑#) _ (EApp ESucc #)])) →
                   (@EPi (Γ · (ENat → Ue)) Nat ENat
                         (@App _ _ _ U Ue (Weaken (Var (Ae:=ENat → Ue))) _ #)
                         [@EApp (Γ · (ENat → Ue) · ENat) _ _ U Ue (Weaken (Var (Ae:=ENat → Ue)))
                                (↑#) _ #])))
       (@Nat_rect Γ).
Proof.
  refine (fun alpha gamma P Pe p0 p0e pS pSe n ne =>
            (@Nat_rect_aux Γ alpha P Pe p0 p0e pS pSe n ne).(spr2)).
Defined.
*)




End Translation.


Section Continuity.

Notation "r ® γ" := (@Inst nat nat _ _ _ _ r γ) (at level 10, left associativity).
Notation "r © γ" := (@EInst nat nat _ _ _ _ _ r γ) (at level 10, left associativity).
Notation "()" := (@ctx_nil nat nat _).
Notation "Γ · A" := (@Ctx_cons nat nat Γ _ A) (at level 50, left associativity).
Notation "γ ·· xe" := (@ctx_cons nat nat _ _ _ _ γ _ xe)
                          (at level 50, left associativity).
Notation "¿ g" := (@Ctx_tl nat nat _ _ _ _ g) (at level 10).
Notation "◊ g" := (@Ctx_hd nat nat _ _ _ _ g) (at level 10).
Notation "⇑ A" := (@Lift nat nat _ _ _ _ A) (at level 10).
Notation "#" := (@EVar nat nat _ _ _) (at level 0).
Notation "↑ M" := (@EWeaken nat nat _ _ _ _ _ _ M) (at level 10).
Notation "S {{ r }} " := (@ESub nat nat _ _ _ _ S _ r) (at level 20, left associativity).
Notation "∏" := (@EPi nat nat _ _ _) .
Notation "Ae → Be" := (@EPi nat nat _ _ Ae _ (@ELift nat nat _ _ Ae _ Be))
                        (at level 99, right associativity, Be at level 200).



Notation "[ A ]" := (@EEL nat nat _ A).
Notation "r ® γ" := (Inst nat nat r γ) (at level 10, left associativity).
Notation "()" := (ctx_nil nat nat).

Notation "'λ'" := (@ELam nat nat _ _ _ _ _ _).
  
Definition Generic {Γ : Ctx nat nat } : @Elt nat nat Γ _ ((@ENat nat nat _) → (@ENat nat nat _)).
Proof.
  unshelve refine (fun alpha gammma n ne => alpha n).
Defined.  


Definition EGeneric {Γ : Ctx nat nat } : @EElt nat nat Γ _ ((@ENat nat nat _) → (@ENat nat nat _)) Generic.
Proof.
  cbv.
  unshelve refine (fun α gammma n ne =>
                     match ne as ne0 in EParanat _ n0
                           return (EParanat α (α n0))
                     with
                     | @EParabêtanat _ _ _ no nb rel => _
                     end).
  rewrite rel.
  rewrite - (NaturGen_aux nb α 0).
  now refine (@EParabêtanat nat nat _ _  (Generic_aux 0 nb) eq_refl).
Defined.
(*unshelve refine (let aux :  forall (k : nat),
                       EParanat alpha (alpha (k + n)) := _ in _).
  - induction ne ; intros.
    + rewrite - plus_n_O.
      induction (alpha k).
      * exact (EPara0 alpha).
      * exact (EParaS IHn).
    + rewrite - plus_n_Sm ; rewrite - plus_Sn_m.
      exact (IHne (S k)).
    + exact (IHne k).
  - induction ne.
    + refine (@EParabêtanat nat nat alpha (alpha 0)
                          (Bêtanat (BNEta nat nat) 0) _ _).
       * cbv.
         induction (alpha 0) ; trivial.
         now apply eq_S.
       * exact (aux 0).
    + refine (@EParabêtanat nat nat alpha (alpha (S no))
                          (Bêtanat (BNEta nat nat) (S no)) _ _).
       * cbv.
         induction (alpha (S no)) ; trivial.
         now apply eq_S.
       * exact (aux 0).
    + refine (@EParabêtanat nat nat alpha (alpha no)
                            (Generic_aux 0 nb) _ _).
      * rewrite (NaturGen_auxBis nb alpha 0).
        rewrite rel.
        reflexivity.
      * exact (aux 0).
Defined.
*)
Definition upn (n : nat) (alpha : nat -> nat) : EParanat alpha n.
Proof.
  induction n.
  - refine (@EParabêtanat nat nat alpha 0 (@B0 nat nat) eq_refl).
  - destruct IHn.
    refine (@EParabêtanat nat nat alpha (S no) (BS nb) _).
    now apply eq_S.
Defined.

Print continuous.
Print EqFin.
Inductive SEqFin (X Y : Type) : (X -> Y) -> list X -> (X -> Y) -> SProp :=
    Eqnil : forall alpha bêta : X -> Y, SEqFin alpha nil bêta
  | Eqcons : forall (alpha bêta : X -> Y) (l : list X) (x : X),
             alpha x = bêta x -> SEqFin alpha l bêta -> SEqFin alpha (x :: l) bêta.

Print eq.


Definition Sf_equal := 
  fun (A B : Type) (f : A -> B) (x y : A) (H : Seq x y) =>
    match H in (Seq _ y0) return (Seq (f x) (f y0)) with
    | eq_refl _ => eq_refl _
    end.

Inductive continuous :
  ((nat -> nat) -> nat) -> (nat -> nat) -> SProp :=
                     | cont : forall (f : (nat -> nat) -> nat)
                                     (alpha : nat -> nat)
                                     (l : list nat)
                                     (rel : forall beta : nat -> nat, SEqFin alpha l beta -> Seq (f alpha) (f beta)),
                         continuous f alpha.

(*Definition continuous (f : (nat -> nat) -> nat) (alpha : nat -> nat) : Type.
Proof.
  Print ssig.
  unshelve refine (ssig _).
  - refine (list nat).
  - exact (fun l => forall beta : nat -> nat, SEqFin alpha l beta -> Seq (f alpha) (f beta) ).
Defined.
*)
About BNDialogue_Continuity.

Definition SBNDialogue_Continuity :
forall (b : BranchingNat nat nat) (alpha : nat -> nat),
  continuous (BNdialogue b) alpha.
Proof.
  intros b alpha.
  induction b.
  - cbv.
    unshelve refine (@cont _ _ _ _).
    + exact nil.
    + intros beta H.
      reflexivity.
  - inversion IHb as [ g lambda  l IHl].
    unshelve refine (@cont _ _ _ _).
    + exact l.
    + intros beta HeqFin.
      refine (Sf_equal S  (IHl beta HeqFin)).
  - specialize H with (alpha x).
    inversion_clear H as [g lambda l Ihl].
    exists (cons x l).
    intros beta HeqFin.
    simpl.
    inversion_clear HeqFin.
    rewrite - H.
    simpl.
    exact (Ihl beta H0).
Defined.

Inductive Seloquent : ((nat -> nat) -> nat) -> SProp :=
| Sdial : forall
    (f : (nat -> nat) -> nat)
    (nb : @BranchingNat nat nat)
    (rel : forall (alpha : nat -> nat),
        f alpha = BNdialogue nb alpha),
       Seloquent f.

Theorem continuity {Γ : Ctx nat nat }
        (f : @Elt nat nat (@Ctx_nil nat nat) _ (((@ENat nat nat _) → (@ENat nat nat _)) → (@ENat nat nat _)))
        (fe: EElt f)
        (α : nat -> nat) : sUnit.
Proof.
  pose h := fun p => (App f EGeneric) p (@ctx_nil nat nat p).
  simpl in h.
  unfold App in h ; simpl in h.
  unfold Generic in h ; unfold EGeneric in h ; simpl in h.
  pose he := fun p => (EApp fe EGeneric) p (@ctx_nil nat nat p).
  simpl in he.
  have aux: Seloquent h.
  cbv in he.
  pose g := h α.
  pose ge := he α.
  destruct ge.
  unshelve refine (@Sdial _ _ _).
  assumption.
  Print BNDialogue_Continuity.
  cbv in q.
  
  pose grhiu := (fun (p : nat -> nat) => fe p (@ctx_nil nat nat p)
                                            (fun n ne => @Generic (@Ctx_nil nat nat) p (@ctx_nil nat nat p) n ne)
                                            (fun n ne => @EGeneric (@Ctx_nil nat nat) p (@ctx_nil nat nat p) n ne)) α.
  simpl in grhiu.
  
  
  unshelve refine (SBNDialogue_Continuity _ α).
  
  
  continuous (fun p => f.(q1) p ().(q1) p) α.
Proof.
  pose proof (@RParanat_equal nat nat (Ctx_nil nat nat) (f • RGeneric)) as Eq.
  destruct (BNDialogue_Continuity (f.(q2) (@ctx_nil nat nat α).(q1) (Generic_aux 0)) α)
    as [l ihl].
  exists l ; intros β Finite_Eq.
  simpl in Eq ; rewrite - (Eq α () ) ; rewrite - (Eq β () ).
  exact: ihl.
Qed.  
End Continuity.

Section AxiomTranslation.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Variables (X Y : Type).
Implicit Type (A B : Type).

Inductive BranchingBool :=
| Btrue_aux : BranchingBool
| Bfalse_aux : BranchingBool
| Bêtabool : (Y -> BranchingBool) -> X -> BranchingBool.

Inductive EParabool (α : X -> Y) :
  bool -> SProp :=
| EParatrue : EParabool α true
| EParafalse : EParabool α false
| EParabêtabool : forall  (bo : bool) (f : (Y -> BranchingBool)) (x : X)
                        (rel : EParabool α bo),
    EParabool α bo.

Variable alpha : X -> Y.
Variable x : X.
Lemma egrkj (a c : EParabool alpha true) : True.
Proof.
  have aux: (a = c).
  reflexivity.
  
(*In this section we present the Axiom translation,
which adds an oracle α : X -> Y in the context.
We use a Cwf-like presentation *)

(*In the axiom translation, the empty context is
translated as α : X -> Y. As such, there is no empty
context in the image of the translation, and all contexts
depend on α.*)
Definition ACtx := (X -> Y) -> Type.

(*A type is a function from contexts to Type*)
Definition ATy (Gamma : ACtx) : Type :=
  forall α : X -> Y, Gamma α -> Type.

(*Type is translated as the constant function
that always returns Type*)
Definition AType {Gamma : ACtx} : ATy Gamma :=
  fun α gamma => Type.

(*AEl is the axiom version of Tarsky's El function*)
Definition AEl (Gamma : ACtx) (A : ATy Gamma) :
  Type :=
  forall (α : X -> Y) (gamma : Gamma α), A α gamma. 

(*Empty context*)
Definition ACtx_nil : ACtx := fun α => unit.

Notation "()" := (ACtx_nil).

(*Context extension*)
Definition ACtx_cons (Gamma : ACtx) (A : ATy Gamma) :
  ACtx:=
  fun (α : X -> Y) => tsig (fun (gamma : Gamma α) => A α gamma).

Notation "Γ · A" := (@ACtx_cons Γ A) (at level 50, left associativity).

Definition Actx_cons (Gamma : ACtx) (A : ATy Gamma) (α : X -> Y)
           (gamma : Gamma α) (a : A α gamma) : (ACtx_cons A) α.
Proof.
  exists gamma ; exact a.
Defined.

Notation "γ ·· x" := (@Actx_cons γ x) (at level 50, left associativity).

(*In what follows, we deal with the negative fragment
of type theory : Weakening, Pi-types, Substitution, etc*)


(*Lifting rule*)
Definition ALift
           (Gamma : ACtx) (A : ATy Gamma) (B : ATy Gamma) :
  ATy (ACtx_cons A) :=
  fun α gamma => B α gamma.(fst).

Notation "⇑ A" := (@ALift _ _ A) (at level 10).

(*Weakening rule*)
Definition AWeaken {Γ : ACtx} {R S : ATy Γ} (r : AEl R) :
  @AEl (ACtx_cons S) (@ALift _ S R).
Proof.
  + refine (fun α γr => r α γr.(fst)).
Defined.

Notation "↑ M" := (@AWeaken _ _ _ M) (at level 10).

(*Variable rule*)
Definition AVar (Gamma : ACtx)
           (A : ATy Gamma) :
  AEl  (@ALift Gamma A A) :=
  fun (α : X -> Y) (gamma : ACtx_cons A α) => gamma.(snd).

Notation "#" := (@AVar _ _) (at level 0).

(*Substitution*)
Definition ASub (Gamma : ACtx) (A : ATy Gamma)
           (B : ATy (ACtx_cons A)) (a : AEl A) : ATy Gamma :=
  fun α gamma => B α (exist (a α gamma)).

Notation "S {{ r }} " := (ASub S r) (at level 20, left associativity).

(*Pi-type*)
Definition APi (Gamma : ACtx) (A : ATy Gamma)
           (B : ATy (ACtx_cons A)) : ATy Gamma :=
  fun α gamma =>
    forall a : A α gamma, B α (exist a).

Notation "∏" := (@APi).
Notation "A → B" := (@APi _ A (⇑ B)) (at level 99, right associativity, B at level 200).

(*Introduction for Pi*)
Definition ALam (Gamma : ACtx) (A : ATy Gamma)
           (B : ATy (ACtx_cons A))(f : AEl B)
  : AEl (APi B) := 
  fun (α : X -> Y) (gamma : Gamma α) (a : A α gamma) => f α (exist a).

Notation "'λ'" := (@ALam).

(*Introduction of the non-dependent arrow*)
Definition ALamN {Γ : ACtx } {A : ATy Γ} {B : ATy Γ}
           (f : @AEl (Γ · A) (⇑ B)) : @AEl Γ (A → B) :=
  @ALam Γ A (⇑ B) f.

(*Elimination for Pi*)
Definition AApp {Γ : ACtx}
           {A : ATy Γ} {B : ATy (@ACtx_cons Γ A)}
           (f : @AEl Γ (APi B)) (x : @AEl Γ A) : @AEl Γ (ASub B x) :=
  fun α γr => f α γr (x α γr).

Notation "t • u" := (@AApp _ _ _ t u) (at level 12, left associativity).


(*Elimination of the non-dependent arrow*)
Definition AAppN {Γ : ACtx } {A : ATy Γ} {B : ATy Γ}
           (f : @AEl Γ (A → B))(x : @AEl Γ A) : @AEl Γ B :=
  @AApp Γ A (⇑ B) f x.


Notation "t ◊ u" := (@AAppN _ _ _ t u) (at level 12, left associativity).



(*It is now time to define the translation
of various inductive types*)

(*The translation of booleans*)
Definition Abool {Gamma : ACtx} : ATy Gamma := fun α gamma => bool. 

Definition Atrue (Gamma : ACtx) : AEl (@Abool Gamma) :=
  fun α gamma => true.

Definition Afalse (Gamma : ACtx) : AEl (@Abool Gamma) :=
  fun α gamma => false.

(* The Axiom translation builds a model of full-CIC as soon as X->Y
is not empty, but the global translation will be a model of BTT.
Thus, we only define the Axiom translation for BTT*)


(*Non-dependent eliminator for booleans*)
Definition Abool_rec (Γ : ACtx) :
  @AEl Γ (∏ Γ AType ((@AVar _ AType) → (@AVar _ AType) → Abool → (@AVar _ AType))) :=
  fun α ga P ptrue pfalse b => match b with
          | true => ptrue
          | false => pfalse
         end.

(*Storage operator for booleans*)
Definition Abool_stor {Γ : ACtx} :
  AEl (Abool → (Abool → AType) → AType) :=
  fun α ga b P => @Abool_rec Γ α ga ((bool -> Type) -> Type)
                             (fun k => k true)
                             (fun k => k false)
                             b P.


(*Restricted dependent-eliminator for booleans.*)
Definition Abool_rect (Γ : ACtx) :
  AEl (∏ Γ (Abool → AType)
            ((@AApp _ Abool AType # (@Atrue _)) →
                  ((@AApp _ Abool AType # (@Afalse _)) →
                   (∏ _ Abool (Abool_stor • # • (↑#)))))) :=
  fun α gamma P ptrue pfalse b =>
    match b with
    | true => ptrue
    | false => pfalse
    end.


(*The translation of natural numbers*)
Definition Anat {Γ : ACtx} : ATy Γ := fun α gamma => nat.

Definition A0 {Γ : ACtx} : AEl (@Anat Γ) := fun α gamma => 0.

Definition ASucc {Γ : ACtx} :
  AEl (∏ Γ (@Anat Γ) (@Anat (ACtx_cons (@Anat Γ)))) :=
    fun _ _ n => S n.


(*Non-dependent eliminator for natural numbers*)
Definition Anat_rec (Γ : ACtx) :
  @AEl Γ (∏ Γ AType
            (@AVar _ AType →
             (Anat → (@AVar _ AType) → (@AVar _ AType)) →
             Anat →
             (@AVar _ AType))).
Proof.
  refine (fun α gamma P p0 pS => _).
  refine (fix f n := match n with
                       | 0 => p0
                       | S k => pS k (f k)
                       end).
Defined.


(*Storage operator for natural numbers. In direct style it amounts to :
Definition Nat_stor (n : nat) (P : nat -> Type) : Type :=
  @Nat_rec ((nat -> Type) -> Type)
            (fun P => P 0)
            (fun n P k => k (S n))
             n P.*)
Definition Anat_stor{Γ : ACtx } :
  @AEl Γ (Anat → (Anat → AType) → AType).
Proof.
  pose (r := @AApp _ _ _ (@Anat_rec Γ) ((Anat → AType) → AType)).
  let T := constr:(@AEl Γ (((Anat → AType) → AType) → (Anat → ((Anat → AType) → AType) → ((Anat → AType) → AType)) → Anat → ((Anat → AType) → AType))) in
  unshelve refine (let r : T := r in _).
  unshelve refine (AApp (AApp r _) _).
  - refine (ALamN (@AAppN _ Anat _ _ _)).
    + now refine (@AVar _ _).
    + now refine (@A0 _).
  - refine (@ALamN _ _ _ _).
    refine (@ALamN (@ACtx_cons Γ Anat) (⇑ ((Anat → AType) → AType)) (⇑ ((Anat → AType) → AType)) _).
    refine (@ALamN (Γ · Anat · (⇑ ((Anat → AType) → AType)))
                   (⇑(⇑(Anat → AType))) (⇑ (⇑ AType)) _).
    refine (@AAppN _ Anat _ _ _).
    + now refine #.
    + now refine (@AAppN _ Anat _ (@ASucc _) (↑↑#)).
Defined.
  
(*    refine (@AAppN _ (Anat → AType) _ _ _).
    + unfold AEl. now refine (↑#).
    + apply ALamN.
      refine (@AAppN _ Anat _ _ _).
      * now refine (↑#).
      * now refine (@AAppN _ Anat _ (@ASucc _) (↑↑↑#)).*)




(*Restricted dependent-eliminator for natural numbers.*)
Definition Anat_rect {Γ : ACtx} :
  @AEl Γ (∏ _ (Anat → AType)
              ((@AApp _ _ AType # A0) →
              (∏ _ Anat ((Anat_stor • # • (↑#)) →
                       (Anat_stor • (ASucc • #) • (↑#)))) →
              ∏ _ Anat (Anat_stor • # • (↑#)))).
Proof.
  unshelve refine (fun α γ P p0 pS => _).
  refine (fix g n := match n as n0 return
                                       (fix f (n : nat) : (nat -> Type) -> Type :=
                                          match n with
                                          | 0 => fun a0 : nat -> Type => a0 0
                                          | S k => fun a0 : nat -> Type => a0 (S k)
                                          end) n0 P
                                 with
                                 | 0 => p0
                                 | S k => pS k (g k)
                                 end).
Defined.

(*Our definitions "computes", that is : Anat_rect P p0 pS 0 reduces to p0
and Anat_rect P p0 pS (S k) reduces to pS k (Anat_rect P p0 pS k) *)
Goal forall (Γ : ACtx)
  (P : @AEl Γ (Anat → AType))
  (T0 := (P • (@A0 _)))
  (p0 : @AEl Γ T0)

  (TS := (∏ _ Anat ((@AApp (Γ · Anat) _ AType
                              (@AApp (Γ · Anat) _ _ Anat_stor #) (↑P)) →
                       (@AApp (Γ · Anat) _ AType
                              (@AApp (Γ · Anat) _ _ Anat_stor
                                     (@AApp (Γ · Anat) _ _ (@ASucc _) #)) (↑P)))))

  
  (pS : @AEl Γ TS)
  (TR := ∏ Γ Anat (Anat_stor • @AVar _ Anat • ↑ P))
  ,

  @AApp Γ _ _ (@AAppN Γ _ _ (@AAppN Γ T0 (TS → TR) ((@Anat_rect Γ) • P) p0) pS) (@A0 _)
  =
  p0.
Proof. reflexivity. Qed.

Goal forall (Γ : ACtx)
  (P : @AEl Γ (Anat → AType))
  (T0 := (P • (@A0 _)))
  (p0 : @AEl Γ T0)
  (TS := (∏ _ Anat ((@AApp (Γ · Anat) _ AType
                         (@AApp (Γ · Anat) _ _ Anat_stor #) (↑P)) →
                  (@AApp (Γ · Anat) _ AType
                         (@AApp (Γ · Anat) _ _ Anat_stor
                                (@AApp (Γ · Anat) _ _ (@ASucc _) #)) (↑P)))))
  (pS : @AEl Γ TS)
  (TR := ∏ Γ Anat (Anat_stor • @AVar _ Anat • ↑ P))
  (n : @AEl Γ Anat)
  ,
    forall (α : X -> Y) (γr : Γ α),
    
    (@AApp Γ _ _ (@AAppN Γ _ _ (@AAppN Γ T0 (TS → TR) ((@Anat_rect Γ) • P) p0) pS) (AApp (@ASucc _) n)) α γr
    =
    (@AAppN Γ (@AApp Γ (Anat → AType) _ (@AApp Γ _ _ Anat_stor n) P) (Anat_stor • ((@ASucc _) • n) • P) (AApp pS n) (@AApp Γ _ _ (@AAppN Γ _ _ (@AAppN Γ T0 (TS → TR) ((@Anat_rect Γ) • P) p0) pS) n)) α γr.
Proof. reflexivity. Qed.


(*Translation of equality*)
Inductive Eq {A : Type} (x : A) : A -> Type :=  Eq_refl : Eq x x.

Definition Aeq {Γ : ACtx} : AEl (∏ Γ AType (# → # → AType)) :=
  fun _ _ => @Eq.

Definition Arefl {Γ : ACtx} :
  AEl (∏ Γ AType
         (∏ _ #
            (Aeq • (↑#) ◊ # ◊ #))) :=
  fun _ _ => @Eq_refl.

(*Non-dependent eliminator for equality*)

Definition Aeq_rec (Γ : ACtx) :
  @AEl Γ (∏ Γ AType
            (∏ _ #
               (∏ (Γ · AType · (@AVar _ AType))
                  ((↑#) → AType)
                  (((@AVar (Γ · AType · (@AVar _ AType))
                          ((↑(@AVar _ AType)) → AType)) ◊ ↑#) →
                (∏ (Γ · AType · (@AVar _ AType) · ((↑#) → AType))
                   (↑↑#)
                   ((((Aeq • (↑↑↑(@AVar _ AType))) ◊ ↑↑#) ◊ #) →
                 ((↑(@AVar (Γ · AType · (@AVar _ AType))
                           ((↑(@AVar _ AType)) → AType))) ◊ #))))))) :=
  fun α γ A x P px y e =>
    match e as e0 in (Eq _ y0) return (P y0) with
    | Eq_refl _ => px
    end.

(*Storage operator for equality, in direct style*)
  
Definition Aeq_stor {Γ : ACtx} :
  @AEl Γ (∏ Γ AType
            (∏ _ #
               (∏ _ (↑#)
                  ((((Aeq • (↑↑(@AVar _ AType))) ◊ ↑#) ◊ #) →
                   (∏ _ (↑↑#)
                      ((((Aeq • (↑↑↑(@AVar _ AType))) ◊ (↑↑#)) ◊ #) →
                       AType)) →
                   AType)))) :=
  fun α γ A x y e P =>
    @Aeq_rec Γ α γ A x (fun z => ((Eq x z -> Type) -> Type))
             (fun k => k (Eq_refl x)) y e (P y).


(*
Definition dummy (Γ : ACtx) : True.
Proof.
  (*pose (rtu := (∏ (Γ · AType · (@AVar Γ AType))
                  (↑(@AVar Γ AType))
                  ((((Aeq • (↑↑(@AVar _ AType)))
                       ◊ (↑(@AVar (Γ · AType) (@AVar Γ AType))))
                      ◊ (@AVar (Γ · AType · (@AVar Γ AType)) (↑(@AVar Γ AType)))) → AType))).
  
  pose (tu := (∏ (Γ · AType · (@AVar Γ AType) ·
                    (∏ (Γ · AType · (@AVar Γ AType))
                       (↑(@AVar Γ AType))
                       ((((Aeq • (↑↑(@AVar _ AType)))
                            ◊ (↑(@AVar (Γ · AType) (@AVar Γ AType))))
                           ◊ (@AVar (Γ · AType · (@AVar Γ AType)) (↑(@AVar Γ AType)))) → AType)) · AType)
                 (@AVar (Γ · AType · (@AVar Γ AType) ·
                           (∏ (Γ · AType · (@AVar Γ AType))
                              (↑(@AVar Γ AType))
                              ((((Aeq • (↑↑(@AVar _ AType)))
                                   ◊ (↑(@AVar (Γ · AType) (@AVar Γ AType))))
                                  ◊ (@AVar (Γ · AType · (@AVar Γ AType)) (↑(@AVar Γ AType)))) → AType))) AType)
                 (∏ _
                    (↑#)
                    ((((Aeq • (↑↑(@AVar _ AType))) ◊ ↑#) ◊ #) →
                     (∏ _ (↑↑#)
                        ((((Aeq • (↑↑↑(@AVar _ AType))) ◊ (↑↑#)) ◊ #) →
                         AType)) →
                     AType)))).*)
  (*let T := constr: (Γ · AType · (@AVar Γ AType) ·
                         (∏ (Γ · AType · (@AVar Γ AType))
                            (↑(@AVar Γ AType))
                            ((((Aeq • (↑↑(@AVar _ AType)))
                                 ◊ (↑(@AVar (Γ · AType) (@AVar Γ AType))))
                                ◊ (@AVar (Γ · AType · (@AVar Γ AType)) (↑(@AVar Γ AType)))) → AType)))
  in pose (q:= (@AApp T
                   AType
                   (∏ (T · AType)
                      (@AVar T AType)
                      (∏ (T · AType · (@AVar T AType))
                         (↑(@AVar T AType))
                         (((((@Aeq (T · AType · (@AVar T AType) · (↑(@AVar T AType))))
                               • (↑↑(@AVar T AType))) ◊ ↑#) ◊ #) →
                          (∏ (T · AType · (@AVar T AType) · (↑(@AVar T AType))) (↑↑#)
                             ((((Aeq • (↑↑↑(@AVar _ AType))) ◊ (↑↑#)) ◊ #) →
                              AType)) →
                          AType)))
                   (@Aeq_stor T)
                   (↑↑(@AVar Γ AType)))).*)
  Unset Printing Notations.
  Set Printing Implicit Defensive.
  let T:= constr: ((Γ · AType · (@AVar Γ AType) ·
                         (∏ (Γ · AType · (@AVar Γ AType))
                            (↑(@AVar Γ AType))
                            ((((Aeq • (↑↑(@AVar Γ AType)))
                                 ◊ (↑(@AVar (Γ · AType) (@AVar Γ AType))))
                                ◊ (@AVar (Γ · AType · (@AVar Γ AType)) (↑(@AVar Γ AType)))) → AType))
                   · (↑↑(@AVar Γ AType)) ·
                   (((Aeq • (↑↑↑(@AVar Γ AType)))
                       ◊ (↑↑(@AVar (Γ · AType) (@AVar Γ AType))))
                      ◊ (@AVar (Γ · AType · (@AVar Γ AType) ·
                                  (∏ (Γ · AType · (@AVar Γ AType))
                                     (↑(@AVar Γ AType))
                                     ((((Aeq • (↑↑(@AVar Γ AType)))
                                          ◊ (↑(@AVar (Γ · AType) (@AVar Γ AType))))
                                         ◊ (@AVar (Γ · AType · (@AVar Γ AType)) (↑(@AVar Γ AType)))) → AType))) (↑↑(@AVar Γ AType))))))
    in pose (r := (@AApp T
                         AType
                         (∏ (T · AType)
                            (@AVar T AType)
                            (∏ (T · AType · (@AVar T AType))
                               (↑(@AVar T AType))
                               (((((@Aeq (T · AType · (@AVar T AType) · (↑(@AVar T AType))))
                                     • (↑↑(@AVar T AType))) ◊ ↑#) ◊ #) →
                                (∏ (T · AType · (@AVar T AType) · (↑(@AVar T AType))) (↑↑#)
                                   ((((Aeq • (↑↑↑(@AVar _ AType))) ◊ (↑↑#)) ◊ #) →
                                    AType)) →
                                AType)))
                         (@Aeq_stor T)
                         (↑↑↑↑(@AVar Γ AType)))).
    
  in pose (r := (@AApp T
                       (↑↑↑↑(@AVar Γ AType))
                       _
                       (@AApp T
                              AType
                              (∏ (T · AType)
                                 (@AVar T AType)
                                 (∏ (T · AType · (@AVar T AType))
                                    (↑(@AVar T AType))
                                    (((((@Aeq (T · AType · (@AVar T AType) · (↑(@AVar T AType))))
                                          • (↑↑(@AVar T AType))) ◊ ↑#) ◊ #) →
                                     (∏ (T · AType · (@AVar T AType) · (↑(@AVar T AType))) (↑↑#)
                                        ((((Aeq • (↑↑↑(@AVar _ AType))) ◊ (↑↑#)) ◊ #) →
                                         AType)) →
                                     AType)))
                              (@Aeq_stor T)
                              (↑↑↑↑(@AVar Γ AType)))
                       (↑↑↑#))).

  
  Show Proof.
  Print dummy.

  
  pose r := (@AApp (Γ · AType · (@AVar _ AType) ·
                      (∏ _ (↑#) ((((@AApp _ AType ((# → # → AType)) Aeq (↑↑(@AVar _ AType))) ◊ (↑#)) ◊ #) → AType)) ·
                      (↑↑#) ·
                      (((Aeq • (↑↑↑(@AVar _ AType))) ◊ (↑↑#)) ◊ #))
                   (↑↑↑↑(@AVar _ AType)) _
                   (@AApp _ AType _
                          Aeq_stor (↑↑↑↑(@AVar _ AType)))
                   (↑↑↑#)).

  
(*Definition dummy (Γ : ACtx) :=  (@AApp (Γ · AType · (@AVar _ AType) ·
                                          (∏ _ (↑#) ((((@AApp _ AType ((# → # → AType)) Aeq (↑↑(@AVar _ AType))) ◊ (↑#)) ◊ #) → AType)) ·
                                          (↑↑#) ·
                                          (((Aeq • (↑↑↑(@AVar _ AType))) ◊ (↑↑#)) ◊ #))
                                       (↑↑↑↑(@AVar _ AType)) _
                                       (@AApp _ AType _
                                              Aeq_stor (↑↑↑↑(@AVar _ AType)))
                                       (↑↑↑#)).*)


Definition Aeq_rect (Γ : ACtx) :
  @AEl Γ (∏ Γ AType
            (∏ _ #
               (∏ _ (∏ _ (↑#) ((((Aeq • (↑↑(@AVar _ AType))) ◊ (↑#)) ◊ #) →
                           AType))
                  (((@AApp (Γ · AType · (@AVar _ AType) · (∏ _ (↑#) ((((Aeq • (↑↑(@AVar _ AType))) ◊ (↑#)) ◊ #) → AType)))
                           (↑↑#) ((((Aeq • (↑↑↑(@AVar _ AType))) ◊ (↑↑#)) ◊ #) → AType) # (↑#)) ◊
                                                                                                (@AApp (Γ · AType · (@AVar _ AType) · (∏ _ (↑#) ((((Aeq • (↑↑(@AVar _ AType))) ◊ (↑#)) ◊ #) → AType))) (↑ (↑ #)) (Aeq • ↑ (↑ (↑ #)) ◊ # ◊ #) (Arefl • (↑↑#)) (↑#))) →
                   ∏ (Γ · AType · (@AVar _ AType) · (∏ _ (↑#) ((((Aeq • (↑↑(@AVar _ AType))) ◊ (↑#)) ◊ #) → AType)))
                     (↑↑#)
                     (∏ _ (((Aeq • (↑↑↑(@AVar _ AType))) ◊ (↑↑#)) ◊ #)
                        ((@AApp _ (↑↑↑↑(@AVar _ AType)) _
                                (@AApp (Γ · AType · (@AVar _ AType) ·
                                          (∏ _ (↑#) ((((@AApp _ AType ((# → # → AType)) Aeq (↑↑(@AVar _ AType))) ◊ (↑#)) ◊ #) → AType)) ·
                                          (↑↑#) ·
                                          (((Aeq • (↑↑↑(@AVar _ AType))) ◊ (↑↑#)) ◊ #))
                                       (↑↑↑↑(@AVar _ AType)) _
                                       (@AApp _ AType _
                                              Aeq_stor (↑↑↑↑(@AVar _ AType)))
                                       (↑↑↑#))
                                (↑#))
                           ◊ # ◊ (↑↑#))))))).
 *)


(*Translation of lists*)

Inductive list (A : Type) : Type :=
  nil : list A | cons : A -> list A -> list A.
Arguments nil {A}.
Definition Alist {Γ : ACtx} : @AEl Γ (AType → AType) :=
  fun α gamma a => list a.

Definition Anil {Γ : ACtx} :
  AEl (∏ Γ AType (Alist • (@AVar _ AType))) := fun α ga A => @nil A.


Definition Acons {Γ : ACtx} :
  AEl (∏ Γ AType
         ((@AVar _ AType) → (Alist • (@AVar _ AType)) → (Alist • (@AVar _ AType)))) :=
  fun α gamma A a la => cons a la.


(*Non-dependent eliminator for lists*)
Definition Alist_rec (Γ : ACtx) :
  @AEl Γ (∏ Γ AType
            (∏ _ AType
               (# →
                (↑ # → Alist • ↑ # → # → #) →
                Alist • ↑ # →
                #))).
Proof.
  unshelve refine (fun alpha gamma A P pnil pcons => _). 
  refine (fix f l {struct l} := match l with
                     | nil => pnil
                     | cons x q => pcons x q (f q)
                     end).
Defined.

(*Storage operator for lists*)
Definition Alist_stor{Γ : ACtx } :
  AEl (∏ Γ AType (Alist • # → (Alist • # → AType) → AType)) :=
  fun (α : X -> Y) (_ : Γ α) (A : Type) =>
       fix f (l : list A) : (list A -> Type) -> Type :=
         match l with
         | nil => fun a0 : list A -> Type => a0 nil
         | cons x q =>
             fun P : list A -> Type =>
             f q (fun q' : list A => P (cons x q'))
         end.

(*Proof.
  let T1 := constr: (∏ _ (@AType (Γ · AType · AType)) (# →
                                               (↑ # → Alist • ↑ # → # → #) →
                                               Alist • ↑ # →
                                               #)) in
  let T := constr: ((@AVar (Γ · AType) AType ) → ((↑ # → Alist • ↑ # → # → #) → Alist • ↑ # → #)) in
  pose (r := @AApp (Γ · AType) _ T (@AApp (Γ · AType) _ T1 (↑ (@Alist_rec Γ)) (@AVar _ AType)) ((((@Alist (Γ · AType)) • (@AVar Γ AType)) → (@AType (Γ · AType))) → (@AType (Γ · AType)))).
  let T := constr: (@AEl (Γ · AType) ((((Alist • #) → AType) → AType) → (# → (Alist • #) → (((Alist • #) → AType) → AType) → (((Alist • #) → AType) → AType)) → (Alist • #) → (((Alist • #) → AType) → AType))) in
  unshelve refine (let r : T := r in _).
  unshelve refine (ALam _).
  unshelve refine (AApp (AApp r _) _).
  - unshelve refine (@ALam _ (Alist • # → AType) _ _).
    unshelve refine (@AAppN _ (Alist • ↑ #) (⇑ AType) _ _).
    + now refine (@AVar _ _).
    + now refine (@Anil _ _).
  - refine (@ALamN _ _ _ _).
    refine (@ALamN _ (⇑ (Alist • #))
                   (⇑ (((Alist • # → AType) → AType) → ((Alist • # → AType) → AType))) _).
    refine (@ALamN _ (⇑ ⇑ ((Alist • # → AType) → AType)) (⇑ ⇑ ((Alist • # → AType) → AType)) _).
    refine (@ALamN _ (⇑ ⇑ ⇑ ((Alist • # → AType))) (⇑ ⇑ ⇑ AType) _).
    refine (@AApp _ ((Alist • ↑ ↑ ↑ ↑ #) → AType) (⇑ (⇑ (⇑ (⇑ AType)))) _ _).
    + now refine (↑#).
    + apply ALamN.
      refine (@AAppN _ (Alist • ↑ ↑ ↑ ↑ ↑ #) _ _ _).
      * now refine (↑#).
      * refine (@AAppN _ (Alist • ↑ ↑ ↑ ↑ ↑ #) _ _ _).
        refine (@AAppN _ (↑ ↑ ↑ ↑ ↑ #) _ _ _).
        refine (Acons • (↑ ↑ ↑ ↑ ↑ #)).
        refine (↑ ↑ ↑ ↑  #).
        exact #.
Defined.  *)

(*Restricted dependent-eliminator for lists.*)
Definition Alist_rect {Γ : ACtx} :
  @AEl Γ (∏ Γ AType
            (∏ _ ((Alist • (@AVar _ AType)) → AType)
               ((@AAppN _ _ AType # (Anil • (↑(@AVar _ AType)))) →
                 (∏ _ (↑(@AVar _ AType))
                    (∏ (Γ · AType · ((Alist • (@AVar _ AType)) → AType) · (↑(@AVar _ AType)))
                       (Alist • (↑↑(@AVar _ AType)))
                       ((((Alist_stor • (↑↑↑(@AVar _ AType)))
                            ◊ (@AVar _ ((Alist • (↑↑(@AVar _ AType)))))
                            ◊ (↑↑(@AVar _ ((Alist • (@AVar _ AType)) → AType))))) →
                        (((Alist_stor • (↑↑↑(@AVar _ AType)))
                            ◊ ((Acons • (↑↑↑(@AVar _ AType))) ◊ (↑#) ◊ #)
                            ◊ (↑↑(@AVar _ ((Alist • (@AVar _ AType)) → AType)))))))) →
                ∏ (Γ · AType · ((Alist • (@AVar _ AType)) → AType)) (Alist • ↑#)
                  ((Alist_stor • (↑↑(@AVar _ AType))) ◊ # ◊ (↑#))))) :=
  fun alpha gamma A P pnil pcons =>
    fix f l := match l as l0 return
                     (fix g (l : list A) : (list A -> Type) -> Type :=
                        match l with
                        | nil => fun a0 : list A -> Type => a0 nil
                        | cons x q =>
                          fun P : list A -> Type =>
                            g q (fun q' : list A => P (cons x q'))
                        end) l0 P
               with
               | nil => pnil
               | cons x q => pcons x q (f q)
               end.
(*Proof.
  cbv.
  unshelve refine (fun alpha gamma A P pnil pcons => _).
  refine (fix f l := match l as l0 return
                           (fix g (l : list A) : (list A -> Type) -> Type :=
                              match l with
                              | nil => fun a0 : list A -> Type => a0 nil
                              | (x :: q)%list =>
                                fun a0 : list A -> Type =>
                                  g q (fun a1 : list A => a0 (x :: a1)%list)
                              end) l0 P
                     with
                     | nil => pnil
                     | cons x q => pcons x q (f q)
                     end).*)


End AxiomTranslation.



Section BranchingTranslation.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Variables (X Y : Type).


(*In this section we present the Branching translation,
which interprets all types as algebras of the Dialogue monad.
Again, we use a Cwf-like presentation *)

(*Let us first formalize what is an algebra of the Dialogue monad.*)
Definition BranchingTYPE := {A : Type & (Y -> A) -> X -> A}.

(*In the Branching translation, contexts are translated pointwise.
As such, they are simply types.*)
Definition BCtx := Type.

(*A type is a function from contexts to BranchingType*)
Definition BTy (Gamma : BCtx) := Gamma -> BranchingTYPE.

(*Type is translated as the constant function that always return
BranchingTYPE. The proof of algebraicity is a dummy function.*)
Definition Bunit : BranchingTYPE :=
  @exist Type (fun A =>  (Y -> A) -> X -> A) unit (fun _ _ => tt).

Definition BType {Gamma : BCtx} : BTy Gamma :=
  fun _ => @exist Type (fun A =>  (Y -> A) -> X -> A)
                  BranchingTYPE (fun _ _ => Bunit).


(*BEl is the branching version of Tarsky's El function*)
Definition BEl {Gamma : BCtx} (A : BTy Gamma) : Type :=
  forall (gamma : Gamma), (A gamma).(fst). 

(*Empty context*)
Definition BCtx_nil : BCtx := unit.

Notation "()" := (BCtx_nil).

(*Context extension*)
Definition BCtx_cons (Gamma : BCtx) (A : BTy Gamma) :=
  tsig (fun (gamma : Gamma) => (A gamma).(fst)).

Notation "Γ · A" := (@BCtx_cons Γ A) (at level 50, left associativity).


Definition Bctx_cons (Gamma : BCtx) (B : BTy Gamma)
           (gamma : Gamma) (b : (B gamma).(fst)) : Gamma · B.
Proof.
  exists gamma ; exact b.
Defined.

Notation "γ ·· x" := (@Actx_cons γ x) (at level 50, left associativity).


(*In what follows, we deal with the negative fragment
of type theory : Weakening, Pi-types, Substitution, etc*)

(*Lifting rule*)
Definition BLift {Gamma : BCtx} {A : BTy Gamma} (B : BTy Gamma) : BTy (Gamma · A) :=
  fun gamma => B gamma.(fst).

Notation "⇑ A" := (@BLift _ _ A) (at level 10).

(*Weakening rule*)
Definition BWeaken {Γ : BCtx} {R S : BTy Γ} (r : BEl R) :
  @BEl (Γ · S) (⇑ R).
Proof.
  + refine (fun γr => r γr.(fst)).
Defined.

Notation "↑ M" := (@BWeaken _ _ _ M) (at level 10).

(*Variable rule*)
Definition BVar {Gamma : BCtx}
           (A : BTy Gamma) :
  BEl (@BLift Gamma A A) :=
  fun (gamma : (Gamma · A)) => gamma.(snd).

Notation "#" := (@BVar _ _) (at level 0).
 
(*Substitution*)
Definition BSub {Gamma : BCtx} (A : BTy Gamma) (B : BTy (Gamma · A)) (a : BEl A) : BTy Gamma :=
  fun gamma => B ({|fst := gamma ; snd := a gamma |}).

Notation "S {{ r }} " := (@BSub _ _ S r) (at level 20, left associativity).


(*Pi-type*)
Definition BPi {Gamma : BCtx} {A : BTy Gamma} (B : BTy (Gamma · A)) : BTy Gamma.
Proof.
  unshelve refine (fun gamma => exist _). 
  - exact (forall a : (A gamma).(fst),
             (B {| fst := gamma; snd := a|}).(fst)).
  - exact (fun f x a => (B {| fst := gamma; snd := a |}).(snd) (fun y => f y a) x).
Defined.

Notation "∏" := (@BPi).
Notation "A → B" := (@BPi _ A (⇑ B)) (at level 99, right associativity, B at level 200).

(*Introduction for Pi*)
Definition BLam {Gamma : BCtx} (A : BTy Gamma) (B : BTy (Gamma · A)) (f : BEl B)
  : BEl (BPi B) := 
  fun (gamma : Gamma) (a : (A gamma).(fst)) => f {| fst := gamma; snd := a |}.

Notation "'λ'" := (@ALam).

(*Introduction of the non-dependent arrow*)
Definition BLamN {Γ : BCtx } {A : BTy Γ} {B : BTy Γ}
           (f : @BEl (Γ · A) (⇑ B)) : @BEl Γ (A → B) :=
  @BLam Γ A (⇑ B) f.

(*Elimination for Pi*)

Definition BApp {Γ : BCtx} {A : BTy Γ} {B : BTy (Γ · A)}
           (f : BEl (∏ Γ A B)) (x : BEl A) : @BEl _ (B {{ x }}) :=
  fun gamma => f gamma (x gamma).

Notation "t • u" := (@BApp _ _ _ t u) (at level 12, left associativity).

(*Elimination of the non-dependent arrow*)
Definition BAppN {Γ : BCtx } {A : BTy Γ} {B : BTy Γ}
           (f : @BEl Γ (A → B)) (x : @BEl Γ A) : @BEl Γ B :=
  @BApp Γ A (⇑ B) f x.


Notation "t ◊ u" := (@BAppN _ _ _ t u) (at level 12, left associativity).


Definition BEL {Γ : BCtx } (A : @BEl Γ BType) : BTy Γ := A.

Notation "[ A ]" := (@BEL _ A). 
(*It is now time to define the translation
of various inductive types*)


(*The translation of booleans*)
Inductive BranchingBool :=
| Btrue_aux : BranchingBool
| Bfalse_aux : BranchingBool
| Bêtabool : (Y -> BranchingBool) -> X -> BranchingBool.


Definition Bbool {Gamma : BCtx} : BTy Gamma :=
  fun gamma => {| fst := BranchingBool; snd := Bêtabool |}.

Definition Btrue {Gamma : BCtx} : BEl (@Bbool Gamma) := (fun _ => Btrue_aux).

Definition Bfalse {Gamma : BCtx} : BEl (@Bbool Gamma) := (fun _ => Bfalse_aux ).


(*Non-dependent eliminator for booleans*)
Definition Bbool_rec {Γ : BCtx} :
  @BEl Γ (∏ Γ BType ((@BVar _ BType) → (@BVar _ BType) → Bbool → (@BVar _ BType))).
Proof.
  unshelve refine (fun ga P ptrue pfalse => _).
  now refine (fix f b := match b with
                         | Btrue_aux  => ptrue
                         | Bfalse_aux  => pfalse
                         | Bêtabool g x => P.(snd) (fun y => f (g y)) x
                         end).
Defined.

(*Storage operator for booleans*)
Definition Bbool_stor {Gamma : BCtx} :
  @BEl Gamma (Bbool → (Bbool → BType) → BType).
Proof.
  exact (fun ga b P => @Bbool_rec Gamma ga
                                  (@BPi Gamma (@BPi Gamma (@Bbool Gamma) (@BType _))
                                        (@BType _) ga)
                                  (fun k => k Btrue_aux)
                                  (fun k => k Bfalse_aux) b P).
Defined.

(*Restricted dependent-eliminator for booleans.*)
Definition Bbool_rect (Γ : BCtx) :
  @BEl Γ (∏ _ (Bbool → BType) (
    (@BApp (Γ · (Bbool → BType)) Bbool BType # Btrue) →
    (@BApp (Γ · (Bbool → BType)) Bbool BType # Bfalse) →
    ∏ _ Bbool (@BApp _  _ BType
                      (@BApp (Γ · (Bbool → BType) · Bbool) _ _ Bbool_stor #) (↑#)))) :=
  fun ga P ptrue pfalse b =>
    match b with
    | Btrue_aux => ptrue
    | Bfalse_aux => pfalse
    | Bêtabool g x => tt
    end.



(*The translation of natural numbers*)

Definition Bnat {Gamma : BCtx} : BTy Gamma :=
  fun gamma => {| fst := BranchingNat X Y ; snd := @Bêtanat X Y |}.

Definition Bzero {Gamma : BCtx} : BEl (@Bnat Gamma) := fun gamma => @B0 X Y. 

Definition BSucc {Gamma : BCtx} :
  @BEl Gamma (Bnat → Bnat) :=
  fun gamma => (@BS X Y).

(*Non-dependent eliminator for natural numbers*)
Definition Bnat_rec (Γ : BCtx) :
  BEl (∏ Γ BType ([#] →
                  (Bnat → [#] → [#]) →
                  Bnat →
                  [#])).
Proof.
  - unshelve refine (fun gamma P p0 pS => _).
    exact (fix f n := match n with
                       | B0 _ _ => p0
                       | BS k => pS k (f k)
                       | Bêtanat g x => P.(snd) (fun y => f (g y)) x
                       end).
Defined.


(*Storage operator for natural numbers*)
Definition Bnat_stor {Γ : BCtx} :
  @BEl Γ (Bnat → (Bnat → BType) → BType).
Proof.
  pose (r := BApp (@Bnat_rec Γ) ((Bnat → BType) → BType)).
  let T := constr:(@BEl Γ (((Bnat → BType) → BType) → (Bnat → ((Bnat → BType) → BType) → ((Bnat → BType) → BType)) → Bnat → ((Bnat → BType) → BType))) in
  unshelve refine (let r : T := r in _).
  unshelve refine (BAppN (BAppN r _) _).
  - refine (BLamN (@BAppN _ Bnat _ _ _)).
    + now refine #.
    + now refine Bzero.
  - refine (@BLamN _ _ _ _).
    refine (@BLamN _ (⇑ ((Bnat → BType) → BType)) (⇑ ((Bnat → BType) → BType)) _).
    refine (@BLamN _ (⇑(⇑(Bnat → BType))) (⇑ (⇑ BType)) _).
    refine (@BAppN _ Bnat _ _ _).
    + now refine #.
    + now refine (@BAppN _ Bnat _ BSucc (↑↑#)).
Defined.


(*Restricted dependent-eliminator for natural numbers*)
Definition Bnat_rect {Γ : BCtx} :
  BEl (∏ Γ (Bnat → BType) (
           (@BApp (Γ · (Bnat → BType)) Bnat BType # Bzero) →
           (∏ _ Bnat ((Bnat_stor • # • (↑#)) →
                       (Bnat_stor • (BSucc • #) • (↑#)))) →
              ∏ _ Bnat (Bnat_stor • # • (↑#)))).
Proof.
    unshelve refine (fun γ P p0 pS => _).
    exact (fix g n :=
              match n with
              | B0 _ _ => p0
              | BS k => pS k (g k)
              | Bêtanat _ _ => tt
              end).
Defined.


Inductive BranchingEq (A : BranchingTYPE) (a : A.(fst)) : A.(fst) -> Type :=
| Beq_refl : BranchingEq a a
| Bêtaeq : forall (b : A.(fst)), (Y -> BranchingEq a b) -> X -> BranchingEq a b.
 
Definition Beq {Gamma : BCtx}: BEl (∏ Gamma BType ([#] → [#] → BType)).
Proof.
  unshelve refine (fun gamma A x y => exist _).
  - exact (@BranchingEq A x y).
  - exact (@Bêtaeq A x y).
Defined.

Definition Brefl {Γ : BCtx} :
  BEl (∏ Γ BType
         (∏ _ [#]
            [Beq • (↑#) ◊ # ◊ #])) :=
  fun _  => @Beq_refl.

(*Non-dependent eliminator for equality*)

Definition Beq_rec (Γ : BCtx) :
  @BEl Γ (∏ Γ BType
            (∏ _ [#]
               (∏ (Γ · BType · (@BVar _ BType))
                  ([↑#] → BType)
                  ([(@BVar (Γ · BType · (@BVar _ BType))
                          ((↑(@BVar _ BType)) → BType)) ◊ ↑#] →
                (∏ (Γ · BType · (@BVar _ BType) · ([↑#] → BType))
                   (↑↑#)
                   ([((Beq • (↑↑↑(@BVar _ BType))) ◊ ↑↑#) ◊ #] →
                 [(↑(@BVar (Γ · BType · (@BVar _ BType))
                           ((↑(@BVar _ BType)) → BType))) ◊ #])))))):=
  fun γ B x P px =>
    fix g y e :=
    match e as e0 in (BranchingEq _ y0) return (P y0).(fst) with
    | Beq_refl _ => px
    | @Bêtaeq _ _ b f x => (P b).(snd) (fun z => g b (f z)) x
    end.
(*Storage operator for equality, in direct style*)
  
Definition Beq_stor {Γ : BCtx} :
  @BEl Γ (∏ Γ BType
            (∏ _ [#]
               (∏ _ [↑#]
                  ([((Beq • (↑↑(@BVar _ BType))) ◊ ↑#) ◊ #] →
                   (∏ _ [↑↑#]
                      ([((Beq • (↑↑↑(@BVar _ BType))) ◊ (↑↑#)) ◊ #] →
                       BType)) →
                   BType)))) :=
  fun γ B x y e P =>
    match e in (BranchingEq _ b)
          return ((BranchingEq x b -> BranchingTYPE) -> BranchingTYPE)
    with
    | Beq_refl _ => fun k : BranchingEq x x -> BranchingTYPE => k (Beq_refl x)
    | @Bêtaeq _ _ _ _ _ => fun _ => Bunit
    end (P y).

  
Inductive BranchingList {A : BranchingTYPE} : Type :=
| Bnil_aux : BranchingList
| Bcons_aux : A.(fst)  -> BranchingList -> BranchingList
| Bêtalist : (Y -> BranchingList) -> X -> BranchingList.


Definition Blist {Gamma : BCtx} : @BEl Gamma (BType → BType) :=
  fun _ A => {| fst := @BranchingList A ; snd := @Bêtalist A |}.



Definition Bnil {Γ : BCtx} : BEl (∏ Γ BType (Blist • #)) :=
  fun _ B => @Bnil_aux B.


Definition Bcons {Γ : BCtx} :
  BEl (∏ Γ BType ([#] → (Blist • #) → (Blist • #))) :=
  fun gamma A x l => Bcons_aux x l.


(*Non-dependent eliminator for lists*)
Definition Blist_rec (Γ : BCtx) :
  @BEl Γ (∏ Γ BType
            (∏ _ BType
               ([#] →
                ([↑#] → Blist • ↑ # → [#] → [#]) →
                Blist • ↑ # →
                [#]))).
Proof.
  unshelve refine (fun gamma B P pnil pcons => _). 
  refine (fix f l {struct l} := match l with
                     | Bnil_aux => pnil
                     | Bcons_aux x q => pcons x q (f q)
                     | Bêtalist g x => P.(snd) (fun y => f (g y)) x
                     end).
Defined.


(*Storage operator for lists*)
Definition Blist_stor{Γ : BCtx } :
  BEl (∏ Γ BType (Blist • # → (Blist • # → BType) → BType)) :=
  fun (_ : Γ) B =>
       fix f (l : @BranchingList B) : (@BranchingList B -> BranchingTYPE) -> BranchingTYPE :=
         match l with
         | Bnil_aux => fun P : @BranchingList B -> BranchingTYPE => P (Bnil_aux)
         | Bcons_aux x q =>
             fun P : @BranchingList B -> BranchingTYPE =>
               f q (fun l' : @BranchingList B => P (Bcons_aux x l'))
         | Bêtalist g x => fun P : @BranchingList B -> BranchingTYPE => Bunit
         end.
(*Proof.
  let T1 := constr: (∏ _ (@BType (Γ · BType · BType)) ((@BVar _ BType) →
                                               (↑ (@BVar _ BType) → Blist • ↑ # → (@BVar _ BType) → (@BVar _ BType)) →
                                               Blist • ↑ # →
                                               (@BVar _ BType))) in
  let T := constr: ((@BVar (Γ · BType) BType ) → ((↑ (@BVar _ BType) → Blist • ↑ # → (@BVar _ BType) → (@BVar _ BType)) → Blist • ↑ # → (@BVar _ BType))) in
  pose (r := @BApp (Γ · BType) _ T (@BApp (Γ · BType) _ T1 (↑ (@Blist_rec Γ)) (@BVar _ BType)) ((((@Blist (Γ · BType)) • (@BVar Γ BType)) → (@BType (Γ · BType))) → (@BType (Γ · BType)))).
  let T := constr: (@BEl (Γ · BType) ((((Blist • #) → BType) → BType) → ((@BVar _ BType) → (Blist • #) → (((Blist • #) → BType) → BType) → (((Blist • #) → BType) → BType)) → (Blist • #) → (((Blist • #) → BType) → BType))) in
  unshelve refine (let r : T := r in _).
  unshelve refine (BLam _).
  unshelve refine (BApp (BApp r _) _).
  - unshelve refine (@BLam _ (Blist • # → BType) _ _).
    unshelve refine (@BAppN _ (Blist • ↑ #) (⇑ BType) _ _).
    + now refine (@BVar _ _).
    + now refine (@Bnil _ _).
  - refine (@BLamN _ _ _ _).
    refine (@BLamN _ (⇑ (Blist • #))
                   (⇑ (((Blist • # → BType) → BType) → ((Blist • # → BType) → BType))) _).
    refine (@BLamN _ (⇑ ⇑ ((Blist • # → BType) → BType)) (⇑ ⇑ ((Blist • # → BType) → BType)) _).
    refine (@BLamN _ (⇑ ⇑ ⇑ ((Blist • # → BType))) (⇑ ⇑ ⇑ BType) _).
    refine (@BApp _ ((Blist • ↑ ↑ ↑ ↑ #) → BType) (⇑ (⇑ (⇑ (⇑ BType)))) _ _).
    + now refine (↑#).
    + apply BLamN.
      refine (@BAppN _ (Blist • ↑ ↑ ↑ ↑ ↑ #) _ _ _).
      * now refine (↑#).
      * refine (@BAppN _ (Blist • ↑ ↑ ↑ ↑ ↑ #) _ _ _).
        refine (@BAppN _ [↑ ↑ ↑ ↑ ↑ #] _ _ _).
        refine (Bcons • (↑ ↑ ↑ ↑ ↑ #)).
        refine (↑ ↑ ↑ ↑  #).
        exact #.
Defined.*)


(*Restricted dependent-eliminator for lists.*)
Definition Blist_rect {Γ : BCtx} :
  @BEl Γ (∏ Γ BType
            (∏ _ ((Blist • #) → BType)
               ((@BAppN _ _ BType # (Bnil • (↑#))) →
                 (∏ _ [↑(@BVar _ BType)]
                    (∏ (Γ · BType · ((Blist • #) → BType) · (↑#))
                       (Blist • (↑↑(@BVar _ BType)))
                       (([(Blist_stor • (↑↑↑(@BVar _ BType)))
                            ◊ (@BVar _ ((Blist • (↑↑#))))
                            ◊ (↑↑(@BVar _ ((Blist • #) → BType)))]) →
                        ([(Blist_stor • (↑↑↑(@BVar _ BType)))
                            ◊ ((Bcons • (↑↑↑(@BVar _ BType))) ◊ (↑#) ◊ #)
                            ◊ (↑↑(@BVar _ ((Blist • #) → BType)))])))) →
                ∏ (Γ · BType · ((Blist • #) → BType)) (Blist • ↑#)
                  [(Blist_stor • (↑↑(@BVar _ BType))) ◊ # ◊ (↑#)]))) :=
  fun _ B P pnil pcons =>
    fix f l := match l as l0 return
                     ((fix g (l : @BranchingList B) : (@BranchingList B -> BranchingTYPE) -> BranchingTYPE :=
                         match l with
                         | Bnil_aux => fun P => P (Bnil_aux)
                         | Bcons_aux x q =>
                           fun P =>
                             g q (fun l' => P (Bcons_aux x l'))
                         | Bêtalist g x => fun P => Bunit
                         end) l0 P).(fst)
               with
               | Bnil_aux => pnil
               | Bcons_aux x q => pcons x q (f q)
               | Bêtalist _ _ => tt
               end.


(*Proof.
  unshelve refine (fun gamma B P pnil pcons => _).
  refine (fix f l := match l as l0 return
                           ((fix g (l : @BranchingList B) : (@BranchingList B -> BranchingTYPE) -> BranchingTYPE :=
                              match l with
                              | Bnil_aux => fun P => P (Bnil_aux)
                              | Bcons_aux x q =>
                                fun P =>
                                  g q (fun l' => P (Bcons_aux x l'))
                              | Bêtalist g x => fun P => Bunit
                              end) l0 P).(fst)
                     with
                     | Bnil_aux => pnil
                     | Bcons_aux x q => pcons x q (f q)
                     | Bêtalist _ _ => tt
                     end).*)


End BranchingTranslation.


Section Parametricity.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Variables (X Y : Type).
Definition ℙ := X -> Y.
Notation "'ACtx'" := (ACtx X Y).
Notation "'ACtx_nil'" := (@ACtx_nil X Y).
Notation "'BTy'" := (BTy X Y).

Definition ECtx (Γa : ACtx) :=
  forall (α : ℙ),  Γa α -> SProp.


Definition ETy (Γa : ACtx) (Γe : ECtx Γa)
           (A : ATy Γa) :=
  forall (α : ℙ) (γa : Γa α) (γe : Γe α γa),
    (A α γa) -> SProp.



Definition EElt (Γa : ACtx) (Γe : ECtx Γa)
           (Ra : ATy Γa) (Re : ETy Γe Ra)
           (ra : AEl Ra)  :=
  forall (α : ℙ) (γa : Γa α) (γe : Γe α γa),
    (Re α γa γe) (ra α γa).
Check fun (A:Type) (B:A -> SProp) => (forall x:A, B x) : SProp.

Definition Ue {Γa : ACtx}{Γe : ECtx Γa} :
  ETy Γe (@AType _ _ _).
Proof.
  cbv.
  unshelve refine (fun α γa γe => _).
  + unshelve refine (fun A => _).
    Check ((forall x: A, SProp): SProp).
      * exact (fun Te => forall (a : A) (f : Y -> B.(fst)) (x : X) (fe : Te a (f (α x))),
                Te a ((B.(snd) f x))).
  + intros A f x fe.
    unshelve refine (exist _).
    * exact (fun _ _ => unit).
    * exact (fun _ _ _ _ => tt).
Defined.

Definition ETy_inst (Γa : ACtx) (Γb : BCtx) (Γe : ECtx Γa Γb)
           (Ra : ATy Γa) (Rb : BTy Γb) (Re : ETy Γe Ra Rb)
           (α : ℙ) (γa : Γa α) (γb : Γb) (γe : Γe α γa γb)
           (ra : Ra α γa) (rb : (Rb γb).(fst)) :=
  (Re α γa γb γe).(fst) ra rb.

Definition ECtx_nil : ECtx ACtx_nil (BCtx_nil) :=
  fun _ _ _ => unit.


Definition Ectx_nil {α : ℙ} : @ECtx_nil α tt tt := tt.


Notation "()" := Ectx_nil.

Print BCtx_cons.
Definition ECtx_cons (Γa : ACtx) (Γb : BCtx) (Γe : ECtx Γa Γb)
           (Ra : ATy Γa) (Rb : BTy Γb) (Re : ETy Γe Ra Rb) :
  ECtx (ACtx_cons Ra) (BCtx_cons Rb).
Proof.
  unshelve refine (fun α γa γb => _).
  apply (@tsig (Γe α γa.(fst) γb.(fst))).
  intro γe.
  exact ((Re α _ _ γe).(fst) γa.(snd) γb.(snd)).
Defined.


Notation "Γe · Re" := (@ECtx_cons _ _ Γe _ _ Re) (at level 50, left associativity).


Definition Ectx_cons (Γa : ACtx) (Γb : BCtx) (Γe : ECtx Γa Γb)
           (Ra : ATy Γa) (Rb : BTy Γb) (Re : ETy Γe Ra Rb)
           (α : ℙ) (γa : Γa α) (γb : Γb) (γe : Γe α γa γb)
           (ra : Ra α γa) (rb : (Rb γb).(fst)) (re : ETy_inst Re γe ra rb) :
  (Γe · Re) (Actx_cons ra) (Bctx_cons rb) :=
  {| fst := γe; snd := re |}.


Notation "γe ·· xe" := (@Ectx_cons _ _ _ _ _ _ _ _ _ γe _ _ xe) (at level 50, left associativity).


(*Definition Ctx_tl {Γ : Ctx} {R : Ty Γ} {α : ℙ}
           (γ : Ctx_El (Γ · R) α) : Ctx_El Γ α.
Proof.
  exists γ.(q1).(fst) γ.(q2).(fst).
  exact (γ.(q3).(fst)).
Defined.


Notation "¿ g" := (Ctx_tl g) (at level 10).
*)

Definition ELift (Γa : ACtx) (Γb : BCtx) (Γe : ECtx Γa Γb)
           (Ra : ATy Γa) (Rb : BTy Γb) (Re : ETy Γe Ra Rb)
           (Sa : ATy Γa) (Sb : BTy Γb) (Se : ETy Γe Sa Sb)
  : ETy (Γe · Re) (@ALift X Y _ Ra Sa) (@BLift X Y _ Rb Sb) :=
  fun α γa γb γe => Se α γa.(fst) γb.(fst) γe.(fst).
