Set Primitive Projections.
Set Universe Polymorphism.
Require Import Dialogue.
Require Import Base.
Require Import ssreflect.


Section AxiomMonad.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Printing Universes.
Variables (X Y : Set).
Implicit Type (A B : Type).


Definition ACtx@{i} := (X -> Y) -> Type@{i}.

Definition ATy@{i j | i < j} (Gamma : ACtx@{i}) : Type@{j} :=
  forall α : X -> Y, Gamma α -> Type@{i}.

Definition AType@{i j k} {Gamma : ACtx@{j}} : ATy@{j k} Gamma :=
  fun α gamma => Type@{i}.

Definition AEl@{i j | i < j} (Gamma : ACtx@{i}) (A : ATy@{i j} Gamma) :
  Type@{j} :=
  forall (α : X -> Y) (gamma : Gamma α), A α gamma. 

Definition ACtx_nil : ACtx := fun α => unit.

Definition ACtx_cons@{i j | i < j} (Gamma : ACtx@{i}) (A : ATy@{i j} Gamma) :
  ACtx@{i}:=
  fun (α : X -> Y) => tsig (fun (gamma : Gamma α) => A α gamma).

Definition Actx_cons@{i j | i < j} (Gamma : ACtx@{i}) (A : ATy@{i j} Gamma) (α : X -> Y)
           (gamma : Gamma α) (a : A α gamma) : (ACtx_cons A) α.
Proof.
  exists gamma ; exact a.
Defined.

Definition Aweak@{i j | i < j}
           (Gamma : ACtx@{i}) (A : ATy@{i j} Gamma) (B : ATy@{i j} Gamma) :
  ATy@{i j} (ACtx_cons@{i j} A) :=
  fun α gamma => B α gamma.(fst).

Definition AweakEl@{i j | i < j} (Gamma : ACtx@{i})
           (A : ATy@{i j} Gamma) :
  AEl@{i j}  (@Aweak@{i j} Gamma A A) :=
  fun (α : X -> Y) (gamma : ACtx_cons A α) => gamma.(snd).

Definition Aweaken {Γ : ACtx} {R S : ATy Γ} (r : AEl R) :
  @AEl (ACtx_cons S) (@Aweak _ S R).
Proof.
  + refine (fun α γr => r α γr.(fst)).
Defined.

Definition ASub@{i j} (Gamma : ACtx@{i}) (A : ATy@{i j} Gamma)
           (B : ATy@{i j} (ACtx_cons@{i j} A)) (a : AEl@{i j} A) : ATy@{i j} Gamma :=
  fun α gamma => B α (exist (a α gamma)).
  
Definition APi@{i j | i < j} (Gamma : ACtx@{i}) (A : ATy@{i j} Gamma)
           (B : ATy@{i j} (ACtx_cons@{i j} A)) : ATy@{i j} Gamma :=
  fun α gamma =>
    forall a : A α gamma, B α (exist a).


Definition ALam@{i j | i < j} (Gamma : ACtx@{i}) (A : ATy@{i j} Gamma)
           (B : ATy@{i j} (ACtx_cons@{i j} A))(f : AEl@{i j} B)
  : AEl@{i j} (APi B) := 
  fun (α : X -> Y) (gamma : Gamma α) (a : A α gamma) => f α (exist a).


Definition AApp@{i j | i < j} {Γ : ACtx@{i}}
           {A : ATy@{i j} Γ} {B : ATy@{i j} (@ACtx_cons@{i j} Γ A)}
           (f : @AEl@{i j} Γ (APi@{i j} B)) (x : @AEl@{i j} Γ A) : @AEl@{i j} Γ (ASub@{i j} B x) :=
  fun α γr => f α γr (x α γr).

Definition AApp2 (Gamma : ACtx) (A : ATy Gamma)
           (B : ATy (ACtx_cons A)) (f : AEl (APi  B)) : AEl B :=
  fun (alpha : X -> Y) (gamma : ACtx_cons A alpha) => f alpha gamma.(fst) gamma.(snd).


Definition AREL@{i j k | i < j, j < k} {Γ : ACtx@{j}} (A : @AEl@{j k} Γ AType@{i j k}) : ATy@{j k} Γ :=
  fun p γ => A p γ.

Definition Abool@{i j | i < j} (Gamma : ACtx@{i}) : ATy@{i j} Gamma := fun α gamma => bool. 


Definition Atrue@{i j | i < j} (Gamma : ACtx@{i}) : AEl@{i j} (@Abool@{i j} Gamma) :=
  fun α gamma => true.

Definition Afalse@{i j | i < j} (Gamma : ACtx@{i}) : AEl@{i j} (@Abool@{i j} Gamma) :=
  fun α gamma => false.


Definition Abool_rec@{h i j | h < i, i < j} (Gamma : ACtx@{i}) :
  AEl@{i j} (APi@{i j} (APi@{i j} (@APi@{i j} (ACtx_cons@{i j} (@AweakEl@{i j} _ (@AType@{h i j} Gamma)))
                        (Aweak@{i j} (@AweakEl@{i j} _ (@AType@{h i j} Gamma)))
                        (APi@{i j} (@Aweak@{i j} _ (@Abool@{i j} (ACtx_cons@{i j} (Aweak@{i j} (@AweakEl@{i j} _ (@AType@{h i j} Gamma)))))
                                     (Aweak@{i j} (Aweak@{i j} (@AweakEl@{i j} _ (@AType@{h i j} Gamma))))))))).
Proof.
  unfold AEl ; unfold APi ; unfold AType ; unfold Aweak ; unfold Abool ; simpl.
  now refine (fun α ga P ptrue pfalse b => match b with
          | true => ptrue
          | false => pfalse
         end).
Defined.


Definition Abool_stor@{h i j k| h < i, i < j, j < k} (Gamma : ACtx@{i}) :
  AEl@{i j} (APi@{i j} (APi@{i j} (@AType@{h i j} (ACtx_cons@{i j} (@Abool@{i j} (ACtx_cons@{i j} (APi@{i j} (@AType@{h i j} (ACtx_cons@{i j} (@Abool@{i j} Gamma)))))))))).
Proof.
  unfold AEl ; unfold APi ; unfold AType ; unfold Abool ; simpl.
  now refine (fun α ga P b => @Abool_rec@{i j k} Gamma α ga ((bool -> Type@{h}) -> Type@{h}) (fun k => k true) (fun k => k false) b P).
Defined.
Print Abool_stor.

Definition Abool_rect@{h i j k | h < i, i < j, j < k} (Gamma : ACtx@{i}) :
  AEl@{i j} (@APi@{i j} Gamma (@APi@{i j} _ (@Abool@{i j} Gamma) (@AType@{h i j} (ACtx_cons@{i j} (@Abool@{i j} Gamma))))
            (@APi@{i j} _  (fun α (gamma : ACtx_cons@{i j} (APi@{i j} (AType@{h i j} (Gamma:=ACtx_cons@{i j} (Abool@{i j} (Gamma:=Gamma))))) α) =>
                        gamma.(snd) true)
                  (@APi@{i j} _ (fun (α : X -> Y)
                               (gamma : ACtx_cons@{i j} (fun (beta : X -> Y)
                                                       (delta : ACtx_cons@{i j} (APi@{i j} (@AType@{h i j} (ACtx_cons@{i j} (@Abool@{i j} Gamma)))) beta) =>
                                                     delta.(snd) true) α) => gamma.(fst).(snd) false)
                        (@APi@{i j} _ (@Abool@{i j} _)  (fun α gamma => @Abool_stor@{h i j k} _ α gamma gamma.(fst).(fst).(fst).(snd) gamma.(snd)))))) :=
  fun α gamma P ptrue pfalse b =>
    match b with
    | true => ptrue
    | false => pfalse
    end.
Print Abool_rect.

Definition Anat@{i j | i < j} (Gamma : ACtx@{i}) : ATy@{i j} Gamma := fun α gamma => nat.

Definition A0@{i j | i < j} (Gamma : ACtx@{i}) : AEl@{i j} (@Anat@{i j} Gamma) := fun α gamma => 0.

Definition ASucc@{i j | i < j} (Gamma : ACtx@{i}) :
  AEl@{i j} (@APi@{i j} Gamma (@Anat@{i j} Gamma) (@Anat@{i j} (ACtx_cons@{i j} (@Anat@{i j} Gamma)))) :=
    fun _ _ n => S n.

Definition Alist (Gamma : ACtx) : AEl (APi (@AType (ACtx_cons (@AType Gamma)))) :=
  fun α gamma a => list a.
Print Alist.

Definition Anil (Gamma : ACtx) : AEl (AApp2 (@Alist Gamma)) := fun α ga => nil.


Definition Acons (Gamma : ACtx) :
  AEl (@APi Gamma (@AType Gamma)
            (@APi (ACtx_cons (@AType Gamma)) (fun α gamma => gamma.(snd))
                  (@APi (ACtx_cons (fun (α : X -> Y)
                                        (gamma : ACtx_cons (AType (Gamma:=Gamma)) α)
                                    => snd gamma))
                  (@Aweak (ACtx_cons (@AType Gamma)) (fun α gamma => gamma.(snd))
                          (AApp2 (@Alist Gamma)))
                  (@Aweak (ACtx_cons (fun (α : X -> Y)
                                          (gamma : ACtx_cons (AType (Gamma:=Gamma)) α)
                                      => snd gamma))
                          (@Aweak (ACtx_cons (@AType Gamma)) (fun α gamma => gamma.(snd))
                                  (AApp2 (@Alist Gamma)))
                          (@Aweak (ACtx_cons (@AType Gamma)) (fun α gamma => gamma.(snd))
                                  (AApp2 (@Alist Gamma))))))).
Proof.
  unfold AEl ; unfold APi ; unfold AApp2 ; unfold Aweak ; simpl.
  exact (fun α gamma A a la => cons a la).
Defined.

Notation "()" := (ACtx_nil).
Notation "Γ · A" := (@ACtx_cons Γ A) (at level 50, left associativity).
Notation "γ ·· x" := (@Actx_cons γ x) (at level 50, left associativity).
Notation "⇑ A" := (@Aweak _ _ A) (at level 10).
Notation "#" := (@AweakEl _ _) (at level 0).
Notation "S {{ r }} " := (ASub S r) (at level 20, left associativity).
Notation "∏" := (@APi).
Notation "A → B" := (@APi _ A (⇑ B)) (at level 99, right associativity, B at level 200).
Notation "'λ'" := (@ALam).
Notation "t • u" := (@AApp _ _ _ t u) (at level 12, left associativity).
Notation "↑ M" := (@Aweaken _ _ _ M) (at level 10).
Notation "[ A ]" := (@AREL _ A).
Print Aweaken.

Definition AAppN@{i j | i < j} {Γ : ACtx@{i} } {A : ATy@{i j} Γ} {B : ATy@{i j} Γ}
           (f : @AEl@{i j} Γ (A → B))(x : @AEl@{i j} Γ A) : @AEl@{i j} Γ B :=
  @AApp Γ A (⇑ B) f x.

Definition ALamN@{i j | i < j} {Γ : ACtx@{i} } {A : ATy@{i j} Γ} {B : ATy@{i j} Γ}
           (f : @AEl@{i j} (Γ · A) (⇑ B)) : @AEl@{i j} Γ (A → B) :=
  @ALam Γ A (⇑ B) f.


Definition Anat_rec@{i j k | i < j, j < k} (Γ : ACtx@{j}) :
  @AEl@{j k} Γ (@APi Γ AType@{i j k}
            ([@AweakEl@{j k} _ AType] →
             ((@Anat@{j k} _) → [@AweakEl _ AType] → [@AweakEl _ AType]) →
             (@Anat@{j k} _) →
             [@AweakEl _ AType])).
Proof.
  refine (fun α gamma P p0 pS => _).
  refine (fix f n := match n with
                       | 0 => p0
                       | S k => pS k (f k)
                       end).
Defined.


Definition Anat_stor@{j k l m| j < k, k < l, l < m} {Γ : ACtx@{j} } :
  @AEl@{k l} Γ ((@Anat@{k l} _) → ((@Anat@{k l} _) → AType@{j k l}) → AType@{j k l}).
Proof.
  pose (r := @AApp _ _ _ (@Anat_rec@{k l m} Γ) (((@Anat@{k l} _) → AType@{j k l}) → AType@{j k l})).
  let T := constr:(@AEl@{l m} Γ ((((@Anat _) → AType) → AType) → ((@Anat _) → (((@Anat _) → AType) → AType) → (((@Anat _) → AType) → AType)) → (@Anat _) → (((@Anat _) → AType) → AType))) in
  unshelve refine (let r : T := r in _).
  unshelve refine (AApp (AApp r _) _).
  - refine (ALamN (@AAppN _ (@Anat _) _ _ _)).
    + now refine (@AweakEl _ _).
    + now refine (@A0 _).
  - refine (@ALamN@{l m} _ _ _ _).
    refine (@Aweaken@{m l} _ _ _ (@ALamN@{l m} _ _ _ _)).
    refine (@ALamN@{l m} (@ACtx_cons@{l m} Γ (((@Anat@{l m} _) → AType@{j l m}) → AType@{j l m}))
                  (⇑((@Anat _) → AType)) (⇑ AType) _).
    refine (@AAppN _ ((@Anat _) → AType@{j l m}) _ _ _).
    + now refine (↑#).
    + apply ALamN.
      refine (@AAppN _ (@Anat _) _ _ _).
      * now refine (↑#).
      * now refine (@AAppN _ (@Anat _) _ (@ASucc _) (@AweakEl _ (@Anat _))).
Defined.  


Lemma test1@{i j k l | i < j, j < k, k < l, i <= k, j <= l, l < eq.u0} (Gamma : ACtx@{i}) :
  @AEl@{k l} Gamma AType@{j k l} = ATy@{j k} Gamma.
Proof.
  unfold AEl ; unfold AType ; unfold ATy ; simpl.
  reflexivity.
Qed.

Fixpoint Nat_rec@{i} (P : Type@{i}) (p0 : P) (pS : (nat -> P -> P)) (n : nat) : P :=
  match n with
  | 0 => p0
  | S k => pS k (@Nat_rec P p0 pS k)
  end.

Print Nat_rec.
Definition Nat_stor@{i j} : nat -> (nat -> Type@{i}) -> Type@{i} :=
  fun n P => @Nat_rec@{j} ((nat -> Type@{i}) -> Type@{i}) (fun P => P 0) (fun n P k => P (fun m => k n)) n P.
Print Nat_stor.

Fixpoint Nat_rect@{i j} (P : nat -> Type@{i}) (p0 : P 0)
         (pS : forall n : nat, Nat_stor@{i j} n P -> Nat_stor@{i j} (S n) P)
         (n : nat) : Nat_stor@{i j} n P := match n  with
                                           | 0 => p0
                                           | S k => pS k (@Nat_rect P p0 pS k)
                                           end.


Definition Anat_rect@{i j k l m n o| i < j, j < k, k < l, l < m, m < n, n < o} {Γ : ACtx@{j} } :
  @AEl@{j k} Γ (@APi@{n o} _ ((@Anat@{j k} _) → AType@{i j k})
              ([@AApp (Γ · ((@Anat _) → AType@{i j k})) (@Anat _) AType@{i j k} # (@A0 _)] →
               (@AREL@{j k l} (@ACtx_cons@{k l} Γ ((@Anat _) → AType@{j k l}))
                     (@APi@{k l} (@ACtx_cons@{k l} Γ ((@Anat _) → AType@{j k l})) (@Anat@{k l} _)
                          ([@AApp@{k l} (@ACtx_cons@{k l} Γ ((@Anat _) → AType) · (@Anat _)) _ AType@{j k l}
                                 (@AApp@{k l} ((@ACtx_cons@{k l} Γ ((@Anat _) → AType@{j k l})) · (@Anat _)) _ _ Anat_stor@{k l m n} #) (↑#)] →
                           [@AApp@{k l} ((@ACtx_cons@{k l} Γ ((@Anat _) → AType@{j k l})) · (@Anat _)) _ AType@{j k l}
                                  (@AApp ((@ACtx_cons@{k l} Γ ((@Anat _) → AType@{j k l})) · (@Anat _)) _ _ Anat_stor@{k l m n}
                                         (@AApp@{k l} ((@ACtx_cons@{k l} Γ ((@Anat _) → AType@{j k l})) · (@Anat _)) _ _ (@ASucc@{k l} _) #)) (↑#)]))) →
               (@APi@{k l} (Γ · ((@Anat _) → AType@{i j k})) (@Anat@{k l} _) [@AApp@{l k} (@ACtx_cons@{k l} (@ACtx_cons@{j k} Γ ((@Anat _) → AType@{i j k})) (@Anat@{k l} _)) _ AType
                                        (@AApp (@ACtx_cons@{k l} (@ACtx_cons@{j k} Γ ((@Anat _) → AType@{i j k})) (@Anat@{k l} _)) _ _ Anat_stor@{k l m n} #) (@Aweaken@{k l} _ _ _ #)]))).

Definition Anat_rect@{i j k l m| i < j, j < k, k < l, l < m} {Γ : ACtx@{j} } :
  @AEl@{j k} Γ (@APi@{k l} _ (@APi@{j k} _ (@Anat@{j k} _) (@Aweak@{j k} _ _ (AType@{i j k})))
              (@APi@{k l} _ (@AREL@{i j k} _ (@AApp@{j k} (@ACtx_cons@{j k} Γ (@APi@{j k} _ (@Anat@{j k} _) (@Aweak@{j k} _ _ (AType@{i j k})))) (@Anat@{j k} _) AType@{i j k}  (@AweakEl@{j k} _ _) (@A0@{j k} _)))
               (@Aweak@{j k} _ _ (@APi@{k l} _ (@AREL@{i j k} (@ACtx_cons@{j k} Γ (@APi@{j k} _ (@Anat@{j k} _) (@Aweak@{j k} _ _ (AType@{i j k}))))
                     (@APi@{k l} _ (@Anat@{j k} _)
                          (@APi@{j k} _ (@AREL@{i j k} _ (@AApp@{j k} (@ACtx_cons@{j k} (@ACtx_cons@{j k} Γ (@APi@{j k} _ (@Anat@{j k} _) (@Aweak@{j k} _ _ (AType@{i j k})))) (@Anat@{j k} _)) _ AType@{i j k}
                                 (@AApp@{j k} (@ACtx_cons@{j k} (@ACtx_cons@{j k} Γ (@APi@{j k} _ (@Anat@{j k} _) (@Aweak@{j k} _ _ (AType@{i j k})))) (@Anat@{j k} _)) _ _ Anat_stor@{j k l m} (@AweakEl@{j k} _ _)) (@Aweaken@{j k} _ _ _ (@AweakEl@{j k} _ _))))
                           (@Aweak@{j k} _ _ (@AREL@{i j k} _ (@AApp@{j k} (@ACtx_cons@{j k} (@ACtx_cons@{j k} Γ (@APi@{j k} _ (@Anat@{j k} _) (@Aweak@{j k} _ _ (AType@{i j k})))) (@Anat@{j k} _)) _ AType@{i j k}
                                  (@AApp@{j k} (@ACtx_cons@{j k} (@ACtx_cons@{j k} Γ (@APi@{j k} _ (@Anat@{j k} _) (@Aweak@{j k} _ _ (AType@{i j k})))) (@Anat@{j k} _)) _ _ Anat_stor@{j k l m}
                                         (@AApp@{j k} (@ACtx_cons@{j k} (@ACtx_cons@{j k} Γ (@APi@{j k} _ (@Anat@{j k} _) (@Aweak@{j k} _ _ (AType@{i j k})))) (@Anat@{j k} _)) _ _ (@ASucc@{j k} _) (@AweakEl@{j k} _ _))) (@Aweaken@{j k} _ _ _ (@AweakEl@{j k} _ _))))))))
               (@Aweak@{j k} _ _ ((@APi@{k l} (@ACtx_cons@{j k} Γ (@APi@{j k} _ (@Anat@{j k} _) (@Aweak@{j k} _ _ (AType@{i j k})))) (@Anat@{j k} _) (@AREL@{i j k} _ (@AApp@{j k} (@ACtx_cons@{j k} (@ACtx_cons@{j k} Γ (@APi@{j k} _ (@Anat@{j k} _) (@Aweak@{j k} _ _ (AType@{i j k})))) (@Anat@{j k} _)) _ AType@{i j k}
                                        (@AApp@{j k} (@ACtx_cons@{j k} (@ACtx_cons@{j k} Γ (@APi@{j k} _ (@Anat@{j k} _) (@Aweak@{j k} _ _ (AType@{i j k})))) (@Anat@{j k} _)) _ _ Anat_stor@{j k l m} #) (@Aweaken@{j k} _ _ _ (@AweakEl@{j k} _ _))))))))))).



Definition Anat_rect@{i j k l m n o| i < j, j < k, k < l, l < m, m < n, n < o} {Γ : ACtx@{j} } :
  @AEl@{j k} Γ (@APi@{k l} _ ((@Anat@{j k} _) → AType@{i j k})
              ([@AApp (Γ · ((@Anat _) → AType@{i j k})) (@Anat _) AType@{i j k} # (@A0 _)] →
               (@AREL@{j k l} (@ACtx_cons@{j k} Γ ((@Anat _) → AType@{i j k}))
                     (@APi@{j k} _ (@Anat@{j k} _)
                          ([@AApp@{j k} (Γ · ((@Anat _) → AType) · (@Anat _)) _ AType@{i j k}
                                 (@AApp@{j k} ((@ACtx_cons@{j k} Γ ((@Anat _) → AType@{i j k})) · (@Anat _)) _ _ Anat_stor@{j k l m} #) (↑#)] →
                           [@AApp ((@ACtx_cons@{j k} Γ ((@Anat _) → AType@{i j k})) · (@Anat _)) _ AType@{i j k}
                                  (@AApp ((@ACtx_cons@{j k} Γ ((@Anat _) → AType@{i j k})) · (@Anat _)) _ _ Anat_stor@{j k l m}
                                         (@AApp ((@ACtx_cons@{j k} Γ ((@Anat _) → AType@{i j k})) · (@Anat _)) _ _ (@ASucc@{j k} _) #)) (↑#)]))) →
               (@APi@{k l} (Γ · ((@Anat _) → AType@{i j k})) (@Anat@{k l} _) [@AApp@{l k} (@ACtx_cons@{k l} (@ACtx_cons@{j k} Γ ((@Anat _) → AType@{i j k})) (@Anat@{k l} _)) _ AType
                                        (@AApp (@ACtx_cons@{k l} (@ACtx_cons@{j k} Γ ((@Anat _) → AType@{i j k})) (@Anat@{k l} _)) _ _ Anat_stor@{k l m n} #) (@Aweaken@{k l} _ _ _ #)]))).
Proof.
  refine (fix f α γ P p0 pS n := match n as n0 return
                               ((fix f (n : nat) : (nat -> Type) -> Type :=
                                   match n with
                                   | 0 => fun xr : nat -> Type => xr 0
                                   | S k => fun xr : nat -> Type => f k (fun xr0 : nat => xr (S xr0))
                                   end) n0 P)
                               with
                               | 0 => p0
                               | S k => f α γ (fun k => P (S k)) (pS 0 p0) (fun k => pS (S k)) k
                               end).
Defined.


Goal forall (Γ : ACtx)
  (P : @AEl Γ ((@Anat _) → AType))
  (T0 := [P • (@A0 _)])
  (p0 : @AEl Γ T0)

  (TS := (∏ _ (@Anat _) ([@AApp (Γ · (@Anat _)) _ AType
                              (@AApp (Γ · (@Anat _)) _ _ Anat_stor #) (↑P)] →
                       [@AApp (Γ · (@Anat _)) _ AType
                              (@AApp (Γ · (@Anat _)) _ _ Anat_stor
                                     (@AApp (Γ · (@Anat _)) _ _ (@ASucc _) #)) (↑P)])))

  
  (pS : @AEl Γ TS)
  (TR := @APi Γ (@Anat _) [Anat_stor • @AweakEl _ (@Anat _) • ↑ P])
  ,

  @AApp Γ _ _ (@AAppN Γ _ _ (@AAppN Γ T0 (TS → TR) ((@Anat_rect Γ) • P) p0) pS) (@A0 _)
  =
  p0.
Proof.
reflexivity.
Qed.
Unset Implicit Arguments.
Goal forall (Γ : ACtx)
  (P : @AEl Γ ((@Anat _) → AType))
  (T0 := [P • (@A0 _)])
  (p0 : @AEl Γ T0)
  (TS := (∏ _ (@Anat _) ([@AApp (Γ · (@Anat _)) _ AType
                         (@AApp (Γ · (@Anat _)) _ _ Anat_stor #) (↑P)] →
                  [@AApp (Γ · (@Anat _)) _ AType
                         (@AApp (Γ · (@Anat _)) _ _ Anat_stor
                                (@AApp (Γ · (@Anat _)) _ _ (@ASucc _) #)) (↑P)])))
  (pS : @AEl Γ TS)
  (TR := @APi Γ (@Anat _) [Anat_stor • @AweakEl _ (@Anat _) • ↑ P])
  (n : @AEl Γ (@Anat _))
  ,
    forall (α : X -> Y) (γr : Γ α),
    
    (@AApp Γ _ _ (@AAppN Γ _ _ (@AAppN Γ T0 (TS → TR) ((@Anat_rect Γ) • P) p0) pS) (AApp (@ASucc _) n)) α γr
    =
    (@AAppN Γ (@AApp Γ ((@Anat _) → AType) _ (@AApp Γ _ _ Anat_stor n) P) (Anat_stor • ((@ASucc _) • n) • P) (AApp pS n) (@AApp Γ _ _ (@AAppN Γ _ _ (@AAppN Γ T0 (TS → TR) ((@Anat_rect Γ) • P) p0) pS) n)) α γr.
Proof.
  simpl.
  unfold AAppN ; simpl. unfold ALamN ; unfold ALam ; simpl.
  unfold AApp ; simpl.
  intros.
  revert pS ; revert p0 ; revert P.
  have aux : forall P p0 pS (m : nat),
      @Anat_rect _ α γr (fun k : Anat γr => P α γr (S k)) (pS α γr 0 (p0 α γr)) (fun k => pS α γr (S k))
                 m = pS α γr m (@Anat_rect _ α γr (P α γr) (p0 α γr) (pS α γr) m).
  - induction m.
    
    + reflexivity.
    + simpl.
      rewrite IHm.
      
      
    induction (n α γr).
  unfold Anat_rect ; simpl.
  unfold APi ; simpl.
  
  reflexivity.

  
  unfold AAppN ; unfold AApp ; unfold ALamN ; unfold ALam ; simpl.
  intros.

  
End AxiomMonad.


Section DialogueMonadwFamilies.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Variables (X Y : Type).


Definition BCtx := Type.

Definition BTy (Gamma : BCtx) := Gamma -> BranchingTYPE X Y.

Definition BType (Gamma : BCtx) : BTy Gamma :=
  fun _ => @exist Type (fun A =>  (Y -> A) -> X -> A)
                  (BranchingTYPE X Y) (fun _ _ => Bunit X Y).

Definition BEl' (Gamma : BCtx) (A : BTy Gamma) :=
  forall (gamma : Gamma), BEl (A gamma). 

Definition BCtx_nil : BCtx := unit.

Definition BCtx_cons (Gamma : BCtx) (A : BTy Gamma) :=
  tsig (fun (gamma : Gamma) => BEl (A gamma)).

Definition Bctx_cons (Gamma : BCtx) (B : BTy Gamma)
           (gamma : Gamma) (b : BEl (B gamma)) : BCtx_cons B.
Proof.
  exists gamma ; exact b.
Defined.


Definition Bweak (Gamma : BCtx) (A : BTy Gamma) (B : BTy Gamma) : BTy (BCtx_cons A) :=
  fun gamma => B gamma.(fst).

Definition BweakEl (Gamma : BCtx) (A : BTy Gamma) : BEl' (@Bweak Gamma A A) :=
  fun (gamma : BCtx_cons A) => gamma.(snd).

Definition BSub (Gamma : BCtx) (A : BTy Gamma) (B : BTy (BCtx_cons A)) (a : BEl' A) : BTy Gamma :=
  fun gamma => B ({|fst := gamma ; snd :=(a gamma) |}).

Definition BPi (Gamma : BCtx) (A : BTy Gamma) (B : BTy (BCtx_cons A)) : BTy Gamma.
Proof.
  intro gamma.
  unfold BTy in B ; simpl in B.
  exists (forall a : BEl (A gamma),
             BEl (B {| fst := gamma; snd := a|})).
  intros f x a.
  apply (B {| fst := gamma; snd := a |}).(snd) ; trivial.
  exact (fun y => f y a).
Defined.


Definition BLam (Gamma : BCtx) (A : BTy Gamma) (B : BTy (BCtx_cons A)) (f : BEl' B)
  : BEl' (BPi B) := 
  fun (gamma : Gamma) (a : BEl (A gamma)) => f {| fst := gamma; snd := a |}.


Definition BApp' (Gamma : BCtx) (A : BTy Gamma)
           (B : BTy (BCtx_cons A)) (f : BEl' (BPi  B)) : BEl' B :=
  fun (gamma : BCtx_cons A) => f gamma.(fst) gamma.(snd).

Definition Bbool' (Gamma : BCtx) : BTy Gamma := fun gamma => Bbool X Y.
Definition Btrue' (Gamma : BCtx) : BEl' (@Bbool' Gamma) := (fun _ => @Btrue X Y).
Definition Bfalse' (Gamma : BCtx) : BEl' (@Bbool' Gamma) := (fun _ => @Bfalse X Y).



Definition Bbool_rec (Gamma : BCtx) :
  BEl' (@BPi Gamma (@BType Gamma)
            (@BPi (BCtx_cons (@BType Gamma)) (@BweakEl _ (@BType Gamma))
                  (@BPi (BCtx_cons (@BweakEl _ (@BType Gamma)))
                        (@Bweak (BCtx_cons (@BType Gamma))
                                (@BweakEl _ (@BType Gamma))
                                (@BweakEl _ (@BType Gamma)))
                        (@BPi (BCtx_cons (Bweak (@BweakEl _ (@BType Gamma))))
                              (@Bbool' (BCtx_cons (Bweak (@BweakEl _ (@BType Gamma)))))
                              (@Bweak (BCtx_cons (Bweak (@BweakEl _ (@BType Gamma))))
                                      (@Bbool' (BCtx_cons (Bweak (@BweakEl _ (@BType Gamma)))))
                                      (Bweak (Bweak (@BweakEl _ (@BType Gamma))))))))).
Proof.
  unfold BEl' ; unfold BPi ; unfold BType ; unfold Bweak ; unfold Bbool' ; simpl.
  unshelve refine (fun ga P ptrue pfalse => _).
  now refine (fix f b := match b with
                         | Btrue _ _  => ptrue
                         | Bfalse _ _ => pfalse
                         | Bêtabool g x => P.(snd) (fun y => f (g y)) x
                         end).
Defined.

Definition Bbool_stor (Gamma : BCtx) :
  BEl' (@BPi Gamma (@BPi Gamma (@Bbool' Gamma) (@BType (BCtx_cons (@Bbool' Gamma))))
            (@BPi (BCtx_cons (@BPi Gamma (@Bbool' Gamma) (@BType (BCtx_cons (@Bbool' Gamma)))))
                  (@Bbool' (BCtx_cons (@BPi Gamma (@Bbool' Gamma) (@BType (BCtx_cons (@Bbool' Gamma))))))
                  (@BType (BCtx_cons
                             (@Bbool' (BCtx_cons (@BPi Gamma (@Bbool' Gamma) (@BType (BCtx_cons (@Bbool' Gamma)))))))))).
Proof.
  unfold BEl' ; unfold BPi ; unfold BType ; unfold Bbool' ; simpl.
  exact (fun ga P b => @Bbool_rec Gamma ga
                                   (@BPi Gamma (@BPi Gamma (@Bbool' Gamma) (@BType _)) (@BType _) ga)
                                  (fun k => k (@Btrue X Y))
                                  (fun k => k (@Bfalse X Y)) b P).
Defined.


Definition Bbool_rect (Gamma : BCtx) :
    BEl' (@BPi Gamma (@BPi _ (@Bbool' Gamma) (@BType (BCtx_cons (@Bbool' Gamma))))
            (@BPi _ (fun (gamma : BCtx_cons (@BPi Gamma (@Bbool' Gamma) (@BType (BCtx_cons (@Bbool' Gamma))))) =>
                       gamma.(snd) (@Btrue X Y))
                  (@BPi _ (fun (gamma : BCtx_cons (fun (delta : BCtx_cons (BPi (@BType (BCtx_cons (@Bbool' Gamma))))) =>
                                                     delta.(snd) (@Btrue X Y))) => gamma.(fst).(snd) (@Bfalse X Y))
                        (@BPi _ (@Bbool' _)
                              (fun gamma => @Bbool_stor _ gamma gamma.(fst).(fst).(fst).(snd) gamma.(snd)))))) :=
  fun ga P ptrue pfalse b =>
    match b with
    | Btrue _ _ => ptrue
    | Bfalse _ _ => pfalse
    | Bêtabool g x => tt
    end.



Definition Bnat' (Gamma : BCtx) : BTy Gamma := fun gamma => Bnat X Y.

Definition B0' (Gamma : BCtx) : BEl' (@Bnat' Gamma) := fun gamma => B0 X Y. 

Definition BSucc (Gamma : BCtx) :
  BEl' (@BPi Gamma (@Bnat' Gamma) (@Bnat' (BCtx_cons (@Bnat' Gamma)))) :=
  fun gamma => (@BS X Y).


Definition Blist' (Gamma : BCtx) :
  BEl' (BPi (@BType (BCtx_cons (@BType Gamma)))) := fun gamma A => @Blist X Y A.


Definition Bnil' (Gamma : BCtx) : BEl' (BApp' (@Blist' Gamma)) :=
  fun ga => @Bnil X Y ga.(snd).


Definition Bcons' (Gamma : BCtx) :
  BEl' (@BPi Gamma (@BType Gamma)
            (@BPi (BCtx_cons (@BType Gamma)) (fun gamma => gamma.(snd))
                  (@BPi (BCtx_cons (fun (gamma : BCtx_cons (BType (Gamma:= Gamma)) )
                                    => snd gamma))
                  (@Bweak (BCtx_cons (@BType Gamma)) (fun  gamma => gamma.(snd))
                          (BApp' (@Blist' Gamma)))
                  (@Bweak (BCtx_cons (fun (gamma : BCtx_cons (BType (Gamma:=Gamma)) )
                                      => snd gamma))
                          (@Bweak (BCtx_cons (@BType Gamma)) (fun  gamma => gamma.(snd))
                                  (BApp' (@Blist' Gamma)))
                          (@Bweak (BCtx_cons (@BType Gamma)) (fun  gamma => gamma.(snd))
                                  (BApp' (@Blist' Gamma))))))) :=
  fun gamma A x l => Bcons x l.



  
End DialogueMonadwFamilies.



Section AvareXY.

Unset Implicit Arguments.
Variables X Y : Set.
Definition ℙ := X -> Y.
Notation "'ACtx'" := (ACtx X Y).
Notation "'BTy'" := (BTy X Y).

Definition Ctx :=
  triple (fun (A : ACtx) (B : BCtx) =>
            forall (α : ℙ),  A α -> B -> Type).

Definition Ctx_El (Γ : Ctx) (α : ℙ) :=
    triple (fun (a : Γ.(q1) α) (b : Γ.(q2)) =>
              Γ.(q3) α a b).

Notation " Γ ‡ α " := (Ctx_El Γ α) (at level 11).


Definition Ty (Γ : Ctx) : Type :=
    triple (fun (A :  ATy Γ.(q1)) (B : BTy Γ.(q2)) =>
              forall (α : ℙ) (γ : Ctx_El Γ α),
                tsig (fun (Te : A α (γ.(q1)) -> BEl (B γ.(q2)) -> Type) =>
                        forall (a : A α (γ.(q1))) (f : Y -> BEl (B γ.(q2))) (x : X) (fe : Te a (f (α x))), Te a (((B γ.(q2))).(snd) f x))).

Definition RElt (Γ : Ctx) (R : Ty Γ) : Type :=
    triple (fun (a : AEl R.(q1)) (b : BEl' R.(q2)) =>
              forall (α : ℙ) (γ : Ctx_El Γ α),
                (R.(q3) α γ).(fst) (a α γ.(q1)) (b γ.(q2))).



Definition Ty_inst {Γ : Ctx} {α : ℙ} (R : Ty Γ)  (γ : Ctx_El Γ α) : Type
  :=
    triple (fun (a : R.(q1) α γ.(q1)) (b : BEl (R.(q2) γ.(q2))) =>
            (R.(q3) α γ).(fst) a b).


Definition Inst {Γ : Ctx} {R : Ty Γ} {α : ℙ} (r : RElt _ R)
            (γ : Ctx_El Γ α) : Ty_inst R γ.
Proof.
  exists (r.(q1) α γ.(q1)) (r.(q2) γ.(q2)).
  exact (r.(q3) α γ).
Defined.  


Notation "r ® γ" := (Inst r γ) (at level 10, left associativity).



Definition Ctx_nil : Ctx.
Proof.
  exists (@ACtx_nil X Y) (BCtx_nil).
  apply (fun _ _ _ => unit).
Defined.


Definition ctx_nil {α : ℙ} : Ctx_nil ‡ α.
Proof.
  exists tt tt.
  exact: tt.
Defined.

Notation "()" := ctx_nil.

Definition Ctx_cons (Γ : Ctx) (R : Ty Γ) : Ctx.
Proof.
  exists (ACtx_cons R.(q1)) (BCtx_cons R.(q2)).
  intros α Acons Bcons.
  apply (@tsig (Γ.(q3) α Acons.(fst) Bcons.(fst))).
  intro γe.
  exact ((R.(q3) α (exist3 γe)).(fst) Acons.(snd) Bcons.(snd)).
Defined.


Notation "Γ · A" := (Ctx_cons Γ A) (at level 50, left associativity).



Definition ctx_cons {Γ : Ctx} {R : Ty Γ} {α : ℙ}
           (γ : Γ ‡ α) (x : Ty_inst R γ) :
  (Γ · R) ‡ α.
Proof.
  destruct x as [xa xb xe].
  exists (Actx_cons xa) (Bctx_cons xb).
  exists γ.(q3).
  exact xe.
Defined.

Notation "γ ·· x" := (ctx_cons γ x) (at level 50, left associativity).


Definition Ctx_tl {Γ : Ctx} {R : Ty Γ} {α : ℙ}
           (γ : Ctx_El (Γ · R) α) : Ctx_El Γ α.
Proof.
  exists γ.(q1).(fst) γ.(q2).(fst).
  exact (γ.(q3).(fst)).
Defined.


Notation "◊ g" := (Ctx_tl g) (at level 10).


Definition Ctx_hd {Γ : Ctx} {R : Ty Γ} {α : ℙ}
           (γ : Ctx_El (Γ · R) α) : Ty_inst R (◊ γ).
Proof.
  exists γ.(q1).(snd) γ.(q2).(snd).
  exact γ.(q3).(snd).
Defined.

Notation "¿ g" := (Ctx_hd g) (at level 10).


Definition Lift {Γ : Ctx} {R1 : Ty Γ}  (R2 : Ty Γ) : Ty (Γ · R1).
Proof.
  unshelve refine (exist3 _).
  + refine (fun α γr => R2.(q1) α γr.(fst)).
  + refine (fun γd => R2.(q2) γd.(fst)).
  + unshelve refine (fun α γ => _).
    exact (R2.(q3) α (◊ γ)).
Defined.

Notation "⇑ A" := (Lift A) (at level 10).


Definition Var {Γ : Ctx} (R : Ty Γ) : RElt (Γ · R) (⇑ R).
Proof.
  unshelve refine (exist3 _).
  + refine (fun α γr => γr.(snd)).
  + refine (fun γd => γd.(snd)).
  + exact (fun α γ => γ.(q3).(snd)). 
Defined.

Notation "#" := (Var _) (at level 0).


Definition Weaken {Γ : Ctx} {R S : Ty Γ} (r : RElt Γ R) :
  RElt (Γ · S) (⇑ R).
Proof.
  unshelve refine (exist3 _).
  + refine (fun α γr => r.(q1) α γr.(fst)).
  + refine (fun γd => r.(q2) γd.(fst)).
  + exact (fun α γ => r.(q3) α (◊ γ)).
Defined.

Notation "↑ M" := (Weaken M) (at level 10).

Definition Sub {Γ : Ctx} {R : Ty Γ} (S : Ty (Γ · R)) (r : RElt Γ R) :
  Ty Γ.
Proof.
  unshelve refine (exist3 _).
  + refine (fun α γr => S.(q1) α (@exist _ _ γr (r.(q1) α γr))).
  + refine (fun γd => S.(q2) (@exist _ _ γd (r.(q2) γd))).
  + exact (fun α γ =>
           S.(q3) α (γ ·· (r ® γ))).
Defined.


Notation "S {{ r }} " := (Sub S r) (at level 20, left associativity).

         
Definition RPi {Γ : Ctx} (A : Ty Γ) (B : Ty (Γ ·  A)) : Ty Γ.
Proof.
  unshelve refine (exist3 _).
  - refine (fun α γr => forall (x : A.(q1) α γr), B.(q1) α (@exist _ _ γr x)).
  - intro γd.
    unshelve refine (exist _) ; simpl.
    + refine (forall (x : BEl (A.(q2) γd)), BEl (B.(q2) (@exist _ _ γd x))).
    + refine (fun f x a => (B.(q2) (@exist _ _ γd a)).(snd) (fun y => f y a) x).
  - simpl ; unshelve refine (fun α γ => _).
    unshelve refine (exist _).
    + exact (fun fr fd => forall a : Ty_inst A γ,
            (B.(q3) α (γ ·· a)).(fst)
                   (fr a.(q1))
                   (fd a.(q2))).
    + exact (fun _ _ _ fe a => (B.(q3) α (γ ·· a)).(snd) _ _ _ (fe a)). 
Defined.


Notation "∏" := (RPi).

Notation "A → B" := (RPi A (⇑ B)) (at level 99, right associativity, B at level 200).


Definition RLam {Γ : Ctx} {A : Ty Γ} (B : Ty (Γ · A)) (f : RElt (Γ · A) B)
  : RElt Γ (RPi A B).
Proof.
  unshelve refine (exist3 _).
  + refine (fun α γr xr => f.(q1) α (@exist _ _ γr xr)).
  + refine (fun γd xd => f.(q2) (@exist _ _ γd xd)).
  + exact (fun α γ x => f.(q3) _ _).
Defined.


Notation "'λ'" := RLam.


Definition RApp {Γ : Ctx} {A : Ty Γ} {B : Ty (Γ · A)}
           (f : RElt Γ (RPi A B)) (x : RElt Γ A) : RElt Γ (B {{ x }}).
Proof.
  unshelve refine (exist3 _).
  + refine (fun α γr => f.(q1) α γr (x.(q1) α γr)).
  + refine (fun γd => f.(q2) γd (x.(q2) γd)).
  + exact (fun α γ =>
           f.(q3) α γ (x ® γ)).
Defined.

Notation "t • u" := (RApp t u) (at level 12, left associativity).


Definition U {Γ : Ctx} : Ty Γ.
Proof.
  unshelve refine (exist3 _).
  - refine (fun α γr => Type).
  - intro γd.
    unshelve refine (exist _).
    + refine (BranchingTYPE X Y).
    + refine (fun _ _ => Bunit X Y).
  - unshelve refine (fun (α : ℙ) (γ : Γ ‡ α) => exist _).
    + exact (fun A B => tsig (fun (Te : A -> BEl B -> Type) =>
              forall (a : A) (f : Y -> BEl B) (x : X) (fe : Te a (f (α x))),
                Te a ((B.(snd) f x)))).
    + simpl ; intros.
      unshelve refine (exist _).
      * exact (fun _ _ => unit).
      * simpl.
        exact (fun _ _ _ _ => tt).
Defined.


Definition REL {Γ : Ctx} (A : RElt Γ U) : Ty Γ.
Proof.
  exists (fun p γ => A.(q1) p γ) (fun γ => A.(q2) γ).
  unshelve refine (fun (α : ℙ) (γ : Γ ‡ α) => exist _).
  - exact (A.(q3) α γ).(fst).
  - exact (A.(q3) α γ).(snd).
Defined.

Notation "[ A ]" := (REL A).


Inductive RParabool (α : X -> Y) :
  bool -> (BranchingBool X Y) -> Type :=
| RParatrue : RParabool α true (@Btrue X Y)
| RParafalse : RParabool α false (@Bfalse X Y)
| RParabêtabool : forall  (bo : bool) (f : (Y ->  BranchingBool X Y)) (x : X)
                        (rel : RParabool α bo (f (α x)) ),
    RParabool α bo (Bêtabool f x).

Definition Rbool {Γ : Ctx} : Ty Γ.
Proof.
  exists (@Abool X Y Γ.(q1)) (@Bbool' X Y Γ.(q2)).
  exact (fun (α : ℙ) (γ : Γ ‡ α) => {| fst := RParabool α; snd := RParabêtabool α |}).
Defined.

Definition Rtrue {Γ : Ctx} : RElt Γ Rbool.
Proof.
  exists (@Atrue X Y Γ.(q1)) (@Btrue' X Y Γ.(q2)).
  exact (fun α _ => RParatrue α).
Defined.


Definition Rfalse {Γ : Ctx} : RElt Γ Rbool.
Proof.
  exists (@Afalse X Y Γ.(q1)) (@Bfalse' X Y Γ.(q2)).
  exact (fun α _ => RParafalse α).
Defined.


Definition Rbool_rec (Γ : Ctx) :
   RElt Γ (∏ U ([Var U] → [Var U] → Rbool → [Var U])).
Proof.
  exists (@Abool_rec X Y Γ.(q1)) (@Bbool_rec X Y Γ.(q2)).
  simpl ; intros α g P ptrue pfalse b.
  destruct b as [bo bb be].
  revert be ; revert  bb ; revert bo ; simpl.
  unshelve refine (fix f _ _ b := match b with
                              | RParatrue _ => _
                              | RParafalse _ => _
                              | RParabêtabool _ _ _ _ rel =>
                                P.(q3).(snd) _ _  _ (f _ _ rel)
                              end).
  - exact ptrue.(q3).
  - exact pfalse.(q3).
Defined.

Definition RAppN {Γ : Ctx} {A : Ty Γ} {B : Ty Γ} (f : RElt Γ (A → B))(x : RElt Γ A) : RElt Γ B :=
  @RApp Γ A (⇑ B) f x.

Definition RLamN {Γ : Ctx} {A : Ty Γ} {B : Ty Γ} (f : RElt (Γ · A) (⇑ B)) : RElt Γ (A → B) :=
  @RLam Γ A (⇑ B) f.

Definition Rbool_stor {Γ : Ctx} :
  RElt Γ (Rbool → (Rbool → U) → U).
Proof.
  pose (r := RApp (Rbool_rec Γ) ((Rbool → U) → U)).
  let T := constr:(RElt Γ (((Rbool → U) → U) → ((Rbool → U) → U) → Rbool → ((Rbool → U) → U))) in
  unshelve refine (let r : T := r in _).
  unshelve refine (RAppN (RAppN r _) _).
  + refine (RLamN (@RAppN _ Rbool _ _ _)).
    - refine #.
    - refine Rtrue.
  + refine (RLamN (@RAppN _ Rbool _ _ _)).
    - refine #.
    - refine Rfalse.
Defined.

Definition Rbool_rect {Γ : Ctx} :
  RElt Γ (∏ (Rbool → U) (
    [@RApp (Γ · (Rbool → U)) Rbool U (Var (Rbool → U)) Rtrue] →
    [@RApp (Γ · (Rbool → U)) Rbool U # Rfalse] →
    ∏ Rbool [@RApp _  _ U
                   (@RApp (Γ · (Rbool → U) · Rbool) _ _ Rbool_stor #) (↑#)])).
Proof.
  unshelve refine (exist3 _).
  - refine (fun α γ P ptrue pfalse b =>
    match b with
    | true => ptrue
    | false => pfalse
    end).
  - unshelve refine (fun g P ptrue pfalse bb =>
              match bb with
              | Btrue _ _ => ptrue
              | Bfalse _ _ => pfalse
              | Bêtabool _ _ => tt
              end).
  - intros α γ P ptrue pfalse b.
    destruct b as [bo bb be].
    revert be ; revert  bb ; revert bo.
    unshelve refine (fun  _ _ b => match b with
                                   | RParatrue _ => _
                                   | RParafalse _ => _
                                   | RParabêtabool _ _ _ _ _ => _
                              end).
    + now apply ptrue.
    + now apply pfalse.
    + now apply tt.
Defined.

Inductive RParanat (α : X -> Y) : nat -> BranchingNat X Y -> Type :=
| RP0 : RParanat α 0 (B0 X Y)
| RPS : forall (no : nat) (nt :  BranchingNat X Y)
                     (ne : RParanat α no nt),
    RParanat α (S no) (BS nt)
| RPbêta : forall  (no : nat) (f : (Y ->  BranchingNat X Y)) (x : X)
                        (rel : RParanat α no (f (α x)) ),
    RParanat α no (Bêtanat f x).


Definition Rnat {Γ : Ctx} : Ty Γ.
Proof.
  unshelve refine (exist3 _).
  - refine (fun (α : X -> Y) (γ : Γ.(q1) α) => nat ).
  - refine (fun γ => Bnat X Y).
  - exact (fun α γ => exist (RPbêta α)).
Defined.

Definition R0 {Γ : Ctx} : RElt Γ Rnat.
Proof.
  exists (fun _ _ => 0) (fun _ => B0 X Y).
  now refine (fun α _ => RP0 α).
Defined.

Definition RS {Γ : Ctx} : RElt Γ (Rnat → Rnat).
Proof.
  exists (fun _ _ => S) (fun _ => @BS X Y).
  now refine (fun α _ n => RPS α n.(q1) n.(q2) n.(q3)).
Defined.


Lemma RParanat_equal {Γ : Ctx} (n : RElt Γ Rnat)
      (α : X -> Y) (γ : Γ ‡ α)  :
    BNdialogue (n ® γ).(q2) α = (n ® γ).(q1).
Proof.
  destruct n as [nt no ne] ; simpl.
  elim: (ne α γ) =>
  [ | β mt mo me | β f x mo rel] ; trivial ; simpl.
  exact: eq_S.
Qed.



Definition Rnat_rec (Γ : Ctx) :
   RElt Γ (∏ U ([Var U] → (Rnat → [Var U] → [Var U]) → Rnat → [Var U])).
Proof.
  unshelve refine (exist3 _).
  - simpl ; unshelve refine (fun α gamma P p0 pS => _).
    refine (fix f n := match n with
                       | 0 => p0
                       | S k => pS k (f k)
                       end).
  - simpl ; unshelve refine (fun gamma P p0 pS => _).
    refine (fix f n := match n with
                       | B0 _ _ => p0
                       | BS k => pS k (f k)
                       | Bêtanat g x => P.(snd) (fun y => f (g y)) x
                       end).
  - simpl.
    unshelve refine (fun α gamma P p0 pS n => _).
    unshelve refine (let aux :=
                         (fix g no nt ne :=
                            match ne as n in RParanat _ mo mt return
                                  fst (q3 P)
                                      ((fix f (n : nat) : q1 P :=
                                          match n with
                                          | 0 => q1 p0
                                          | S k => q1 pS k (f k)
                                          end) mo)
                                      ((fix f n : BEl (X:=X) (Y:=Y) (q2 P) :=
                                          match n with
                                          | B0 _ _ => q2 p0
                                          | BS k => q2 pS k (f k)
                                          | Bêtanat g x => snd (q2 P) (fun y => f (g y)) x
                                          end) mt)                                     
                            with
                            | RP0 _ => p0.(q3)
                            | RPS _ ko kt ke => pS.(q3) (exist3 ke) (exist3 (g ko kt ke))
                            | RPbêta _ ko f x rel => P.(q3).(snd) _ _ _ (g ko (f (α x)) rel)
                            end)
                     in aux n.(q1) n.(q2) n.(q3)).
Defined.      


Definition Rnat_stor {Γ : Ctx} :
  RElt Γ (Rnat → (Rnat → U) → U).
Proof.
  pose (r := RApp (Rnat_rec Γ) ((Rnat → U) → U)).
  let T := constr:(RElt Γ (((Rnat → U) → U) → (Rnat → ((Rnat → U) → U) → ((Rnat → U) → U)) → Rnat → ((Rnat → U) → U))) in
  unshelve refine (let r : T := r in _).
  unshelve refine (RAppN (RAppN r _) _).
  - refine (RLamN (@RAppN _ Rnat _ _ _)).
    + now refine #.
    + now refine R0.
  - refine (@RLamN _ _ _ _).
    refine (↑(@RLamN _ _ _ _)).
    refine (@RLamN (Γ · ((Rnat → U) → U)) (⇑(Rnat → U)) (⇑ U) _).
    refine (@RAppN _ (Rnat → U) _ _ _).
    + now refine (↑#).
    + apply RLamN.
      refine (@RAppN _ Rnat _ _ _).
      * now refine (↑#).
      * now refine (@RAppN _ Rnat _ RS (Var Rnat)).
Defined.      

Definition Rnat_rect {Γ : Ctx} :
  RElt Γ (∏ (Rnat → U) (
              [@RApp (Γ · (Rnat → U)) Rnat U # R0] →
              [∏ Rnat ([@RApp (Γ · (Rnat → U) · Rnat) _ U
                              (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor #) (↑#)] →
                       [@RApp (Γ · (Rnat → U) · Rnat) _ U
                              (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor
                                     (@RApp (Γ · (Rnat → U) · Rnat) _ _ RS #)) (↑#)])] →
              ∏ Rnat [@RApp (Γ · (Rnat → U) · Rnat) _ U
                            (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor #) (↑#)])).
Proof.
  unshelve refine (exist3 _).
  - refine (fix f _ _ P p0 pS n := match n as n0 return
                               ((fix f (n : nat) : (nat -> Type) -> Type :=
                                   match n with
                                   | 0 => fun xr : nat -> Type => xr 0
                                   | S k => fun xr : nat -> Type => f k (fun xr0 : nat => xr (S xr0))
                                   end) n0 P)
                               with
                               | 0 => p0
                               | S k => f _ _ (fun k => P (S k)) (pS 0 p0) (fun k => pS (S k)) k
                               end).
  - refine (fix g _ P p0 pS n :=
              match n return
                    (fst
                       ((fix f (n0 : BranchingNat _ _) :=
                           match n0 with
                           | B0 _ _ => fun xd => xd (B0 X Y)
                           | BS k => fun xd => f k (fun xd0 => xd (BS xd0))
                           | Bêtanat _ _ => fun _ => Bunit X Y
                           end) n P))
              with
              | B0 _ _ => p0
              | BS k => g _ (fun i => P (BS i)) (pS (@B0 _ _) p0) (fun i => pS (BS i)) k
              | Bêtanat _ _ => tt
              end).
  - simpl ; unshelve refine (fun α gamma P p0 pS n => _).
    destruct n as [ho ht he] ; simpl.
    destruct P as [Po Pt Pe] ; simpl.
    destruct p0 as [p0o p0t p0e] ; simpl.
    destruct pS as [pSo pSt pSe] ; simpl.
    simpl in Po ; simpl in Pt ; simpl in Pe.
    simpl in p0o ; simpl in p0t ; simpl in p0e.
    simpl in pSo ; simpl in pSt ; simpl in pSe.
    unfold Ty_inst in Pe ; unfold Inst in p0e ; unfold Ty_inst in pSe.
    simpl in Pe ; simpl in p0e ; simpl in pSe.
    simpl in ho ; simpl in ht ; simpl in he.
    simpl.
    revert pSe ; revert pSt ; revert pSo.
    revert p0e ; revert p0t ; revert p0o.
    revert Pe ; revert Pt ; revert Po.
    induction he.
    + simpl ; intros.
      exact p0e.
    + simpl ; intros.
      specialize IHhe with (fun i => Po (S i)) (fun i => Pt (BS i))
                           (fun n => Pe {|q1 := S n.(q1) ;
                                          q2 := BS n.(q2) ;
                                          q3 := RPS _ _ _ n.(q3)|})
                           (pSo 0 p0o) (pSt (@B0 _ _) p0t)
                           (pSe (exist3 (RP0 _)) (exist3 p0e))
                           (fun i => pSo (S i)) (fun i => pSt (BS i))
      (fun n npe => pSe (exist3 (RPS _ _ _ n.(q3))) npe).
      simpl in IHhe.
      apply IHhe.
    + simpl ; intros.
      exact tt.
Defined.

(*  RElt Γ (∏ (Rnat → U) (
              [@RApp (Γ · (Rnat → U)) Rnat U # R0] →
              [∏ Rnat ([@RApp (Γ · (Rnat → U) · Rnat) Rnat U (↑#) #] →
                      [@RApp (Γ · (Rnat → U) · Rnat) Rnat U (↑#)
                             (@RApp (Γ · (Rnat → U) · Rnat) _ _ RS #)])] →
              ∏ Rnat [@RApp (Γ · (Rnat → U) · Rnat) _ U
                            (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor #) (↑#)])).
[∏ Rnat ([@RApp (Γ · (Rnat → U) · Rnat) _ U
                              (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor #) (↑#)] →
                       [@RApp (Γ · (Rnat → U) · Rnat) _ U
                              (@RApp (Γ · (Rnat → U) · Rnat) _ _ Rnat_stor
                                     (@RApp (Γ · (Rnat → U) · Rnat) _ _ RS #)) (↑#)])]*)

Goal forall (Γ : Ctx)
  (P : RElt Γ (Rnat → U))
  (T0 := [P • R0])
  (p0 : RElt Γ T0)

  (TS := (∏ Rnat ([@RApp (Γ · Rnat) _ U
                              (@RApp (Γ · Rnat) _ _ Rnat_stor #) (↑P)] →
                       [@RApp (Γ · Rnat) _ U
                              (@RApp (Γ · Rnat) _ _ Rnat_stor
                                     (@RApp (Γ · Rnat) _ _ RS #)) (↑P)])))

  
  (pS : RElt Γ TS)
  (TR := @RPi Γ Rnat [Rnat_stor • Var Rnat • ↑ P])
  ,

  @RApp Γ _ _ (@RAppN Γ _ _ (@RAppN Γ T0 (TS → TR) ((@Rnat_rect Γ) • P) p0) pS) R0
  =
  p0.
Proof.
reflexivity.
Qed.
Print RApp.
Goal forall (Γ : Ctx)
  (P : RElt Γ (Rnat → U))
  (T0 := [P • R0])
  (p0 : RElt Γ T0)
  (TS := (∏ Rnat ([@RApp (Γ · Rnat) _ U
                         (@RApp (Γ · Rnat) _ _ Rnat_stor #) (↑P)] →
                  [@RApp (Γ · Rnat) _ U
                         (@RApp (Γ · Rnat) _ _ Rnat_stor
                                (@RApp (Γ · Rnat) _ _ RS #)) (↑P)])))
  (pS : RElt Γ TS)
  (TR := @RPi Γ Rnat [Rnat_stor • Var Rnat • ↑ P])
  (n : RElt Γ Rnat)
  ,
    
    q1 (@RApp Γ _ _ (@RAppN Γ _ _ (@RAppN Γ T0 (TS → TR) ((@Rnat_rect Γ) • P) p0) pS) (RApp RS n))
    =
    q1 (@RAppN Γ (@RApp Γ (Rnat → U) _ (@RApp Γ _ _ Rnat_stor n) P) (Rnat_stor • (RS • n) • P) (RApp pS n) (@RApp Γ _ _ (@RAppN Γ _ _ (@RAppN Γ T0 (TS → TR) ((@Rnat_rect Γ) • P) p0) pS) n)).
Proof. reflexivity.
  simpl.
  unfold Rnat_stor ; unfold Rnat_rect.
  unfold RAppN ; unfold RApp ; simpl. 

  reflexivity. Qed.
  simpl ; intros.
  unfold RAppN ; simpl.
  unfold RApp ; simpl.
  unfold Rnat_rect ; simpl.
Qed.*)
End AvareXY.

Section Avarenat.

Notation " Γ ‡ α " := (Ctx_El nat nat Γ α) (at level 11).
Notation "r ® γ" := (Inst nat nat r γ) (at level 10, left associativity).
Notation "()" := (ctx_nil nat nat).
Notation "Γ · A" := (Ctx_cons nat nat Γ A) (at level 50, left associativity).
Notation "γ ·· x" := (ctx_cons nat nat γ x) (at level 50, left associativity).
Notation "◊ g" := (Ctx_tl nat nat g) (at level 10).
Notation "¿ g" := (Ctx_hd nat nat g) (at level 10).
Notation "⇑ A" := (Lift nat nat A) (at level 10).
Notation "#" := (Var nat nat _) (at level 0).
Notation "↑ M" := (Weaken nat nat M) (at level 10).
Notation "S {{ r }} " := (Sub nat nat S r) (at level 20, left associativity).
Notation "∏" := (RPi nat nat).
Notation "A → B" := (RPi nat nat A (⇑ B)) (at level 99, right associativity, B at level 200).
Notation "'λ'" := (RLam nat nat).
Notation "t • u" := (RApp nat nat t u) (at level 12, left associativity).
Notation "'Ctx'" := (Ctx nat nat).
Notation "'RElt'" := (RElt nat nat).
Notation "'Rnat'" := (@Rnat nat nat).


Definition RGeneric {Γ : Ctx} : RElt Γ (Rnat → Rnat).
Proof.
  exists  (fun α gamma => α) (fun gamma => Generic_aux 0).
  intros α gamma n.
  destruct n as [no nt ne] ; simpl.
  have aux :  forall (k : nat),
      RParanat _ _ α (α (k + no)) (Generic_aux k nt).
  - intro k.
    elim: ne k.
    + simpl ; intros k.
      rewrite - plus_n_O.
      apply (RPbêta).
      induction (α k) ;  simpl. 
      * exact (RP0 _ _ α).
      * exact: (RPS).
    + simpl.
      intros mt mo me1 me2 k.
      rewrite - plus_n_Sm ; rewrite - plus_Sn_m.
      exact (me2 (S k)).
    + intros f x mo me1 me2 k.
      simpl.
      apply RPbêta.
      now apply me2.
  - exact: (aux 0).
Defined.      


Lemma NaturRadinPGeneric {Γ : Ctx} {α : nat -> nat} (n : RElt Γ Rnat)
  (γ : Γ ‡ α):
  BNdialogue ((RGeneric • n) ® γ).(q2) α = α (n ® γ).(q1). 
Proof.
  destruct n as [no nt ne] ; simpl.
  have aux : forall (k : nat),
      BNdialogue (Generic_aux k (nt (q2 γ))) α = α (k + (no α (q1 γ))).
  - elim: (ne α γ) ; simpl.
    + intros k ; now rewrite NaturBNEtaDialogue ; rewrite - plus_n_O.
    + intros mt mo me ihme k.
      rewrite - plus_n_Sm ; rewrite - plus_Sn_m.
      exact: ihme.
    + intros f x mo me ihme k.
      now specialize ihme with k.
  - exact: aux.
Qed.


Theorem continuity (f : RElt (Ctx_nil nat nat) ((Rnat → Rnat) → Rnat)) (α : nat -> nat) :
  continuous (fun p => f.(q1) p ().(q1) p) α.
Proof.
  pose proof (@RParanat_equal nat nat (Ctx_nil nat nat) (f • RGeneric)) as Eq.
  destruct (BNDialogue_Continuity (f.(q2) (@ctx_nil nat nat α).(q1) (Generic_aux 0)) α)
    as [l ihl].
  exists l ; intros β Finite_Eq.
  simpl in Eq ; rewrite - (Eq α () ) ; rewrite - (Eq β () ).
  exact: ihl.
Qed.  

  
End Avarenat.
