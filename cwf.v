Set Primitive Projections.
Set Universe Polymorphism.

Inductive unit := tt.

Record sig (A : Type) (B : A -> Type) := pair { fst : A; snd : B fst }.

Arguments fst {_ _}.
Arguments snd {_ _}.

Record triple (A : Type) (B : Type) (P : A -> B -> Type) : Type :=
  exist3 { qr : A; qd : B ; qε : P qr qd}.

Arguments qr {_ _ _} _.
Arguments qd {_ _ _} _.
Arguments qε {_ _ _} _.

Axiom X Y : Set.
Definition ℙ := X -> Y.

Inductive D (A : Type) :=
| ret : A -> D A
| ask : X -> (Y -> D A) -> D A.

Definition Ctx := triple (ℙ -> Type) Type (fun Γr Γ => forall (α : ℙ),  Γr α -> Γ -> Type).

Definition Typ (Γ : Ctx) : Type.
Proof.
unshelve refine (triple _ _ _).
+ refine (forall (α : ℙ), Γ.(qr) α -> Type).
+ refine (Γ.(qd) -> sig Type (fun A => X -> (Y -> A) -> A)).
+ refine (fun Ar Ad => forall (α : ℙ) (γr : Γ.(qr) α) (γd : Γ.(qd)) (γε : Γ.(qε) α γr γd ), Ar α γr -> (Ad γd).(fst) -> Type).
Defined.

Definition Elt (Γ : Ctx) (A : Typ Γ) : Type.
Proof.
unshelve refine (triple _ _ _).
+ refine (forall (α : ℙ) (γr : Γ.(qr) α), A.(qr) α γr).
+ refine (forall (γd : Γ.(qd)), (A.(qd) γd).(fst)).
+ refine (fun xr xd =>
  forall (α : ℙ) (γr : Γ.(qr) α) (γd : Γ.(qd)) (γε : Γ.(qε) α γr γd), A.(qε) α γr γd γε (xr α γr) (xd γd)).
Defined.

Definition Nil : Ctx.
Proof.
unshelve refine (exist3 _ _ _ _ _ _).
+ refine (fun _ => unit).
+ refine (unit).
+ refine (fun α _ _ => unit).
Defined.

Definition Ext (Γ : Ctx) (A : Typ Γ) : Ctx.
Proof.
unshelve refine (exist3 _ _ _ _ _ _).
+ refine (fun α => sig (Γ.(qr) α) (A.(qr) α)).
+ refine (sig (Γ.(qd)) (fun γ => (A.(qd) γ).(fst))).
+ unshelve refine (fun α γr γd => sig (Γ.(qε) α γr.(fst) γd.(fst)) (fun γε => A.(qε) α _ _ γε γr.(snd) γd.(snd))).
Defined.

Notation "Γ · A" := (Ext Γ A) (at level 50, left associativity).

Definition Lft {Γ : Ctx} {A : Typ Γ} (B : Typ Γ) : Typ (Γ · A).
Proof.
unshelve refine (exist3 _ _ _ _ _ _).
+ refine (fun α γr => B.(qr) α γr.(fst)).
+ refine (fun γd => B.(qd) γd.(fst)).
+ refine (fun α γr γd γε xr xd => B.(qε) α _ _ γε.(fst) xr xd).
Defined.

Notation "⇑ A" := (Lft A) (at level 10).

Definition Var {Γ : Ctx} (A : Typ Γ) : Elt (Γ · A) (⇑ A).
Proof.
unshelve refine (exist3 _ _ _ _ _ _).
+ refine (fun α γr => γr.(snd)).
+ refine (fun γd => γd.(snd)).
+ refine (fun α γr γd γε => γε.(snd)).
Defined.

Notation "#" := (Var _) (at level 0).

Definition Wkn {Γ : Ctx} {A : Typ Γ} {B : Typ Γ} (x : Elt Γ A) : Elt (Γ · B) (⇑ A).
Proof.
unshelve refine (exist3 _ _ _ _ _ _).
+ refine (fun α γr => x.(qr) α γr.(fst)).
+ refine (fun γd => x.(qd) γd.(fst)).
+ refine (fun α γr γd γε => x.(qε) α _ _ γε.(fst)).
Defined.

Notation "↑ M" := (Wkn M) (at level 10).

Definition Sub {Γ : Ctx} {A : Typ Γ} (B : Typ (Γ · A)) (x : Elt Γ A) : Typ Γ.
Proof.
unshelve refine (exist3 _ _ _ _ _ _).
+ refine (fun α γr => B.(qr) α (pair _ _ γr (x.(qr) α γr))).
+ refine (fun γd => B.(qd) (pair _ _ γd (x.(qd) γd))).
+ unshelve refine (fun α γr γd γε => B.(qε) _ _ _ (pair _ _ _ _)).
  - refine (γε).
  - refine (x.(qε) α _ _ γε).
Defined.

Notation "A {{ M }} " := (Sub A M) (at level 20, left associativity).

Definition Prod {Γ : Ctx} (A : Typ Γ) (B : Typ (Γ ·  A)) : Typ Γ.
Proof.
unshelve refine (exist3 _ _ _ _ _ _).
+ refine (fun α γr => forall (x : A.(qr) α γr), B.(qr) α (pair _ _ γr x)).
+ unshelve refine (fun γd => pair _ _ (forall (x : (A.(qd) γd).(fst)), (B.(qd) (pair _ _ γd x)).(fst)) _).
  { refine (fun i k x => (B.(qd) (pair _ _ γd x)).(snd) i (fun o => k o x)). }
+ unshelve refine (fun α γr γd γε fr fd => _).
  cbn in *.
  unshelve refine (forall
    (xr : A.(qr) α γr)
    (xd : (A.(qd) γd).(fst))
    (xε : A.(qε) α γr γd γε xr xd),
    B.(qε) α _ _ _ _ _
  ).
  - refine (pair _ _ γr xr).
  - refine (pair _ _ γd xd).
  - refine (pair _ _ γε xε).
  - refine (fr xr).
  - refine (fd xd).
Defined.

Notation "'Π'" := Prod.

Notation "A → B" := (Prod A (⇑ B)) (at level 99, right associativity, B at level 200).

Definition Lam {Γ : Ctx} {A : Typ Γ} {B : Typ (Γ · A)} (f : Elt (Γ · A) B) : Elt Γ (Prod A B).
Proof.
unshelve refine (exist3 _ _ _ _ _ _).
+ refine (fun α γr xr => f.(qr) α (pair _ _ γr xr)).
+ refine (fun γd xd => f.(qd) (pair _ _ γd xd)).
+ refine (fun α γr γd γε xr xd xε => f.(qε) _ _ _ _).
Defined.

Notation "'λ'" := Lam.

Definition App {Γ : Ctx} {A : Typ Γ} {B : Typ (Γ · A)} (f : Elt Γ (Prod A B)) (x : Elt Γ A) :
  Elt Γ (B {{ x }}).
Proof.
unshelve refine (exist3 _ _ _ _ _ _).
+ refine (fun α γr => f.(qr) α γr (x.(qr) α γr)).
+ refine (fun γd => f.(qd) γd (x.(qd) γd)).
+ unshelve refine (fun α γr γd γε => f.(qε) _ _ _ _ _ _ _).
Defined.

Notation "M • N" := (App M N) (at level 11, left associativity).

Definition U {Γ : Ctx} : Typ Γ.
Proof.
unshelve refine (exist3 _ _ _ _ _ _).
+ refine (fun α γr => Type).
+ refine (fun γd => pair _ _ (sig Type (fun A : Type => X -> (Y -> A) -> A)) _).
  { unshelve refine (fun i k => pair _ _ unit (fun i k => tt)). }
+ refine (fun α γr γd γε Ar Ad => Ar -> Ad.(fst) -> Type).
Defined.

Definition EL {Γ : Ctx} (A : Elt Γ U) : Typ Γ.
Proof.
unshelve refine (exist3 _ _ _ _ _ _).
+ refine (fun α γr => A.(qr) α γr).
+ refine (fun γd => A.(qd) γd).
+ unshelve refine (fun α γr γd γε xr xd => A.(qε) α _ _ γε xr xd).
Defined.

Notation "[ A ]" := (EL A).

Inductive boolD :=
| trueD : boolD
| falseD : boolD
| bool_ask : X -> (Y -> boolD) -> boolD.

Definition bool {Γ : Ctx} : Typ Γ.
Proof.
unshelve refine (exist3 _ _ _ _ _ _).
+ refine (fun α γr => bool).
+ refine (fun γd => pair _ _ boolD bool_ask).
+ refine (fun α γr γd γε br bd => unit).
(** FIXME: faut coller une relation qui tient la route *)
Defined.

Definition true {Γ : Ctx} : Elt Γ bool.
Proof.
unshelve refine (exist3 _ _ _ _ _ _).
+ refine (fun α γr => true).
+ refine (fun γd => trueD).
+ refine (fun α γr γd γε => tt).
Defined.

Definition false {Γ : Ctx} : Elt Γ bool.
Proof.
unshelve refine (exist3 _ _ _ _ _ _).
+ refine (fun α γr => false).
+ refine (fun γd => falseD).
+ refine (fun α γr γd γε => tt).
Defined.

Definition bool_rect {Γ : Ctx} :
  Elt Γ (Π (bool → U) (
    [@App (Γ · (bool → U)) bool U # true] →
    [@App (Γ · (bool → U)) bool U # false] →
    Π bool [@App (Γ · (bool → U) · bool) bool U (↑ #) #])).
Proof.
unshelve refine (exist3 _ _ _ _ _ _).
+
  refine (
  fun α γ P ptt pff b => _
  ); cbn in *.
 destruct b; assumption.
+ refine (
  fun γ P ptt pff b => _
  ); cbn in *.
 destruct b.
  - apply ptt.
  - apply pff.
  - 
(** Évidemment c'est du bluff *)
Abort.

(* + cbn in *. *)
(* intros α γr γd γε Pr Pd Pε Ptr Ptd Ptε. *)
